# JPlag extension for Tamarin

Source code, created for the Bachelor Thesis "Plagiarism Detection for Tamarin" by Jan Stauffer.

The plagiarism detection tool JPlag (https://github.com/jplag/JPlag) was extended for Tamarin (https://tamarin-prover.github.io/).

## Prerequisites

This extension was written for Java 17.0.9 and Maven 3.6.3.

## Usage

For in-depth information regarding the JPlag CLI, refer to [JPlag's documentation](https://github.com/jplag/JPlag). 

To use JPlag with Tamarin, select Tamarin using the language argument. 

The command for a simple comparison could look as follows:
```
java -jar jplag-4.3.0-jar-with-dependencies.jar -l tamarin -t <sensitivity> -bc <base-code-dir> <submission-dir>
```
- `-l tamarin` Selects Tamarin as the language used to parse the submissions in `<submission-dir>`.
- `-t <sensitivity>` Tunes the sensitivity of the comparison algorithm. It specifies the minimum number of tokens that need to align for a section to be considered as matching. 
- `-bc <base-code-dir>` Specifies the directory that contains the base code, which is used in all submissions. This code is excluded from the comparison.
- `<submission-dir>` Specifies the directory that contains the submissions to be analysed.

## Notes
The `notes` directory contains a semi-formal specification of the Tamarin language. This specification was used as reference to write the ANTLR grammar for Tamarin and was inferred from the parser in the Tamarin Prover (https://tamarin-prover.github.io/). This specification should be used with caution since it is not completely accurate. Several mistakes were found in this specification during the creation of the ANTLR grammar, not all of which were addressed. Consider the [ANTLR grammar](https://gitlab.ethz.ch/jplag-for-tamarin/jplag-extension-for-tamarin/-/tree/main/jplag/languages/tamarin/src/main/antlr4/de/jplag/tamarin/grammar) for a more accurate specification. 

## Code generation
The programs in `code-generation` were used to specify the token generation behaviour for each of the grammar rules.
It was a quick and dirty solution to avoid writing hundreds of Java functions over and over. 
The current version `populateV2` can be used to configure the token generation behaviour in a simple GUI and generate the corresponding Java classes. 
It also has capabilities to save and reload configurations.
Several paths in the main method of the script require adaption for the script to function.

The script generates three classes in the `outputV2`, which can be copied into `jplag/languages/tamarin/src/main/java/de/jplag/tamarin` directory.
To apply the new behaviour, JPlag needs to be rebuilt.
