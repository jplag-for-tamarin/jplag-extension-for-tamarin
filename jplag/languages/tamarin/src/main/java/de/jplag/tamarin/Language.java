package de.jplag.tamarin;

import de.jplag.Token;
import de.jplag.ParsingException;
import org.kohsuke.MetaInfServices;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

@MetaInfServices(de.jplag.Language.class)
public class Language implements de.jplag.Language {

    private static final String NAME = "Tamarin Parser";
    private static final String IDENTIFIER = "tamarin";
    private static final int DEFAUL_MIN_TOKEN_MATCH = 8;
    private static final String[] FILE_EXTENSIONS = {".spthy"};
    private final Parser parser;

    public Language() {
        parser = new Parser();
    }

    @Override
    public String[] suffixes() {
        return FILE_EXTENSIONS;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public int minimumTokenMatch() {
        return DEFAUL_MIN_TOKEN_MATCH;
    }

    @Override
    public List<Token> parse(Set<File> files) throws ParsingException {
        return this.parser.parse(files);
    }

    public boolean tryParse(Path path) throws ParsingException {
        return this.parser.tryParse(path);
    }
}
