package de.jplag.tamarin;

import de.jplag.ParsingException;
import de.jplag.AbstractParser;
import de.jplag.Token;
import de.jplag.TokenType;
import de.jplag.tamarin.grammar.TamarinParser;
import de.jplag.tamarin.grammar.TamarinLexer;

import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static de.jplag.tamarin.TamarinTokenType.CHILD;

public class Parser extends AbstractParser {

    private ArrayList<Token> tokens;
    private File currentFile;

    ParseTreeNode pt;
    ParseTreeNode current;

    public Parser() {
        super();
        pt = new InitialNode();
        pt.setParent(pt);
        current = pt;
    }

    public List<Token> parse(Set<File> files) throws ParsingException {
        tokens = new ArrayList<Token>();
        for (File file : files) {
            logger.trace("Parsing file {}", file.getName());
            parseFile(file);
            pt.sort();
            tokens.addAll(pt.getTokens());
            tokens.add(Token.fileEnd(file));

            pt = new InitialNode();
            pt.setParent(pt);
            current = pt;
        }
        return tokens;
    }

    private void parseFile(File file) throws ParsingException {
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            currentFile = file;

            TamarinLexer lexer = new TamarinLexer(CharStreams.fromStream(fileInputStream));
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            TamarinParser parser = new TamarinParser(tokens);

            ParserRuleContext entryContext = parser.tamarin_file();
            ParseTreeWalker treeWalker = new ParseTreeWalker();

            JplagTamarinListener listener = new JplagTamarinListener(this);
            for(int i = 0; i < entryContext.getChildCount(); i++) {
                ParseTree pt = entryContext.getChild(i);
                treeWalker.walk(listener, pt);
            }
        } catch (IOException e) {
            throw new ParsingException(file, e.getMessage(), e);
        }
    }

    public boolean tryParse(Path path) throws ParsingException {
        try {
            InputStream stream = Files.newInputStream(path);

            TamarinLexer lexer = new TamarinLexer(CharStreams.fromStream(stream));
            CommonTokenStream tokenStream = new CommonTokenStream(lexer);
            TamarinParser parser = new TamarinParser(tokenStream);

            parser.tamarin_file();
            return parser.getNumberOfSyntaxErrors() == 0;
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public void addToken(TokenType type, org.antlr.v4.runtime.Token token) {
        current.addToken(new Token(type, currentFile, token.getLine(), token.getCharPositionInLine() + 1, token.getText().length()));
    }

    public void addChildNode(ParseTreeNode child) {
        current.addChild(child);
        if(child.priority != 10000) {
            current.addToken(new Token(CHILD, currentFile, 1, 1, 1));
        }
        child.setParent(current);
        current = child;
    }

    public void finishChild() {
        if (current.parent.equals(current)) {
            System.err.println("Calling finishChild on the initial node, this shouldn't happen!");
        }
        current = current.parent;
    }
}
