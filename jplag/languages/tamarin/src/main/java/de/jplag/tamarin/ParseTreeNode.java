package de.jplag.tamarin;

import de.jplag.Token;
import de.jplag.TokenType;
import de.jplag.tamarin.grammar.TamarinParser;
import de.jplag.tamarin.grammar.TamarinLexer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static de.jplag.tamarin.TamarinTokenType.*;



abstract class ParseTreeNode implements Comparable<ParseTreeNode>{
    ArrayList<ParseTreeNode> Children;
    ParseTreeNode parent;
    ArrayList<Token> nodeTokens;
    int priority;
    ParseTreeNode() {
        Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
    }

    abstract void sort();
    List<Token> getTokens() {
        Children.removeIf(s -> s.priority == 10000);
        List<Token> tokens = new ArrayList<Token>();
        int childIndex = 0;
        for(int i = 0; i < nodeTokens.size(); i++) {
            if(nodeTokens.get(i).getType() == CHILD) {
                tokens.addAll(Children.get(childIndex).getTokens());
                childIndex++;
            }
            else {
                tokens.add(nodeTokens.get(i));
            }
        }
        return tokens;
    }
    void setParent(ParseTreeNode parent) {
        this.parent = parent;
    }
    void addChild(ParseTreeNode child) {
        Children.add(child);
    }
    void addToken(Token token)  {
        nodeTokens.add(token);
    }
    public int compareTo(ParseTreeNode other) {
        if (this.priority < other.priority) {
            return -1;
        }
        else if(this.priority > other.priority) {
            return 1;
        }
        else {
            this.sort();
            other.sort();
            for(int i = 0; i < Math.min(this.Children.size(), other.Children.size()); i++) {
                int comp = this.Children.get(i).compareTo(other.Children.get(i));
                if(comp != 0) {
                    return comp;
                }
            }
            return 0;
        }
    }
}

class InitialNode extends ParseTreeNode {

    InitialNode() {
        Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
        priority = -1;
    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
    }

    @Override
    void addToken(Token token)  {
        if(token.getType() == CHILD) {
            nodeTokens.add(token);
        }
        else {
            System.err.println("Trying to add token " + token.getType().getDescription() + " to initial node!");
        }
    }

}class Tamarin_fileNode extends ParseTreeNode {

	Tamarin_fileNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 0;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class TheoryNode extends ParseTreeNode {

	TheoryNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 1;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class BodyNode extends ParseTreeNode {

	BodyNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 2;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class DefineNode extends ParseTreeNode {

	DefineNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 3;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class IncludeNode extends ParseTreeNode {

	IncludeNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 4;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class IfdefNode extends ParseTreeNode {

	IfdefNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 5;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FlagDisjunctsNode extends ParseTreeNode {

	FlagDisjunctsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 6;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class FlagConjunctsNode extends ParseTreeNode {

	FlagConjunctsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 7;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class FlagNegationNode extends ParseTreeNode {

	FlagNegationNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 8;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FlagAtomNode extends ParseTreeNode {

	FlagAtomNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 9;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class TupleTermNode extends ParseTreeNode {

	TupleTermNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class MsettermNode extends ParseTreeNode {

	MsettermNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 11;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class MsettermListNode extends ParseTreeNode {

	MsettermListNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 12;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class XortermNode extends ParseTreeNode {

	XortermNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 13;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class MulttermNode extends ParseTreeNode {

	MulttermNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 14;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class ExptermNode extends ParseTreeNode {

	ExptermNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 15;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class TermNode extends ParseTreeNode {

	TermNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 16;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ApplicationNode extends ParseTreeNode {

	ApplicationNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 17;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class UnaryOpApplicationNode extends ParseTreeNode {

	UnaryOpApplicationNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 18;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class NonUnaryOpApplicationNode extends ParseTreeNode {

	NonUnaryOpApplicationNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 19;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class BinaryAlgApplicationNode extends ParseTreeNode {

	BinaryAlgApplicationNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 20;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class NullaryApplicationNode extends ParseTreeNode {

	NullaryApplicationNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 21;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ReservedBuiltinsNode extends ParseTreeNode {

	ReservedBuiltinsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class GenericRuleNode extends ParseTreeNode {

	GenericRuleNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 23;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class IntruderRuleNode extends ParseTreeNode {

	IntruderRuleNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 24;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class IntruderInfoNode extends ParseTreeNode {

	IntruderInfoNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 25;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class ProtoRuleACNode extends ParseTreeNode {

	ProtoRuleACNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 26;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ProtoRuleACInfoNode extends ParseTreeNode {

	ProtoRuleACInfoNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 27;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class ProtoRuleNode extends ParseTreeNode {

	ProtoRuleNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 28;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ProtoRuleInfoNode extends ParseTreeNode {

	ProtoRuleInfoNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 29;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class RuleAttributesNode extends ParseTreeNode {

	RuleAttributesNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class RuleAttributeNode extends ParseTreeNode {

	RuleAttributeNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class EmbeddedRestrictionNode extends ParseTreeNode {

	EmbeddedRestrictionNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 32;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FactOrRestrictionNode extends ParseTreeNode {

	FactOrRestrictionNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 33;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FactOrRestrictionListNode extends ParseTreeNode {

	FactOrRestrictionListNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 34;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class ReservedRuleNamesNode extends ParseTreeNode {

	ReservedRuleNamesNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class RuleFreshNode extends ParseTreeNode {

	RuleFreshNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class RulePubNode extends ParseTreeNode {

	RulePubNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class HexColorNode extends ParseTreeNode {

	HexColorNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class ModuloENode extends ParseTreeNode {

	ModuloENode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class ModuloACNode extends ParseTreeNode {

	ModuloACNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class FactListNode extends ParseTreeNode {

	FactListNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 41;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class FactNode extends ParseTreeNode {

	FactNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 42;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FreshFactNode extends ParseTreeNode {

	FreshFactNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 43;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ReservedFactNode extends ParseTreeNode {

	ReservedFactNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 44;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ProtoFactNode extends ParseTreeNode {

	ProtoFactNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 45;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class MultiplicityNode extends ParseTreeNode {

	MultiplicityNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 46;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class FactAnnotationsNode extends ParseTreeNode {

	FactAnnotationsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class FactAnnotationNode extends ParseTreeNode {

	FactAnnotationNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class LemmaAttributesNode extends ParseTreeNode {

	LemmaAttributesNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class LemmaAttributeNode extends ParseTreeNode {

	LemmaAttributeNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class TraceQuantifierNode extends ParseTreeNode {

	TraceQuantifierNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 51;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ForallTracesNode extends ParseTreeNode {

	ForallTracesNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 52;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class ExistsTraceNode extends ParseTreeNode {

	ExistsTraceNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 53;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class LemmaNode extends ParseTreeNode {

	LemmaNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 54;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class StandardFormulaNode extends ParseTreeNode {

	StandardFormulaNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 55;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class ImplicationNode extends ParseTreeNode {

	ImplicationNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 56;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class DisjunctionNode extends ParseTreeNode {

	DisjunctionNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 57;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class ConjunctionNode extends ParseTreeNode {

	ConjunctionNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 58;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class NegationNode extends ParseTreeNode {

	NegationNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 59;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FatomNode extends ParseTreeNode {

	FatomNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 60;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class BlatomNode extends ParseTreeNode {

	BlatomNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 61;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Blatom_lastNode extends ParseTreeNode {

	Blatom_lastNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 62;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Blatom_actionNode extends ParseTreeNode {

	Blatom_actionNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 63;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Blatom_predicateNode extends ParseTreeNode {

	Blatom_predicateNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 64;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Blatom_lessNode extends ParseTreeNode {

	Blatom_lessNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 65;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Blatom_smallerpNode extends ParseTreeNode {

	Blatom_smallerpNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 66;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Blatom_node_eqNode extends ParseTreeNode {

	Blatom_node_eqNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 67;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class Blatom_term_eqNode extends ParseTreeNode {

	Blatom_term_eqNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 68;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class QuantificationNode extends ParseTreeNode {

	QuantificationNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 69;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class SmallerpNode extends ParseTreeNode {

	SmallerpNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 70;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class CaseTestNode extends ParseTreeNode {

	CaseTestNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class LemmaAccNode extends ParseTreeNode {

	LemmaAccNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class TacticNode extends ParseTreeNode {

	TacticNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class DeprioNode extends ParseTreeNode {

	DeprioNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class PrioNode extends ParseTreeNode {

	PrioNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class TacticDisjunctsNode extends ParseTreeNode {

	TacticDisjunctsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class TacticConjunctsNode extends ParseTreeNode {

	TacticConjunctsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class TacticNegationNode extends ParseTreeNode {

	TacticNegationNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class TacticFunctionNode extends ParseTreeNode {

	TacticFunctionNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class SelectedPresortNode extends ParseTreeNode {

	SelectedPresortNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class GoalRankingPresortNode extends ParseTreeNode {

	GoalRankingPresortNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class TacticFunctionNameNode extends ParseTreeNode {

	TacticFunctionNameNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class RankingIdentifierNode extends ParseTreeNode {

	RankingIdentifierNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class TacticNameNode extends ParseTreeNode {

	TacticNameNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class HeuristicNode extends ParseTreeNode {

	HeuristicNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class GoalRankingNode extends ParseTreeNode {

	GoalRankingNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class RegularRankingNode extends ParseTreeNode {

	RegularRankingNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class InternalTacticRankingNode extends ParseTreeNode {

	InternalTacticRankingNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class OracleRankingNode extends ParseTreeNode {

	OracleRankingNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class ExportNode extends ParseTreeNode {

	ExportNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class PredicateDeclarationNode extends ParseTreeNode {

	PredicateDeclarationNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 91;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class PredicateNode extends ParseTreeNode {

	PredicateNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 92;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class SignatureOptionsNode extends ParseTreeNode {

	SignatureOptionsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinOptionsNode extends ParseTreeNode {

	BuiltinOptionsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class EquationsNode extends ParseTreeNode {

	EquationsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 95;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class EquationNode extends ParseTreeNode {

	EquationNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 96;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class SignatureFunctionsNode extends ParseTreeNode {

	SignatureFunctionsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 97;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class SignatureFunctionNode extends ParseTreeNode {

	SignatureFunctionNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 98;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FunctionAttributeNode extends ParseTreeNode {

	FunctionAttributeNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class FunctionTypeNode extends ParseTreeNode {

	FunctionTypeNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 100;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Function_type_naturalNode extends ParseTreeNode {

	Function_type_naturalNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 101;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class Function_type_sapicNode extends ParseTreeNode {

	Function_type_sapicNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 102;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class BuiltinsNode extends ParseTreeNode {

	BuiltinsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNamesNode extends ParseTreeNode {

	BuiltinNamesNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameLocationsReportNode extends ParseTreeNode {

	BuiltinNameLocationsReportNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameReliableChannelNode extends ParseTreeNode {

	BuiltinNameReliableChannelNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameDHNode extends ParseTreeNode {

	BuiltinNameDHNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameBilinearPairingNode extends ParseTreeNode {

	BuiltinNameBilinearPairingNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameMultisetNode extends ParseTreeNode {

	BuiltinNameMultisetNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameSymmetricEncryptionNode extends ParseTreeNode {

	BuiltinNameSymmetricEncryptionNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameAsymmetricEncryptionNode extends ParseTreeNode {

	BuiltinNameAsymmetricEncryptionNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameSigningNode extends ParseTreeNode {

	BuiltinNameSigningNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameDestPairingNode extends ParseTreeNode {

	BuiltinNameDestPairingNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameDestSymmetricEncryptionNode extends ParseTreeNode {

	BuiltinNameDestSymmetricEncryptionNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameDestAsymmetricEncryptionNode extends ParseTreeNode {

	BuiltinNameDestAsymmetricEncryptionNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameDestSigningNode extends ParseTreeNode {

	BuiltinNameDestSigningNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameRevealingSigningNode extends ParseTreeNode {

	BuiltinNameRevealingSigningNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameHashingNode extends ParseTreeNode {

	BuiltinNameHashingNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinNameXorNode extends ParseTreeNode {

	BuiltinNameXorNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class ProofSkeletonNode extends ParseTreeNode {

	ProofSkeletonNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class StartProofSkeletonNode extends ParseTreeNode {

	StartProofSkeletonNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class SolvedProofNode extends ParseTreeNode {

	SolvedProofNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class FinalProofNode extends ParseTreeNode {

	FinalProofNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class InterProofNode extends ParseTreeNode {

	InterProofNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class ProofCaseNode extends ParseTreeNode {

	ProofCaseNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class ProofMethodNode extends ParseTreeNode {

	ProofMethodNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class GoalNode extends ParseTreeNode {

	GoalNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class ActionGoalNode extends ParseTreeNode {

	ActionGoalNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 128;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class PremiseGoalNode extends ParseTreeNode {

	PremiseGoalNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 129;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ChainGoalNode extends ParseTreeNode {

	ChainGoalNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 130;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class DisjSplitGoalNode extends ParseTreeNode {

	DisjSplitGoalNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class EqSplitGoalNode extends ParseTreeNode {

	EqSplitGoalNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class NodePremiseNode extends ParseTreeNode {

	NodePremiseNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class NodeConclusionNode extends ParseTreeNode {

	NodeConclusionNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class DiffEquivLemmaNode extends ParseTreeNode {

	DiffEquivLemmaNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class EquivLemmaNode extends ParseTreeNode {

	EquivLemmaNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class ProcessNode extends ParseTreeNode {

	ProcessNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class ElseProcessNode extends ParseTreeNode {

	ElseProcessNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class TopLevelProcessNode extends ParseTreeNode {

	TopLevelProcessNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class ActionProcessNode extends ParseTreeNode {

	ActionProcessNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class SapicActionNode extends ParseTreeNode {

	SapicActionNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class SapicActionInNode extends ParseTreeNode {

	SapicActionInNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class SapicActionOutNode extends ParseTreeNode {

	SapicActionOutNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class ProcessDefNode extends ParseTreeNode {

	ProcessDefNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class SapicPatternTermNode extends ParseTreeNode {

	SapicPatternTermNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 145;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class SapicTermNode extends ParseTreeNode {

	SapicTermNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 146;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LogicalTypedPatternLiteralNode extends ParseTreeNode {

	LogicalTypedPatternLiteralNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 147;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LogicalTypedLiteralNode extends ParseTreeNode {

	LogicalTypedLiteralNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 148;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class SapicNodeVarNode extends ParseTreeNode {

	SapicNodeVarNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 149;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class SapicPatternVarNode extends ParseTreeNode {

	SapicPatternVarNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 150;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class SapicVarNode extends ParseTreeNode {

	SapicVarNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 151;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class SapicTypeNode extends ParseTreeNode {

	SapicTypeNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 152;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class RestrictionNode extends ParseTreeNode {

	RestrictionNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 153;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class RestrictionAttributesNode extends ParseTreeNode {

	RestrictionAttributesNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class RestrictionAttributeNode extends ParseTreeNode {

	RestrictionAttributeNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class LegacyAxiomNode extends ParseTreeNode {

	LegacyAxiomNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 156;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LetBlockNode extends ParseTreeNode {

	LetBlockNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 157;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class LetAssignmentNode extends ParseTreeNode {

	LetAssignmentNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 158;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ProtoFactIdentifierNode extends ParseTreeNode {

	ProtoFactIdentifierNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 159;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ReservedFactIdentifierNode extends ParseTreeNode {

	ReservedFactIdentifierNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 160;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Reserved_fact_outNode extends ParseTreeNode {

	Reserved_fact_outNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 161;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class Reserved_fact_inNode extends ParseTreeNode {

	Reserved_fact_inNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 162;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class Reserved_fact_kuNode extends ParseTreeNode {

	Reserved_fact_kuNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 163;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class Reserved_fact_kdNode extends ParseTreeNode {

	Reserved_fact_kdNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 164;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class Reserved_fact_dedNode extends ParseTreeNode {

	Reserved_fact_dedNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 165;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class IdentifierNode extends ParseTreeNode {

	IdentifierNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 166;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class NaturalNode extends ParseTreeNode {

	NaturalNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class IndexedIdentifierNode extends ParseTreeNode {

	IndexedIdentifierNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 168;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class FilePathNode extends ParseTreeNode {

	FilePathNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 169;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class KeywordsNode extends ParseTreeNode {

	KeywordsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class LogMsgVarNode extends ParseTreeNode {

	LogMsgVarNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 171;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class LogFreshVarNode extends ParseTreeNode {

	LogFreshVarNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 172;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class LogPubVarNode extends ParseTreeNode {

	LogPubVarNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 173;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class LogNodeVarNode extends ParseTreeNode {

	LogNodeVarNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 174;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class LogVarNode extends ParseTreeNode {

	LogVarNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 175;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class MsgVarNode extends ParseTreeNode {

	MsgVarNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 176;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LogicalLiteralNode extends ParseTreeNode {

	LogicalLiteralNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 177;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LogicalLiteralWithNodeNode extends ParseTreeNode {

	LogicalLiteralWithNodeNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 178;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LogicalLiteralNoPubNode extends ParseTreeNode {

	LogicalLiteralNoPubNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 179;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LiteralsNode extends ParseTreeNode {

	LiteralsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 180;
	}
	@Override void sort() {

		Children.forEach(c -> c.sort());
		Collections.sort(Children);
	}
}

class LiteralNode extends ParseTreeNode {

	LiteralNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 181;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class NodeVarNode extends ParseTreeNode {

	NodeVarNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 182;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FreshNameNode extends ParseTreeNode {

	FreshNameNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 183;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class PubNameNode extends ParseTreeNode {

	PubNameNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 184;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class FormalCommentNode extends ParseTreeNode {

	FormalCommentNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class TacticFunctionIdentifierNode extends ParseTreeNode {

	TacticFunctionIdentifierNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class InternalTacticNameNode extends ParseTreeNode {

	InternalTacticNameNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class OracleRelativePathNode extends ParseTreeNode {

	OracleRelativePathNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class ExportBodyCharsNode extends ParseTreeNode {

	ExportBodyCharsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class CharDirNode extends ParseTreeNode {

	CharDirNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class EquivOpNode extends ParseTreeNode {

	EquivOpNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 191;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class OrOpNode extends ParseTreeNode {

	OrOpNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 192;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class NotOpNode extends ParseTreeNode {

	NotOpNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 193;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class AndOpNode extends ParseTreeNode {

	AndOpNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 194;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class EqOpNode extends ParseTreeNode {

	EqOpNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 195;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class BotOpNode extends ParseTreeNode {

	BotOpNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 196;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class FOpNode extends ParseTreeNode {

	FOpNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 197;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class TOpNode extends ParseTreeNode {

	TOpNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 198;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class TopOpNode extends ParseTreeNode {

	TopOpNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 199;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class ForallOpNode extends ParseTreeNode {

	ForallOpNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 200;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class ExistsOpNode extends ParseTreeNode {

	ExistsOpNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 201;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(Arrays.asList(nodeTokens.get(0)));}}

class BuiltinXorNode extends ParseTreeNode {

	BuiltinXorNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinMunNode extends ParseTreeNode {

	BuiltinMunNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinOneNode extends ParseTreeNode {

	BuiltinOneNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinExpNode extends ParseTreeNode {

	BuiltinExpNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinMultNode extends ParseTreeNode {

	BuiltinMultNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinInvNode extends ParseTreeNode {

	BuiltinInvNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinPmultNode extends ParseTreeNode {

	BuiltinPmultNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinEmNode extends ParseTreeNode {

	BuiltinEmNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class BuiltinZeroNode extends ParseTreeNode {

	BuiltinZeroNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class DhNeutralNode extends ParseTreeNode {

	DhNeutralNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class OperatorsNode extends ParseTreeNode {

	OperatorsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class PunctuationNode extends ParseTreeNode {

	PunctuationNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

class AllowedIdentifierKeywordsNode extends ParseTreeNode {

	AllowedIdentifierKeywordsNode() {
		Children = new ArrayList<>();
		nodeTokens = new ArrayList<>();
		priority = 10000;
	}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }
}

