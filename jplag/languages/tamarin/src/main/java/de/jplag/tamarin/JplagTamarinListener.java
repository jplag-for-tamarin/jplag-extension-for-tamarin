package de.jplag.tamarin;

import static de.jplag.tamarin.TamarinTokenType.*;
import de.jplag.tamarin.grammar.TamarinParserBaseListener;
import de.jplag.tamarin.grammar.TamarinParser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import javax.swing.text.html.Option;
import java.util.Optional;

public class JplagTamarinListener extends TamarinParserBaseListener {
    private final Parser parser;
	@Override public void enterTamarin_file(TamarinParser.Tamarin_fileContext ctx) { 
		parser.addChildNode(new Tamarin_fileNode());
		parser.addToken(TAMARIN_FILE_BEGIN, ctx.getStart());
	}
	@Override public void exitTamarin_file(TamarinParser.Tamarin_fileContext ctx) { 
		parser.addToken(TAMARIN_FILE_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterTheory(TamarinParser.TheoryContext ctx) { 
		parser.addChildNode(new TheoryNode());
		parser.addToken(THEORY_BEGIN, ctx.getStart());
	}
	@Override public void exitTheory(TamarinParser.TheoryContext ctx) { 
		parser.addToken(THEORY_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterBody(TamarinParser.BodyContext ctx) { 
		parser.addChildNode(new BodyNode());
		parser.addToken(BODY_BEGIN, ctx.getStart());
	}
	@Override public void exitBody(TamarinParser.BodyContext ctx) { 
		parser.addToken(BODY_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterDefine(TamarinParser.DefineContext ctx) { 
		parser.addChildNode(new DefineNode());
		parser.addToken(DEFINE_BEGIN, ctx.getStart());
	}
	@Override public void exitDefine(TamarinParser.DefineContext ctx) { 
		parser.addToken(DEFINE_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterInclude(TamarinParser.IncludeContext ctx) { 
		parser.addChildNode(new IncludeNode());
		parser.addToken(INCLUDE_BEGIN, ctx.getStart());
	}
	@Override public void exitInclude(TamarinParser.IncludeContext ctx) { 
		parser.addToken(INCLUDE_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterIfdef(TamarinParser.IfdefContext ctx) { 
		parser.addChildNode(new IfdefNode());
		parser.addToken(IFDEF_BEGIN, ctx.getStart());
	}
	@Override public void exitIfdef(TamarinParser.IfdefContext ctx) { 
		parser.addToken(IFDEF_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterFlagDisjuncts(TamarinParser.FlagDisjunctsContext ctx) { 
		parser.addChildNode(new FlagDisjunctsNode());
		parser.addToken(FLAGDISJUNCTS_BEGIN, ctx.getStart());
	}
	@Override public void exitFlagDisjuncts(TamarinParser.FlagDisjunctsContext ctx) { 
		parser.addToken(FLAGDISJUNCTS_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterFlagConjuncts(TamarinParser.FlagConjunctsContext ctx) { 
		parser.addChildNode(new FlagConjunctsNode());
		parser.addToken(FLAGCONJUNCTS_BEGIN, ctx.getStart());
	}
	@Override public void exitFlagConjuncts(TamarinParser.FlagConjunctsContext ctx) { 
		parser.addToken(FLAGCONJUNCTS_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterFlagNegation(TamarinParser.FlagNegationContext ctx) { 
		parser.addChildNode(new FlagNegationNode());
		parser.addToken(FLAGNEGATION_BEGIN, ctx.getStart());
	}
	@Override public void exitFlagNegation(TamarinParser.FlagNegationContext ctx) { 
		parser.addToken(FLAGNEGATION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterFlagAtom(TamarinParser.FlagAtomContext ctx) { 
		parser.addChildNode(new FlagAtomNode());
		parser.addToken(FLAGATOM, ctx.getStart());
	}
	@Override public void exitFlagAtom(TamarinParser.FlagAtomContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTupleTerm(TamarinParser.TupleTermContext ctx) { 
		parser.addChildNode(new TupleTermNode());
		parser.addToken(TUPLETERM_BEGIN, ctx.getStart());
	}
	@Override public void exitTupleTerm(TamarinParser.TupleTermContext ctx) { 
		parser.addToken(TUPLETERM_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterMsetterm(TamarinParser.MsettermContext ctx) { 
		parser.addChildNode(new MsettermNode());
		parser.addToken(MSETTERM_BEGIN, ctx.getStart());
	}
	@Override public void exitMsetterm(TamarinParser.MsettermContext ctx) { 
		parser.addToken(MSETTERM_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterMsettermList(TamarinParser.MsettermListContext ctx) { 
		parser.addChildNode(new MsettermListNode());
		parser.addToken(MSETTERMLIST_BEGIN, ctx.getStart());
	}
	@Override public void exitMsettermList(TamarinParser.MsettermListContext ctx) { 
		parser.addToken(MSETTERMLIST_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterXorterm(TamarinParser.XortermContext ctx) { 
		parser.addChildNode(new XortermNode());
		parser.addToken(XORTERM_BEGIN, ctx.getStart());
	}
	@Override public void exitXorterm(TamarinParser.XortermContext ctx) { 
		parser.addToken(XORTERM_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterMultterm(TamarinParser.MulttermContext ctx) { 
		parser.addChildNode(new MulttermNode());
		parser.addToken(MULTTERM_BEGIN, ctx.getStart());
	}
	@Override public void exitMultterm(TamarinParser.MulttermContext ctx) { 
		parser.addToken(MULTTERM_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterExpterm(TamarinParser.ExptermContext ctx) { 
		parser.addChildNode(new ExptermNode());
		parser.addToken(EXPTERM_BEGIN, ctx.getStart());
	}
	@Override public void exitExpterm(TamarinParser.ExptermContext ctx) { 
		parser.addToken(EXPTERM_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterTerm(TamarinParser.TermContext ctx) { 
		parser.addChildNode(new TermNode());
		parser.addToken(TERM_BEGIN, ctx.getStart());
	}
	@Override public void exitTerm(TamarinParser.TermContext ctx) { 
		parser.addToken(TERM_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterApplication(TamarinParser.ApplicationContext ctx) { 
		parser.addChildNode(new ApplicationNode());
		parser.addToken(APPLICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitApplication(TamarinParser.ApplicationContext ctx) { 
		parser.addToken(APPLICATION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterUnaryOpApplication(TamarinParser.UnaryOpApplicationContext ctx) { 
		parser.addChildNode(new UnaryOpApplicationNode());
		parser.addToken(UNARYOPAPPLICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitUnaryOpApplication(TamarinParser.UnaryOpApplicationContext ctx) { 
		parser.addToken(UNARYOPAPPLICATION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterNonUnaryOpApplication(TamarinParser.NonUnaryOpApplicationContext ctx) { 
		parser.addChildNode(new NonUnaryOpApplicationNode());
		parser.addToken(NONUNARYOPAPPLICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitNonUnaryOpApplication(TamarinParser.NonUnaryOpApplicationContext ctx) { 
		parser.addToken(NONUNARYOPAPPLICATION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterBinaryAlgApplication(TamarinParser.BinaryAlgApplicationContext ctx) { 
		parser.addChildNode(new BinaryAlgApplicationNode());
		parser.addToken(BINARYALGAPPLICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitBinaryAlgApplication(TamarinParser.BinaryAlgApplicationContext ctx) { 
		parser.addToken(BINARYALGAPPLICATION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterNullaryApplication(TamarinParser.NullaryApplicationContext ctx) { 
		parser.addChildNode(new NullaryApplicationNode());
		parser.addToken(NULLARYAPPLICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitNullaryApplication(TamarinParser.NullaryApplicationContext ctx) { 
		parser.addToken(NULLARYAPPLICATION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterReservedBuiltins(TamarinParser.ReservedBuiltinsContext ctx) { 
		parser.addChildNode(new ReservedBuiltinsNode());
		parser.addToken(RESERVEDBUILTINS, ctx.getStart());
	}
	@Override public void exitReservedBuiltins(TamarinParser.ReservedBuiltinsContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterGenericRule(TamarinParser.GenericRuleContext ctx) { 
		parser.addChildNode(new GenericRuleNode());
		parser.addToken(GENERICRULE_BEGIN, ctx.getStart());
	}
	@Override public void exitGenericRule(TamarinParser.GenericRuleContext ctx) { 
		parser.addToken(GENERICRULE_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterIntruderRule(TamarinParser.IntruderRuleContext ctx) { 
		parser.addChildNode(new IntruderRuleNode());
		parser.addToken(INTRUDERRULE_BEGIN, ctx.getStart());
	}
	@Override public void exitIntruderRule(TamarinParser.IntruderRuleContext ctx) { 
		parser.addToken(INTRUDERRULE_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterIntruderInfo(TamarinParser.IntruderInfoContext ctx) { 
		parser.addChildNode(new IntruderInfoNode());
		parser.addToken(INTRUDERINFO, ctx.getStart());
	}
	@Override public void exitIntruderInfo(TamarinParser.IntruderInfoContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterProtoRuleAC(TamarinParser.ProtoRuleACContext ctx) { 
		parser.addChildNode(new ProtoRuleACNode());
		parser.addToken(PROTORULEAC_BEGIN, ctx.getStart());
	}
	@Override public void exitProtoRuleAC(TamarinParser.ProtoRuleACContext ctx) { 
		parser.addToken(PROTORULEAC_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterProtoRuleACInfo(TamarinParser.ProtoRuleACInfoContext ctx) { 
		parser.addChildNode(new ProtoRuleACInfoNode());
		parser.addToken(PROTORULEACINFO, ctx.getStart());
	}
	@Override public void exitProtoRuleACInfo(TamarinParser.ProtoRuleACInfoContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterProtoRule(TamarinParser.ProtoRuleContext ctx) { 
		parser.addChildNode(new ProtoRuleNode());
		parser.addToken(PROTORULE_BEGIN, ctx.getStart());
	}
	@Override public void exitProtoRule(TamarinParser.ProtoRuleContext ctx) { 
		parser.addToken(PROTORULE_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterProtoRuleInfo(TamarinParser.ProtoRuleInfoContext ctx) { 
		parser.addChildNode(new ProtoRuleInfoNode());
		parser.addToken(PROTORULEINFO, ctx.getStart());
	}
	@Override public void exitProtoRuleInfo(TamarinParser.ProtoRuleInfoContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterRuleAttributes(TamarinParser.RuleAttributesContext ctx) { 
		parser.addChildNode(new RuleAttributesNode());
		parser.addToken(RULEATTRIBUTES, ctx.getStart());
	}
	@Override public void exitRuleAttributes(TamarinParser.RuleAttributesContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterRuleAttribute(TamarinParser.RuleAttributeContext ctx) { 
		parser.addChildNode(new RuleAttributeNode());
		parser.addToken(RULEATTRIBUTE, ctx.getStart());
	}
	@Override public void exitRuleAttribute(TamarinParser.RuleAttributeContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterEmbeddedRestriction(TamarinParser.EmbeddedRestrictionContext ctx) { 
		parser.addChildNode(new EmbeddedRestrictionNode());
		parser.addToken(EMBEDDEDRESTRICTION_BEGIN, ctx.getStart());
	}
	@Override public void exitEmbeddedRestriction(TamarinParser.EmbeddedRestrictionContext ctx) { 
		parser.addToken(EMBEDDEDRESTRICTION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterFactOrRestriction(TamarinParser.FactOrRestrictionContext ctx) { 
		parser.addChildNode(new FactOrRestrictionNode());
	}
	@Override public void exitFactOrRestriction(TamarinParser.FactOrRestrictionContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterFactOrRestrictionList(TamarinParser.FactOrRestrictionListContext ctx) { 
		parser.addChildNode(new FactOrRestrictionListNode());
		parser.addToken(FACTORRESTRICTIONLIST_BEGIN, ctx.getStart());
	}
	@Override public void exitFactOrRestrictionList(TamarinParser.FactOrRestrictionListContext ctx) { 
		parser.addToken(FACTORRESTRICTIONLIST_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterReservedRuleNames(TamarinParser.ReservedRuleNamesContext ctx) { 
		parser.addChildNode(new ReservedRuleNamesNode());
		parser.addToken(RESERVEDRULENAMES, ctx.getStart());
	}
	@Override public void exitReservedRuleNames(TamarinParser.ReservedRuleNamesContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterRuleFresh(TamarinParser.RuleFreshContext ctx) { 
		parser.addChildNode(new RuleFreshNode());
		parser.addToken(RULEFRESH, ctx.getStart());
	}
	@Override public void exitRuleFresh(TamarinParser.RuleFreshContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterRulePub(TamarinParser.RulePubContext ctx) { 
		parser.addChildNode(new RulePubNode());
		parser.addToken(RULEPUB, ctx.getStart());
	}
	@Override public void exitRulePub(TamarinParser.RulePubContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterHexColor(TamarinParser.HexColorContext ctx) { 
		parser.addChildNode(new HexColorNode());
		parser.addToken(HEXCOLOR, ctx.getStart());
	}
	@Override public void exitHexColor(TamarinParser.HexColorContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterModuloE(TamarinParser.ModuloEContext ctx) { 
		parser.addChildNode(new ModuloENode());
		parser.addToken(MODULOE, ctx.getStart());
	}
	@Override public void exitModuloE(TamarinParser.ModuloEContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterModuloAC(TamarinParser.ModuloACContext ctx) { 
		parser.addChildNode(new ModuloACNode());
		parser.addToken(MODULOAC, ctx.getStart());
	}
	@Override public void exitModuloAC(TamarinParser.ModuloACContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterFactList(TamarinParser.FactListContext ctx) { 
		parser.addChildNode(new FactListNode());
		parser.addToken(FACTLIST_BEGIN, ctx.getStart());
	}
	@Override public void exitFactList(TamarinParser.FactListContext ctx) { 
		parser.addToken(FACTLIST_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterFact(TamarinParser.FactContext ctx) { 
		parser.addChildNode(new FactNode());
		parser.addToken(FACT_BEGIN, ctx.getStart());
	}
	@Override public void exitFact(TamarinParser.FactContext ctx) { 
		parser.addToken(FACT_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterFreshFact(TamarinParser.FreshFactContext ctx) { 
		parser.addChildNode(new FreshFactNode());
		parser.addToken(FRESHFACT_BEGIN, ctx.getStart());
	}
	@Override public void exitFreshFact(TamarinParser.FreshFactContext ctx) { 
		parser.addToken(FRESHFACT_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterReservedFact(TamarinParser.ReservedFactContext ctx) { 
		parser.addChildNode(new ReservedFactNode());
		parser.addToken(RESERVEDFACT_BEGIN, ctx.getStart());
	}
	@Override public void exitReservedFact(TamarinParser.ReservedFactContext ctx) { 
		parser.addToken(RESERVEDFACT_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterProtoFact(TamarinParser.ProtoFactContext ctx) { 
		parser.addChildNode(new ProtoFactNode());
		parser.addToken(PROTOFACT_BEGIN, ctx.getStart());
	}
	@Override public void exitProtoFact(TamarinParser.ProtoFactContext ctx) { 
		parser.addToken(PROTOFACT_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterMultiplicity(TamarinParser.MultiplicityContext ctx) { 
		parser.addChildNode(new MultiplicityNode());
		parser.addToken(MULTIPLICITY, ctx.getStart());
	}
	@Override public void exitMultiplicity(TamarinParser.MultiplicityContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterFactAnnotations(TamarinParser.FactAnnotationsContext ctx) { 
		parser.addChildNode(new FactAnnotationsNode());
		parser.addToken(FACTANNOTATIONS, ctx.getStart());
	}
	@Override public void exitFactAnnotations(TamarinParser.FactAnnotationsContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterFactAnnotation(TamarinParser.FactAnnotationContext ctx) { 
		parser.addChildNode(new FactAnnotationNode());
		parser.addToken(FACTANNOTATION, ctx.getStart());
	}
	@Override public void exitFactAnnotation(TamarinParser.FactAnnotationContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLemmaAttributes(TamarinParser.LemmaAttributesContext ctx) { 
		parser.addChildNode(new LemmaAttributesNode());
		parser.addToken(LEMMAATTRIBUTES, ctx.getStart());
	}
	@Override public void exitLemmaAttributes(TamarinParser.LemmaAttributesContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLemmaAttribute(TamarinParser.LemmaAttributeContext ctx) { 
		parser.addChildNode(new LemmaAttributeNode());
		parser.addToken(LEMMAATTRIBUTE, ctx.getStart());
	}
	@Override public void exitLemmaAttribute(TamarinParser.LemmaAttributeContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTraceQuantifier(TamarinParser.TraceQuantifierContext ctx) { 
		parser.addChildNode(new TraceQuantifierNode());
		parser.addToken(TRACEQUANTIFIER_BEGIN, ctx.getStart());
	}
	@Override public void exitTraceQuantifier(TamarinParser.TraceQuantifierContext ctx) { 
		parser.addToken(TRACEQUANTIFIER_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterForallTraces(TamarinParser.ForallTracesContext ctx) { 
		parser.addChildNode(new ForallTracesNode());
		parser.addToken(FORALLTRACES, ctx.getStart());
	}
	@Override public void exitForallTraces(TamarinParser.ForallTracesContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterExistsTrace(TamarinParser.ExistsTraceContext ctx) { 
		parser.addChildNode(new ExistsTraceNode());
		parser.addToken(EXISTSTRACE, ctx.getStart());
	}
	@Override public void exitExistsTrace(TamarinParser.ExistsTraceContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLemma(TamarinParser.LemmaContext ctx) { 
		parser.addChildNode(new LemmaNode());
		parser.addToken(LEMMA_BEGIN, ctx.getStart());
	}
	@Override public void exitLemma(TamarinParser.LemmaContext ctx) { 
		parser.addToken(LEMMA_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterStandardFormula(TamarinParser.StandardFormulaContext ctx) { 
		parser.addChildNode(new StandardFormulaNode());
		parser.addToken(STANDARDFORMULA_BEGIN, ctx.getStart());
	}
	@Override public void exitStandardFormula(TamarinParser.StandardFormulaContext ctx) { 
		parser.addToken(STANDARDFORMULA_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterImplication(TamarinParser.ImplicationContext ctx) { 
		parser.addChildNode(new ImplicationNode());
		parser.addToken(IMPLICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitImplication(TamarinParser.ImplicationContext ctx) { 
		parser.addToken(IMPLICATION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterDisjunction(TamarinParser.DisjunctionContext ctx) { 
		parser.addChildNode(new DisjunctionNode());
		parser.addToken(DISJUNCTION_BEGIN, ctx.getStart());
	}
	@Override public void exitDisjunction(TamarinParser.DisjunctionContext ctx) { 
		parser.addToken(DISJUNCTION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterConjunction(TamarinParser.ConjunctionContext ctx) { 
		parser.addChildNode(new ConjunctionNode());
		parser.addToken(CONJUNCTION_BEGIN, ctx.getStart());
	}
	@Override public void exitConjunction(TamarinParser.ConjunctionContext ctx) { 
		parser.addToken(CONJUNCTION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterNegation(TamarinParser.NegationContext ctx) { 
		parser.addChildNode(new NegationNode());
		parser.addToken(NEGATION_BEGIN, ctx.getStart());
	}
	@Override public void exitNegation(TamarinParser.NegationContext ctx) { 
		parser.addToken(NEGATION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterFatom(TamarinParser.FatomContext ctx) { 
		parser.addChildNode(new FatomNode());
		parser.addToken(FATOM_BEGIN, ctx.getStart());
	}
	@Override public void exitFatom(TamarinParser.FatomContext ctx) { 
		parser.addToken(FATOM_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterBlatom_last(TamarinParser.Blatom_lastContext ctx) { 
		parser.addChildNode(new Blatom_lastNode());
		parser.addToken(BLATOM_LAST_BEGIN, ctx.getStart());
	}
	@Override public void exitBlatom_last(TamarinParser.Blatom_lastContext ctx) { 
		parser.addToken(BLATOM_LAST_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterBlatom_action(TamarinParser.Blatom_actionContext ctx) { 
		parser.addChildNode(new Blatom_actionNode());
		parser.addToken(BLATOM_ACTION_BEGIN, ctx.getStart());
	}
	@Override public void exitBlatom_action(TamarinParser.Blatom_actionContext ctx) { 
		parser.addToken(BLATOM_ACTION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterBlatom_predicate(TamarinParser.Blatom_predicateContext ctx) { 
		parser.addChildNode(new Blatom_predicateNode());
		parser.addToken(BLATOM_PREDICATE_BEGIN, ctx.getStart());
	}
	@Override public void exitBlatom_predicate(TamarinParser.Blatom_predicateContext ctx) { 
		parser.addToken(BLATOM_PREDICATE_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterBlatom_less(TamarinParser.Blatom_lessContext ctx) { 
		parser.addChildNode(new Blatom_lessNode());
		parser.addToken(BLATOM_LESS_BEGIN, ctx.getStart());
	}
	@Override public void exitBlatom_less(TamarinParser.Blatom_lessContext ctx) { 
		parser.addToken(BLATOM_LESS_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterBlatom_smallerp(TamarinParser.Blatom_smallerpContext ctx) { 
		parser.addChildNode(new Blatom_smallerpNode());
		parser.addToken(BLATOM_SMALLERP_BEGIN, ctx.getStart());
	}
	@Override public void exitBlatom_smallerp(TamarinParser.Blatom_smallerpContext ctx) { 
		parser.addToken(BLATOM_SMALLERP_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterBlatom_node_eq(TamarinParser.Blatom_node_eqContext ctx) { 
		parser.addChildNode(new Blatom_node_eqNode());
		parser.addToken(BLATOM_NODE_EQ_BEGIN, ctx.getStart());
	}
	@Override public void exitBlatom_node_eq(TamarinParser.Blatom_node_eqContext ctx) { 
		parser.addToken(BLATOM_NODE_EQ_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterBlatom_term_eq(TamarinParser.Blatom_term_eqContext ctx) { 
		parser.addChildNode(new Blatom_term_eqNode());
		parser.addToken(BLATOM_TERM_EQ_BEGIN, ctx.getStart());
	}
	@Override public void exitBlatom_term_eq(TamarinParser.Blatom_term_eqContext ctx) { 
		parser.addToken(BLATOM_TERM_EQ_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterQuantification(TamarinParser.QuantificationContext ctx) { 
		parser.addChildNode(new QuantificationNode());
		parser.addToken(QUANTIFICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitQuantification(TamarinParser.QuantificationContext ctx) { 
		parser.addToken(QUANTIFICATION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterSmallerp(TamarinParser.SmallerpContext ctx) { 
		parser.addChildNode(new SmallerpNode());
		parser.addToken(SMALLERP_BEGIN, ctx.getStart());
	}
	@Override public void exitSmallerp(TamarinParser.SmallerpContext ctx) { 
		parser.addToken(SMALLERP_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterCaseTest(TamarinParser.CaseTestContext ctx) { 
		parser.addChildNode(new CaseTestNode());
		parser.addToken(CASETEST, ctx.getStart());
	}
	@Override public void exitCaseTest(TamarinParser.CaseTestContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLemmaAcc(TamarinParser.LemmaAccContext ctx) { 
		parser.addChildNode(new LemmaAccNode());
		parser.addToken(LEMMAACC, ctx.getStart());
	}
	@Override public void exitLemmaAcc(TamarinParser.LemmaAccContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTactic(TamarinParser.TacticContext ctx) { 
		parser.addChildNode(new TacticNode());
		parser.addToken(TACTIC, ctx.getStart());
	}
	@Override public void exitTactic(TamarinParser.TacticContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterDeprio(TamarinParser.DeprioContext ctx) { 
		parser.addChildNode(new DeprioNode());
		parser.addToken(DEPRIO, ctx.getStart());
	}
	@Override public void exitDeprio(TamarinParser.DeprioContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterPrio(TamarinParser.PrioContext ctx) { 
		parser.addChildNode(new PrioNode());
		parser.addToken(PRIO, ctx.getStart());
	}
	@Override public void exitPrio(TamarinParser.PrioContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTacticDisjuncts(TamarinParser.TacticDisjunctsContext ctx) { 
		parser.addChildNode(new TacticDisjunctsNode());
		parser.addToken(TACTICDISJUNCTS, ctx.getStart());
	}
	@Override public void exitTacticDisjuncts(TamarinParser.TacticDisjunctsContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTacticConjuncts(TamarinParser.TacticConjunctsContext ctx) { 
		parser.addChildNode(new TacticConjunctsNode());
		parser.addToken(TACTICCONJUNCTS, ctx.getStart());
	}
	@Override public void exitTacticConjuncts(TamarinParser.TacticConjunctsContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTacticNegation(TamarinParser.TacticNegationContext ctx) { 
		parser.addChildNode(new TacticNegationNode());
		parser.addToken(TACTICNEGATION, ctx.getStart());
	}
	@Override public void exitTacticNegation(TamarinParser.TacticNegationContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTacticFunction(TamarinParser.TacticFunctionContext ctx) { 
		parser.addChildNode(new TacticFunctionNode());
		parser.addToken(TACTICFUNCTION, ctx.getStart());
	}
	@Override public void exitTacticFunction(TamarinParser.TacticFunctionContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterSelectedPresort(TamarinParser.SelectedPresortContext ctx) { 
		parser.addChildNode(new SelectedPresortNode());
		parser.addToken(SELECTEDPRESORT, ctx.getStart());
	}
	@Override public void exitSelectedPresort(TamarinParser.SelectedPresortContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterGoalRankingPresort(TamarinParser.GoalRankingPresortContext ctx) { 
		parser.addChildNode(new GoalRankingPresortNode());
		parser.addToken(GOALRANKINGPRESORT, ctx.getStart());
	}
	@Override public void exitGoalRankingPresort(TamarinParser.GoalRankingPresortContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTacticFunctionName(TamarinParser.TacticFunctionNameContext ctx) { 
		parser.addChildNode(new TacticFunctionNameNode());
		parser.addToken(TACTICFUNCTIONNAME, ctx.getStart());
	}
	@Override public void exitTacticFunctionName(TamarinParser.TacticFunctionNameContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterRankingIdentifier(TamarinParser.RankingIdentifierContext ctx) { 
		parser.addChildNode(new RankingIdentifierNode());
		parser.addToken(RANKINGIDENTIFIER, ctx.getStart());
	}
	@Override public void exitRankingIdentifier(TamarinParser.RankingIdentifierContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTacticName(TamarinParser.TacticNameContext ctx) { 
		parser.addChildNode(new TacticNameNode());
		parser.addToken(TACTICNAME, ctx.getStart());
	}
	@Override public void exitTacticName(TamarinParser.TacticNameContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterHeuristic(TamarinParser.HeuristicContext ctx) { 
		parser.addChildNode(new HeuristicNode());
		parser.addToken(HEURISTIC, ctx.getStart());
	}
	@Override public void exitHeuristic(TamarinParser.HeuristicContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterGoalRanking(TamarinParser.GoalRankingContext ctx) { 
		parser.addChildNode(new GoalRankingNode());
		parser.addToken(GOALRANKING, ctx.getStart());
	}
	@Override public void exitGoalRanking(TamarinParser.GoalRankingContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterRegularRanking(TamarinParser.RegularRankingContext ctx) { 
		parser.addChildNode(new RegularRankingNode());
		parser.addToken(REGULARRANKING, ctx.getStart());
	}
	@Override public void exitRegularRanking(TamarinParser.RegularRankingContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterInternalTacticRanking(TamarinParser.InternalTacticRankingContext ctx) { 
		parser.addChildNode(new InternalTacticRankingNode());
		parser.addToken(INTERNALTACTICRANKING, ctx.getStart());
	}
	@Override public void exitInternalTacticRanking(TamarinParser.InternalTacticRankingContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterOracleRanking(TamarinParser.OracleRankingContext ctx) { 
		parser.addChildNode(new OracleRankingNode());
		parser.addToken(ORACLERANKING, ctx.getStart());
	}
	@Override public void exitOracleRanking(TamarinParser.OracleRankingContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterExport(TamarinParser.ExportContext ctx) { 
		parser.addChildNode(new ExportNode());
		parser.addToken(EXPORT, ctx.getStart());
	}
	@Override public void exitExport(TamarinParser.ExportContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterPredicateDeclaration(TamarinParser.PredicateDeclarationContext ctx) { 
		parser.addChildNode(new PredicateDeclarationNode());
		parser.addToken(PREDICATEDECLARATION_BEGIN, ctx.getStart());
	}
	@Override public void exitPredicateDeclaration(TamarinParser.PredicateDeclarationContext ctx) { 
		parser.addToken(PREDICATEDECLARATION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterPredicate(TamarinParser.PredicateContext ctx) { 
		parser.addChildNode(new PredicateNode());
		parser.addToken(PREDICATE_BEGIN, ctx.getStart());
	}
	@Override public void exitPredicate(TamarinParser.PredicateContext ctx) { 
		parser.addToken(PREDICATE_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterSignatureOptions(TamarinParser.SignatureOptionsContext ctx) { 
		parser.addChildNode(new SignatureOptionsNode());
		parser.addToken(SIGNATUREOPTIONS, ctx.getStart());
	}
	@Override public void exitSignatureOptions(TamarinParser.SignatureOptionsContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinOptions(TamarinParser.BuiltinOptionsContext ctx) { 
		parser.addChildNode(new BuiltinOptionsNode());
		parser.addToken(BUILTINOPTIONS, ctx.getStart());
	}
	@Override public void exitBuiltinOptions(TamarinParser.BuiltinOptionsContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterEquations(TamarinParser.EquationsContext ctx) { 
		parser.addChildNode(new EquationsNode());
		parser.addToken(EQUATIONS_BEGIN, ctx.getStart());
	}
	@Override public void exitEquations(TamarinParser.EquationsContext ctx) { 
		parser.addToken(EQUATIONS_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterEquation(TamarinParser.EquationContext ctx) { 
		parser.addChildNode(new EquationNode());
		parser.addToken(EQUATION_BEGIN, ctx.getStart());
	}
	@Override public void exitEquation(TamarinParser.EquationContext ctx) { 
		parser.addToken(EQUATION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterSignatureFunctions(TamarinParser.SignatureFunctionsContext ctx) { 
		parser.addChildNode(new SignatureFunctionsNode());
		parser.addToken(SIGNATUREFUNCTIONS_BEGIN, ctx.getStart());
	}
	@Override public void exitSignatureFunctions(TamarinParser.SignatureFunctionsContext ctx) { 
		parser.addToken(SIGNATUREFUNCTIONS_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterSignatureFunction(TamarinParser.SignatureFunctionContext ctx) { 
		parser.addChildNode(new SignatureFunctionNode());
		parser.addToken(SIGNATUREFUNCTION_BEGIN, ctx.getStart());
	}
	@Override public void exitSignatureFunction(TamarinParser.SignatureFunctionContext ctx) { 
		parser.addToken(SIGNATUREFUNCTION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterFunctionAttribute(TamarinParser.FunctionAttributeContext ctx) { 
		parser.addChildNode(new FunctionAttributeNode());
		parser.addToken(FUNCTIONATTRIBUTE, ctx.getStart());
	}
	@Override public void exitFunctionAttribute(TamarinParser.FunctionAttributeContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterFunction_type_natural(TamarinParser.Function_type_naturalContext ctx) { 
		parser.addChildNode(new Function_type_naturalNode());
		parser.addToken(FUNCTION_TYPE_NATURAL, ctx.getStart());
	}
	@Override public void exitFunction_type_natural(TamarinParser.Function_type_naturalContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterFunction_type_sapic(TamarinParser.Function_type_sapicContext ctx) { 
		parser.addChildNode(new Function_type_sapicNode());
		parser.addToken(FUNCTION_TYPE_SAPIC_BEGIN, ctx.getStart());
	}
	@Override public void exitFunction_type_sapic(TamarinParser.Function_type_sapicContext ctx) { 
		parser.addToken(FUNCTION_TYPE_SAPIC_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterBuiltins(TamarinParser.BuiltinsContext ctx) { 
		parser.addChildNode(new BuiltinsNode());
		parser.addToken(BUILTINS, ctx.getStart());
	}
	@Override public void exitBuiltins(TamarinParser.BuiltinsContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNames(TamarinParser.BuiltinNamesContext ctx) { 
		parser.addChildNode(new BuiltinNamesNode());
		parser.addToken(BUILTINNAMES, ctx.getStart());
	}
	@Override public void exitBuiltinNames(TamarinParser.BuiltinNamesContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameLocationsReport(TamarinParser.BuiltinNameLocationsReportContext ctx) { 
		parser.addChildNode(new BuiltinNameLocationsReportNode());
		parser.addToken(BUILTINNAMELOCATIONSREPORT, ctx.getStart());
	}
	@Override public void exitBuiltinNameLocationsReport(TamarinParser.BuiltinNameLocationsReportContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameReliableChannel(TamarinParser.BuiltinNameReliableChannelContext ctx) { 
		parser.addChildNode(new BuiltinNameReliableChannelNode());
		parser.addToken(BUILTINNAMERELIABLECHANNEL, ctx.getStart());
	}
	@Override public void exitBuiltinNameReliableChannel(TamarinParser.BuiltinNameReliableChannelContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameDH(TamarinParser.BuiltinNameDHContext ctx) { 
		parser.addChildNode(new BuiltinNameDHNode());
		parser.addToken(BUILTINNAMEDH, ctx.getStart());
	}
	@Override public void exitBuiltinNameDH(TamarinParser.BuiltinNameDHContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameBilinearPairing(TamarinParser.BuiltinNameBilinearPairingContext ctx) { 
		parser.addChildNode(new BuiltinNameBilinearPairingNode());
		parser.addToken(BUILTINNAMEBILINEARPAIRING, ctx.getStart());
	}
	@Override public void exitBuiltinNameBilinearPairing(TamarinParser.BuiltinNameBilinearPairingContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameMultiset(TamarinParser.BuiltinNameMultisetContext ctx) { 
		parser.addChildNode(new BuiltinNameMultisetNode());
		parser.addToken(BUILTINNAMEMULTISET, ctx.getStart());
	}
	@Override public void exitBuiltinNameMultiset(TamarinParser.BuiltinNameMultisetContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameSymmetricEncryption(TamarinParser.BuiltinNameSymmetricEncryptionContext ctx) { 
		parser.addChildNode(new BuiltinNameSymmetricEncryptionNode());
		parser.addToken(BUILTINNAMESYMMETRICENCRYPTION, ctx.getStart());
	}
	@Override public void exitBuiltinNameSymmetricEncryption(TamarinParser.BuiltinNameSymmetricEncryptionContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameAsymmetricEncryption(TamarinParser.BuiltinNameAsymmetricEncryptionContext ctx) { 
		parser.addChildNode(new BuiltinNameAsymmetricEncryptionNode());
		parser.addToken(BUILTINNAMEASYMMETRICENCRYPTION, ctx.getStart());
	}
	@Override public void exitBuiltinNameAsymmetricEncryption(TamarinParser.BuiltinNameAsymmetricEncryptionContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameSigning(TamarinParser.BuiltinNameSigningContext ctx) { 
		parser.addChildNode(new BuiltinNameSigningNode());
		parser.addToken(BUILTINNAMESIGNING, ctx.getStart());
	}
	@Override public void exitBuiltinNameSigning(TamarinParser.BuiltinNameSigningContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameDestPairing(TamarinParser.BuiltinNameDestPairingContext ctx) { 
		parser.addChildNode(new BuiltinNameDestPairingNode());
		parser.addToken(BUILTINNAMEDESTPAIRING, ctx.getStart());
	}
	@Override public void exitBuiltinNameDestPairing(TamarinParser.BuiltinNameDestPairingContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameDestSymmetricEncryption(TamarinParser.BuiltinNameDestSymmetricEncryptionContext ctx) { 
		parser.addChildNode(new BuiltinNameDestSymmetricEncryptionNode());
		parser.addToken(BUILTINNAMEDESTSYMMETRICENCRYPTION, ctx.getStart());
	}
	@Override public void exitBuiltinNameDestSymmetricEncryption(TamarinParser.BuiltinNameDestSymmetricEncryptionContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameDestAsymmetricEncryption(TamarinParser.BuiltinNameDestAsymmetricEncryptionContext ctx) { 
		parser.addChildNode(new BuiltinNameDestAsymmetricEncryptionNode());
		parser.addToken(BUILTINNAMEDESTASYMMETRICENCRYPTION, ctx.getStart());
	}
	@Override public void exitBuiltinNameDestAsymmetricEncryption(TamarinParser.BuiltinNameDestAsymmetricEncryptionContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameDestSigning(TamarinParser.BuiltinNameDestSigningContext ctx) { 
		parser.addChildNode(new BuiltinNameDestSigningNode());
		parser.addToken(BUILTINNAMEDESTSIGNING, ctx.getStart());
	}
	@Override public void exitBuiltinNameDestSigning(TamarinParser.BuiltinNameDestSigningContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameRevealingSigning(TamarinParser.BuiltinNameRevealingSigningContext ctx) { 
		parser.addChildNode(new BuiltinNameRevealingSigningNode());
		parser.addToken(BUILTINNAMEREVEALINGSIGNING, ctx.getStart());
	}
	@Override public void exitBuiltinNameRevealingSigning(TamarinParser.BuiltinNameRevealingSigningContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameHashing(TamarinParser.BuiltinNameHashingContext ctx) { 
		parser.addChildNode(new BuiltinNameHashingNode());
		parser.addToken(BUILTINNAMEHASHING, ctx.getStart());
	}
	@Override public void exitBuiltinNameHashing(TamarinParser.BuiltinNameHashingContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameXor(TamarinParser.BuiltinNameXorContext ctx) { 
		parser.addChildNode(new BuiltinNameXorNode());
		parser.addToken(BUILTINNAMEXOR, ctx.getStart());
	}
	@Override public void exitBuiltinNameXor(TamarinParser.BuiltinNameXorContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterProofSkeleton(TamarinParser.ProofSkeletonContext ctx) { 
		parser.addChildNode(new ProofSkeletonNode());
		parser.addToken(PROOFSKELETON, ctx.getStart());
	}
	@Override public void exitProofSkeleton(TamarinParser.ProofSkeletonContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterStartProofSkeleton(TamarinParser.StartProofSkeletonContext ctx) { 
		parser.addChildNode(new StartProofSkeletonNode());
		parser.addToken(STARTPROOFSKELETON, ctx.getStart());
	}
	@Override public void exitStartProofSkeleton(TamarinParser.StartProofSkeletonContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterSolvedProof(TamarinParser.SolvedProofContext ctx) { 
		parser.addChildNode(new SolvedProofNode());
		parser.addToken(SOLVEDPROOF, ctx.getStart());
	}
	@Override public void exitSolvedProof(TamarinParser.SolvedProofContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterFinalProof(TamarinParser.FinalProofContext ctx) { 
		parser.addChildNode(new FinalProofNode());
		parser.addToken(FINALPROOF, ctx.getStart());
	}
	@Override public void exitFinalProof(TamarinParser.FinalProofContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterInterProof(TamarinParser.InterProofContext ctx) { 
		parser.addChildNode(new InterProofNode());
		parser.addToken(INTERPROOF, ctx.getStart());
	}
	@Override public void exitInterProof(TamarinParser.InterProofContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterProofCase(TamarinParser.ProofCaseContext ctx) { 
		parser.addChildNode(new ProofCaseNode());
		parser.addToken(PROOFCASE, ctx.getStart());
	}
	@Override public void exitProofCase(TamarinParser.ProofCaseContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterProofMethod(TamarinParser.ProofMethodContext ctx) { 
		parser.addChildNode(new ProofMethodNode());
		parser.addToken(PROOFMETHOD, ctx.getStart());
	}
	@Override public void exitProofMethod(TamarinParser.ProofMethodContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterGoal(TamarinParser.GoalContext ctx) { 
		parser.addChildNode(new GoalNode());
		parser.addToken(GOAL, ctx.getStart());
	}
	@Override public void exitGoal(TamarinParser.GoalContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterActionGoal(TamarinParser.ActionGoalContext ctx) { 
		parser.addChildNode(new ActionGoalNode());
		parser.addToken(ACTIONGOAL_BEGIN, ctx.getStart());
	}
	@Override public void exitActionGoal(TamarinParser.ActionGoalContext ctx) { 
		parser.addToken(ACTIONGOAL_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterPremiseGoal(TamarinParser.PremiseGoalContext ctx) { 
		parser.addChildNode(new PremiseGoalNode());
		parser.addToken(PREMISEGOAL_BEGIN, ctx.getStart());
	}
	@Override public void exitPremiseGoal(TamarinParser.PremiseGoalContext ctx) { 
		parser.addToken(PREMISEGOAL_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterChainGoal(TamarinParser.ChainGoalContext ctx) { 
		parser.addChildNode(new ChainGoalNode());
		parser.addToken(CHAINGOAL_BEGIN, ctx.getStart());
	}
	@Override public void exitChainGoal(TamarinParser.ChainGoalContext ctx) { 
		parser.addToken(CHAINGOAL_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterDisjSplitGoal(TamarinParser.DisjSplitGoalContext ctx) { 
		parser.addChildNode(new DisjSplitGoalNode());
		parser.addToken(DISJSPLITGOAL, ctx.getStart());
	}
	@Override public void exitDisjSplitGoal(TamarinParser.DisjSplitGoalContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterEqSplitGoal(TamarinParser.EqSplitGoalContext ctx) { 
		parser.addChildNode(new EqSplitGoalNode());
		parser.addToken(EQSPLITGOAL, ctx.getStart());
	}
	@Override public void exitEqSplitGoal(TamarinParser.EqSplitGoalContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterNodePremise(TamarinParser.NodePremiseContext ctx) { 
		parser.addChildNode(new NodePremiseNode());
		parser.addToken(NODEPREMISE, ctx.getStart());
	}
	@Override public void exitNodePremise(TamarinParser.NodePremiseContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterNodeConclusion(TamarinParser.NodeConclusionContext ctx) { 
		parser.addChildNode(new NodeConclusionNode());
		parser.addToken(NODECONCLUSION, ctx.getStart());
	}
	@Override public void exitNodeConclusion(TamarinParser.NodeConclusionContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterDiffEquivLemma(TamarinParser.DiffEquivLemmaContext ctx) { 
		parser.addChildNode(new DiffEquivLemmaNode());
		parser.addToken(DIFFEQUIVLEMMA, ctx.getStart());
	}
	@Override public void exitDiffEquivLemma(TamarinParser.DiffEquivLemmaContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterEquivLemma(TamarinParser.EquivLemmaContext ctx) { 
		parser.addChildNode(new EquivLemmaNode());
		parser.addToken(EQUIVLEMMA, ctx.getStart());
	}
	@Override public void exitEquivLemma(TamarinParser.EquivLemmaContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterProcess(TamarinParser.ProcessContext ctx) { 
		parser.addChildNode(new ProcessNode());
		parser.addToken(PROCESS, ctx.getStart());
	}
	@Override public void exitProcess(TamarinParser.ProcessContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterElseProcess(TamarinParser.ElseProcessContext ctx) { 
		parser.addChildNode(new ElseProcessNode());
		parser.addToken(ELSEPROCESS, ctx.getStart());
	}
	@Override public void exitElseProcess(TamarinParser.ElseProcessContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTopLevelProcess(TamarinParser.TopLevelProcessContext ctx) { 
		parser.addChildNode(new TopLevelProcessNode());
		parser.addToken(TOPLEVELPROCESS, ctx.getStart());
	}
	@Override public void exitTopLevelProcess(TamarinParser.TopLevelProcessContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterActionProcess(TamarinParser.ActionProcessContext ctx) { 
		parser.addChildNode(new ActionProcessNode());
		parser.addToken(ACTIONPROCESS, ctx.getStart());
	}
	@Override public void exitActionProcess(TamarinParser.ActionProcessContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterSapicAction(TamarinParser.SapicActionContext ctx) { 
		parser.addChildNode(new SapicActionNode());
		parser.addToken(SAPICACTION, ctx.getStart());
	}
	@Override public void exitSapicAction(TamarinParser.SapicActionContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterSapicActionIn(TamarinParser.SapicActionInContext ctx) { 
		parser.addChildNode(new SapicActionInNode());
		parser.addToken(SAPICACTIONIN, ctx.getStart());
	}
	@Override public void exitSapicActionIn(TamarinParser.SapicActionInContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterSapicActionOut(TamarinParser.SapicActionOutContext ctx) { 
		parser.addChildNode(new SapicActionOutNode());
		parser.addToken(SAPICACTIONOUT, ctx.getStart());
	}
	@Override public void exitSapicActionOut(TamarinParser.SapicActionOutContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterProcessDef(TamarinParser.ProcessDefContext ctx) { 
		parser.addChildNode(new ProcessDefNode());
		parser.addToken(PROCESSDEF, ctx.getStart());
	}
	@Override public void exitProcessDef(TamarinParser.ProcessDefContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterSapicPatternTerm(TamarinParser.SapicPatternTermContext ctx) { 
		parser.addChildNode(new SapicPatternTermNode());
	}
	@Override public void exitSapicPatternTerm(TamarinParser.SapicPatternTermContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterSapicTerm(TamarinParser.SapicTermContext ctx) { 
		parser.addChildNode(new SapicTermNode());
	}
	@Override public void exitSapicTerm(TamarinParser.SapicTermContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLogicalTypedPatternLiteral(TamarinParser.LogicalTypedPatternLiteralContext ctx) { 
		parser.addChildNode(new LogicalTypedPatternLiteralNode());
	}
	@Override public void exitLogicalTypedPatternLiteral(TamarinParser.LogicalTypedPatternLiteralContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLogicalTypedLiteral(TamarinParser.LogicalTypedLiteralContext ctx) { 
		parser.addChildNode(new LogicalTypedLiteralNode());
	}
	@Override public void exitLogicalTypedLiteral(TamarinParser.LogicalTypedLiteralContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterSapicNodeVar(TamarinParser.SapicNodeVarContext ctx) { 
		parser.addChildNode(new SapicNodeVarNode());
	}
	@Override public void exitSapicNodeVar(TamarinParser.SapicNodeVarContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterSapicPatternVar(TamarinParser.SapicPatternVarContext ctx) { 
		parser.addChildNode(new SapicPatternVarNode());
	}
	@Override public void exitSapicPatternVar(TamarinParser.SapicPatternVarContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterSapicVar(TamarinParser.SapicVarContext ctx) { 
		parser.addChildNode(new SapicVarNode());
	}
	@Override public void exitSapicVar(TamarinParser.SapicVarContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterSapicType(TamarinParser.SapicTypeContext ctx) { 
		parser.addChildNode(new SapicTypeNode());
		parser.addToken(SAPICTYPE_BEGIN, ctx.getStart());
	}
	@Override public void exitSapicType(TamarinParser.SapicTypeContext ctx) { 
		parser.addToken(SAPICTYPE_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterRestriction(TamarinParser.RestrictionContext ctx) { 
		parser.addChildNode(new RestrictionNode());
		parser.addToken(RESTRICTION_BEGIN, ctx.getStart());
	}
	@Override public void exitRestriction(TamarinParser.RestrictionContext ctx) { 
		parser.addToken(RESTRICTION_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterRestrictionAttributes(TamarinParser.RestrictionAttributesContext ctx) { 
		parser.addChildNode(new RestrictionAttributesNode());
		parser.addToken(RESTRICTIONATTRIBUTES, ctx.getStart());
	}
	@Override public void exitRestrictionAttributes(TamarinParser.RestrictionAttributesContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterRestrictionAttribute(TamarinParser.RestrictionAttributeContext ctx) { 
		parser.addChildNode(new RestrictionAttributeNode());
		parser.addToken(RESTRICTIONATTRIBUTE, ctx.getStart());
	}
	@Override public void exitRestrictionAttribute(TamarinParser.RestrictionAttributeContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLegacyAxiom(TamarinParser.LegacyAxiomContext ctx) { 
		parser.addChildNode(new LegacyAxiomNode());
		parser.addToken(LEGACYAXIOM_BEGIN, ctx.getStart());
	}
	@Override public void exitLegacyAxiom(TamarinParser.LegacyAxiomContext ctx) { 
		parser.addToken(LEGACYAXIOM_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterLetBlock(TamarinParser.LetBlockContext ctx) { 
		parser.addChildNode(new LetBlockNode());
		parser.addToken(LETBLOCK_BEGIN, ctx.getStart());
	}
	@Override public void exitLetBlock(TamarinParser.LetBlockContext ctx) { 
		parser.addToken(LETBLOCK_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterLetAssignment(TamarinParser.LetAssignmentContext ctx) { 
		parser.addChildNode(new LetAssignmentNode());
		parser.addToken(LETASSIGNMENT_BEGIN, ctx.getStart());
	}
	@Override public void exitLetAssignment(TamarinParser.LetAssignmentContext ctx) { 
		parser.addToken(LETASSIGNMENT_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterProtoFactIdentifier(TamarinParser.ProtoFactIdentifierContext ctx) { 
		parser.addChildNode(new ProtoFactIdentifierNode());
	}
	@Override public void exitProtoFactIdentifier(TamarinParser.ProtoFactIdentifierContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterReserved_fact_out(TamarinParser.Reserved_fact_outContext ctx) { 
		parser.addChildNode(new Reserved_fact_outNode());
		parser.addToken(RESERVED_FACT_OUT, ctx.getStart());
	}
	@Override public void exitReserved_fact_out(TamarinParser.Reserved_fact_outContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterReserved_fact_in(TamarinParser.Reserved_fact_inContext ctx) { 
		parser.addChildNode(new Reserved_fact_inNode());
		parser.addToken(RESERVED_FACT_IN, ctx.getStart());
	}
	@Override public void exitReserved_fact_in(TamarinParser.Reserved_fact_inContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterReserved_fact_ku(TamarinParser.Reserved_fact_kuContext ctx) { 
		parser.addChildNode(new Reserved_fact_kuNode());
		parser.addToken(RESERVED_FACT_KU, ctx.getStart());
	}
	@Override public void exitReserved_fact_ku(TamarinParser.Reserved_fact_kuContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterReserved_fact_kd(TamarinParser.Reserved_fact_kdContext ctx) { 
		parser.addChildNode(new Reserved_fact_kdNode());
		parser.addToken(RESERVED_FACT_KD, ctx.getStart());
	}
	@Override public void exitReserved_fact_kd(TamarinParser.Reserved_fact_kdContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterReserved_fact_ded(TamarinParser.Reserved_fact_dedContext ctx) { 
		parser.addChildNode(new Reserved_fact_dedNode());
		parser.addToken(RESERVED_FACT_DED, ctx.getStart());
	}
	@Override public void exitReserved_fact_ded(TamarinParser.Reserved_fact_dedContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterIdentifier(TamarinParser.IdentifierContext ctx) { 
		parser.addChildNode(new IdentifierNode());
		parser.addToken(IDENTIFIER, ctx.getStart());
	}
	@Override public void exitIdentifier(TamarinParser.IdentifierContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterNatural(TamarinParser.NaturalContext ctx) { 
		parser.addChildNode(new NaturalNode());
		parser.addToken(NATURAL, ctx.getStart());
	}
	@Override public void exitNatural(TamarinParser.NaturalContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterIndexedIdentifier(TamarinParser.IndexedIdentifierContext ctx) { 
		parser.addChildNode(new IndexedIdentifierNode());
		parser.addToken(INDEXEDIDENTIFIER, ctx.getStart());
	}
	@Override public void exitIndexedIdentifier(TamarinParser.IndexedIdentifierContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterFilePath(TamarinParser.FilePathContext ctx) { 
		parser.addChildNode(new FilePathNode());
		parser.addToken(FILEPATH, ctx.getStart());
	}
	@Override public void exitFilePath(TamarinParser.FilePathContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterKeywords(TamarinParser.KeywordsContext ctx) { 
		parser.addChildNode(new KeywordsNode());
		parser.addToken(KEYWORDS, ctx.getStart());
	}
	@Override public void exitKeywords(TamarinParser.KeywordsContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLogMsgVar(TamarinParser.LogMsgVarContext ctx) { 
		parser.addChildNode(new LogMsgVarNode());
		parser.addToken(LOGMSGVAR, ctx.getStart());
	}
	@Override public void exitLogMsgVar(TamarinParser.LogMsgVarContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLogFreshVar(TamarinParser.LogFreshVarContext ctx) { 
		parser.addChildNode(new LogFreshVarNode());
		parser.addToken(LOGFRESHVAR, ctx.getStart());
	}
	@Override public void exitLogFreshVar(TamarinParser.LogFreshVarContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLogPubVar(TamarinParser.LogPubVarContext ctx) { 
		parser.addChildNode(new LogPubVarNode());
		parser.addToken(LOGPUBVAR, ctx.getStart());
	}
	@Override public void exitLogPubVar(TamarinParser.LogPubVarContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLogNodeVar(TamarinParser.LogNodeVarContext ctx) { 
		parser.addChildNode(new LogNodeVarNode());
		parser.addToken(LOGNODEVAR, ctx.getStart());
	}
	@Override public void exitLogNodeVar(TamarinParser.LogNodeVarContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLogVar(TamarinParser.LogVarContext ctx) { 
		parser.addChildNode(new LogVarNode());
	}
	@Override public void exitLogVar(TamarinParser.LogVarContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterMsgVar(TamarinParser.MsgVarContext ctx) { 
		parser.addChildNode(new MsgVarNode());
	}
	@Override public void exitMsgVar(TamarinParser.MsgVarContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLogicalLiteral(TamarinParser.LogicalLiteralContext ctx) { 
		parser.addChildNode(new LogicalLiteralNode());
	}
	@Override public void exitLogicalLiteral(TamarinParser.LogicalLiteralContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLogicalLiteralWithNode(TamarinParser.LogicalLiteralWithNodeContext ctx) { 
		parser.addChildNode(new LogicalLiteralWithNodeNode());
	}
	@Override public void exitLogicalLiteralWithNode(TamarinParser.LogicalLiteralWithNodeContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLogicalLiteralNoPub(TamarinParser.LogicalLiteralNoPubContext ctx) { 
		parser.addChildNode(new LogicalLiteralNoPubNode());
	}
	@Override public void exitLogicalLiteralNoPub(TamarinParser.LogicalLiteralNoPubContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLiterals(TamarinParser.LiteralsContext ctx) { 
		parser.addChildNode(new LiteralsNode());
		parser.addToken(LITERALS_BEGIN, ctx.getStart());
	}
	@Override public void exitLiterals(TamarinParser.LiteralsContext ctx) { 
		parser.addToken(LITERALS_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterLiteral(TamarinParser.LiteralContext ctx) { 
		parser.addChildNode(new LiteralNode());
		parser.addToken(LITERAL_BEGIN, ctx.getStart());
	}
	@Override public void exitLiteral(TamarinParser.LiteralContext ctx) { 
		parser.addToken(LITERAL_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterNodeVar(TamarinParser.NodeVarContext ctx) { 
		parser.addChildNode(new NodeVarNode());
		parser.addToken(LOGNODEVAR_BEGIN, ctx.getStart());
	}
	@Override public void exitNodeVar(TamarinParser.NodeVarContext ctx) { 
		parser.addToken(LOGNODEVAR_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterFreshName(TamarinParser.FreshNameContext ctx) { 
		parser.addChildNode(new FreshNameNode());
		parser.addToken(FRESHNAME, ctx.getStart());
	}
	@Override public void exitFreshName(TamarinParser.FreshNameContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterPubName(TamarinParser.PubNameContext ctx) { 
		parser.addChildNode(new PubNameNode());
		parser.addToken(PUBNAME, ctx.getStart());
	}
	@Override public void exitPubName(TamarinParser.PubNameContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterFormalComment(TamarinParser.FormalCommentContext ctx) { 
		parser.addChildNode(new FormalCommentNode());
		parser.addToken(FORMALCOMMENT, ctx.getStart());
	}
	@Override public void exitFormalComment(TamarinParser.FormalCommentContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTacticFunctionIdentifier(TamarinParser.TacticFunctionIdentifierContext ctx) { 
		parser.addChildNode(new TacticFunctionIdentifierNode());
		parser.addToken(TACTICFUNCTIONIDENTIFIER, ctx.getStart());
	}
	@Override public void exitTacticFunctionIdentifier(TamarinParser.TacticFunctionIdentifierContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterInternalTacticName(TamarinParser.InternalTacticNameContext ctx) { 
		parser.addChildNode(new InternalTacticNameNode());
		parser.addToken(INTERNALTACTICNAME, ctx.getStart());
	}
	@Override public void exitInternalTacticName(TamarinParser.InternalTacticNameContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterOracleRelativePath(TamarinParser.OracleRelativePathContext ctx) { 
		parser.addChildNode(new OracleRelativePathNode());
		parser.addToken(ORACLERELATIVEPATH, ctx.getStart());
	}
	@Override public void exitOracleRelativePath(TamarinParser.OracleRelativePathContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterExportBodyChars(TamarinParser.ExportBodyCharsContext ctx) { 
		parser.addChildNode(new ExportBodyCharsNode());
		parser.addToken(EXPORTBODYCHARS, ctx.getStart());
	}
	@Override public void exitExportBodyChars(TamarinParser.ExportBodyCharsContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterCharDir(TamarinParser.CharDirContext ctx) { 
		parser.addChildNode(new CharDirNode());
		parser.addToken(CHARDIR, ctx.getStart());
	}
	@Override public void exitCharDir(TamarinParser.CharDirContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterEquivOp(TamarinParser.EquivOpContext ctx) { 
		parser.addChildNode(new EquivOpNode());
		parser.addToken(EQUIVOP, ctx.getStart());
	}
	@Override public void exitEquivOp(TamarinParser.EquivOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterOrOp(TamarinParser.OrOpContext ctx) { 
		parser.addChildNode(new OrOpNode());
		parser.addToken(OROP, ctx.getStart());
	}
	@Override public void exitOrOp(TamarinParser.OrOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterNotOp(TamarinParser.NotOpContext ctx) { 
		parser.addChildNode(new NotOpNode());
		parser.addToken(NOTOP, ctx.getStart());
	}
	@Override public void exitNotOp(TamarinParser.NotOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterAndOp(TamarinParser.AndOpContext ctx) { 
		parser.addChildNode(new AndOpNode());
		parser.addToken(ANDOP, ctx.getStart());
	}
	@Override public void exitAndOp(TamarinParser.AndOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterEqOp(TamarinParser.EqOpContext ctx) { 
		parser.addChildNode(new EqOpNode());
		parser.addToken(EQOP_BEGIN, ctx.getStart());
	}
	@Override public void exitEqOp(TamarinParser.EqOpContext ctx) { 
		parser.addToken(EQOP_END, ctx.getStop());
		parser.finishChild();
	}
	@Override public void enterBotOp(TamarinParser.BotOpContext ctx) { 
		parser.addChildNode(new BotOpNode());
		parser.addToken(BOTOP, ctx.getStart());
	}
	@Override public void exitBotOp(TamarinParser.BotOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterFOp(TamarinParser.FOpContext ctx) { 
		parser.addChildNode(new FOpNode());
		parser.addToken(BOTOP, ctx.getStart());
	}
	@Override public void exitFOp(TamarinParser.FOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTOp(TamarinParser.TOpContext ctx) { 
		parser.addChildNode(new TOpNode());
		parser.addToken(TOPOP, ctx.getStart());
	}
	@Override public void exitTOp(TamarinParser.TOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTopOp(TamarinParser.TopOpContext ctx) { 
		parser.addChildNode(new TopOpNode());
		parser.addToken(TOPOP, ctx.getStart());
	}
	@Override public void exitTopOp(TamarinParser.TopOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterForallOp(TamarinParser.ForallOpContext ctx) { 
		parser.addChildNode(new ForallOpNode());
		parser.addToken(FORALLOP, ctx.getStart());
	}
	@Override public void exitForallOp(TamarinParser.ForallOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterExistsOp(TamarinParser.ExistsOpContext ctx) { 
		parser.addChildNode(new ExistsOpNode());
		parser.addToken(EXISTSOP, ctx.getStart());
	}
	@Override public void exitExistsOp(TamarinParser.ExistsOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinXor(TamarinParser.BuiltinXorContext ctx) { 
		parser.addChildNode(new BuiltinXorNode());
		parser.addToken(BUILTINXOR, ctx.getStart());
	}
	@Override public void exitBuiltinXor(TamarinParser.BuiltinXorContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinMun(TamarinParser.BuiltinMunContext ctx) { 
		parser.addChildNode(new BuiltinMunNode());
		parser.addToken(BUILTINMUN, ctx.getStart());
	}
	@Override public void exitBuiltinMun(TamarinParser.BuiltinMunContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinOne(TamarinParser.BuiltinOneContext ctx) { 
		parser.addChildNode(new BuiltinOneNode());
		parser.addToken(BUILTINONE, ctx.getStart());
	}
	@Override public void exitBuiltinOne(TamarinParser.BuiltinOneContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinExp(TamarinParser.BuiltinExpContext ctx) { 
		parser.addChildNode(new BuiltinExpNode());
		parser.addToken(BUILTINEXP, ctx.getStart());
	}
	@Override public void exitBuiltinExp(TamarinParser.BuiltinExpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinMult(TamarinParser.BuiltinMultContext ctx) { 
		parser.addChildNode(new BuiltinMultNode());
		parser.addToken(BUILTINMULT, ctx.getStart());
	}
	@Override public void exitBuiltinMult(TamarinParser.BuiltinMultContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinInv(TamarinParser.BuiltinInvContext ctx) { 
		parser.addChildNode(new BuiltinInvNode());
		parser.addToken(BUILTININV, ctx.getStart());
	}
	@Override public void exitBuiltinInv(TamarinParser.BuiltinInvContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinPmult(TamarinParser.BuiltinPmultContext ctx) { 
		parser.addChildNode(new BuiltinPmultNode());
		parser.addToken(BUILTINPMULT, ctx.getStart());
	}
	@Override public void exitBuiltinPmult(TamarinParser.BuiltinPmultContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinEm(TamarinParser.BuiltinEmContext ctx) { 
		parser.addChildNode(new BuiltinEmNode());
		parser.addToken(BUILTINEM, ctx.getStart());
	}
	@Override public void exitBuiltinEm(TamarinParser.BuiltinEmContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinZero(TamarinParser.BuiltinZeroContext ctx) { 
		parser.addChildNode(new BuiltinZeroNode());
		parser.addToken(BUILTINZERO, ctx.getStart());
	}
	@Override public void exitBuiltinZero(TamarinParser.BuiltinZeroContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterDhNeutral(TamarinParser.DhNeutralContext ctx) { 
		parser.addChildNode(new DhNeutralNode());
		parser.addToken(DHNEUTRAL, ctx.getStart());
	}
	@Override public void exitDhNeutral(TamarinParser.DhNeutralContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterOperators(TamarinParser.OperatorsContext ctx) { 
		parser.addChildNode(new OperatorsNode());
		parser.addToken(OPERATORS, ctx.getStart());
	}
	@Override public void exitOperators(TamarinParser.OperatorsContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterPunctuation(TamarinParser.PunctuationContext ctx) { 
		parser.addChildNode(new PunctuationNode());
		parser.addToken(PUNCTUATION, ctx.getStart());
	}
	@Override public void exitPunctuation(TamarinParser.PunctuationContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterAllowedIdentifierKeywords(TamarinParser.AllowedIdentifierKeywordsContext ctx) { 
		parser.addChildNode(new AllowedIdentifierKeywordsNode());
		parser.addToken(ALLOWEDIDENTIFIERKEYWORDS, ctx.getStart());
	}
	@Override public void exitAllowedIdentifierKeywords(TamarinParser.AllowedIdentifierKeywordsContext ctx) { 
		parser.finishChild();
	}
	public JplagTamarinListener(Parser parser) { this.parser = parser; }
	public void visitErrorNode(Error node) { }
}