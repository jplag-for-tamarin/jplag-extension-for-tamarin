/*
    Lexer Rules
    Note: Lexer always matches first rule if multiple match
*/
lexer grammar TamarinLexer;

KeywordLet : 'let' ;
KeywordIn : 'in' ;
KeywordRule : 'rule' ;
KeywordDiff : 'diff' ;

KeywordBy : 'by' ;
KeywordLemma : 'lemma' ;
KeywordIf : 'if' ;
KeywordElse : 'else' ;
KeywordProcess : 'process' ;
KeywordAs : 'as' ;
KeywordLookup : 'lookup' ;
KeywordLast : 'last' ;
KeywordPrio : 'prio' ;
KeywordDeprio : 'deprio' ;
KeywordPresort : 'presort' ;
KeywordSmallest : 'smallest' ;
KeywordId : 'id' ;
KeywordTactic : 'tactic' ;
KeywordRestriction : 'restriction' ;
KeywordAxiom : 'axiom' ;
KeywordAny : 'Any' ;
KeywordTest : 'test' ;
KeywordNoPrecomp : 'no_precomp' ;
KeywordThen : 'then' ;
KeywordOut : 'out' ;
KeywordFresh : 'fresh' ;
KeywordPub : 'pub' ;
KeywordXor : 'xor' ;
KeywordOutput : 'output' ;
KeywordAccountsFor : 'accounts for' | 'account for' ; // TODO : allow whitespaces
KeywordModulo : 'modulo' ;
KeywordTheory : 'theory' ;
KeywordBegin : 'begin' ;
KeywordEnd : 'end' ;
KeywordVariants : 'variants' ;
KeywordRestrict : '_restrict' ;
KeywordHeuristic : 'heuristic' ;
KeywordExport : 'export' ;
KeywordPredicate : 'predicate' | 'predicates' ;
KeywordOptions : 'options' ;
KeywordEquations : 'equations' ;
KeywordFunctions : 'functions' | 'function' ;
KeywordBuiltins : 'builtins' ;
KeywordCase : 'case' ;
KeywordQED : 'qed' ;
KeywordNext : 'next' ;
KeywordSplitEqs : 'splitEqs' ;
KeywordEquivLemma : 'equivLemma' ;
KeywordDiffEquivLemma : 'diffEquivLemma' ;

E : 'E';
AC : 'AC';

RuleFresh : 'Fresh' ;
RuleIrecv : 'irecv' ;
RuleIsend : 'isend' ;
RuleCoerce : 'coerce' ;
RuleIequality : 'iequality' ;


SuffixMsg : 'msg' ;
SuffixNode : 'node' ;


GoalRankingIdentifierNoOracle : 's' | 'S' | 'p' | 'P' | 'c' | 'C' | 'i' | 'I' ;
GoalRankingIdentifierOracle : 'o' | 'O' ;
GoalRankingIdentifiersNoOracle : GoalRankingIdentifierNoOracle+ ;

DHneutral : 'DH_neutral' ;


BuiltinMun : 'mun' ;
BuiltinOne : 'one' ;
BuiltinExp : 'exp' ;
BuiltinMult : 'mult' ;
BuiltinInv : 'inv' ;
BuiltinPmult : 'pmult' ;
BuiltinEm : 'em' ;
BuiltinZero : 'zero' ;


SolvedProof : 'SOLVED' ;
ProofMethodSorry : 'sorry' ;
ProofMethodSimplify : 'simplify' ;
ProofMethodSolve : 'solve' ;
ProofMethodContradiction : 'contradiction' ;
ProofMethodInduction : 'induction' ;


BuiltinOptions : 'translation-progress' | 'translation-allow-pattern-lookups' | 'translation-state-optimisation' | 'translation-asynchronous-channels' | 'translation-compress-events' ;


BuiltinNameLocationsReport : 'locations-report' ;
BuiltinNameReliableChannel : 'reliable-channel' ;
BuiltinNameDH : 'diffie-hellman' ;
BuiltinNameBilinearPairing : 'bilinear-pairing' ;
BuiltinNameMultiset : 'multiset' ;
BuiltinNameSymmetricEncryption : 'symmetric-encryption' ;
BuiltinNameAsymmetricEncryption : 'asymmetric-encryption' ;
BuiltinNameSigning : 'signing' ;
BuiltinNameDestPairing : 'dest-pairing' ;
BuiltinNameDestSymmetricEncryption : 'dest-symmetric-encryption' ;
BuiltinNameDestAsymmetricEncryption : 'dest-asymmetric-encryption' ;
BuiltinNameDestSigning : 'dest-signing' ;
BuiltinNameRevealingSigning : 'revealing-signing' ;
BuiltinNameHashing : 'hashing' ;




TacticFunctionName : 'regex' | 'isFactName' | 'isInFactTerms' | 'nonAbsurdGoal' | 'dhreNoise' | 'defaultNoise' | 'reasonableNoncesNoise' ;


SapicActionNew : 'new' ;
SapicActionInsert : 'insert' ;
SapicActionDelete : 'delete' ;
SapicActionLock : 'lock' ;
SapicActionUnlock : 'unlock' ;
SapicActionEvent : 'event' ;

Colon : ':' ;
Semicolon : ';' ;
Comma : ',' ;
Dot : '.' ;
Slash : '/' ;
Backslash : '\\' ;
SingleQuote : '\'' ;
DoubleQuote : '"' ;
LeftParenthesis : '(' ;
RightParenthesis : ')' ;
LeftBrace : '{' ;
RightBrace : '}' ;
LeftBracket : '[' ;
RightBracket : ']' ;
Underscore : '_' ;

OrOp : '∨' ;
PipeOp : '|' ;
DoublePipeOp : '||' ;
AndOp : '&' | '∧' ;
NotOp : '¬' | 'not' ;
PlusOp : '+' ;
MinusOp : '-' ;
XorOp : 'XOR' | '⊕' ;
MultOp : '*' ;
ExpOp : '^' ;
EqOp : '=' ;
AtOp : '@' ;
OneOp : '1' ;
NullOp : '0' ;
RequiresOp : '▶' ;
SplitOp : '∥' ;
BangOp : '!' ;
HashtagOp : '#' ;
LessOp : '<' ;
LessTermOp : '(<)' ;
GreaterOp : '>' ;
ForallOp : 'All' | '∀' ;
ExistsOp : 'Ex' | '∃' ;
ImpliesOp : '==>' | '⇒' ;
EquivOp : '<=>' ;
EquivSignOp : '⇔' ;
TildeOp : '~' ;
DollarOp : '$' ;
ChainOp : '~~>';
ActionArrowStart : '--[' ;
ActionArrowTip : ']->' ;
LongRightArrowOp : '-->' ;
TOp : 'T' ;
FOp : 'F' ;
TopOp : '⊤' ;
BotOp : '⊥' ;


Left : 'left' ;
Right : 'right' ;
Both : 'both' ;

Define : '#define' ;
Include : '#include' ;
IfDef : '#ifdef' ;
Else : '#else' ;
EndIf : '#endif' ;


ForallTraces : 'all-traces' ;
ExistsTrace : 'exists-trace' ;


OutFactIdent : 'OUT' | 'Out' ;
InFactIdent : 'IN' | 'In' ;
KuFactIdent : 'KU' | 'Ku' ;
KdFactIdent : 'KD' | 'Kd' ;
DedLogFactIdent : 'DED' | 'Ded' ;
FreshFactIdent : 'FR' | 'Fr' ;

RuleAttributeColor : 'colour' | 'color' ;

LemmaAttributeTyping : 'typing' ;
LemmaAttributeSources : 'sources' ;
LemmaAttributeReuse : 'reuse' ;
LemmaAttributeDiffReuse : 'diff_reuse' ;
LemmaAttributeInduction : 'use_induction' ;
LemmaAttributeHideLemma : 'hide_lemma' ;

FunctionAttribute : 'private' | 'destructor' ;

fragment AlphaUpper : [A-Z] ;
fragment AlphaLower : [a-z];

fragment Digit : [0-9] ;
fragment Alpha : AlphaUpper | AlphaLower ;
fragment AlphaNum : AlphaUpper | AlphaLower | Digit ;

Natural : Digit+ ;
Letters : Alpha+ ;
AlphaNums : AlphaNum+ ;

IDLower : AlphaLower (AlphaNum | Underscore)+ ;
IDUpper : AlphaUpper (AlphaNum | Underscore)+ ;
IDNum : Digit (AlphaNum | Underscore)+ ;


NaturalSubscript : ('₀'|'₁'|'₂'|'₃'|'₄'|'₅'|'₆'|'₇'|'₈'|'₉')+ ;


// skip any whitespace
WS : [ \t\n\r]+ -> skip ;

// skip any comments, matching lazily
Comment : '/*' (Comment|.)*? '*/' -> skip ; // TODO: does not support nested comments

// skip any line comments
LineComment : '//' ~[\r\n]* -> skip ;

FormalComment : '{*' ~[\\*]+? '*}' ;

BodyChar : ~["\n\r{}\\] | '\\"' ;