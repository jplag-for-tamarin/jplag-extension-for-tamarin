parser grammar TamarinParser;
options {tokenVocab = TamarinLexer;}
/*
    Parser Rules
*/

/*
    Theory
*/
tamarin_file : theory ;

theory : KeywordTheory identifier KeywordBegin body* KeywordEnd ; //DIFF
body : heuristic // DIFF
     | tactic // DIFF
     | builtins
     | signatureOptions
     | signatureFunctions
     | equations
     | restriction
     | legacyAxiom
     | caseTest
     | lemmaAcc
     | lemma
     | protoRule
     | intruderRule
     | topLevelProcess
     | processDef
     | equivLemma
     | diffEquivLemma
     | predicateDeclaration
     | export
     | ifdef
     | define
     | include
     | formalComment
     ;

define : Define identifier ; //
include : Include filePath ; //  TODO: handle included files
ifdef : IfDef flagDisjuncts (body)* (Else (body)*)? EndIf ; //

flagDisjuncts : flagConjuncts (orOp flagConjuncts)* ;
flagConjuncts : flagNegation (andOp flagNegation)* ;
flagNegation : notOp? flagAtom ; //
flagAtom : identifier ;


/*
    Terms
*/
tupleTerm [String plit] : msetterm[$plit] (Comma msetterm[$plit])* ; // EQN, $plit
msetterm [String plit] : xorterm[$plit] (PlusOp xorterm[$plit])* ; // TODO: EQN, enableMSet
msettermList [String plit] : msetterm[$plit] (Comma msetterm[$plit])* (Comma)? ;
xorterm [String plit] : multterm[$plit] (XorOp multterm[$plit])* ; // TODO: EQN, enableXor
multterm [String plit] : expterm[$plit] (MultOp expterm[$plit])* ; // TODO: EQN, enableDH
expterm [String plit] : term[$plit] (ExpOp term[$plit])* ; // EQN, $plit

term [String plit] : LessOp tupleTerm[$plit] GreaterOp
     | LeftParenthesis msetterm[$plit] RightParenthesis
     | OneOp
     | dhNeutral
     | application[$plit] // EQN, plit
     | literal[$plit]
     ;

application [String plit] :
    | unaryOpApplication[$plit]
    | nonUnaryOpApplication[$plit]
    | binaryAlgApplication[$plit]
    | nullaryApplication
    ;
// DIFF diffOp ; // EQN, plit

unaryOpApplication [String plit] : identifier LeftParenthesis tupleTerm[$plit] RightParenthesis ; // EQN, $plit
nonUnaryOpApplication [String plit] : identifier LeftParenthesis (msettermList[$plit])? RightParenthesis ; // EQN, $plit
// TODO : number of terms parsed has to be equal to the arity of the operator
// TODO : the identifier cannot be element of the reserved builtins
binaryAlgApplication [String plit] : identifier LeftBrace tupleTerm[$plit] RightBrace term[$plit] ; // EQN, plit
// TODO : the identifier cannot be element of the reserved builtins
// TODO : the arity of the operator has to be two

nullaryApplication : builtinOne
                   | dhNeutral
                   | builtinZero
                   ;
reservedBuiltins : builtinMun
                 | builtinOne
                 | builtinExp
                 | builtinMult
                 | builtinInv
                 | builtinPmult
                 | builtinEm
                 | builtinZero
                 | builtinXor
                 ;

/*
    Rule
*/
// TODO : parametrize
genericRule [String varp, String nodep] : factList["vlit" + $varp]
    (LongRightArrowOp | ActionArrowStart (factOrRestrictionList[$varp, $nodep])? ActionArrowTip)
    factList["vlit" + $varp] ;
// TODO : make an empty list be equal to -->

intruderRule : KeywordRule moduloAC intruderInfo Colon genericRule["msgVar", "nodeVar"] ; //
intruderInfo : identifier (natural)? ; //  TODO : identifier has to start with c or d

protoRuleAC : protoRuleACInfo (letBlock)? genericRule["msgVar", "nodeVar"] ; //
protoRuleACInfo : KeywordRule moduloAC identifier ruleAttributes? Colon ; //  TODO : identifier cannot be in reserved names

protoRule : protoRuleInfo (letBlock)? genericRule["msgVar", "nodeVar"] (KeywordVariants protoRuleAC (Comma protoRuleAC)* (Comma)? )? ;
protoRuleInfo : KeywordRule moduloE identifier ruleAttributes? Colon ; // TODO : identifier cannot be in reserved names
// TODO : DIFF rule

ruleAttributes : LeftBracket (ruleAttribute (Comma ruleAttribute)* (Comma)? )? RightBracket ;
// more lenient since it allows whitespaces
ruleAttribute : RuleAttributeColor EqOp hexColor
              | KeywordProcess EqOp DoubleQuote .*? DoubleQuote
              ;


embeddedRestriction [String varp, String nodep] : KeywordRestrict LeftParenthesis standardFormula[$varp, $nodep] RightParenthesis ; // varp, nodep

factOrRestriction [String varp, String nodep] : fact["vlit" + $varp] | embeddedRestriction[$varp, $nodep] ;
factOrRestrictionList [String varp, String nodep] : factOrRestriction[$varp, $nodep] (Comma factOrRestriction[$varp, $nodep])* (Comma)? ;

reservedRuleNames : ruleFresh | RuleIrecv | RuleIsend | RuleCoerce | rulePub | RuleIequality ;
ruleFresh : RuleFresh | KeywordFresh ;
rulePub : KeywordPub ;

hexColor : SingleQuote (HashtagOp)? (natural | AlphaNums | Letters) SingleQuote | (HashtagOp)? (natural | AlphaNums | Letters) ; // TODO : imprecise due to lexer rules

moduloE : (LeftParenthesis KeywordModulo E RightParenthesis)? ;
moduloAC : LeftParenthesis KeywordModulo AC RightParenthesis ;


/*
    Facts
*/

factList [String plit] : LeftBracket (fact[$plit] (Comma fact[$plit])* (Comma)? )? RightBracket ; // plit

fact [String plit] : freshFact[$plit] | reservedFact[$plit] | protoFact[$plit] ; // plit

freshFact [String plit] : FreshFactIdent LeftParenthesis msetterm[$plit] RightParenthesis factAnnotations ;
reservedFact [String plit] : multiplicity? reservedFactIdentifier LeftParenthesis msetterm[$plit] RightParenthesis factAnnotations ;
protoFact [String plit] : multiplicity? protoFactIdentifier LeftParenthesis (msettermList[$plit])? RightParenthesis factAnnotations ;

multiplicity : BangOp ;

factAnnotations : (LeftBracket (factAnnotation (Comma factAnnotation)* (Comma)? )? RightBracket)? ;
factAnnotation : PlusOp
               | MinusOp
               | KeywordNoPrecomp
               ;


/*
    Lemma
*/
lemmaAttributes : LeftBracket (lemmaAttribute (Comma lemmaAttribute)* (Comma)? )? RightBracket ; // DIFF
lemmaAttribute : LemmaAttributeTyping
               | LemmaAttributeSources
               | LemmaAttributeReuse
               | LemmaAttributeDiffReuse
               | LemmaAttributeInduction
               | LemmaAttributeHideLemma EqOp identifier
               | KeywordHeuristic EqOp goalRanking // DIFF
               | KeywordOutput EqOp LeftBracket (identifier (Comma identifier)* (Comma)? )? RightBracket
               | Left
               | Right
               ;

traceQuantifier : forallTraces | existsTrace ;
forallTraces : ForallTraces ;
existsTrace : ExistsTrace ;

// TODO : do proto lemma, plain lemma and diff lemma
lemma : KeywordLemma moduloE identifier
    lemmaAttributes? Colon traceQuantifier? DoubleQuote standardFormula["msgVar", "nodeVar"] DoubleQuote startProofSkeleton? ;


/*
    Formulas
*/

standardFormula [String varp, String nodep] : implication[$varp, $nodep] (equivOp implication[$varp, $nodep])? ; // varp nodep

implication [String varp, String nodep] : disjunction[$varp, $nodep] (ImpliesOp disjunction[$varp, $nodep])* ; // varp nodep

disjunction [String varp, String nodep] : conjunction[$varp, $nodep] (orOp conjunction[$varp, $nodep])* ; // varp nodep
conjunction [String varp, String nodep] : negation[$varp, $nodep] (andOp negation[$varp, $nodep])* ; // varp nodep
negation [String varp, String nodep] : notOp? fatom[$varp, $nodep] ; // varp nodep

fatom [String varp, String nodep] : (botOp | fOp)
      | (topOp | tOp)
      | blatom[$varp, $nodep]
      | quantification[$varp, $nodep]
      | LeftParenthesis standardFormula[$varp, $nodep] RightParenthesis
      ;

blatom [String varp, String nodep] :
         KeywordLast LeftParenthesis literal[$nodep] RightParenthesis #blatom_last
       | fact["vlit" + $varp] AtOp literal[$nodep] #blatom_action
       | fact["vlit" + $varp + $nodep] #blatom_predicate
       | literal[$nodep] LessOp literal[$nodep] #blatom_less
       | smallerp[$varp] #blatom_smallerp // msgVar
       | literal["vlit" + $varp + $nodep] eqOp literal["vlit" + $varp + $nodep] #blatom_node_eq // temporary fix, superset of tamarin's language since a node lit could be equalld with a var lit
       // TODO : prediction can't view past literals so chooses the wrong rule sometimes
       | msetterm["vlit" + $varp] eqOp msetterm["vlit" + $varp] #blatom_term_eq // EQN
       ;

quantification [String varp, String nodep] : (forallOp | existsOp) literals[$varp + $nodep] Dot standardFormula[$varp, $nodep];

smallerp [String varp] : msetterm["vlit" + $varp] LessTermOp msetterm["vlit" + $varp] ; // TODO : enableMSet, EQN


/*
    Accountability
*/
caseTest : KeywordTest identifier Colon DoubleQuote standardFormula["msgVar", "nodeVar"] DoubleQuote ;

// DIFF
lemmaAcc : KeywordLemma identifier lemmaAttributes? Colon identifier (Comma identifier)*
    (Comma)? KeywordAccountsFor DoubleQuote standardFormula["msgVar", "nodeVar"] DoubleQuote ;


/*
    Tactics
*/
tactic : tacticName (selectedPresort)? prio* deprio* ; // DIFF

deprio : KeywordDeprio Colon (LeftBrace rankingIdentifier RightBrace)? tacticDisjuncts+ ;
prio : KeywordPrio Colon (LeftBrace rankingIdentifier RightBrace)? tacticDisjuncts+ ;

tacticDisjuncts : tacticConjuncts (orOp tacticConjuncts)* ;
tacticConjuncts : tacticNegation (AndOp tacticNegation)* ;
tacticNegation : notOp? tacticFunction ;

tacticFunction : tacticFunctionName (DoubleQuote tacticFunctionIdentifier DoubleQuote)+ ;

selectedPresort : KeywordPresort Colon goalRankingPresort ; // DIFF
goalRankingPresort : GoalRankingIdentifierNoOracle ;

tacticFunctionName : TacticFunctionName ;
rankingIdentifier : KeywordSmallest
                  | KeywordId
                  ;
tacticName : KeywordTactic Colon identifier ;

/*
    Signatures
*/
heuristic : KeywordHeuristic Colon goalRanking+ ; // DIFF
goalRanking : oracleRanking // DIFF
            | internalTacticRanking // DIFF
            | regularRanking // DIFF
            ;
regularRanking : GoalRankingIdentifiersNoOracle | GoalRankingIdentifierNoOracle; // DIFF
internalTacticRanking : LeftBrace internalTacticName RightBrace ;
oracleRanking : GoalRankingIdentifierOracle (DoubleQuote oracleRelativePath DoubleQuote)? ;

export : KeywordExport identifier Colon DoubleQuote exportBodyChars DoubleQuote ;

predicateDeclaration : KeywordPredicate Colon predicate (Comma predicate)* (Comma)? ;
predicate : fact["logVar"] EquivOp standardFormula["msgVar", "nodeVar"] ;

signatureOptions : KeywordOptions Colon builtinOptions (Comma builtinOptions)* (Comma)? ;
builtinOptions : BuiltinOptions ;

equations : KeywordEquations Colon equation (Comma equation)* (Comma)? ;
equation : term["logicalLiteralNoPub"] eqOp term["logicalLiteralNoPub"] ; // TODO: enable equational mode for terms and logLiteralNoPub

signatureFunctions :
    KeywordFunctions Colon signatureFunction (Comma signatureFunction)* (Comma)? ;
signatureFunction : identifier functionType (LeftBracket
    (functionAttribute (Comma functionAttribute)* (Comma)? )? RightBracket)? ;
// TODO: identifier cannot be reservedBuiltins
// TODO: identifier has to be a defined function
functionAttribute : FunctionAttribute ;
functionType :
               Slash natural #function_type_natural
             | LeftParenthesis (sapicType (Comma sapicType)* (Comma)? )? RightParenthesis Colon sapicType #function_type_sapic
             ;

builtins : KeywordBuiltins Colon builtinNames (Comma builtinNames)* (Comma)? ;
builtinNames : builtinNameLocationsReport
               | builtinNameReliableChannel
               | builtinNameDH
               | builtinNameBilinearPairing
               | builtinNameMultiset
               | builtinNameSymmetricEncryption
               | builtinNameAsymmetricEncryption
               | builtinNameSigning
               | builtinNameDestPairing
               | builtinNameDestSymmetricEncryption
               | builtinNameDestAsymmetricEncryption
               | builtinNameDestSigning
               | builtinNameRevealingSigning
               | builtinNameHashing
               | builtinNameXor;
builtinNameLocationsReport : BuiltinNameLocationsReport ;
builtinNameReliableChannel : BuiltinNameReliableChannel ;
builtinNameDH : BuiltinNameDH ;
builtinNameBilinearPairing : BuiltinNameBilinearPairing ;
builtinNameMultiset : BuiltinNameMultiset ;
builtinNameSymmetricEncryption : BuiltinNameSymmetricEncryption ;
builtinNameAsymmetricEncryption : BuiltinNameAsymmetricEncryption ;
builtinNameSigning : BuiltinNameSigning ;
builtinNameDestPairing : BuiltinNameDestPairing ;
builtinNameDestSymmetricEncryption : BuiltinNameDestSymmetricEncryption ;
builtinNameDestAsymmetricEncryption : BuiltinNameDestAsymmetricEncryption ;
builtinNameDestSigning : BuiltinNameDestSigning ;
builtinNameRevealingSigning : BuiltinNameRevealingSigning ;
builtinNameHashing : BuiltinNameHashing ;
builtinNameXor : KeywordXor ;


/*
    Proof
*/
// TODO : diffProofSkeleton
proofSkeleton : solvedProof
              | finalProof
              | interProof
              ;
startProofSkeleton : finalProof| interProof ;
solvedProof : SolvedProof ;
finalProof : KeywordBy proofMethod ;
interProof : proofMethod (proofCase (KeywordNext proofCase)* KeywordQED | proofSkeleton) ;
proofCase : KeywordCase identifier proofSkeleton ;

proofMethod : ProofMethodSorry
            | ProofMethodSimplify
            | ProofMethodSolve LeftParenthesis goal RightParenthesis
            | ProofMethodContradiction
            | ProofMethodInduction
            ;

goal : actionGoal
     | premiseGoal
     | chainGoal
     | disjSplitGoal
     | eqSplitGoal
     ;
actionGoal : fact["logicalLiteral"] AtOp nodeVar ;
premiseGoal : fact["logicalLiteral"] RequiresOp NaturalSubscript nodeVar ;
chainGoal : nodeConclusion ChainOp nodePremise ;
disjSplitGoal : standardFormula["msgVar", "nodeVar"] (SplitOp standardFormula["msgVar", "nodeVar"])* ; // actually guarded formula
eqSplitGoal : KeywordSplitEqs LeftParenthesis natural RightParenthesis ;

nodePremise : LeftParenthesis nodeVar Comma natural RightParenthesis ;
nodeConclusion : LeftParenthesis nodeVar Comma natural RightParenthesis ;


/*
    Sapic
*/
diffEquivLemma : KeywordDiffEquivLemma Colon process ; // TODO : enable DiffmaudeSig
equivLemma : KeywordEquivLemma Colon process process ; // TODO : process(thy)

process : actionProcess ((PlusOp | DoublePipeOp | PipeOp) actionProcess)* ; // TODO : actionProcess(thy)
elseProcess : KeywordElse process ; // TODO : process(thy)
topLevelProcess : KeywordProcess Colon process ; // TODO : process(thy)
actionProcess : BangOp process
              | KeywordLookup sapicTerm KeywordAs sapicVar KeywordIn process (elseProcess)?
              | KeywordIf (sapicTerm EqOp sapicTerm | standardFormula["sapicVar", "sapicNodeVar"]) KeywordThen process (elseProcess)?
              | KeywordLet (sapicPatternTerm EqOp sapicTerm)+ KeywordIn process (elseProcess)? // TODO : genericLetBlock
              | NullOp
              | sapicAction (Semicolon actionProcess)?
              | LeftParenthesis process RightParenthesis (AtOp sapicTerm)?
              | identifier (LeftParenthesis (msetterm["logicalTypedLiteral"] (Comma msetterm["logicalTypedLiteral"])* (Comma)? )? RightParenthesis )?
              ;

sapicAction : SapicActionNew sapicVar
            | SapicActionInsert msetterm["logicalTypedLiteral"] Comma msetterm["logicalTypedLiteral"]
            | sapicActionIn LeftParenthesis (msetterm["logicalTypedPatternLiteral"] RightParenthesis | msetterm["logicalTypedLiteral"] Comma msetterm["logicalTypedPatternLiteral"] RightParenthesis)
            | sapicActionOut LeftParenthesis (msetterm["logicalTypedPatternLiteral"] RightParenthesis | msetterm["logicalTypedLiteral"] Comma msetterm["logicalTypedPatternLiteral"] RightParenthesis)
            | SapicActionDelete msetterm["logicalTypedLiteral"]
            | SapicActionLock msetterm["logicalTypedLiteral"]
            | SapicActionUnlock msetterm["logicalTypedLiteral"]
            | SapicActionEvent fact["logicalTypedLiteral"]
            | genericRule["sapicPatternVar", "sapicNodeVar"]
            ;

sapicActionIn : KeywordIn;
sapicActionOut : KeywordOut;

processDef : KeywordLet identifier (LeftParenthesis (sapicVar (Comma sapicVar)* (Comma)? )? RightParenthesis )? EqOp process ; // TODO : process(thy)

sapicPatternTerm : msetterm["logicalTypedPatternLiteral"] ;
sapicTerm : msetterm["logicalTypedLiteral"] ;

logicalTypedPatternLiteral : freshName | pubName | sapicPatternVar ;
logicalTypedLiteral : freshName | pubName | sapicVar ;

sapicNodeVar : nodeVar ;
sapicPatternVar : (eqOp)? sapicVar ;
sapicVar : logVar (Colon sapicType)? ;
sapicType : KeywordAny | identifier ;

/*
    Restriction
*/
restriction : KeywordRestriction identifier Colon DoubleQuote standardFormula["msgVar", "nodeVar"] DoubleQuote ;

restrictionAttributes : LeftBracket (restrictionAttribute (Comma restrictionAttribute)* (Comma)? )? RightBracket ;
restrictionAttribute : Left | Right | Both ;

legacyAxiom : KeywordAxiom identifier Colon DoubleQuote standardFormula["msgVar", "nodeVar"] DoubleQuote ;

// TODO : DIFF restriction and legacy diff axiom

/*
    Let
*/
letBlock : KeywordLet letAssignment+ KeywordIn ; // EQN
letAssignment : logMsgVar eqOp msetterm["logicalLiteral"] ;

/*
    Identifiers
*/

// Until there is a difference from pure identifiers this rule should be transparent
protoFactIdentifier : identifier; // TODO : only accept uppercase
reservedFactIdentifier :
                         OutFactIdent #reserved_fact_out
                       | InFactIdent #reserved_fact_in
                       | KuFactIdent #reserved_fact_ku
                       | KdFactIdent #reserved_fact_kd
                       | DedLogFactIdent #reserved_fact_ded
                       ;

// Identifiers should be terminal and not output any tokens from subrules
identifier : allowedIdentifierKeywords
           | natural
           | Letters
           | AlphaNums
           | IDLower
           | IDUpper
           | IDNum
           ;

natural : NullOp | OneOp | Natural ;

// Indexed identifiers should be terminal and not output any tokens from subrules
indexedIdentifier : identifier (Dot natural)? ;
filePath : DoubleQuote charDir DoubleQuote ;
keywords : KeywordLet | KeywordIn | KeywordLet | KeywordDiff ;

// Logical Variables of different Sorts
// Should be terminal and not output any tokens from subrules
logMsgVar : indexedIdentifier (Colon SuffixMsg)? ;
logFreshVar : indexedIdentifier Colon KeywordFresh | TildeOp indexedIdentifier ;
logPubVar : indexedIdentifier Colon KeywordPub | DollarOp indexedIdentifier ;
logNodeVar : indexedIdentifier Colon SuffixNode | HashtagOp indexedIdentifier ;
// Rules combining logical variables, should be transparent
// refers to all possible logical variable sorts
logVar : logMsgVar | logFreshVar | logPubVar | logNodeVar ;
// refers to a non-node logical variable, keeping this naming for consistency with the tamarin parser
msgVar : logMsgVar | logFreshVar | logPubVar ;

// These should also be transparent
logicalLiteral : freshName | pubName | msgVar ;
logicalLiteralWithNode : freshName | pubName | logVar ;
logicalLiteralNoPub : freshName | msgVar ;

literals [String lit] : (literal[$lit])+ ;
literal [String lit] :
     {$lit.contains("vlit")}? (freshName | pubName)
    | {$lit.contains("logVar")}? logVar
    | {$lit.contains("msgVar")}? msgVar
    | {$lit.contains("nodeVar")}? nodeVar
    | {$lit.contains("logicalLiteral") && !$lit.contains("logicalLiteralNoPub")}? logicalLiteral
    | {$lit.contains("logicalLiteralNoPub")}? logicalLiteralNoPub
    | {$lit.contains("sapicVar")}? sapicVar
    | {$lit.contains("sapicNodeVar")}? sapicNodeVar
    | {$lit.contains("logicalTypedLiteral")}? logicalTypedLiteral
    | {$lit.contains("sapicPatternVar")}? sapicPatternVar
    | {$lit.contains("logicalTypedPatternLiteral")}? logicalTypedPatternLiteral
    ;

// similar to logNodeVar but allows identifiers without suffix or prefix
nodeVar : indexedIdentifier Colon SuffixNode | HashtagOp? indexedIdentifier ;

freshName : TildeOp SingleQuote identifier SingleQuote ;
pubName : SingleQuote identifier SingleQuote ;

formalComment : Letters FormalComment ;

tacticFunctionIdentifier : (
      identifier
    | keywords
    | punctuation
    | Backslash
    | LeftBrace | RightBrace
    | operators
    | BodyChar
)* ;

internalTacticName : (
      identifier
    | keywords
    | punctuation
    | Backslash
    | operators
    | BodyChar
)+ ;

oracleRelativePath : (
      identifier
    | keywords
    | punctuation
    | Backslash
    | LeftBrace | RightBrace
    | operators
    | BodyChar
)+ ;

exportBodyChars : (
      identifier
    | keywords
    | punctuation
    | LeftBrace | RightBrace
    | operators
    | BodyChar
)* ;

charDir : (
    identifier // TODO should not allow all identifers (spaces and special characters)
    | Letters
    | Natural
    | AlphaNums
    | Backslash
    | Slash
    | Dot
)* ;

/*
    Token Rules to prevent Lexer Overlap
*/
equivOp : EquivOp | EquivSignOp ;
orOp : OrOp | PipeOp ;
notOp : NotOp ;
andOp : AndOp ;
eqOp : EqOp ;
botOp : BotOp ;
fOp : FOp ;
tOp : TOp ;
topOp : TopOp ;
forallOp : ForallOp ;
existsOp : ExistsOp ;
builtinXor : KeywordXor ;
builtinMun : BuiltinMun;
builtinOne : BuiltinOne ;
builtinExp : BuiltinExp ;
builtinMult : BuiltinMult ;
builtinInv : BuiltinInv ;
builtinPmult : BuiltinPmult ;
builtinEm : BuiltinEm ;
builtinZero : BuiltinZero ;
dhNeutral : DHneutral ;

operators : OrOp
          | PipeOp
          | DoublePipeOp
          | AndOp
          | NotOp
          | PlusOp
          | MinusOp
          | XorOp
          | MultOp
          | ExpOp
          | EqOp
          | AtOp
          | OneOp
          | NullOp
          | RequiresOp
          | SplitOp
          | BangOp
          | HashtagOp
          | LessOp
          | LessTermOp
          | GreaterOp
          | ForallOp
          | ExistsOp
          | ImpliesOp
          | EquivOp
          | EquivSignOp
          | TildeOp
          | DollarOp
          | ChainOp
          | ActionArrowStart
          | ActionArrowTip
          | LongRightArrowOp
          | TOp
          | FOp
          | TopOp
          | BotOp
          ;


punctuation : Colon
             | Semicolon
             | Comma
             | Dot
             | Slash
             | Backslash
             | SingleQuote
             // | DoubleQuote
             | LeftParenthesis
             | RightParenthesis
             | LeftBrace
             | RightBrace
             | LeftBracket
             | RightBracket
             | Underscore
             ;

allowedIdentifierKeywords : E
                          | AC
                          | GoalRankingIdentifierNoOracle
                          | GoalRankingIdentifiersNoOracle
                          | GoalRankingIdentifierOracle
                          | KeywordBy
                          | KeywordDiff
                          | KeywordLemma
                          | KeywordIf
                          | KeywordElse
                          | KeywordProcess
                          | KeywordAs
                          | KeywordLookup
                          | KeywordLast
                          | KeywordPrio
                          | KeywordDeprio
                          | KeywordPresort
                          | KeywordSmallest
                          | KeywordId
                          | KeywordTactic
                          | KeywordRestriction
                          | KeywordAxiom
                          | KeywordAny
                          | KeywordTest
                          | KeywordNoPrecomp
                          | KeywordThen
                          | KeywordOut
                          | KeywordFresh
                          | KeywordPub
                          | KeywordXor
                          | KeywordOutput
                          | KeywordAccountsFor
                          | KeywordModulo
                          | KeywordTheory
                          | KeywordBegin
                          | KeywordEnd
                          | KeywordVariants
                          | KeywordRestrict
                          | KeywordHeuristic
                          | KeywordExport
                          | KeywordPredicate
                          | KeywordOptions
                          | KeywordEquations
                          | KeywordFunctions
                          | KeywordBuiltins
                          | KeywordCase
                          | KeywordQED
                          | KeywordNext
                          | KeywordSplitEqs
                          | KeywordEquivLemma
                          | KeywordDiffEquivLemma
                          | RuleFresh
                          | RuleIrecv
                          | RuleIsend
                          | RuleCoerce
                          | RuleIequality
                          | SuffixMsg
                          | SuffixNode
                          | DHneutral
                          | BuiltinMun
                          | BuiltinOne
                          | BuiltinExp
                          | BuiltinMult
                          | BuiltinInv
                          | BuiltinPmult
                          | BuiltinEm
                          | BuiltinZero
                          | SolvedProof
                          | ProofMethodSorry
                          | ProofMethodSimplify
                          | ProofMethodSolve
                          | ProofMethodContradiction
                          | ProofMethodInduction
                          | BuiltinOptions
                          | BuiltinNameLocationsReport
                          | BuiltinNameReliableChannel
                          | BuiltinNameDH
                          | BuiltinNameBilinearPairing
                          | BuiltinNameMultiset
                          | BuiltinNameSymmetricEncryption
                          | BuiltinNameAsymmetricEncryption
                          | BuiltinNameSigning
                          | BuiltinNameDestPairing
                          | BuiltinNameDestSymmetricEncryption
                          | BuiltinNameDestAsymmetricEncryption
                          | BuiltinNameDestSigning
                          | BuiltinNameRevealingSigning
                          | BuiltinNameHashing
                          | TacticFunctionName
                          | SapicActionNew
                          | SapicActionInsert
                          | SapicActionDelete
                          | SapicActionLock
                          | SapicActionUnlock
                          | SapicActionEvent
                          | TOp
                          | FOp
                          | Left
                          | Right
                          | Both
                          | ForallTraces
                          | ExistsTrace
                          | OutFactIdent
                          | InFactIdent
                          | KuFactIdent
                          | KdFactIdent
                          | DedLogFactIdent
                          | FreshFactIdent
                          | RuleAttributeColor
                          | LemmaAttributeTyping
                          | LemmaAttributeSources
                          | LemmaAttributeReuse
                          | LemmaAttributeDiffReuse
                          | LemmaAttributeInduction
                          | LemmaAttributeHideLemma
                          | FunctionAttribute
                          ;
