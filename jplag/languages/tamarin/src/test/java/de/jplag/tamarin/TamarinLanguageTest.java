package de.jplag.tamarin;

import de.jplag.ParsingException;
import de.jplag.Token;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class TamarinLanguageTest {
    private Language language;
    Path resourcePath = Path.of("src", "test", "resources");

    @BeforeEach
    void setup() {
        language = new Language();
    }

    @Test
    void parseTreeTest() throws ParsingException {
        File testFile = new File(resourcePath.toFile(), "Tutorial.spthy");
        List<Token> tokens = language.parse(Set.of(testFile));
        tokens.forEach(t -> System.out.println(t.toString() + ':' + t.getLine()));
    }

    @Disabled("Full test disabled")
    @ParameterizedTest
    @MethodSource("filepathSource")
    void parseTestFiles(Path path) throws Exception {
        try {
            assert(language.tryParse(path));
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

    private static Stream<Path> filepathSource() throws IOException {
        Stream<Path> paths = Files.walk(Path.of("src", "test", "resources"));
        return paths.filter(Files::isRegularFile).filter(f -> f.toString().endsWith("spthy"));
    }
}
