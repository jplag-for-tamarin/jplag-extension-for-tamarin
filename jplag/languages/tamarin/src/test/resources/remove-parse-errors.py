import sys 
import subprocess
import os

for root, dirs, files in os.walk('.'):
    for filename in files:
        if '.spthy' in filename:
            path = os.path.join(root, filename)
            completed = subprocess.run("tamarin-prover --parse-only " + path, shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            if completed.returncode != 0:
                print("Deleting file: " + path)
                os.remove(path)
