// Generated from /home/jastau/ownCloud - jstauffer@192.168.0.69/ETH/Bachelor Thesis/code-JPlag-Extension-Tamarin/languages/tamarin/src/main/antlr4/de/jplag/tamarin/grammar/TamarinParser.g4 by ANTLR 4.12.0
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link TamarinParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface TamarinParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link TamarinParser#tamarin_file}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTamarin_file(TamarinParser.Tamarin_fileContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#theory}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTheory(TamarinParser.TheoryContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody(TamarinParser.BodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#define}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefine(TamarinParser.DefineContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#include}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInclude(TamarinParser.IncludeContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#ifdef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfdef(TamarinParser.IfdefContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#flagDisjuncts}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFlagDisjuncts(TamarinParser.FlagDisjunctsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#flagConjuncts}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFlagConjuncts(TamarinParser.FlagConjunctsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#flagNegation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFlagNegation(TamarinParser.FlagNegationContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#flagAtom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFlagAtom(TamarinParser.FlagAtomContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#tupleTerm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTupleTerm(TamarinParser.TupleTermContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#msetterm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMsetterm(TamarinParser.MsettermContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#msettermList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMsettermList(TamarinParser.MsettermListContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#xorterm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitXorterm(TamarinParser.XortermContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#multterm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultterm(TamarinParser.MulttermContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#expterm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpterm(TamarinParser.ExptermContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTerm(TamarinParser.TermContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#application}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitApplication(TamarinParser.ApplicationContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#unaryOpApplication}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryOpApplication(TamarinParser.UnaryOpApplicationContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#nonUnaryOpApplication}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNonUnaryOpApplication(TamarinParser.NonUnaryOpApplicationContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#binaryAlgApplication}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryAlgApplication(TamarinParser.BinaryAlgApplicationContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#nullaryApplication}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullaryApplication(TamarinParser.NullaryApplicationContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#reservedBuiltins}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReservedBuiltins(TamarinParser.ReservedBuiltinsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#genericRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGenericRule(TamarinParser.GenericRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#intruderRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntruderRule(TamarinParser.IntruderRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#intruderInfo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntruderInfo(TamarinParser.IntruderInfoContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#protoRuleAC}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProtoRuleAC(TamarinParser.ProtoRuleACContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#protoRuleACInfo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProtoRuleACInfo(TamarinParser.ProtoRuleACInfoContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#protoRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProtoRule(TamarinParser.ProtoRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#protoRuleInfo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProtoRuleInfo(TamarinParser.ProtoRuleInfoContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#ruleAttributes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRuleAttributes(TamarinParser.RuleAttributesContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#ruleAttribute}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRuleAttribute(TamarinParser.RuleAttributeContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#embeddedRestriction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmbeddedRestriction(TamarinParser.EmbeddedRestrictionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#factOrRestriction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactOrRestriction(TamarinParser.FactOrRestrictionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#factOrRestrictionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactOrRestrictionList(TamarinParser.FactOrRestrictionListContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#reservedRuleNames}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReservedRuleNames(TamarinParser.ReservedRuleNamesContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#ruleFresh}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRuleFresh(TamarinParser.RuleFreshContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#rulePub}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRulePub(TamarinParser.RulePubContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#hexColor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHexColor(TamarinParser.HexColorContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#moduloE}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModuloE(TamarinParser.ModuloEContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#moduloAC}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModuloAC(TamarinParser.ModuloACContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#factList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactList(TamarinParser.FactListContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#fact}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFact(TamarinParser.FactContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#freshFact}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFreshFact(TamarinParser.FreshFactContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#reservedFact}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReservedFact(TamarinParser.ReservedFactContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#protoFact}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProtoFact(TamarinParser.ProtoFactContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#multiplicity}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicity(TamarinParser.MultiplicityContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#factAnnotations}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactAnnotations(TamarinParser.FactAnnotationsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#factAnnotation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactAnnotation(TamarinParser.FactAnnotationContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#lemmaAttributes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLemmaAttributes(TamarinParser.LemmaAttributesContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#lemmaAttribute}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLemmaAttribute(TamarinParser.LemmaAttributeContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#traceQuantifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTraceQuantifier(TamarinParser.TraceQuantifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#forallTraces}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForallTraces(TamarinParser.ForallTracesContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#existsTrace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExistsTrace(TamarinParser.ExistsTraceContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#lemma}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLemma(TamarinParser.LemmaContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#standardFormula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStandardFormula(TamarinParser.StandardFormulaContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#implication}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImplication(TamarinParser.ImplicationContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#disjunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDisjunction(TamarinParser.DisjunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#conjunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConjunction(TamarinParser.ConjunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#negation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegation(TamarinParser.NegationContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#fatom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFatom(TamarinParser.FatomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code blatom_last}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlatom_last(TamarinParser.Blatom_lastContext ctx);
	/**
	 * Visit a parse tree produced by the {@code blatom_action}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlatom_action(TamarinParser.Blatom_actionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code blatom_predicate}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlatom_predicate(TamarinParser.Blatom_predicateContext ctx);
	/**
	 * Visit a parse tree produced by the {@code blatom_less}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlatom_less(TamarinParser.Blatom_lessContext ctx);
	/**
	 * Visit a parse tree produced by the {@code blatom_smallerp}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlatom_smallerp(TamarinParser.Blatom_smallerpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code blatom_node_eq}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlatom_node_eq(TamarinParser.Blatom_node_eqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code blatom_term_eq}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlatom_term_eq(TamarinParser.Blatom_term_eqContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#quantification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQuantification(TamarinParser.QuantificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#smallerp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSmallerp(TamarinParser.SmallerpContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#caseTest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseTest(TamarinParser.CaseTestContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#lemmaAcc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLemmaAcc(TamarinParser.LemmaAccContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#tactic}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTactic(TamarinParser.TacticContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#deprio}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeprio(TamarinParser.DeprioContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#prio}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrio(TamarinParser.PrioContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#tacticDisjuncts}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTacticDisjuncts(TamarinParser.TacticDisjunctsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#tacticConjuncts}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTacticConjuncts(TamarinParser.TacticConjunctsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#tacticNegation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTacticNegation(TamarinParser.TacticNegationContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#tacticFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTacticFunction(TamarinParser.TacticFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#selectedPresort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectedPresort(TamarinParser.SelectedPresortContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#goalRankingPresort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGoalRankingPresort(TamarinParser.GoalRankingPresortContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#tacticFunctionName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTacticFunctionName(TamarinParser.TacticFunctionNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#rankingIdentifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRankingIdentifier(TamarinParser.RankingIdentifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#tacticName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTacticName(TamarinParser.TacticNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#heuristic}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHeuristic(TamarinParser.HeuristicContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#goalRanking}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGoalRanking(TamarinParser.GoalRankingContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#regularRanking}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRegularRanking(TamarinParser.RegularRankingContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#internalTacticRanking}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInternalTacticRanking(TamarinParser.InternalTacticRankingContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#oracleRanking}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOracleRanking(TamarinParser.OracleRankingContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#export}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExport(TamarinParser.ExportContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#predicateDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicateDeclaration(TamarinParser.PredicateDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#predicate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicate(TamarinParser.PredicateContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#signatureOptions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSignatureOptions(TamarinParser.SignatureOptionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinOptions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinOptions(TamarinParser.BuiltinOptionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#equations}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEquations(TamarinParser.EquationsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#equation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEquation(TamarinParser.EquationContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#signatureFunctions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSignatureFunctions(TamarinParser.SignatureFunctionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#signatureFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSignatureFunction(TamarinParser.SignatureFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#functionAttribute}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionAttribute(TamarinParser.FunctionAttributeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code function_type_natural}
	 * labeled alternative in {@link TamarinParser#functionType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_type_natural(TamarinParser.Function_type_naturalContext ctx);
	/**
	 * Visit a parse tree produced by the {@code function_type_sapic}
	 * labeled alternative in {@link TamarinParser#functionType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_type_sapic(TamarinParser.Function_type_sapicContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtins}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltins(TamarinParser.BuiltinsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNames}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNames(TamarinParser.BuiltinNamesContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameLocationsReport}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameLocationsReport(TamarinParser.BuiltinNameLocationsReportContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameReliableChannel}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameReliableChannel(TamarinParser.BuiltinNameReliableChannelContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameDH}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameDH(TamarinParser.BuiltinNameDHContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameBilinearPairing}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameBilinearPairing(TamarinParser.BuiltinNameBilinearPairingContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameMultiset}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameMultiset(TamarinParser.BuiltinNameMultisetContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameSymmetricEncryption}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameSymmetricEncryption(TamarinParser.BuiltinNameSymmetricEncryptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameAsymmetricEncryption}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameAsymmetricEncryption(TamarinParser.BuiltinNameAsymmetricEncryptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameSigning}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameSigning(TamarinParser.BuiltinNameSigningContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameDestPairing}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameDestPairing(TamarinParser.BuiltinNameDestPairingContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameDestSymmetricEncryption}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameDestSymmetricEncryption(TamarinParser.BuiltinNameDestSymmetricEncryptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameDestAsymmetricEncryption}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameDestAsymmetricEncryption(TamarinParser.BuiltinNameDestAsymmetricEncryptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameDestSigning}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameDestSigning(TamarinParser.BuiltinNameDestSigningContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameRevealingSigning}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameRevealingSigning(TamarinParser.BuiltinNameRevealingSigningContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameHashing}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameHashing(TamarinParser.BuiltinNameHashingContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinNameXor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinNameXor(TamarinParser.BuiltinNameXorContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#proofSkeleton}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProofSkeleton(TamarinParser.ProofSkeletonContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#startProofSkeleton}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStartProofSkeleton(TamarinParser.StartProofSkeletonContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#solvedProof}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSolvedProof(TamarinParser.SolvedProofContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#finalProof}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFinalProof(TamarinParser.FinalProofContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#interProof}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterProof(TamarinParser.InterProofContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#proofCase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProofCase(TamarinParser.ProofCaseContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#proofMethod}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProofMethod(TamarinParser.ProofMethodContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#goal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGoal(TamarinParser.GoalContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#actionGoal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitActionGoal(TamarinParser.ActionGoalContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#premiseGoal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPremiseGoal(TamarinParser.PremiseGoalContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#chainGoal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChainGoal(TamarinParser.ChainGoalContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#disjSplitGoal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDisjSplitGoal(TamarinParser.DisjSplitGoalContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#eqSplitGoal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqSplitGoal(TamarinParser.EqSplitGoalContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#nodePremise}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNodePremise(TamarinParser.NodePremiseContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#nodeConclusion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNodeConclusion(TamarinParser.NodeConclusionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#diffEquivLemma}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiffEquivLemma(TamarinParser.DiffEquivLemmaContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#equivLemma}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEquivLemma(TamarinParser.EquivLemmaContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#process}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProcess(TamarinParser.ProcessContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#elseProcess}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElseProcess(TamarinParser.ElseProcessContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#topLevelProcess}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTopLevelProcess(TamarinParser.TopLevelProcessContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#actionProcess}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitActionProcess(TamarinParser.ActionProcessContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#sapicAction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSapicAction(TamarinParser.SapicActionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#sapicActionIn}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSapicActionIn(TamarinParser.SapicActionInContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#sapicActionOut}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSapicActionOut(TamarinParser.SapicActionOutContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#processDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProcessDef(TamarinParser.ProcessDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#sapicPatternTerm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSapicPatternTerm(TamarinParser.SapicPatternTermContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#sapicTerm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSapicTerm(TamarinParser.SapicTermContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#logicalTypedPatternLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalTypedPatternLiteral(TamarinParser.LogicalTypedPatternLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#logicalTypedLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalTypedLiteral(TamarinParser.LogicalTypedLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#sapicNodeVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSapicNodeVar(TamarinParser.SapicNodeVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#sapicPatternVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSapicPatternVar(TamarinParser.SapicPatternVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#sapicVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSapicVar(TamarinParser.SapicVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#sapicType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSapicType(TamarinParser.SapicTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#restriction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRestriction(TamarinParser.RestrictionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#restrictionAttributes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRestrictionAttributes(TamarinParser.RestrictionAttributesContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#restrictionAttribute}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRestrictionAttribute(TamarinParser.RestrictionAttributeContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#legacyAxiom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLegacyAxiom(TamarinParser.LegacyAxiomContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#letBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLetBlock(TamarinParser.LetBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#letAssignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLetAssignment(TamarinParser.LetAssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#protoFactIdentifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProtoFactIdentifier(TamarinParser.ProtoFactIdentifierContext ctx);
	/**
	 * Visit a parse tree produced by the {@code reserved_fact_out}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReserved_fact_out(TamarinParser.Reserved_fact_outContext ctx);
	/**
	 * Visit a parse tree produced by the {@code reserved_fact_in}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReserved_fact_in(TamarinParser.Reserved_fact_inContext ctx);
	/**
	 * Visit a parse tree produced by the {@code reserved_fact_ku}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReserved_fact_ku(TamarinParser.Reserved_fact_kuContext ctx);
	/**
	 * Visit a parse tree produced by the {@code reserved_fact_kd}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReserved_fact_kd(TamarinParser.Reserved_fact_kdContext ctx);
	/**
	 * Visit a parse tree produced by the {@code reserved_fact_ded}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReserved_fact_ded(TamarinParser.Reserved_fact_dedContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#identifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifier(TamarinParser.IdentifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#natural}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNatural(TamarinParser.NaturalContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#indexedIdentifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexedIdentifier(TamarinParser.IndexedIdentifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#filePath}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFilePath(TamarinParser.FilePathContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#keywords}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeywords(TamarinParser.KeywordsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#logMsgVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogMsgVar(TamarinParser.LogMsgVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#logFreshVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogFreshVar(TamarinParser.LogFreshVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#logPubVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogPubVar(TamarinParser.LogPubVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#logNodeVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogNodeVar(TamarinParser.LogNodeVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#logVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogVar(TamarinParser.LogVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#msgVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMsgVar(TamarinParser.MsgVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#logicalLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalLiteral(TamarinParser.LogicalLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#logicalLiteralWithNode}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalLiteralWithNode(TamarinParser.LogicalLiteralWithNodeContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#logicalLiteralNoPub}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalLiteralNoPub(TamarinParser.LogicalLiteralNoPubContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#literals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiterals(TamarinParser.LiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(TamarinParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#nodeVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNodeVar(TamarinParser.NodeVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#freshName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFreshName(TamarinParser.FreshNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#pubName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPubName(TamarinParser.PubNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#formalComment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalComment(TamarinParser.FormalCommentContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#tacticFunctionIdentifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTacticFunctionIdentifier(TamarinParser.TacticFunctionIdentifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#internalTacticName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInternalTacticName(TamarinParser.InternalTacticNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#oracleRelativePath}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOracleRelativePath(TamarinParser.OracleRelativePathContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#exportBodyChars}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExportBodyChars(TamarinParser.ExportBodyCharsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#charDir}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharDir(TamarinParser.CharDirContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#equivOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEquivOp(TamarinParser.EquivOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#orOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrOp(TamarinParser.OrOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#notOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotOp(TamarinParser.NotOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#andOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndOp(TamarinParser.AndOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#eqOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqOp(TamarinParser.EqOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#botOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBotOp(TamarinParser.BotOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#fOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFOp(TamarinParser.FOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#tOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTOp(TamarinParser.TOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#topOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTopOp(TamarinParser.TopOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#forallOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForallOp(TamarinParser.ForallOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#existsOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExistsOp(TamarinParser.ExistsOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinXor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinXor(TamarinParser.BuiltinXorContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinMun}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinMun(TamarinParser.BuiltinMunContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinOne}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinOne(TamarinParser.BuiltinOneContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinExp(TamarinParser.BuiltinExpContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinMult}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinMult(TamarinParser.BuiltinMultContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinInv}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinInv(TamarinParser.BuiltinInvContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinPmult}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinPmult(TamarinParser.BuiltinPmultContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinEm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinEm(TamarinParser.BuiltinEmContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#builtinZero}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltinZero(TamarinParser.BuiltinZeroContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#dhNeutral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDhNeutral(TamarinParser.DhNeutralContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#operators}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperators(TamarinParser.OperatorsContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#punctuation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPunctuation(TamarinParser.PunctuationContext ctx);
	/**
	 * Visit a parse tree produced by {@link TamarinParser#allowedIdentifierKeywords}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAllowedIdentifierKeywords(TamarinParser.AllowedIdentifierKeywordsContext ctx);
}