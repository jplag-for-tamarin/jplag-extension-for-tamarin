// Generated from /home/jastau/ownCloud - jstauffer@192.168.0.69/ETH/Bachelor Thesis/code-JPlag-Extension-Tamarin/languages/tamarin/src/main/antlr4/de/jplag/tamarin/grammar/TamarinParser.g4 by ANTLR 4.12.0
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue"})
public class TamarinParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.12.0", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		KeywordLet=1, KeywordIn=2, KeywordRule=3, KeywordDiff=4, KeywordBy=5, 
		KeywordLemma=6, KeywordIf=7, KeywordElse=8, KeywordProcess=9, KeywordAs=10, 
		KeywordLookup=11, KeywordLast=12, KeywordPrio=13, KeywordDeprio=14, KeywordPresort=15, 
		KeywordSmallest=16, KeywordId=17, KeywordTactic=18, KeywordRestriction=19, 
		KeywordAxiom=20, KeywordAny=21, KeywordTest=22, KeywordNoPrecomp=23, KeywordThen=24, 
		KeywordOut=25, KeywordFresh=26, KeywordPub=27, KeywordXor=28, KeywordOutput=29, 
		KeywordAccountsFor=30, KeywordModulo=31, KeywordTheory=32, KeywordBegin=33, 
		KeywordEnd=34, KeywordVariants=35, KeywordRestrict=36, KeywordHeuristic=37, 
		KeywordExport=38, KeywordPredicate=39, KeywordOptions=40, KeywordEquations=41, 
		KeywordFunctions=42, KeywordBuiltins=43, KeywordCase=44, KeywordQED=45, 
		KeywordNext=46, KeywordSplitEqs=47, KeywordEquivLemma=48, KeywordDiffEquivLemma=49, 
		E=50, AC=51, RuleFresh=52, RuleIrecv=53, RuleIsend=54, RuleCoerce=55, 
		RuleIequality=56, SuffixMsg=57, SuffixNode=58, GoalRankingIdentifierNoOracle=59, 
		GoalRankingIdentifierOracle=60, GoalRankingIdentifiersNoOracle=61, DHneutral=62, 
		BuiltinMun=63, BuiltinOne=64, BuiltinExp=65, BuiltinMult=66, BuiltinInv=67, 
		BuiltinPmult=68, BuiltinEm=69, BuiltinZero=70, SolvedProof=71, ProofMethodSorry=72, 
		ProofMethodSimplify=73, ProofMethodSolve=74, ProofMethodContradiction=75, 
		ProofMethodInduction=76, BuiltinOptions=77, BuiltinNameLocationsReport=78, 
		BuiltinNameReliableChannel=79, BuiltinNameDH=80, BuiltinNameBilinearPairing=81, 
		BuiltinNameMultiset=82, BuiltinNameSymmetricEncryption=83, BuiltinNameAsymmetricEncryption=84, 
		BuiltinNameSigning=85, BuiltinNameDestPairing=86, BuiltinNameDestSymmetricEncryption=87, 
		BuiltinNameDestAsymmetricEncryption=88, BuiltinNameDestSigning=89, BuiltinNameRevealingSigning=90, 
		BuiltinNameHashing=91, TacticFunctionName=92, SapicActionNew=93, SapicActionInsert=94, 
		SapicActionDelete=95, SapicActionLock=96, SapicActionUnlock=97, SapicActionEvent=98, 
		Colon=99, Semicolon=100, Comma=101, Dot=102, Slash=103, Backslash=104, 
		SingleQuote=105, DoubleQuote=106, LeftParenthesis=107, RightParenthesis=108, 
		LeftBrace=109, RightBrace=110, LeftBracket=111, RightBracket=112, Underscore=113, 
		OrOp=114, PipeOp=115, DoublePipeOp=116, AndOp=117, NotOp=118, PlusOp=119, 
		MinusOp=120, XorOp=121, MultOp=122, ExpOp=123, EqOp=124, AtOp=125, OneOp=126, 
		NullOp=127, RequiresOp=128, SplitOp=129, BangOp=130, HashtagOp=131, LessOp=132, 
		LessTermOp=133, GreaterOp=134, ForallOp=135, ExistsOp=136, ImpliesOp=137, 
		EquivOp=138, EquivSignOp=139, TildeOp=140, DollarOp=141, ChainOp=142, 
		ActionArrowStart=143, ActionArrowTip=144, LongRightArrowOp=145, TOp=146, 
		FOp=147, TopOp=148, BotOp=149, Left=150, Right=151, Both=152, Define=153, 
		Include=154, IfDef=155, Else=156, EndIf=157, ForallTraces=158, ExistsTrace=159, 
		OutFactIdent=160, InFactIdent=161, KuFactIdent=162, KdFactIdent=163, DedLogFactIdent=164, 
		FreshFactIdent=165, RuleAttributeColor=166, LemmaAttributeTyping=167, 
		LemmaAttributeSources=168, LemmaAttributeReuse=169, LemmaAttributeDiffReuse=170, 
		LemmaAttributeInduction=171, LemmaAttributeHideLemma=172, FunctionAttribute=173, 
		Natural=174, Letters=175, AlphaNums=176, IDLower=177, IDUpper=178, IDNum=179, 
		NaturalSubscript=180, WS=181, Comment=182, LineComment=183, FormalComment=184, 
		BodyChar=185;
	public static final int
		RULE_tamarin_file = 0, RULE_theory = 1, RULE_body = 2, RULE_define = 3, 
		RULE_include = 4, RULE_ifdef = 5, RULE_flagDisjuncts = 6, RULE_flagConjuncts = 7, 
		RULE_flagNegation = 8, RULE_flagAtom = 9, RULE_tupleTerm = 10, RULE_msetterm = 11, 
		RULE_msettermList = 12, RULE_xorterm = 13, RULE_multterm = 14, RULE_expterm = 15, 
		RULE_term = 16, RULE_application = 17, RULE_unaryOpApplication = 18, RULE_nonUnaryOpApplication = 19, 
		RULE_binaryAlgApplication = 20, RULE_nullaryApplication = 21, RULE_reservedBuiltins = 22, 
		RULE_genericRule = 23, RULE_intruderRule = 24, RULE_intruderInfo = 25, 
		RULE_protoRuleAC = 26, RULE_protoRuleACInfo = 27, RULE_protoRule = 28, 
		RULE_protoRuleInfo = 29, RULE_ruleAttributes = 30, RULE_ruleAttribute = 31, 
		RULE_embeddedRestriction = 32, RULE_factOrRestriction = 33, RULE_factOrRestrictionList = 34, 
		RULE_reservedRuleNames = 35, RULE_ruleFresh = 36, RULE_rulePub = 37, RULE_hexColor = 38, 
		RULE_moduloE = 39, RULE_moduloAC = 40, RULE_factList = 41, RULE_fact = 42, 
		RULE_freshFact = 43, RULE_reservedFact = 44, RULE_protoFact = 45, RULE_multiplicity = 46, 
		RULE_factAnnotations = 47, RULE_factAnnotation = 48, RULE_lemmaAttributes = 49, 
		RULE_lemmaAttribute = 50, RULE_traceQuantifier = 51, RULE_forallTraces = 52, 
		RULE_existsTrace = 53, RULE_lemma = 54, RULE_standardFormula = 55, RULE_implication = 56, 
		RULE_disjunction = 57, RULE_conjunction = 58, RULE_negation = 59, RULE_fatom = 60, 
		RULE_blatom = 61, RULE_quantification = 62, RULE_smallerp = 63, RULE_caseTest = 64, 
		RULE_lemmaAcc = 65, RULE_tactic = 66, RULE_deprio = 67, RULE_prio = 68, 
		RULE_tacticDisjuncts = 69, RULE_tacticConjuncts = 70, RULE_tacticNegation = 71, 
		RULE_tacticFunction = 72, RULE_selectedPresort = 73, RULE_goalRankingPresort = 74, 
		RULE_tacticFunctionName = 75, RULE_rankingIdentifier = 76, RULE_tacticName = 77, 
		RULE_heuristic = 78, RULE_goalRanking = 79, RULE_regularRanking = 80, 
		RULE_internalTacticRanking = 81, RULE_oracleRanking = 82, RULE_export = 83, 
		RULE_predicateDeclaration = 84, RULE_predicate = 85, RULE_signatureOptions = 86, 
		RULE_builtinOptions = 87, RULE_equations = 88, RULE_equation = 89, RULE_signatureFunctions = 90, 
		RULE_signatureFunction = 91, RULE_functionAttribute = 92, RULE_functionType = 93, 
		RULE_builtins = 94, RULE_builtinNames = 95, RULE_builtinNameLocationsReport = 96, 
		RULE_builtinNameReliableChannel = 97, RULE_builtinNameDH = 98, RULE_builtinNameBilinearPairing = 99, 
		RULE_builtinNameMultiset = 100, RULE_builtinNameSymmetricEncryption = 101, 
		RULE_builtinNameAsymmetricEncryption = 102, RULE_builtinNameSigning = 103, 
		RULE_builtinNameDestPairing = 104, RULE_builtinNameDestSymmetricEncryption = 105, 
		RULE_builtinNameDestAsymmetricEncryption = 106, RULE_builtinNameDestSigning = 107, 
		RULE_builtinNameRevealingSigning = 108, RULE_builtinNameHashing = 109, 
		RULE_builtinNameXor = 110, RULE_proofSkeleton = 111, RULE_startProofSkeleton = 112, 
		RULE_solvedProof = 113, RULE_finalProof = 114, RULE_interProof = 115, 
		RULE_proofCase = 116, RULE_proofMethod = 117, RULE_goal = 118, RULE_actionGoal = 119, 
		RULE_premiseGoal = 120, RULE_chainGoal = 121, RULE_disjSplitGoal = 122, 
		RULE_eqSplitGoal = 123, RULE_nodePremise = 124, RULE_nodeConclusion = 125, 
		RULE_diffEquivLemma = 126, RULE_equivLemma = 127, RULE_process = 128, 
		RULE_elseProcess = 129, RULE_topLevelProcess = 130, RULE_actionProcess = 131, 
		RULE_sapicAction = 132, RULE_sapicActionIn = 133, RULE_sapicActionOut = 134, 
		RULE_processDef = 135, RULE_sapicPatternTerm = 136, RULE_sapicTerm = 137, 
		RULE_logicalTypedPatternLiteral = 138, RULE_logicalTypedLiteral = 139, 
		RULE_sapicNodeVar = 140, RULE_sapicPatternVar = 141, RULE_sapicVar = 142, 
		RULE_sapicType = 143, RULE_restriction = 144, RULE_restrictionAttributes = 145, 
		RULE_restrictionAttribute = 146, RULE_legacyAxiom = 147, RULE_letBlock = 148, 
		RULE_letAssignment = 149, RULE_protoFactIdentifier = 150, RULE_reservedFactIdentifier = 151, 
		RULE_identifier = 152, RULE_natural = 153, RULE_indexedIdentifier = 154, 
		RULE_filePath = 155, RULE_keywords = 156, RULE_logMsgVar = 157, RULE_logFreshVar = 158, 
		RULE_logPubVar = 159, RULE_logNodeVar = 160, RULE_logVar = 161, RULE_msgVar = 162, 
		RULE_logicalLiteral = 163, RULE_logicalLiteralWithNode = 164, RULE_logicalLiteralNoPub = 165, 
		RULE_literals = 166, RULE_literal = 167, RULE_nodeVar = 168, RULE_freshName = 169, 
		RULE_pubName = 170, RULE_formalComment = 171, RULE_tacticFunctionIdentifier = 172, 
		RULE_internalTacticName = 173, RULE_oracleRelativePath = 174, RULE_exportBodyChars = 175, 
		RULE_charDir = 176, RULE_equivOp = 177, RULE_orOp = 178, RULE_notOp = 179, 
		RULE_andOp = 180, RULE_eqOp = 181, RULE_botOp = 182, RULE_fOp = 183, RULE_tOp = 184, 
		RULE_topOp = 185, RULE_forallOp = 186, RULE_existsOp = 187, RULE_builtinXor = 188, 
		RULE_builtinMun = 189, RULE_builtinOne = 190, RULE_builtinExp = 191, RULE_builtinMult = 192, 
		RULE_builtinInv = 193, RULE_builtinPmult = 194, RULE_builtinEm = 195, 
		RULE_builtinZero = 196, RULE_dhNeutral = 197, RULE_operators = 198, RULE_punctuation = 199, 
		RULE_allowedIdentifierKeywords = 200;
	private static String[] makeRuleNames() {
		return new String[] {
			"tamarin_file", "theory", "body", "define", "include", "ifdef", "flagDisjuncts", 
			"flagConjuncts", "flagNegation", "flagAtom", "tupleTerm", "msetterm", 
			"msettermList", "xorterm", "multterm", "expterm", "term", "application", 
			"unaryOpApplication", "nonUnaryOpApplication", "binaryAlgApplication", 
			"nullaryApplication", "reservedBuiltins", "genericRule", "intruderRule", 
			"intruderInfo", "protoRuleAC", "protoRuleACInfo", "protoRule", "protoRuleInfo", 
			"ruleAttributes", "ruleAttribute", "embeddedRestriction", "factOrRestriction", 
			"factOrRestrictionList", "reservedRuleNames", "ruleFresh", "rulePub", 
			"hexColor", "moduloE", "moduloAC", "factList", "fact", "freshFact", "reservedFact", 
			"protoFact", "multiplicity", "factAnnotations", "factAnnotation", "lemmaAttributes", 
			"lemmaAttribute", "traceQuantifier", "forallTraces", "existsTrace", "lemma", 
			"standardFormula", "implication", "disjunction", "conjunction", "negation", 
			"fatom", "blatom", "quantification", "smallerp", "caseTest", "lemmaAcc", 
			"tactic", "deprio", "prio", "tacticDisjuncts", "tacticConjuncts", "tacticNegation", 
			"tacticFunction", "selectedPresort", "goalRankingPresort", "tacticFunctionName", 
			"rankingIdentifier", "tacticName", "heuristic", "goalRanking", "regularRanking", 
			"internalTacticRanking", "oracleRanking", "export", "predicateDeclaration", 
			"predicate", "signatureOptions", "builtinOptions", "equations", "equation", 
			"signatureFunctions", "signatureFunction", "functionAttribute", "functionType", 
			"builtins", "builtinNames", "builtinNameLocationsReport", "builtinNameReliableChannel", 
			"builtinNameDH", "builtinNameBilinearPairing", "builtinNameMultiset", 
			"builtinNameSymmetricEncryption", "builtinNameAsymmetricEncryption", 
			"builtinNameSigning", "builtinNameDestPairing", "builtinNameDestSymmetricEncryption", 
			"builtinNameDestAsymmetricEncryption", "builtinNameDestSigning", "builtinNameRevealingSigning", 
			"builtinNameHashing", "builtinNameXor", "proofSkeleton", "startProofSkeleton", 
			"solvedProof", "finalProof", "interProof", "proofCase", "proofMethod", 
			"goal", "actionGoal", "premiseGoal", "chainGoal", "disjSplitGoal", "eqSplitGoal", 
			"nodePremise", "nodeConclusion", "diffEquivLemma", "equivLemma", "process", 
			"elseProcess", "topLevelProcess", "actionProcess", "sapicAction", "sapicActionIn", 
			"sapicActionOut", "processDef", "sapicPatternTerm", "sapicTerm", "logicalTypedPatternLiteral", 
			"logicalTypedLiteral", "sapicNodeVar", "sapicPatternVar", "sapicVar", 
			"sapicType", "restriction", "restrictionAttributes", "restrictionAttribute", 
			"legacyAxiom", "letBlock", "letAssignment", "protoFactIdentifier", "reservedFactIdentifier", 
			"identifier", "natural", "indexedIdentifier", "filePath", "keywords", 
			"logMsgVar", "logFreshVar", "logPubVar", "logNodeVar", "logVar", "msgVar", 
			"logicalLiteral", "logicalLiteralWithNode", "logicalLiteralNoPub", "literals", 
			"literal", "nodeVar", "freshName", "pubName", "formalComment", "tacticFunctionIdentifier", 
			"internalTacticName", "oracleRelativePath", "exportBodyChars", "charDir", 
			"equivOp", "orOp", "notOp", "andOp", "eqOp", "botOp", "fOp", "tOp", "topOp", 
			"forallOp", "existsOp", "builtinXor", "builtinMun", "builtinOne", "builtinExp", 
			"builtinMult", "builtinInv", "builtinPmult", "builtinEm", "builtinZero", 
			"dhNeutral", "operators", "punctuation", "allowedIdentifierKeywords"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'let'", "'in'", "'rule'", "'diff'", "'by'", "'lemma'", "'if'", 
			"'else'", "'process'", "'as'", "'lookup'", "'last'", "'prio'", "'deprio'", 
			"'presort'", "'smallest'", "'id'", "'tactic'", "'restriction'", "'axiom'", 
			"'Any'", "'test'", "'no_precomp'", "'then'", "'out'", "'fresh'", "'pub'", 
			"'xor'", "'output'", null, "'modulo'", "'theory'", "'begin'", "'end'", 
			"'variants'", "'_restrict'", "'heuristic'", "'export'", null, "'options'", 
			"'equations'", null, "'builtins'", "'case'", "'qed'", "'next'", "'splitEqs'", 
			"'equivLemma'", "'diffEquivLemma'", "'E'", "'AC'", "'Fresh'", "'irecv'", 
			"'isend'", "'coerce'", "'iequality'", "'msg'", "'node'", null, null, 
			null, "'DH_neutral'", "'mun'", "'one'", "'exp'", "'mult'", "'inv'", "'pmult'", 
			"'em'", "'zero'", "'SOLVED'", "'sorry'", "'simplify'", "'solve'", "'contradiction'", 
			"'induction'", null, "'locations-report'", "'reliable-channel'", "'diffie-hellman'", 
			"'bilinear-pairing'", "'multiset'", "'symmetric-encryption'", "'asymmetric-encryption'", 
			"'signing'", "'dest-pairing'", "'dest-symmetric-encryption'", "'dest-asymmetric-encryption'", 
			"'dest-signing'", "'revealing-signing'", "'hashing'", null, "'new'", 
			"'insert'", "'delete'", "'lock'", "'unlock'", "'event'", "':'", "';'", 
			"','", "'.'", "'/'", "'\\'", "'''", "'\"'", "'('", "')'", "'{'", "'}'", 
			"'['", "']'", "'_'", "'\\u2228'", "'|'", "'||'", null, null, "'+'", "'-'", 
			null, "'*'", "'^'", "'='", "'@'", "'1'", "'0'", "'\\u25B6'", "'\\u2225'", 
			"'!'", "'#'", "'<'", "'(<)'", "'>'", null, null, null, "'<=>'", "'\\u21D4'", 
			"'~'", "'$'", "'~~>'", "'--['", "']->'", "'-->'", "'T'", "'F'", "'\\u22A4'", 
			"'\\u22A5'", "'left'", "'right'", "'both'", "'#define'", "'#include'", 
			"'#ifdef'", "'#else'", "'#endif'", "'all-traces'", "'exists-trace'", 
			null, null, null, null, null, null, null, "'typing'", "'sources'", "'reuse'", 
			"'diff_reuse'", "'use_induction'", "'hide_lemma'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "KeywordLet", "KeywordIn", "KeywordRule", "KeywordDiff", "KeywordBy", 
			"KeywordLemma", "KeywordIf", "KeywordElse", "KeywordProcess", "KeywordAs", 
			"KeywordLookup", "KeywordLast", "KeywordPrio", "KeywordDeprio", "KeywordPresort", 
			"KeywordSmallest", "KeywordId", "KeywordTactic", "KeywordRestriction", 
			"KeywordAxiom", "KeywordAny", "KeywordTest", "KeywordNoPrecomp", "KeywordThen", 
			"KeywordOut", "KeywordFresh", "KeywordPub", "KeywordXor", "KeywordOutput", 
			"KeywordAccountsFor", "KeywordModulo", "KeywordTheory", "KeywordBegin", 
			"KeywordEnd", "KeywordVariants", "KeywordRestrict", "KeywordHeuristic", 
			"KeywordExport", "KeywordPredicate", "KeywordOptions", "KeywordEquations", 
			"KeywordFunctions", "KeywordBuiltins", "KeywordCase", "KeywordQED", "KeywordNext", 
			"KeywordSplitEqs", "KeywordEquivLemma", "KeywordDiffEquivLemma", "E", 
			"AC", "RuleFresh", "RuleIrecv", "RuleIsend", "RuleCoerce", "RuleIequality", 
			"SuffixMsg", "SuffixNode", "GoalRankingIdentifierNoOracle", "GoalRankingIdentifierOracle", 
			"GoalRankingIdentifiersNoOracle", "DHneutral", "BuiltinMun", "BuiltinOne", 
			"BuiltinExp", "BuiltinMult", "BuiltinInv", "BuiltinPmult", "BuiltinEm", 
			"BuiltinZero", "SolvedProof", "ProofMethodSorry", "ProofMethodSimplify", 
			"ProofMethodSolve", "ProofMethodContradiction", "ProofMethodInduction", 
			"BuiltinOptions", "BuiltinNameLocationsReport", "BuiltinNameReliableChannel", 
			"BuiltinNameDH", "BuiltinNameBilinearPairing", "BuiltinNameMultiset", 
			"BuiltinNameSymmetricEncryption", "BuiltinNameAsymmetricEncryption", 
			"BuiltinNameSigning", "BuiltinNameDestPairing", "BuiltinNameDestSymmetricEncryption", 
			"BuiltinNameDestAsymmetricEncryption", "BuiltinNameDestSigning", "BuiltinNameRevealingSigning", 
			"BuiltinNameHashing", "TacticFunctionName", "SapicActionNew", "SapicActionInsert", 
			"SapicActionDelete", "SapicActionLock", "SapicActionUnlock", "SapicActionEvent", 
			"Colon", "Semicolon", "Comma", "Dot", "Slash", "Backslash", "SingleQuote", 
			"DoubleQuote", "LeftParenthesis", "RightParenthesis", "LeftBrace", "RightBrace", 
			"LeftBracket", "RightBracket", "Underscore", "OrOp", "PipeOp", "DoublePipeOp", 
			"AndOp", "NotOp", "PlusOp", "MinusOp", "XorOp", "MultOp", "ExpOp", "EqOp", 
			"AtOp", "OneOp", "NullOp", "RequiresOp", "SplitOp", "BangOp", "HashtagOp", 
			"LessOp", "LessTermOp", "GreaterOp", "ForallOp", "ExistsOp", "ImpliesOp", 
			"EquivOp", "EquivSignOp", "TildeOp", "DollarOp", "ChainOp", "ActionArrowStart", 
			"ActionArrowTip", "LongRightArrowOp", "TOp", "FOp", "TopOp", "BotOp", 
			"Left", "Right", "Both", "Define", "Include", "IfDef", "Else", "EndIf", 
			"ForallTraces", "ExistsTrace", "OutFactIdent", "InFactIdent", "KuFactIdent", 
			"KdFactIdent", "DedLogFactIdent", "FreshFactIdent", "RuleAttributeColor", 
			"LemmaAttributeTyping", "LemmaAttributeSources", "LemmaAttributeReuse", 
			"LemmaAttributeDiffReuse", "LemmaAttributeInduction", "LemmaAttributeHideLemma", 
			"FunctionAttribute", "Natural", "Letters", "AlphaNums", "IDLower", "IDUpper", 
			"IDNum", "NaturalSubscript", "WS", "Comment", "LineComment", "FormalComment", 
			"BodyChar"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "TamarinParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public TamarinParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Tamarin_fileContext extends ParserRuleContext {
		public TheoryContext theory() {
			return getRuleContext(TheoryContext.class,0);
		}
		public Tamarin_fileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tamarin_file; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTamarin_file(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTamarin_file(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTamarin_file(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Tamarin_fileContext tamarin_file() throws RecognitionException {
		Tamarin_fileContext _localctx = new Tamarin_fileContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_tamarin_file);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(402);
			theory();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TheoryContext extends ParserRuleContext {
		public TerminalNode KeywordTheory() { return getToken(TamarinParser.KeywordTheory, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode KeywordBegin() { return getToken(TamarinParser.KeywordBegin, 0); }
		public TerminalNode KeywordEnd() { return getToken(TamarinParser.KeywordEnd, 0); }
		public List<BodyContext> body() {
			return getRuleContexts(BodyContext.class);
		}
		public BodyContext body(int i) {
			return getRuleContext(BodyContext.class,i);
		}
		public TheoryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_theory; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTheory(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTheory(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTheory(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TheoryContext theory() throws RecognitionException {
		TheoryContext _localctx = new TheoryContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_theory);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(404);
			match(KeywordTheory);
			setState(405);
			identifier();
			setState(406);
			match(KeywordBegin);
			setState(410);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & 861879683252810L) != 0) || ((((_la - 153)) & ~0x3f) == 0 && ((1L << (_la - 153)) & 4194311L) != 0)) {
				{
				{
				setState(407);
				body();
				}
				}
				setState(412);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(413);
			match(KeywordEnd);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BodyContext extends ParserRuleContext {
		public HeuristicContext heuristic() {
			return getRuleContext(HeuristicContext.class,0);
		}
		public TacticContext tactic() {
			return getRuleContext(TacticContext.class,0);
		}
		public BuiltinsContext builtins() {
			return getRuleContext(BuiltinsContext.class,0);
		}
		public SignatureOptionsContext signatureOptions() {
			return getRuleContext(SignatureOptionsContext.class,0);
		}
		public SignatureFunctionsContext signatureFunctions() {
			return getRuleContext(SignatureFunctionsContext.class,0);
		}
		public EquationsContext equations() {
			return getRuleContext(EquationsContext.class,0);
		}
		public RestrictionContext restriction() {
			return getRuleContext(RestrictionContext.class,0);
		}
		public LegacyAxiomContext legacyAxiom() {
			return getRuleContext(LegacyAxiomContext.class,0);
		}
		public CaseTestContext caseTest() {
			return getRuleContext(CaseTestContext.class,0);
		}
		public LemmaAccContext lemmaAcc() {
			return getRuleContext(LemmaAccContext.class,0);
		}
		public LemmaContext lemma() {
			return getRuleContext(LemmaContext.class,0);
		}
		public ProtoRuleContext protoRule() {
			return getRuleContext(ProtoRuleContext.class,0);
		}
		public IntruderRuleContext intruderRule() {
			return getRuleContext(IntruderRuleContext.class,0);
		}
		public TopLevelProcessContext topLevelProcess() {
			return getRuleContext(TopLevelProcessContext.class,0);
		}
		public ProcessDefContext processDef() {
			return getRuleContext(ProcessDefContext.class,0);
		}
		public EquivLemmaContext equivLemma() {
			return getRuleContext(EquivLemmaContext.class,0);
		}
		public DiffEquivLemmaContext diffEquivLemma() {
			return getRuleContext(DiffEquivLemmaContext.class,0);
		}
		public PredicateDeclarationContext predicateDeclaration() {
			return getRuleContext(PredicateDeclarationContext.class,0);
		}
		public ExportContext export() {
			return getRuleContext(ExportContext.class,0);
		}
		public IfdefContext ifdef() {
			return getRuleContext(IfdefContext.class,0);
		}
		public DefineContext define() {
			return getRuleContext(DefineContext.class,0);
		}
		public IncludeContext include() {
			return getRuleContext(IncludeContext.class,0);
		}
		public FormalCommentContext formalComment() {
			return getRuleContext(FormalCommentContext.class,0);
		}
		public BodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BodyContext body() throws RecognitionException {
		BodyContext _localctx = new BodyContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_body);
		try {
			setState(438);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(415);
				heuristic();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(416);
				tactic();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(417);
				builtins();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(418);
				signatureOptions();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(419);
				signatureFunctions();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(420);
				equations();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(421);
				restriction();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(422);
				legacyAxiom();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(423);
				caseTest();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(424);
				lemmaAcc();
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(425);
				lemma();
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(426);
				protoRule();
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(427);
				intruderRule();
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(428);
				topLevelProcess();
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(429);
				processDef();
				}
				break;
			case 16:
				enterOuterAlt(_localctx, 16);
				{
				setState(430);
				equivLemma();
				}
				break;
			case 17:
				enterOuterAlt(_localctx, 17);
				{
				setState(431);
				diffEquivLemma();
				}
				break;
			case 18:
				enterOuterAlt(_localctx, 18);
				{
				setState(432);
				predicateDeclaration();
				}
				break;
			case 19:
				enterOuterAlt(_localctx, 19);
				{
				setState(433);
				export();
				}
				break;
			case 20:
				enterOuterAlt(_localctx, 20);
				{
				setState(434);
				ifdef();
				}
				break;
			case 21:
				enterOuterAlt(_localctx, 21);
				{
				setState(435);
				define();
				}
				break;
			case 22:
				enterOuterAlt(_localctx, 22);
				{
				setState(436);
				include();
				}
				break;
			case 23:
				enterOuterAlt(_localctx, 23);
				{
				setState(437);
				formalComment();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DefineContext extends ParserRuleContext {
		public TerminalNode Define() { return getToken(TamarinParser.Define, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public DefineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_define; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterDefine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitDefine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitDefine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DefineContext define() throws RecognitionException {
		DefineContext _localctx = new DefineContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_define);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(440);
			match(Define);
			setState(441);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class IncludeContext extends ParserRuleContext {
		public TerminalNode Include() { return getToken(TamarinParser.Include, 0); }
		public FilePathContext filePath() {
			return getRuleContext(FilePathContext.class,0);
		}
		public IncludeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_include; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterInclude(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitInclude(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitInclude(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IncludeContext include() throws RecognitionException {
		IncludeContext _localctx = new IncludeContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_include);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(443);
			match(Include);
			setState(444);
			filePath();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class IfdefContext extends ParserRuleContext {
		public TerminalNode IfDef() { return getToken(TamarinParser.IfDef, 0); }
		public FlagDisjunctsContext flagDisjuncts() {
			return getRuleContext(FlagDisjunctsContext.class,0);
		}
		public TerminalNode EndIf() { return getToken(TamarinParser.EndIf, 0); }
		public List<BodyContext> body() {
			return getRuleContexts(BodyContext.class);
		}
		public BodyContext body(int i) {
			return getRuleContext(BodyContext.class,i);
		}
		public TerminalNode Else() { return getToken(TamarinParser.Else, 0); }
		public IfdefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifdef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterIfdef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitIfdef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitIfdef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfdefContext ifdef() throws RecognitionException {
		IfdefContext _localctx = new IfdefContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_ifdef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(446);
			match(IfDef);
			setState(447);
			flagDisjuncts();
			setState(451);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & 861879683252810L) != 0) || ((((_la - 153)) & ~0x3f) == 0 && ((1L << (_la - 153)) & 4194311L) != 0)) {
				{
				{
				setState(448);
				body();
				}
				}
				setState(453);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(461);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Else) {
				{
				setState(454);
				match(Else);
				setState(458);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & 861879683252810L) != 0) || ((((_la - 153)) & ~0x3f) == 0 && ((1L << (_la - 153)) & 4194311L) != 0)) {
					{
					{
					setState(455);
					body();
					}
					}
					setState(460);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(463);
			match(EndIf);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FlagDisjunctsContext extends ParserRuleContext {
		public List<FlagConjunctsContext> flagConjuncts() {
			return getRuleContexts(FlagConjunctsContext.class);
		}
		public FlagConjunctsContext flagConjuncts(int i) {
			return getRuleContext(FlagConjunctsContext.class,i);
		}
		public List<OrOpContext> orOp() {
			return getRuleContexts(OrOpContext.class);
		}
		public OrOpContext orOp(int i) {
			return getRuleContext(OrOpContext.class,i);
		}
		public FlagDisjunctsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_flagDisjuncts; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFlagDisjuncts(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFlagDisjuncts(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFlagDisjuncts(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FlagDisjunctsContext flagDisjuncts() throws RecognitionException {
		FlagDisjunctsContext _localctx = new FlagDisjunctsContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_flagDisjuncts);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(465);
			flagConjuncts();
			setState(471);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==OrOp || _la==PipeOp) {
				{
				{
				setState(466);
				orOp();
				setState(467);
				flagConjuncts();
				}
				}
				setState(473);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FlagConjunctsContext extends ParserRuleContext {
		public List<FlagNegationContext> flagNegation() {
			return getRuleContexts(FlagNegationContext.class);
		}
		public FlagNegationContext flagNegation(int i) {
			return getRuleContext(FlagNegationContext.class,i);
		}
		public List<AndOpContext> andOp() {
			return getRuleContexts(AndOpContext.class);
		}
		public AndOpContext andOp(int i) {
			return getRuleContext(AndOpContext.class,i);
		}
		public FlagConjunctsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_flagConjuncts; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFlagConjuncts(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFlagConjuncts(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFlagConjuncts(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FlagConjunctsContext flagConjuncts() throws RecognitionException {
		FlagConjunctsContext _localctx = new FlagConjunctsContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_flagConjuncts);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(474);
			flagNegation();
			setState(480);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AndOp) {
				{
				{
				setState(475);
				andOp();
				setState(476);
				flagNegation();
				}
				}
				setState(482);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FlagNegationContext extends ParserRuleContext {
		public FlagAtomContext flagAtom() {
			return getRuleContext(FlagAtomContext.class,0);
		}
		public NotOpContext notOp() {
			return getRuleContext(NotOpContext.class,0);
		}
		public FlagNegationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_flagNegation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFlagNegation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFlagNegation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFlagNegation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FlagNegationContext flagNegation() throws RecognitionException {
		FlagNegationContext _localctx = new FlagNegationContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_flagNegation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(484);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==NotOp) {
				{
				setState(483);
				notOp();
				}
			}

			setState(486);
			flagAtom();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FlagAtomContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public FlagAtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_flagAtom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFlagAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFlagAtom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFlagAtom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FlagAtomContext flagAtom() throws RecognitionException {
		FlagAtomContext _localctx = new FlagAtomContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_flagAtom);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(488);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TupleTermContext extends ParserRuleContext {
		public String plit;
		public List<MsettermContext> msetterm() {
			return getRuleContexts(MsettermContext.class);
		}
		public MsettermContext msetterm(int i) {
			return getRuleContext(MsettermContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public TupleTermContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public TupleTermContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_tupleTerm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTupleTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTupleTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTupleTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TupleTermContext tupleTerm(String plit) throws RecognitionException {
		TupleTermContext _localctx = new TupleTermContext(_ctx, getState(), plit);
		enterRule(_localctx, 20, RULE_tupleTerm);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(490);
			msetterm(_localctx.plit);
			setState(495);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Comma) {
				{
				{
				setState(491);
				match(Comma);
				setState(492);
				msetterm(_localctx.plit);
				}
				}
				setState(497);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class MsettermContext extends ParserRuleContext {
		public String plit;
		public List<XortermContext> xorterm() {
			return getRuleContexts(XortermContext.class);
		}
		public XortermContext xorterm(int i) {
			return getRuleContext(XortermContext.class,i);
		}
		public List<TerminalNode> PlusOp() { return getTokens(TamarinParser.PlusOp); }
		public TerminalNode PlusOp(int i) {
			return getToken(TamarinParser.PlusOp, i);
		}
		public MsettermContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public MsettermContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_msetterm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterMsetterm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitMsetterm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitMsetterm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MsettermContext msetterm(String plit) throws RecognitionException {
		MsettermContext _localctx = new MsettermContext(_ctx, getState(), plit);
		enterRule(_localctx, 22, RULE_msetterm);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(498);
			xorterm(_localctx.plit);
			setState(503);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(499);
					match(PlusOp);
					setState(500);
					xorterm(_localctx.plit);
					}
					} 
				}
				setState(505);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class MsettermListContext extends ParserRuleContext {
		public String plit;
		public List<MsettermContext> msetterm() {
			return getRuleContexts(MsettermContext.class);
		}
		public MsettermContext msetterm(int i) {
			return getRuleContext(MsettermContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public MsettermListContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public MsettermListContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_msettermList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterMsettermList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitMsettermList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitMsettermList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MsettermListContext msettermList(String plit) throws RecognitionException {
		MsettermListContext _localctx = new MsettermListContext(_ctx, getState(), plit);
		enterRule(_localctx, 24, RULE_msettermList);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(506);
			msetterm(_localctx.plit);
			setState(511);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(507);
					match(Comma);
					setState(508);
					msetterm(_localctx.plit);
					}
					} 
				}
				setState(513);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			setState(515);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Comma) {
				{
				setState(514);
				match(Comma);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class XortermContext extends ParserRuleContext {
		public String plit;
		public List<MulttermContext> multterm() {
			return getRuleContexts(MulttermContext.class);
		}
		public MulttermContext multterm(int i) {
			return getRuleContext(MulttermContext.class,i);
		}
		public List<TerminalNode> XorOp() { return getTokens(TamarinParser.XorOp); }
		public TerminalNode XorOp(int i) {
			return getToken(TamarinParser.XorOp, i);
		}
		public XortermContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public XortermContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_xorterm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterXorterm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitXorterm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitXorterm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final XortermContext xorterm(String plit) throws RecognitionException {
		XortermContext _localctx = new XortermContext(_ctx, getState(), plit);
		enterRule(_localctx, 26, RULE_xorterm);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(517);
			multterm(_localctx.plit);
			setState(522);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(518);
					match(XorOp);
					setState(519);
					multterm(_localctx.plit);
					}
					} 
				}
				setState(524);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class MulttermContext extends ParserRuleContext {
		public String plit;
		public List<ExptermContext> expterm() {
			return getRuleContexts(ExptermContext.class);
		}
		public ExptermContext expterm(int i) {
			return getRuleContext(ExptermContext.class,i);
		}
		public List<TerminalNode> MultOp() { return getTokens(TamarinParser.MultOp); }
		public TerminalNode MultOp(int i) {
			return getToken(TamarinParser.MultOp, i);
		}
		public MulttermContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public MulttermContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_multterm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterMultterm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitMultterm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitMultterm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MulttermContext multterm(String plit) throws RecognitionException {
		MulttermContext _localctx = new MulttermContext(_ctx, getState(), plit);
		enterRule(_localctx, 28, RULE_multterm);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(525);
			expterm(_localctx.plit);
			setState(530);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(526);
					match(MultOp);
					setState(527);
					expterm(_localctx.plit);
					}
					} 
				}
				setState(532);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ExptermContext extends ParserRuleContext {
		public String plit;
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public List<TerminalNode> ExpOp() { return getTokens(TamarinParser.ExpOp); }
		public TerminalNode ExpOp(int i) {
			return getToken(TamarinParser.ExpOp, i);
		}
		public ExptermContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ExptermContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_expterm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterExpterm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitExpterm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitExpterm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExptermContext expterm(String plit) throws RecognitionException {
		ExptermContext _localctx = new ExptermContext(_ctx, getState(), plit);
		enterRule(_localctx, 30, RULE_expterm);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(533);
			term(_localctx.plit);
			setState(538);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(534);
					match(ExpOp);
					setState(535);
					term(_localctx.plit);
					}
					} 
				}
				setState(540);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TermContext extends ParserRuleContext {
		public String plit;
		public TerminalNode LessOp() { return getToken(TamarinParser.LessOp, 0); }
		public TupleTermContext tupleTerm() {
			return getRuleContext(TupleTermContext.class,0);
		}
		public TerminalNode GreaterOp() { return getToken(TamarinParser.GreaterOp, 0); }
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public MsettermContext msetterm() {
			return getRuleContext(MsettermContext.class,0);
		}
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public TerminalNode OneOp() { return getToken(TamarinParser.OneOp, 0); }
		public DhNeutralContext dhNeutral() {
			return getRuleContext(DhNeutralContext.class,0);
		}
		public ApplicationContext application() {
			return getRuleContext(ApplicationContext.class,0);
		}
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public TermContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public TermContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TermContext term(String plit) throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState(), plit);
		enterRule(_localctx, 32, RULE_term);
		try {
			setState(553);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(541);
				match(LessOp);
				setState(542);
				tupleTerm(_localctx.plit);
				setState(543);
				match(GreaterOp);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(545);
				match(LeftParenthesis);
				setState(546);
				msetterm(_localctx.plit);
				setState(547);
				match(RightParenthesis);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(549);
				match(OneOp);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(550);
				dhNeutral();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(551);
				application(_localctx.plit);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(552);
				literal(_localctx.plit);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ApplicationContext extends ParserRuleContext {
		public String plit;
		public UnaryOpApplicationContext unaryOpApplication() {
			return getRuleContext(UnaryOpApplicationContext.class,0);
		}
		public NonUnaryOpApplicationContext nonUnaryOpApplication() {
			return getRuleContext(NonUnaryOpApplicationContext.class,0);
		}
		public BinaryAlgApplicationContext binaryAlgApplication() {
			return getRuleContext(BinaryAlgApplicationContext.class,0);
		}
		public NullaryApplicationContext nullaryApplication() {
			return getRuleContext(NullaryApplicationContext.class,0);
		}
		public ApplicationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ApplicationContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_application; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterApplication(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitApplication(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitApplication(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ApplicationContext application(String plit) throws RecognitionException {
		ApplicationContext _localctx = new ApplicationContext(_ctx, getState(), plit);
		enterRule(_localctx, 34, RULE_application);
		try {
			setState(560);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(556);
				unaryOpApplication(_localctx.plit);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(557);
				nonUnaryOpApplication(_localctx.plit);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(558);
				binaryAlgApplication(_localctx.plit);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(559);
				nullaryApplication();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class UnaryOpApplicationContext extends ParserRuleContext {
		public String plit;
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public TupleTermContext tupleTerm() {
			return getRuleContext(TupleTermContext.class,0);
		}
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public UnaryOpApplicationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public UnaryOpApplicationContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_unaryOpApplication; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterUnaryOpApplication(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitUnaryOpApplication(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitUnaryOpApplication(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnaryOpApplicationContext unaryOpApplication(String plit) throws RecognitionException {
		UnaryOpApplicationContext _localctx = new UnaryOpApplicationContext(_ctx, getState(), plit);
		enterRule(_localctx, 36, RULE_unaryOpApplication);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(562);
			identifier();
			setState(563);
			match(LeftParenthesis);
			setState(564);
			tupleTerm(_localctx.plit);
			setState(565);
			match(RightParenthesis);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class NonUnaryOpApplicationContext extends ParserRuleContext {
		public String plit;
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public MsettermListContext msettermList() {
			return getRuleContext(MsettermListContext.class,0);
		}
		public NonUnaryOpApplicationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public NonUnaryOpApplicationContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_nonUnaryOpApplication; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterNonUnaryOpApplication(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitNonUnaryOpApplication(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitNonUnaryOpApplication(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NonUnaryOpApplicationContext nonUnaryOpApplication(String plit) throws RecognitionException {
		NonUnaryOpApplicationContext _localctx = new NonUnaryOpApplicationContext(_ctx, getState(), plit);
		enterRule(_localctx, 38, RULE_nonUnaryOpApplication);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(567);
			identifier();
			setState(568);
			match(LeftParenthesis);
			setState(570);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				{
				setState(569);
				msettermList(_localctx.plit);
				}
				break;
			}
			setState(572);
			match(RightParenthesis);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BinaryAlgApplicationContext extends ParserRuleContext {
		public String plit;
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode LeftBrace() { return getToken(TamarinParser.LeftBrace, 0); }
		public TupleTermContext tupleTerm() {
			return getRuleContext(TupleTermContext.class,0);
		}
		public TerminalNode RightBrace() { return getToken(TamarinParser.RightBrace, 0); }
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public BinaryAlgApplicationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public BinaryAlgApplicationContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_binaryAlgApplication; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBinaryAlgApplication(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBinaryAlgApplication(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBinaryAlgApplication(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BinaryAlgApplicationContext binaryAlgApplication(String plit) throws RecognitionException {
		BinaryAlgApplicationContext _localctx = new BinaryAlgApplicationContext(_ctx, getState(), plit);
		enterRule(_localctx, 40, RULE_binaryAlgApplication);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(574);
			identifier();
			setState(575);
			match(LeftBrace);
			setState(576);
			tupleTerm(_localctx.plit);
			setState(577);
			match(RightBrace);
			setState(578);
			term(_localctx.plit);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class NullaryApplicationContext extends ParserRuleContext {
		public BuiltinOneContext builtinOne() {
			return getRuleContext(BuiltinOneContext.class,0);
		}
		public DhNeutralContext dhNeutral() {
			return getRuleContext(DhNeutralContext.class,0);
		}
		public BuiltinZeroContext builtinZero() {
			return getRuleContext(BuiltinZeroContext.class,0);
		}
		public NullaryApplicationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nullaryApplication; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterNullaryApplication(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitNullaryApplication(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitNullaryApplication(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NullaryApplicationContext nullaryApplication() throws RecognitionException {
		NullaryApplicationContext _localctx = new NullaryApplicationContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_nullaryApplication);
		try {
			setState(583);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BuiltinOne:
				enterOuterAlt(_localctx, 1);
				{
				setState(580);
				builtinOne();
				}
				break;
			case DHneutral:
				enterOuterAlt(_localctx, 2);
				{
				setState(581);
				dhNeutral();
				}
				break;
			case BuiltinZero:
				enterOuterAlt(_localctx, 3);
				{
				setState(582);
				builtinZero();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ReservedBuiltinsContext extends ParserRuleContext {
		public BuiltinMunContext builtinMun() {
			return getRuleContext(BuiltinMunContext.class,0);
		}
		public BuiltinOneContext builtinOne() {
			return getRuleContext(BuiltinOneContext.class,0);
		}
		public BuiltinExpContext builtinExp() {
			return getRuleContext(BuiltinExpContext.class,0);
		}
		public BuiltinMultContext builtinMult() {
			return getRuleContext(BuiltinMultContext.class,0);
		}
		public BuiltinInvContext builtinInv() {
			return getRuleContext(BuiltinInvContext.class,0);
		}
		public BuiltinPmultContext builtinPmult() {
			return getRuleContext(BuiltinPmultContext.class,0);
		}
		public BuiltinEmContext builtinEm() {
			return getRuleContext(BuiltinEmContext.class,0);
		}
		public BuiltinZeroContext builtinZero() {
			return getRuleContext(BuiltinZeroContext.class,0);
		}
		public BuiltinXorContext builtinXor() {
			return getRuleContext(BuiltinXorContext.class,0);
		}
		public ReservedBuiltinsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reservedBuiltins; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterReservedBuiltins(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitReservedBuiltins(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitReservedBuiltins(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReservedBuiltinsContext reservedBuiltins() throws RecognitionException {
		ReservedBuiltinsContext _localctx = new ReservedBuiltinsContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_reservedBuiltins);
		try {
			setState(594);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BuiltinMun:
				enterOuterAlt(_localctx, 1);
				{
				setState(585);
				builtinMun();
				}
				break;
			case BuiltinOne:
				enterOuterAlt(_localctx, 2);
				{
				setState(586);
				builtinOne();
				}
				break;
			case BuiltinExp:
				enterOuterAlt(_localctx, 3);
				{
				setState(587);
				builtinExp();
				}
				break;
			case BuiltinMult:
				enterOuterAlt(_localctx, 4);
				{
				setState(588);
				builtinMult();
				}
				break;
			case BuiltinInv:
				enterOuterAlt(_localctx, 5);
				{
				setState(589);
				builtinInv();
				}
				break;
			case BuiltinPmult:
				enterOuterAlt(_localctx, 6);
				{
				setState(590);
				builtinPmult();
				}
				break;
			case BuiltinEm:
				enterOuterAlt(_localctx, 7);
				{
				setState(591);
				builtinEm();
				}
				break;
			case BuiltinZero:
				enterOuterAlt(_localctx, 8);
				{
				setState(592);
				builtinZero();
				}
				break;
			case KeywordXor:
				enterOuterAlt(_localctx, 9);
				{
				setState(593);
				builtinXor();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class GenericRuleContext extends ParserRuleContext {
		public String varp;
		public String nodep;
		public List<FactListContext> factList() {
			return getRuleContexts(FactListContext.class);
		}
		public FactListContext factList(int i) {
			return getRuleContext(FactListContext.class,i);
		}
		public TerminalNode LongRightArrowOp() { return getToken(TamarinParser.LongRightArrowOp, 0); }
		public TerminalNode ActionArrowStart() { return getToken(TamarinParser.ActionArrowStart, 0); }
		public TerminalNode ActionArrowTip() { return getToken(TamarinParser.ActionArrowTip, 0); }
		public FactOrRestrictionListContext factOrRestrictionList() {
			return getRuleContext(FactOrRestrictionListContext.class,0);
		}
		public GenericRuleContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public GenericRuleContext(ParserRuleContext parent, int invokingState, String varp, String nodep) {
			super(parent, invokingState);
			this.varp = varp;
			this.nodep = nodep;
		}
		@Override public int getRuleIndex() { return RULE_genericRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterGenericRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitGenericRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitGenericRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GenericRuleContext genericRule(String varp,String nodep) throws RecognitionException {
		GenericRuleContext _localctx = new GenericRuleContext(_ctx, getState(), varp, nodep);
		enterRule(_localctx, 46, RULE_genericRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(596);
			factList("vlit" + _localctx.varp);
			setState(603);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LongRightArrowOp:
				{
				setState(597);
				match(LongRightArrowOp);
				}
				break;
			case ActionArrowStart:
				{
				setState(598);
				match(ActionArrowStart);
				setState(600);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & -16L) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & -4611685984067649537L) != 0) || ((((_la - 130)) & ~0x3f) == 0 && ((1L << (_la - 130)) & 1125899645943809L) != 0)) {
					{
					setState(599);
					factOrRestrictionList(_localctx.varp, _localctx.nodep);
					}
				}

				setState(602);
				match(ActionArrowTip);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(605);
			factList("vlit" + _localctx.varp);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class IntruderRuleContext extends ParserRuleContext {
		public TerminalNode KeywordRule() { return getToken(TamarinParser.KeywordRule, 0); }
		public ModuloACContext moduloAC() {
			return getRuleContext(ModuloACContext.class,0);
		}
		public IntruderInfoContext intruderInfo() {
			return getRuleContext(IntruderInfoContext.class,0);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public GenericRuleContext genericRule() {
			return getRuleContext(GenericRuleContext.class,0);
		}
		public IntruderRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intruderRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterIntruderRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitIntruderRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitIntruderRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntruderRuleContext intruderRule() throws RecognitionException {
		IntruderRuleContext _localctx = new IntruderRuleContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_intruderRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(607);
			match(KeywordRule);
			setState(608);
			moduloAC();
			setState(609);
			intruderInfo();
			setState(610);
			match(Colon);
			setState(611);
			genericRule("msgVar", "nodeVar");
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class IntruderInfoContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public NaturalContext natural() {
			return getRuleContext(NaturalContext.class,0);
		}
		public IntruderInfoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intruderInfo; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterIntruderInfo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitIntruderInfo(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitIntruderInfo(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntruderInfoContext intruderInfo() throws RecognitionException {
		IntruderInfoContext _localctx = new IntruderInfoContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_intruderInfo);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(613);
			identifier();
			setState(615);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 126)) & ~0x3f) == 0 && ((1L << (_la - 126)) & 281474976710659L) != 0)) {
				{
				setState(614);
				natural();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ProtoRuleACContext extends ParserRuleContext {
		public ProtoRuleACInfoContext protoRuleACInfo() {
			return getRuleContext(ProtoRuleACInfoContext.class,0);
		}
		public GenericRuleContext genericRule() {
			return getRuleContext(GenericRuleContext.class,0);
		}
		public LetBlockContext letBlock() {
			return getRuleContext(LetBlockContext.class,0);
		}
		public ProtoRuleACContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_protoRuleAC; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterProtoRuleAC(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitProtoRuleAC(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitProtoRuleAC(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProtoRuleACContext protoRuleAC() throws RecognitionException {
		ProtoRuleACContext _localctx = new ProtoRuleACContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_protoRuleAC);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(617);
			protoRuleACInfo();
			setState(619);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KeywordLet) {
				{
				setState(618);
				letBlock();
				}
			}

			setState(621);
			genericRule("msgVar", "nodeVar");
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ProtoRuleACInfoContext extends ParserRuleContext {
		public TerminalNode KeywordRule() { return getToken(TamarinParser.KeywordRule, 0); }
		public ModuloACContext moduloAC() {
			return getRuleContext(ModuloACContext.class,0);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public RuleAttributesContext ruleAttributes() {
			return getRuleContext(RuleAttributesContext.class,0);
		}
		public ProtoRuleACInfoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_protoRuleACInfo; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterProtoRuleACInfo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitProtoRuleACInfo(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitProtoRuleACInfo(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProtoRuleACInfoContext protoRuleACInfo() throws RecognitionException {
		ProtoRuleACInfoContext _localctx = new ProtoRuleACInfoContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_protoRuleACInfo);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(623);
			match(KeywordRule);
			setState(624);
			moduloAC();
			setState(625);
			identifier();
			setState(627);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LeftBracket) {
				{
				setState(626);
				ruleAttributes();
				}
			}

			setState(629);
			match(Colon);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ProtoRuleContext extends ParserRuleContext {
		public ProtoRuleInfoContext protoRuleInfo() {
			return getRuleContext(ProtoRuleInfoContext.class,0);
		}
		public GenericRuleContext genericRule() {
			return getRuleContext(GenericRuleContext.class,0);
		}
		public LetBlockContext letBlock() {
			return getRuleContext(LetBlockContext.class,0);
		}
		public TerminalNode KeywordVariants() { return getToken(TamarinParser.KeywordVariants, 0); }
		public List<ProtoRuleACContext> protoRuleAC() {
			return getRuleContexts(ProtoRuleACContext.class);
		}
		public ProtoRuleACContext protoRuleAC(int i) {
			return getRuleContext(ProtoRuleACContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public ProtoRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_protoRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterProtoRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitProtoRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitProtoRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProtoRuleContext protoRule() throws RecognitionException {
		ProtoRuleContext _localctx = new ProtoRuleContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_protoRule);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(631);
			protoRuleInfo();
			setState(633);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KeywordLet) {
				{
				setState(632);
				letBlock();
				}
			}

			setState(635);
			genericRule("msgVar", "nodeVar");
			setState(648);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KeywordVariants) {
				{
				setState(636);
				match(KeywordVariants);
				setState(637);
				protoRuleAC();
				setState(642);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(638);
						match(Comma);
						setState(639);
						protoRuleAC();
						}
						} 
					}
					setState(644);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
				}
				setState(646);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma) {
					{
					setState(645);
					match(Comma);
					}
				}

				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ProtoRuleInfoContext extends ParserRuleContext {
		public TerminalNode KeywordRule() { return getToken(TamarinParser.KeywordRule, 0); }
		public ModuloEContext moduloE() {
			return getRuleContext(ModuloEContext.class,0);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public RuleAttributesContext ruleAttributes() {
			return getRuleContext(RuleAttributesContext.class,0);
		}
		public ProtoRuleInfoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_protoRuleInfo; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterProtoRuleInfo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitProtoRuleInfo(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitProtoRuleInfo(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProtoRuleInfoContext protoRuleInfo() throws RecognitionException {
		ProtoRuleInfoContext _localctx = new ProtoRuleInfoContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_protoRuleInfo);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(650);
			match(KeywordRule);
			setState(651);
			moduloE();
			setState(652);
			identifier();
			setState(654);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LeftBracket) {
				{
				setState(653);
				ruleAttributes();
				}
			}

			setState(656);
			match(Colon);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class RuleAttributesContext extends ParserRuleContext {
		public TerminalNode LeftBracket() { return getToken(TamarinParser.LeftBracket, 0); }
		public TerminalNode RightBracket() { return getToken(TamarinParser.RightBracket, 0); }
		public List<RuleAttributeContext> ruleAttribute() {
			return getRuleContexts(RuleAttributeContext.class);
		}
		public RuleAttributeContext ruleAttribute(int i) {
			return getRuleContext(RuleAttributeContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public RuleAttributesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ruleAttributes; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterRuleAttributes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitRuleAttributes(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitRuleAttributes(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RuleAttributesContext ruleAttributes() throws RecognitionException {
		RuleAttributesContext _localctx = new RuleAttributesContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_ruleAttributes);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(658);
			match(LeftBracket);
			setState(670);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KeywordProcess || _la==RuleAttributeColor) {
				{
				setState(659);
				ruleAttribute();
				setState(664);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(660);
						match(Comma);
						setState(661);
						ruleAttribute();
						}
						} 
					}
					setState(666);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
				}
				setState(668);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma) {
					{
					setState(667);
					match(Comma);
					}
				}

				}
			}

			setState(672);
			match(RightBracket);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class RuleAttributeContext extends ParserRuleContext {
		public TerminalNode RuleAttributeColor() { return getToken(TamarinParser.RuleAttributeColor, 0); }
		public TerminalNode EqOp() { return getToken(TamarinParser.EqOp, 0); }
		public HexColorContext hexColor() {
			return getRuleContext(HexColorContext.class,0);
		}
		public TerminalNode KeywordProcess() { return getToken(TamarinParser.KeywordProcess, 0); }
		public List<TerminalNode> DoubleQuote() { return getTokens(TamarinParser.DoubleQuote); }
		public TerminalNode DoubleQuote(int i) {
			return getToken(TamarinParser.DoubleQuote, i);
		}
		public RuleAttributeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ruleAttribute; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterRuleAttribute(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitRuleAttribute(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitRuleAttribute(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RuleAttributeContext ruleAttribute() throws RecognitionException {
		RuleAttributeContext _localctx = new RuleAttributeContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_ruleAttribute);
		try {
			int _alt;
			setState(687);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case RuleAttributeColor:
				enterOuterAlt(_localctx, 1);
				{
				setState(674);
				match(RuleAttributeColor);
				setState(675);
				match(EqOp);
				setState(676);
				hexColor();
				}
				break;
			case KeywordProcess:
				enterOuterAlt(_localctx, 2);
				{
				setState(677);
				match(KeywordProcess);
				setState(678);
				match(EqOp);
				setState(679);
				match(DoubleQuote);
				setState(683);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
				while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1+1 ) {
						{
						{
						setState(680);
						matchWildcard();
						}
						} 
					}
					setState(685);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
				}
				setState(686);
				match(DoubleQuote);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class EmbeddedRestrictionContext extends ParserRuleContext {
		public String varp;
		public String nodep;
		public TerminalNode KeywordRestrict() { return getToken(TamarinParser.KeywordRestrict, 0); }
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public StandardFormulaContext standardFormula() {
			return getRuleContext(StandardFormulaContext.class,0);
		}
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public EmbeddedRestrictionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public EmbeddedRestrictionContext(ParserRuleContext parent, int invokingState, String varp, String nodep) {
			super(parent, invokingState);
			this.varp = varp;
			this.nodep = nodep;
		}
		@Override public int getRuleIndex() { return RULE_embeddedRestriction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterEmbeddedRestriction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitEmbeddedRestriction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitEmbeddedRestriction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EmbeddedRestrictionContext embeddedRestriction(String varp,String nodep) throws RecognitionException {
		EmbeddedRestrictionContext _localctx = new EmbeddedRestrictionContext(_ctx, getState(), varp, nodep);
		enterRule(_localctx, 64, RULE_embeddedRestriction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(689);
			match(KeywordRestrict);
			setState(690);
			match(LeftParenthesis);
			setState(691);
			standardFormula(_localctx.varp, _localctx.nodep);
			setState(692);
			match(RightParenthesis);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FactOrRestrictionContext extends ParserRuleContext {
		public String varp;
		public String nodep;
		public FactContext fact() {
			return getRuleContext(FactContext.class,0);
		}
		public EmbeddedRestrictionContext embeddedRestriction() {
			return getRuleContext(EmbeddedRestrictionContext.class,0);
		}
		public FactOrRestrictionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public FactOrRestrictionContext(ParserRuleContext parent, int invokingState, String varp, String nodep) {
			super(parent, invokingState);
			this.varp = varp;
			this.nodep = nodep;
		}
		@Override public int getRuleIndex() { return RULE_factOrRestriction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFactOrRestriction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFactOrRestriction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFactOrRestriction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FactOrRestrictionContext factOrRestriction(String varp,String nodep) throws RecognitionException {
		FactOrRestrictionContext _localctx = new FactOrRestrictionContext(_ctx, getState(), varp, nodep);
		enterRule(_localctx, 66, RULE_factOrRestriction);
		try {
			setState(696);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(694);
				fact("vlit" + _localctx.varp);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(695);
				embeddedRestriction(_localctx.varp, _localctx.nodep);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FactOrRestrictionListContext extends ParserRuleContext {
		public String varp;
		public String nodep;
		public List<FactOrRestrictionContext> factOrRestriction() {
			return getRuleContexts(FactOrRestrictionContext.class);
		}
		public FactOrRestrictionContext factOrRestriction(int i) {
			return getRuleContext(FactOrRestrictionContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public FactOrRestrictionListContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public FactOrRestrictionListContext(ParserRuleContext parent, int invokingState, String varp, String nodep) {
			super(parent, invokingState);
			this.varp = varp;
			this.nodep = nodep;
		}
		@Override public int getRuleIndex() { return RULE_factOrRestrictionList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFactOrRestrictionList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFactOrRestrictionList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFactOrRestrictionList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FactOrRestrictionListContext factOrRestrictionList(String varp,String nodep) throws RecognitionException {
		FactOrRestrictionListContext _localctx = new FactOrRestrictionListContext(_ctx, getState(), varp, nodep);
		enterRule(_localctx, 68, RULE_factOrRestrictionList);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(698);
			factOrRestriction(_localctx.varp, _localctx.nodep);
			setState(703);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,36,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(699);
					match(Comma);
					setState(700);
					factOrRestriction(_localctx.varp, _localctx.nodep);
					}
					} 
				}
				setState(705);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,36,_ctx);
			}
			setState(707);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Comma) {
				{
				setState(706);
				match(Comma);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ReservedRuleNamesContext extends ParserRuleContext {
		public RuleFreshContext ruleFresh() {
			return getRuleContext(RuleFreshContext.class,0);
		}
		public TerminalNode RuleIrecv() { return getToken(TamarinParser.RuleIrecv, 0); }
		public TerminalNode RuleIsend() { return getToken(TamarinParser.RuleIsend, 0); }
		public TerminalNode RuleCoerce() { return getToken(TamarinParser.RuleCoerce, 0); }
		public RulePubContext rulePub() {
			return getRuleContext(RulePubContext.class,0);
		}
		public TerminalNode RuleIequality() { return getToken(TamarinParser.RuleIequality, 0); }
		public ReservedRuleNamesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reservedRuleNames; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterReservedRuleNames(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitReservedRuleNames(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitReservedRuleNames(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReservedRuleNamesContext reservedRuleNames() throws RecognitionException {
		ReservedRuleNamesContext _localctx = new ReservedRuleNamesContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_reservedRuleNames);
		try {
			setState(715);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case KeywordFresh:
			case RuleFresh:
				enterOuterAlt(_localctx, 1);
				{
				setState(709);
				ruleFresh();
				}
				break;
			case RuleIrecv:
				enterOuterAlt(_localctx, 2);
				{
				setState(710);
				match(RuleIrecv);
				}
				break;
			case RuleIsend:
				enterOuterAlt(_localctx, 3);
				{
				setState(711);
				match(RuleIsend);
				}
				break;
			case RuleCoerce:
				enterOuterAlt(_localctx, 4);
				{
				setState(712);
				match(RuleCoerce);
				}
				break;
			case KeywordPub:
				enterOuterAlt(_localctx, 5);
				{
				setState(713);
				rulePub();
				}
				break;
			case RuleIequality:
				enterOuterAlt(_localctx, 6);
				{
				setState(714);
				match(RuleIequality);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class RuleFreshContext extends ParserRuleContext {
		public TerminalNode RuleFresh() { return getToken(TamarinParser.RuleFresh, 0); }
		public TerminalNode KeywordFresh() { return getToken(TamarinParser.KeywordFresh, 0); }
		public RuleFreshContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ruleFresh; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterRuleFresh(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitRuleFresh(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitRuleFresh(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RuleFreshContext ruleFresh() throws RecognitionException {
		RuleFreshContext _localctx = new RuleFreshContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_ruleFresh);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(717);
			_la = _input.LA(1);
			if ( !(_la==KeywordFresh || _la==RuleFresh) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class RulePubContext extends ParserRuleContext {
		public TerminalNode KeywordPub() { return getToken(TamarinParser.KeywordPub, 0); }
		public RulePubContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rulePub; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterRulePub(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitRulePub(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitRulePub(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RulePubContext rulePub() throws RecognitionException {
		RulePubContext _localctx = new RulePubContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_rulePub);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(719);
			match(KeywordPub);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class HexColorContext extends ParserRuleContext {
		public List<TerminalNode> SingleQuote() { return getTokens(TamarinParser.SingleQuote); }
		public TerminalNode SingleQuote(int i) {
			return getToken(TamarinParser.SingleQuote, i);
		}
		public NaturalContext natural() {
			return getRuleContext(NaturalContext.class,0);
		}
		public TerminalNode AlphaNums() { return getToken(TamarinParser.AlphaNums, 0); }
		public TerminalNode Letters() { return getToken(TamarinParser.Letters, 0); }
		public TerminalNode HashtagOp() { return getToken(TamarinParser.HashtagOp, 0); }
		public HexColorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hexColor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterHexColor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitHexColor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitHexColor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HexColorContext hexColor() throws RecognitionException {
		HexColorContext _localctx = new HexColorContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_hexColor);
		int _la;
		try {
			setState(739);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case SingleQuote:
				enterOuterAlt(_localctx, 1);
				{
				setState(721);
				match(SingleQuote);
				setState(723);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==HashtagOp) {
					{
					setState(722);
					match(HashtagOp);
					}
				}

				setState(728);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case OneOp:
				case NullOp:
				case Natural:
					{
					setState(725);
					natural();
					}
					break;
				case AlphaNums:
					{
					setState(726);
					match(AlphaNums);
					}
					break;
				case Letters:
					{
					setState(727);
					match(Letters);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(730);
				match(SingleQuote);
				}
				break;
			case OneOp:
			case NullOp:
			case HashtagOp:
			case Natural:
			case Letters:
			case AlphaNums:
				enterOuterAlt(_localctx, 2);
				{
				setState(732);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==HashtagOp) {
					{
					setState(731);
					match(HashtagOp);
					}
				}

				setState(737);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case OneOp:
				case NullOp:
				case Natural:
					{
					setState(734);
					natural();
					}
					break;
				case AlphaNums:
					{
					setState(735);
					match(AlphaNums);
					}
					break;
				case Letters:
					{
					setState(736);
					match(Letters);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ModuloEContext extends ParserRuleContext {
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public TerminalNode KeywordModulo() { return getToken(TamarinParser.KeywordModulo, 0); }
		public TerminalNode E() { return getToken(TamarinParser.E, 0); }
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public ModuloEContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_moduloE; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterModuloE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitModuloE(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitModuloE(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ModuloEContext moduloE() throws RecognitionException {
		ModuloEContext _localctx = new ModuloEContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_moduloE);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(745);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LeftParenthesis) {
				{
				setState(741);
				match(LeftParenthesis);
				setState(742);
				match(KeywordModulo);
				setState(743);
				match(E);
				setState(744);
				match(RightParenthesis);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ModuloACContext extends ParserRuleContext {
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public TerminalNode KeywordModulo() { return getToken(TamarinParser.KeywordModulo, 0); }
		public TerminalNode AC() { return getToken(TamarinParser.AC, 0); }
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public ModuloACContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_moduloAC; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterModuloAC(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitModuloAC(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitModuloAC(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ModuloACContext moduloAC() throws RecognitionException {
		ModuloACContext _localctx = new ModuloACContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_moduloAC);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(747);
			match(LeftParenthesis);
			setState(748);
			match(KeywordModulo);
			setState(749);
			match(AC);
			setState(750);
			match(RightParenthesis);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FactListContext extends ParserRuleContext {
		public String plit;
		public TerminalNode LeftBracket() { return getToken(TamarinParser.LeftBracket, 0); }
		public TerminalNode RightBracket() { return getToken(TamarinParser.RightBracket, 0); }
		public List<FactContext> fact() {
			return getRuleContexts(FactContext.class);
		}
		public FactContext fact(int i) {
			return getRuleContext(FactContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public FactListContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public FactListContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_factList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFactList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFactList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFactList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FactListContext factList(String plit) throws RecognitionException {
		FactListContext _localctx = new FactListContext(_ctx, getState(), plit);
		enterRule(_localctx, 82, RULE_factList);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(752);
			match(LeftBracket);
			setState(764);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & -16L) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & -4611685984067649537L) != 0) || ((((_la - 130)) & ~0x3f) == 0 && ((1L << (_la - 130)) & 1125899645943809L) != 0)) {
				{
				setState(753);
				fact(_localctx.plit);
				setState(758);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,45,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(754);
						match(Comma);
						setState(755);
						fact(_localctx.plit);
						}
						} 
					}
					setState(760);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,45,_ctx);
				}
				setState(762);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma) {
					{
					setState(761);
					match(Comma);
					}
				}

				}
			}

			setState(766);
			match(RightBracket);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FactContext extends ParserRuleContext {
		public String plit;
		public FreshFactContext freshFact() {
			return getRuleContext(FreshFactContext.class,0);
		}
		public ReservedFactContext reservedFact() {
			return getRuleContext(ReservedFactContext.class,0);
		}
		public ProtoFactContext protoFact() {
			return getRuleContext(ProtoFactContext.class,0);
		}
		public FactContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public FactContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_fact; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFact(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFact(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFact(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FactContext fact(String plit) throws RecognitionException {
		FactContext _localctx = new FactContext(_ctx, getState(), plit);
		enterRule(_localctx, 84, RULE_fact);
		try {
			setState(771);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,48,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(768);
				freshFact(_localctx.plit);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(769);
				reservedFact(_localctx.plit);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(770);
				protoFact(_localctx.plit);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FreshFactContext extends ParserRuleContext {
		public String plit;
		public TerminalNode FreshFactIdent() { return getToken(TamarinParser.FreshFactIdent, 0); }
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public MsettermContext msetterm() {
			return getRuleContext(MsettermContext.class,0);
		}
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public FactAnnotationsContext factAnnotations() {
			return getRuleContext(FactAnnotationsContext.class,0);
		}
		public FreshFactContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public FreshFactContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_freshFact; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFreshFact(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFreshFact(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFreshFact(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FreshFactContext freshFact(String plit) throws RecognitionException {
		FreshFactContext _localctx = new FreshFactContext(_ctx, getState(), plit);
		enterRule(_localctx, 86, RULE_freshFact);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(773);
			match(FreshFactIdent);
			setState(774);
			match(LeftParenthesis);
			setState(775);
			msetterm(_localctx.plit);
			setState(776);
			match(RightParenthesis);
			setState(777);
			factAnnotations();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ReservedFactContext extends ParserRuleContext {
		public String plit;
		public ReservedFactIdentifierContext reservedFactIdentifier() {
			return getRuleContext(ReservedFactIdentifierContext.class,0);
		}
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public MsettermContext msetterm() {
			return getRuleContext(MsettermContext.class,0);
		}
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public FactAnnotationsContext factAnnotations() {
			return getRuleContext(FactAnnotationsContext.class,0);
		}
		public MultiplicityContext multiplicity() {
			return getRuleContext(MultiplicityContext.class,0);
		}
		public ReservedFactContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ReservedFactContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_reservedFact; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterReservedFact(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitReservedFact(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitReservedFact(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReservedFactContext reservedFact(String plit) throws RecognitionException {
		ReservedFactContext _localctx = new ReservedFactContext(_ctx, getState(), plit);
		enterRule(_localctx, 88, RULE_reservedFact);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(780);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==BangOp) {
				{
				setState(779);
				multiplicity();
				}
			}

			setState(782);
			reservedFactIdentifier();
			setState(783);
			match(LeftParenthesis);
			setState(784);
			msetterm(_localctx.plit);
			setState(785);
			match(RightParenthesis);
			setState(786);
			factAnnotations();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ProtoFactContext extends ParserRuleContext {
		public String plit;
		public ProtoFactIdentifierContext protoFactIdentifier() {
			return getRuleContext(ProtoFactIdentifierContext.class,0);
		}
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public FactAnnotationsContext factAnnotations() {
			return getRuleContext(FactAnnotationsContext.class,0);
		}
		public MultiplicityContext multiplicity() {
			return getRuleContext(MultiplicityContext.class,0);
		}
		public MsettermListContext msettermList() {
			return getRuleContext(MsettermListContext.class,0);
		}
		public ProtoFactContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ProtoFactContext(ParserRuleContext parent, int invokingState, String plit) {
			super(parent, invokingState);
			this.plit = plit;
		}
		@Override public int getRuleIndex() { return RULE_protoFact; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterProtoFact(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitProtoFact(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitProtoFact(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProtoFactContext protoFact(String plit) throws RecognitionException {
		ProtoFactContext _localctx = new ProtoFactContext(_ctx, getState(), plit);
		enterRule(_localctx, 90, RULE_protoFact);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(789);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==BangOp) {
				{
				setState(788);
				multiplicity();
				}
			}

			setState(791);
			protoFactIdentifier();
			setState(792);
			match(LeftParenthesis);
			setState(794);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,51,_ctx) ) {
			case 1:
				{
				setState(793);
				msettermList(_localctx.plit);
				}
				break;
			}
			setState(796);
			match(RightParenthesis);
			setState(797);
			factAnnotations();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class MultiplicityContext extends ParserRuleContext {
		public TerminalNode BangOp() { return getToken(TamarinParser.BangOp, 0); }
		public MultiplicityContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplicity; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterMultiplicity(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitMultiplicity(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitMultiplicity(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultiplicityContext multiplicity() throws RecognitionException {
		MultiplicityContext _localctx = new MultiplicityContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_multiplicity);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(799);
			match(BangOp);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FactAnnotationsContext extends ParserRuleContext {
		public TerminalNode LeftBracket() { return getToken(TamarinParser.LeftBracket, 0); }
		public TerminalNode RightBracket() { return getToken(TamarinParser.RightBracket, 0); }
		public List<FactAnnotationContext> factAnnotation() {
			return getRuleContexts(FactAnnotationContext.class);
		}
		public FactAnnotationContext factAnnotation(int i) {
			return getRuleContext(FactAnnotationContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public FactAnnotationsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factAnnotations; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFactAnnotations(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFactAnnotations(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFactAnnotations(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FactAnnotationsContext factAnnotations() throws RecognitionException {
		FactAnnotationsContext _localctx = new FactAnnotationsContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_factAnnotations);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(816);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,55,_ctx) ) {
			case 1:
				{
				setState(801);
				match(LeftBracket);
				setState(813);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==KeywordNoPrecomp || _la==PlusOp || _la==MinusOp) {
					{
					setState(802);
					factAnnotation();
					setState(807);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,52,_ctx);
					while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
						if ( _alt==1 ) {
							{
							{
							setState(803);
							match(Comma);
							setState(804);
							factAnnotation();
							}
							} 
						}
						setState(809);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,52,_ctx);
					}
					setState(811);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==Comma) {
						{
						setState(810);
						match(Comma);
						}
					}

					}
				}

				setState(815);
				match(RightBracket);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FactAnnotationContext extends ParserRuleContext {
		public TerminalNode PlusOp() { return getToken(TamarinParser.PlusOp, 0); }
		public TerminalNode MinusOp() { return getToken(TamarinParser.MinusOp, 0); }
		public TerminalNode KeywordNoPrecomp() { return getToken(TamarinParser.KeywordNoPrecomp, 0); }
		public FactAnnotationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factAnnotation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFactAnnotation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFactAnnotation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFactAnnotation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FactAnnotationContext factAnnotation() throws RecognitionException {
		FactAnnotationContext _localctx = new FactAnnotationContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_factAnnotation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(818);
			_la = _input.LA(1);
			if ( !(_la==KeywordNoPrecomp || _la==PlusOp || _la==MinusOp) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LemmaAttributesContext extends ParserRuleContext {
		public TerminalNode LeftBracket() { return getToken(TamarinParser.LeftBracket, 0); }
		public TerminalNode RightBracket() { return getToken(TamarinParser.RightBracket, 0); }
		public List<LemmaAttributeContext> lemmaAttribute() {
			return getRuleContexts(LemmaAttributeContext.class);
		}
		public LemmaAttributeContext lemmaAttribute(int i) {
			return getRuleContext(LemmaAttributeContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public LemmaAttributesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lemmaAttributes; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLemmaAttributes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLemmaAttributes(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLemmaAttributes(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LemmaAttributesContext lemmaAttributes() throws RecognitionException {
		LemmaAttributesContext _localctx = new LemmaAttributesContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_lemmaAttributes);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(820);
			match(LeftBracket);
			setState(832);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KeywordOutput || _la==KeywordHeuristic || ((((_la - 150)) & ~0x3f) == 0 && ((1L << (_la - 150)) & 8257539L) != 0)) {
				{
				setState(821);
				lemmaAttribute();
				setState(826);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,56,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(822);
						match(Comma);
						setState(823);
						lemmaAttribute();
						}
						} 
					}
					setState(828);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,56,_ctx);
				}
				setState(830);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma) {
					{
					setState(829);
					match(Comma);
					}
				}

				}
			}

			setState(834);
			match(RightBracket);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LemmaAttributeContext extends ParserRuleContext {
		public TerminalNode LemmaAttributeTyping() { return getToken(TamarinParser.LemmaAttributeTyping, 0); }
		public TerminalNode LemmaAttributeSources() { return getToken(TamarinParser.LemmaAttributeSources, 0); }
		public TerminalNode LemmaAttributeReuse() { return getToken(TamarinParser.LemmaAttributeReuse, 0); }
		public TerminalNode LemmaAttributeDiffReuse() { return getToken(TamarinParser.LemmaAttributeDiffReuse, 0); }
		public TerminalNode LemmaAttributeInduction() { return getToken(TamarinParser.LemmaAttributeInduction, 0); }
		public TerminalNode LemmaAttributeHideLemma() { return getToken(TamarinParser.LemmaAttributeHideLemma, 0); }
		public TerminalNode EqOp() { return getToken(TamarinParser.EqOp, 0); }
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public TerminalNode KeywordHeuristic() { return getToken(TamarinParser.KeywordHeuristic, 0); }
		public GoalRankingContext goalRanking() {
			return getRuleContext(GoalRankingContext.class,0);
		}
		public TerminalNode KeywordOutput() { return getToken(TamarinParser.KeywordOutput, 0); }
		public TerminalNode LeftBracket() { return getToken(TamarinParser.LeftBracket, 0); }
		public TerminalNode RightBracket() { return getToken(TamarinParser.RightBracket, 0); }
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public TerminalNode Left() { return getToken(TamarinParser.Left, 0); }
		public TerminalNode Right() { return getToken(TamarinParser.Right, 0); }
		public LemmaAttributeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lemmaAttribute; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLemmaAttribute(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLemmaAttribute(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLemmaAttribute(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LemmaAttributeContext lemmaAttribute() throws RecognitionException {
		LemmaAttributeContext _localctx = new LemmaAttributeContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_lemmaAttribute);
		int _la;
		try {
			int _alt;
			setState(866);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LemmaAttributeTyping:
				enterOuterAlt(_localctx, 1);
				{
				setState(836);
				match(LemmaAttributeTyping);
				}
				break;
			case LemmaAttributeSources:
				enterOuterAlt(_localctx, 2);
				{
				setState(837);
				match(LemmaAttributeSources);
				}
				break;
			case LemmaAttributeReuse:
				enterOuterAlt(_localctx, 3);
				{
				setState(838);
				match(LemmaAttributeReuse);
				}
				break;
			case LemmaAttributeDiffReuse:
				enterOuterAlt(_localctx, 4);
				{
				setState(839);
				match(LemmaAttributeDiffReuse);
				}
				break;
			case LemmaAttributeInduction:
				enterOuterAlt(_localctx, 5);
				{
				setState(840);
				match(LemmaAttributeInduction);
				}
				break;
			case LemmaAttributeHideLemma:
				enterOuterAlt(_localctx, 6);
				{
				setState(841);
				match(LemmaAttributeHideLemma);
				setState(842);
				match(EqOp);
				setState(843);
				identifier();
				}
				break;
			case KeywordHeuristic:
				enterOuterAlt(_localctx, 7);
				{
				setState(844);
				match(KeywordHeuristic);
				setState(845);
				match(EqOp);
				setState(846);
				goalRanking();
				}
				break;
			case KeywordOutput:
				enterOuterAlt(_localctx, 8);
				{
				setState(847);
				match(KeywordOutput);
				setState(848);
				match(EqOp);
				setState(849);
				match(LeftBracket);
				setState(861);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & -16L) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & -4611685984067649537L) != 0) || ((((_la - 146)) & ~0x3f) == 0 && ((1L << (_la - 146)) & 17179865203L) != 0)) {
					{
					setState(850);
					identifier();
					setState(855);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,59,_ctx);
					while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
						if ( _alt==1 ) {
							{
							{
							setState(851);
							match(Comma);
							setState(852);
							identifier();
							}
							} 
						}
						setState(857);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,59,_ctx);
					}
					setState(859);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==Comma) {
						{
						setState(858);
						match(Comma);
						}
					}

					}
				}

				setState(863);
				match(RightBracket);
				}
				break;
			case Left:
				enterOuterAlt(_localctx, 9);
				{
				setState(864);
				match(Left);
				}
				break;
			case Right:
				enterOuterAlt(_localctx, 10);
				{
				setState(865);
				match(Right);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TraceQuantifierContext extends ParserRuleContext {
		public ForallTracesContext forallTraces() {
			return getRuleContext(ForallTracesContext.class,0);
		}
		public ExistsTraceContext existsTrace() {
			return getRuleContext(ExistsTraceContext.class,0);
		}
		public TraceQuantifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_traceQuantifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTraceQuantifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTraceQuantifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTraceQuantifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TraceQuantifierContext traceQuantifier() throws RecognitionException {
		TraceQuantifierContext _localctx = new TraceQuantifierContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_traceQuantifier);
		try {
			setState(870);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ForallTraces:
				enterOuterAlt(_localctx, 1);
				{
				setState(868);
				forallTraces();
				}
				break;
			case ExistsTrace:
				enterOuterAlt(_localctx, 2);
				{
				setState(869);
				existsTrace();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ForallTracesContext extends ParserRuleContext {
		public TerminalNode ForallTraces() { return getToken(TamarinParser.ForallTraces, 0); }
		public ForallTracesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forallTraces; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterForallTraces(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitForallTraces(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitForallTraces(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForallTracesContext forallTraces() throws RecognitionException {
		ForallTracesContext _localctx = new ForallTracesContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_forallTraces);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(872);
			match(ForallTraces);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ExistsTraceContext extends ParserRuleContext {
		public TerminalNode ExistsTrace() { return getToken(TamarinParser.ExistsTrace, 0); }
		public ExistsTraceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_existsTrace; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterExistsTrace(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitExistsTrace(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitExistsTrace(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExistsTraceContext existsTrace() throws RecognitionException {
		ExistsTraceContext _localctx = new ExistsTraceContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_existsTrace);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(874);
			match(ExistsTrace);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LemmaContext extends ParserRuleContext {
		public TerminalNode KeywordLemma() { return getToken(TamarinParser.KeywordLemma, 0); }
		public ModuloEContext moduloE() {
			return getRuleContext(ModuloEContext.class,0);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public List<TerminalNode> DoubleQuote() { return getTokens(TamarinParser.DoubleQuote); }
		public TerminalNode DoubleQuote(int i) {
			return getToken(TamarinParser.DoubleQuote, i);
		}
		public StandardFormulaContext standardFormula() {
			return getRuleContext(StandardFormulaContext.class,0);
		}
		public LemmaAttributesContext lemmaAttributes() {
			return getRuleContext(LemmaAttributesContext.class,0);
		}
		public TraceQuantifierContext traceQuantifier() {
			return getRuleContext(TraceQuantifierContext.class,0);
		}
		public StartProofSkeletonContext startProofSkeleton() {
			return getRuleContext(StartProofSkeletonContext.class,0);
		}
		public LemmaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lemma; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLemma(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLemma(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLemma(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LemmaContext lemma() throws RecognitionException {
		LemmaContext _localctx = new LemmaContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_lemma);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(876);
			match(KeywordLemma);
			setState(877);
			moduloE();
			setState(878);
			identifier();
			setState(880);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LeftBracket) {
				{
				setState(879);
				lemmaAttributes();
				}
			}

			setState(882);
			match(Colon);
			setState(884);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ForallTraces || _la==ExistsTrace) {
				{
				setState(883);
				traceQuantifier();
				}
			}

			setState(886);
			match(DoubleQuote);
			setState(887);
			standardFormula("msgVar", "nodeVar");
			setState(888);
			match(DoubleQuote);
			setState(890);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KeywordBy || ((((_la - 72)) & ~0x3f) == 0 && ((1L << (_la - 72)) & 31L) != 0)) {
				{
				setState(889);
				startProofSkeleton();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StandardFormulaContext extends ParserRuleContext {
		public String varp;
		public String nodep;
		public List<ImplicationContext> implication() {
			return getRuleContexts(ImplicationContext.class);
		}
		public ImplicationContext implication(int i) {
			return getRuleContext(ImplicationContext.class,i);
		}
		public EquivOpContext equivOp() {
			return getRuleContext(EquivOpContext.class,0);
		}
		public StandardFormulaContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public StandardFormulaContext(ParserRuleContext parent, int invokingState, String varp, String nodep) {
			super(parent, invokingState);
			this.varp = varp;
			this.nodep = nodep;
		}
		@Override public int getRuleIndex() { return RULE_standardFormula; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterStandardFormula(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitStandardFormula(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitStandardFormula(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StandardFormulaContext standardFormula(String varp,String nodep) throws RecognitionException {
		StandardFormulaContext _localctx = new StandardFormulaContext(_ctx, getState(), varp, nodep);
		enterRule(_localctx, 110, RULE_standardFormula);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(892);
			implication(_localctx.varp, _localctx.nodep);
			setState(896);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,67,_ctx) ) {
			case 1:
				{
				setState(893);
				equivOp();
				setState(894);
				implication(_localctx.varp, _localctx.nodep);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ImplicationContext extends ParserRuleContext {
		public String varp;
		public String nodep;
		public List<DisjunctionContext> disjunction() {
			return getRuleContexts(DisjunctionContext.class);
		}
		public DisjunctionContext disjunction(int i) {
			return getRuleContext(DisjunctionContext.class,i);
		}
		public List<TerminalNode> ImpliesOp() { return getTokens(TamarinParser.ImpliesOp); }
		public TerminalNode ImpliesOp(int i) {
			return getToken(TamarinParser.ImpliesOp, i);
		}
		public ImplicationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ImplicationContext(ParserRuleContext parent, int invokingState, String varp, String nodep) {
			super(parent, invokingState);
			this.varp = varp;
			this.nodep = nodep;
		}
		@Override public int getRuleIndex() { return RULE_implication; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterImplication(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitImplication(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitImplication(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ImplicationContext implication(String varp,String nodep) throws RecognitionException {
		ImplicationContext _localctx = new ImplicationContext(_ctx, getState(), varp, nodep);
		enterRule(_localctx, 112, RULE_implication);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(898);
			disjunction(_localctx.varp, _localctx.nodep);
			setState(903);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,68,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(899);
					match(ImpliesOp);
					setState(900);
					disjunction(_localctx.varp, _localctx.nodep);
					}
					} 
				}
				setState(905);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,68,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DisjunctionContext extends ParserRuleContext {
		public String varp;
		public String nodep;
		public List<ConjunctionContext> conjunction() {
			return getRuleContexts(ConjunctionContext.class);
		}
		public ConjunctionContext conjunction(int i) {
			return getRuleContext(ConjunctionContext.class,i);
		}
		public List<OrOpContext> orOp() {
			return getRuleContexts(OrOpContext.class);
		}
		public OrOpContext orOp(int i) {
			return getRuleContext(OrOpContext.class,i);
		}
		public DisjunctionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public DisjunctionContext(ParserRuleContext parent, int invokingState, String varp, String nodep) {
			super(parent, invokingState);
			this.varp = varp;
			this.nodep = nodep;
		}
		@Override public int getRuleIndex() { return RULE_disjunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterDisjunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitDisjunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitDisjunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DisjunctionContext disjunction(String varp,String nodep) throws RecognitionException {
		DisjunctionContext _localctx = new DisjunctionContext(_ctx, getState(), varp, nodep);
		enterRule(_localctx, 114, RULE_disjunction);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(906);
			conjunction(_localctx.varp, _localctx.nodep);
			setState(912);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,69,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(907);
					orOp();
					setState(908);
					conjunction(_localctx.varp, _localctx.nodep);
					}
					} 
				}
				setState(914);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,69,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ConjunctionContext extends ParserRuleContext {
		public String varp;
		public String nodep;
		public List<NegationContext> negation() {
			return getRuleContexts(NegationContext.class);
		}
		public NegationContext negation(int i) {
			return getRuleContext(NegationContext.class,i);
		}
		public List<AndOpContext> andOp() {
			return getRuleContexts(AndOpContext.class);
		}
		public AndOpContext andOp(int i) {
			return getRuleContext(AndOpContext.class,i);
		}
		public ConjunctionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ConjunctionContext(ParserRuleContext parent, int invokingState, String varp, String nodep) {
			super(parent, invokingState);
			this.varp = varp;
			this.nodep = nodep;
		}
		@Override public int getRuleIndex() { return RULE_conjunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterConjunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitConjunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitConjunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConjunctionContext conjunction(String varp,String nodep) throws RecognitionException {
		ConjunctionContext _localctx = new ConjunctionContext(_ctx, getState(), varp, nodep);
		enterRule(_localctx, 116, RULE_conjunction);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(915);
			negation(_localctx.varp, _localctx.nodep);
			setState(921);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,70,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(916);
					andOp();
					setState(917);
					negation(_localctx.varp, _localctx.nodep);
					}
					} 
				}
				setState(923);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,70,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class NegationContext extends ParserRuleContext {
		public String varp;
		public String nodep;
		public FatomContext fatom() {
			return getRuleContext(FatomContext.class,0);
		}
		public NotOpContext notOp() {
			return getRuleContext(NotOpContext.class,0);
		}
		public NegationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public NegationContext(ParserRuleContext parent, int invokingState, String varp, String nodep) {
			super(parent, invokingState);
			this.varp = varp;
			this.nodep = nodep;
		}
		@Override public int getRuleIndex() { return RULE_negation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterNegation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitNegation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitNegation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NegationContext negation(String varp,String nodep) throws RecognitionException {
		NegationContext _localctx = new NegationContext(_ctx, getState(), varp, nodep);
		enterRule(_localctx, 118, RULE_negation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(925);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,71,_ctx) ) {
			case 1:
				{
				setState(924);
				notOp();
				}
				break;
			}
			setState(927);
			fatom(_localctx.varp, _localctx.nodep);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FatomContext extends ParserRuleContext {
		public String varp;
		public String nodep;
		public BotOpContext botOp() {
			return getRuleContext(BotOpContext.class,0);
		}
		public FOpContext fOp() {
			return getRuleContext(FOpContext.class,0);
		}
		public TopOpContext topOp() {
			return getRuleContext(TopOpContext.class,0);
		}
		public TOpContext tOp() {
			return getRuleContext(TOpContext.class,0);
		}
		public BlatomContext blatom() {
			return getRuleContext(BlatomContext.class,0);
		}
		public QuantificationContext quantification() {
			return getRuleContext(QuantificationContext.class,0);
		}
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public StandardFormulaContext standardFormula() {
			return getRuleContext(StandardFormulaContext.class,0);
		}
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public FatomContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public FatomContext(ParserRuleContext parent, int invokingState, String varp, String nodep) {
			super(parent, invokingState);
			this.varp = varp;
			this.nodep = nodep;
		}
		@Override public int getRuleIndex() { return RULE_fatom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFatom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFatom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFatom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FatomContext fatom(String varp,String nodep) throws RecognitionException {
		FatomContext _localctx = new FatomContext(_ctx, getState(), varp, nodep);
		enterRule(_localctx, 120, RULE_fatom);
		try {
			setState(943);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,74,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(931);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case BotOp:
					{
					setState(929);
					botOp();
					}
					break;
				case FOp:
					{
					setState(930);
					fOp();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(935);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case TopOp:
					{
					setState(933);
					topOp();
					}
					break;
				case TOp:
					{
					setState(934);
					tOp();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(937);
				blatom(_localctx.varp, _localctx.nodep);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(938);
				quantification(_localctx.varp, _localctx.nodep);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(939);
				match(LeftParenthesis);
				setState(940);
				standardFormula(_localctx.varp, _localctx.nodep);
				setState(941);
				match(RightParenthesis);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BlatomContext extends ParserRuleContext {
		public String varp;
		public String nodep;
		public BlatomContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public BlatomContext(ParserRuleContext parent, int invokingState, String varp, String nodep) {
			super(parent, invokingState);
			this.varp = varp;
			this.nodep = nodep;
		}
		@Override public int getRuleIndex() { return RULE_blatom; }
	 
		public BlatomContext() { }
		public void copyFrom(BlatomContext ctx) {
			super.copyFrom(ctx);
			this.varp = ctx.varp;
			this.nodep = ctx.nodep;
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Blatom_smallerpContext extends BlatomContext {
		public SmallerpContext smallerp() {
			return getRuleContext(SmallerpContext.class,0);
		}
		public Blatom_smallerpContext(BlatomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBlatom_smallerp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBlatom_smallerp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBlatom_smallerp(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Blatom_lessContext extends BlatomContext {
		public List<LiteralContext> literal() {
			return getRuleContexts(LiteralContext.class);
		}
		public LiteralContext literal(int i) {
			return getRuleContext(LiteralContext.class,i);
		}
		public TerminalNode LessOp() { return getToken(TamarinParser.LessOp, 0); }
		public Blatom_lessContext(BlatomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBlatom_less(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBlatom_less(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBlatom_less(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Blatom_node_eqContext extends BlatomContext {
		public List<LiteralContext> literal() {
			return getRuleContexts(LiteralContext.class);
		}
		public LiteralContext literal(int i) {
			return getRuleContext(LiteralContext.class,i);
		}
		public EqOpContext eqOp() {
			return getRuleContext(EqOpContext.class,0);
		}
		public Blatom_node_eqContext(BlatomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBlatom_node_eq(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBlatom_node_eq(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBlatom_node_eq(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Blatom_term_eqContext extends BlatomContext {
		public List<MsettermContext> msetterm() {
			return getRuleContexts(MsettermContext.class);
		}
		public MsettermContext msetterm(int i) {
			return getRuleContext(MsettermContext.class,i);
		}
		public EqOpContext eqOp() {
			return getRuleContext(EqOpContext.class,0);
		}
		public Blatom_term_eqContext(BlatomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBlatom_term_eq(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBlatom_term_eq(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBlatom_term_eq(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Blatom_lastContext extends BlatomContext {
		public TerminalNode KeywordLast() { return getToken(TamarinParser.KeywordLast, 0); }
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public Blatom_lastContext(BlatomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBlatom_last(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBlatom_last(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBlatom_last(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Blatom_predicateContext extends BlatomContext {
		public FactContext fact() {
			return getRuleContext(FactContext.class,0);
		}
		public Blatom_predicateContext(BlatomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBlatom_predicate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBlatom_predicate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBlatom_predicate(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Blatom_actionContext extends BlatomContext {
		public FactContext fact() {
			return getRuleContext(FactContext.class,0);
		}
		public TerminalNode AtOp() { return getToken(TamarinParser.AtOp, 0); }
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public Blatom_actionContext(BlatomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBlatom_action(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBlatom_action(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBlatom_action(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlatomContext blatom(String varp,String nodep) throws RecognitionException {
		BlatomContext _localctx = new BlatomContext(_ctx, getState(), varp, nodep);
		enterRule(_localctx, 122, RULE_blatom);
		try {
			setState(968);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,75,_ctx) ) {
			case 1:
				_localctx = new Blatom_lastContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(945);
				match(KeywordLast);
				setState(946);
				match(LeftParenthesis);
				setState(947);
				literal(_localctx.nodep);
				setState(948);
				match(RightParenthesis);
				}
				break;
			case 2:
				_localctx = new Blatom_actionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(950);
				fact("vlit" + _localctx.varp);
				setState(951);
				match(AtOp);
				setState(952);
				literal(_localctx.nodep);
				}
				break;
			case 3:
				_localctx = new Blatom_predicateContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(954);
				fact("vlit" + _localctx.varp + _localctx.nodep);
				}
				break;
			case 4:
				_localctx = new Blatom_lessContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(955);
				literal(_localctx.nodep);
				setState(956);
				match(LessOp);
				setState(957);
				literal(_localctx.nodep);
				}
				break;
			case 5:
				_localctx = new Blatom_smallerpContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(959);
				smallerp(_localctx.varp);
				}
				break;
			case 6:
				_localctx = new Blatom_node_eqContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(960);
				literal("vlit" + _localctx.varp + _localctx.nodep);
				setState(961);
				eqOp();
				setState(962);
				literal("vlit" + _localctx.varp + _localctx.nodep);
				}
				break;
			case 7:
				_localctx = new Blatom_term_eqContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(964);
				msetterm("vlit" + _localctx.varp);
				setState(965);
				eqOp();
				setState(966);
				msetterm("vlit" + _localctx.varp);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class QuantificationContext extends ParserRuleContext {
		public String varp;
		public String nodep;
		public LiteralsContext literals() {
			return getRuleContext(LiteralsContext.class,0);
		}
		public TerminalNode Dot() { return getToken(TamarinParser.Dot, 0); }
		public StandardFormulaContext standardFormula() {
			return getRuleContext(StandardFormulaContext.class,0);
		}
		public ForallOpContext forallOp() {
			return getRuleContext(ForallOpContext.class,0);
		}
		public ExistsOpContext existsOp() {
			return getRuleContext(ExistsOpContext.class,0);
		}
		public QuantificationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public QuantificationContext(ParserRuleContext parent, int invokingState, String varp, String nodep) {
			super(parent, invokingState);
			this.varp = varp;
			this.nodep = nodep;
		}
		@Override public int getRuleIndex() { return RULE_quantification; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterQuantification(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitQuantification(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitQuantification(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QuantificationContext quantification(String varp,String nodep) throws RecognitionException {
		QuantificationContext _localctx = new QuantificationContext(_ctx, getState(), varp, nodep);
		enterRule(_localctx, 124, RULE_quantification);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(972);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ForallOp:
				{
				setState(970);
				forallOp();
				}
				break;
			case ExistsOp:
				{
				setState(971);
				existsOp();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(974);
			literals(_localctx.varp + _localctx.nodep);
			setState(975);
			match(Dot);
			setState(976);
			standardFormula(_localctx.varp, _localctx.nodep);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SmallerpContext extends ParserRuleContext {
		public String varp;
		public List<MsettermContext> msetterm() {
			return getRuleContexts(MsettermContext.class);
		}
		public MsettermContext msetterm(int i) {
			return getRuleContext(MsettermContext.class,i);
		}
		public TerminalNode LessTermOp() { return getToken(TamarinParser.LessTermOp, 0); }
		public SmallerpContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public SmallerpContext(ParserRuleContext parent, int invokingState, String varp) {
			super(parent, invokingState);
			this.varp = varp;
		}
		@Override public int getRuleIndex() { return RULE_smallerp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSmallerp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSmallerp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSmallerp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SmallerpContext smallerp(String varp) throws RecognitionException {
		SmallerpContext _localctx = new SmallerpContext(_ctx, getState(), varp);
		enterRule(_localctx, 126, RULE_smallerp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(978);
			msetterm("vlit" + _localctx.varp);
			setState(979);
			match(LessTermOp);
			setState(980);
			msetterm("vlit" + _localctx.varp);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class CaseTestContext extends ParserRuleContext {
		public TerminalNode KeywordTest() { return getToken(TamarinParser.KeywordTest, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public List<TerminalNode> DoubleQuote() { return getTokens(TamarinParser.DoubleQuote); }
		public TerminalNode DoubleQuote(int i) {
			return getToken(TamarinParser.DoubleQuote, i);
		}
		public StandardFormulaContext standardFormula() {
			return getRuleContext(StandardFormulaContext.class,0);
		}
		public CaseTestContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caseTest; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterCaseTest(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitCaseTest(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitCaseTest(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CaseTestContext caseTest() throws RecognitionException {
		CaseTestContext _localctx = new CaseTestContext(_ctx, getState());
		enterRule(_localctx, 128, RULE_caseTest);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(982);
			match(KeywordTest);
			setState(983);
			identifier();
			setState(984);
			match(Colon);
			setState(985);
			match(DoubleQuote);
			setState(986);
			standardFormula("msgVar", "nodeVar");
			setState(987);
			match(DoubleQuote);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LemmaAccContext extends ParserRuleContext {
		public TerminalNode KeywordLemma() { return getToken(TamarinParser.KeywordLemma, 0); }
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public TerminalNode KeywordAccountsFor() { return getToken(TamarinParser.KeywordAccountsFor, 0); }
		public List<TerminalNode> DoubleQuote() { return getTokens(TamarinParser.DoubleQuote); }
		public TerminalNode DoubleQuote(int i) {
			return getToken(TamarinParser.DoubleQuote, i);
		}
		public StandardFormulaContext standardFormula() {
			return getRuleContext(StandardFormulaContext.class,0);
		}
		public LemmaAttributesContext lemmaAttributes() {
			return getRuleContext(LemmaAttributesContext.class,0);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public LemmaAccContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lemmaAcc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLemmaAcc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLemmaAcc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLemmaAcc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LemmaAccContext lemmaAcc() throws RecognitionException {
		LemmaAccContext _localctx = new LemmaAccContext(_ctx, getState());
		enterRule(_localctx, 130, RULE_lemmaAcc);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(989);
			match(KeywordLemma);
			setState(990);
			identifier();
			setState(992);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LeftBracket) {
				{
				setState(991);
				lemmaAttributes();
				}
			}

			setState(994);
			match(Colon);
			setState(995);
			identifier();
			setState(1000);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,78,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(996);
					match(Comma);
					setState(997);
					identifier();
					}
					} 
				}
				setState(1002);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,78,_ctx);
			}
			setState(1004);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Comma) {
				{
				setState(1003);
				match(Comma);
				}
			}

			setState(1006);
			match(KeywordAccountsFor);
			setState(1007);
			match(DoubleQuote);
			setState(1008);
			standardFormula("msgVar", "nodeVar");
			setState(1009);
			match(DoubleQuote);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TacticContext extends ParserRuleContext {
		public TacticNameContext tacticName() {
			return getRuleContext(TacticNameContext.class,0);
		}
		public SelectedPresortContext selectedPresort() {
			return getRuleContext(SelectedPresortContext.class,0);
		}
		public List<PrioContext> prio() {
			return getRuleContexts(PrioContext.class);
		}
		public PrioContext prio(int i) {
			return getRuleContext(PrioContext.class,i);
		}
		public List<DeprioContext> deprio() {
			return getRuleContexts(DeprioContext.class);
		}
		public DeprioContext deprio(int i) {
			return getRuleContext(DeprioContext.class,i);
		}
		public TacticContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tactic; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTactic(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTactic(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTactic(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TacticContext tactic() throws RecognitionException {
		TacticContext _localctx = new TacticContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_tactic);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1011);
			tacticName();
			setState(1013);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KeywordPresort) {
				{
				setState(1012);
				selectedPresort();
				}
			}

			setState(1018);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==KeywordPrio) {
				{
				{
				setState(1015);
				prio();
				}
				}
				setState(1020);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1024);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==KeywordDeprio) {
				{
				{
				setState(1021);
				deprio();
				}
				}
				setState(1026);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DeprioContext extends ParserRuleContext {
		public TerminalNode KeywordDeprio() { return getToken(TamarinParser.KeywordDeprio, 0); }
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public TerminalNode LeftBrace() { return getToken(TamarinParser.LeftBrace, 0); }
		public RankingIdentifierContext rankingIdentifier() {
			return getRuleContext(RankingIdentifierContext.class,0);
		}
		public TerminalNode RightBrace() { return getToken(TamarinParser.RightBrace, 0); }
		public List<TacticDisjunctsContext> tacticDisjuncts() {
			return getRuleContexts(TacticDisjunctsContext.class);
		}
		public TacticDisjunctsContext tacticDisjuncts(int i) {
			return getRuleContext(TacticDisjunctsContext.class,i);
		}
		public DeprioContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_deprio; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterDeprio(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitDeprio(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitDeprio(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeprioContext deprio() throws RecognitionException {
		DeprioContext _localctx = new DeprioContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_deprio);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1027);
			match(KeywordDeprio);
			setState(1028);
			match(Colon);
			setState(1033);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LeftBrace) {
				{
				setState(1029);
				match(LeftBrace);
				setState(1030);
				rankingIdentifier();
				setState(1031);
				match(RightBrace);
				}
			}

			setState(1036); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(1035);
				tacticDisjuncts();
				}
				}
				setState(1038); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TacticFunctionName || _la==NotOp );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class PrioContext extends ParserRuleContext {
		public TerminalNode KeywordPrio() { return getToken(TamarinParser.KeywordPrio, 0); }
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public TerminalNode LeftBrace() { return getToken(TamarinParser.LeftBrace, 0); }
		public RankingIdentifierContext rankingIdentifier() {
			return getRuleContext(RankingIdentifierContext.class,0);
		}
		public TerminalNode RightBrace() { return getToken(TamarinParser.RightBrace, 0); }
		public List<TacticDisjunctsContext> tacticDisjuncts() {
			return getRuleContexts(TacticDisjunctsContext.class);
		}
		public TacticDisjunctsContext tacticDisjuncts(int i) {
			return getRuleContext(TacticDisjunctsContext.class,i);
		}
		public PrioContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prio; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterPrio(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitPrio(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitPrio(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrioContext prio() throws RecognitionException {
		PrioContext _localctx = new PrioContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_prio);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1040);
			match(KeywordPrio);
			setState(1041);
			match(Colon);
			setState(1046);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LeftBrace) {
				{
				setState(1042);
				match(LeftBrace);
				setState(1043);
				rankingIdentifier();
				setState(1044);
				match(RightBrace);
				}
			}

			setState(1049); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(1048);
				tacticDisjuncts();
				}
				}
				setState(1051); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TacticFunctionName || _la==NotOp );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TacticDisjunctsContext extends ParserRuleContext {
		public List<TacticConjunctsContext> tacticConjuncts() {
			return getRuleContexts(TacticConjunctsContext.class);
		}
		public TacticConjunctsContext tacticConjuncts(int i) {
			return getRuleContext(TacticConjunctsContext.class,i);
		}
		public List<OrOpContext> orOp() {
			return getRuleContexts(OrOpContext.class);
		}
		public OrOpContext orOp(int i) {
			return getRuleContext(OrOpContext.class,i);
		}
		public TacticDisjunctsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tacticDisjuncts; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTacticDisjuncts(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTacticDisjuncts(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTacticDisjuncts(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TacticDisjunctsContext tacticDisjuncts() throws RecognitionException {
		TacticDisjunctsContext _localctx = new TacticDisjunctsContext(_ctx, getState());
		enterRule(_localctx, 138, RULE_tacticDisjuncts);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1053);
			tacticConjuncts();
			setState(1059);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==OrOp || _la==PipeOp) {
				{
				{
				setState(1054);
				orOp();
				setState(1055);
				tacticConjuncts();
				}
				}
				setState(1061);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TacticConjunctsContext extends ParserRuleContext {
		public List<TacticNegationContext> tacticNegation() {
			return getRuleContexts(TacticNegationContext.class);
		}
		public TacticNegationContext tacticNegation(int i) {
			return getRuleContext(TacticNegationContext.class,i);
		}
		public List<TerminalNode> AndOp() { return getTokens(TamarinParser.AndOp); }
		public TerminalNode AndOp(int i) {
			return getToken(TamarinParser.AndOp, i);
		}
		public TacticConjunctsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tacticConjuncts; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTacticConjuncts(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTacticConjuncts(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTacticConjuncts(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TacticConjunctsContext tacticConjuncts() throws RecognitionException {
		TacticConjunctsContext _localctx = new TacticConjunctsContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_tacticConjuncts);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1062);
			tacticNegation();
			setState(1067);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AndOp) {
				{
				{
				setState(1063);
				match(AndOp);
				setState(1064);
				tacticNegation();
				}
				}
				setState(1069);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TacticNegationContext extends ParserRuleContext {
		public TacticFunctionContext tacticFunction() {
			return getRuleContext(TacticFunctionContext.class,0);
		}
		public NotOpContext notOp() {
			return getRuleContext(NotOpContext.class,0);
		}
		public TacticNegationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tacticNegation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTacticNegation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTacticNegation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTacticNegation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TacticNegationContext tacticNegation() throws RecognitionException {
		TacticNegationContext _localctx = new TacticNegationContext(_ctx, getState());
		enterRule(_localctx, 142, RULE_tacticNegation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1071);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==NotOp) {
				{
				setState(1070);
				notOp();
				}
			}

			setState(1073);
			tacticFunction();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TacticFunctionContext extends ParserRuleContext {
		public TacticFunctionNameContext tacticFunctionName() {
			return getRuleContext(TacticFunctionNameContext.class,0);
		}
		public List<TerminalNode> DoubleQuote() { return getTokens(TamarinParser.DoubleQuote); }
		public TerminalNode DoubleQuote(int i) {
			return getToken(TamarinParser.DoubleQuote, i);
		}
		public List<TacticFunctionIdentifierContext> tacticFunctionIdentifier() {
			return getRuleContexts(TacticFunctionIdentifierContext.class);
		}
		public TacticFunctionIdentifierContext tacticFunctionIdentifier(int i) {
			return getRuleContext(TacticFunctionIdentifierContext.class,i);
		}
		public TacticFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tacticFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTacticFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTacticFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTacticFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TacticFunctionContext tacticFunction() throws RecognitionException {
		TacticFunctionContext _localctx = new TacticFunctionContext(_ctx, getState());
		enterRule(_localctx, 144, RULE_tacticFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1075);
			tacticFunctionName();
			setState(1080); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(1076);
				match(DoubleQuote);
				setState(1077);
				tacticFunctionIdentifier();
				setState(1078);
				match(DoubleQuote);
				}
				}
				setState(1082); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==DoubleQuote );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SelectedPresortContext extends ParserRuleContext {
		public TerminalNode KeywordPresort() { return getToken(TamarinParser.KeywordPresort, 0); }
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public GoalRankingPresortContext goalRankingPresort() {
			return getRuleContext(GoalRankingPresortContext.class,0);
		}
		public SelectedPresortContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectedPresort; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSelectedPresort(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSelectedPresort(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSelectedPresort(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectedPresortContext selectedPresort() throws RecognitionException {
		SelectedPresortContext _localctx = new SelectedPresortContext(_ctx, getState());
		enterRule(_localctx, 146, RULE_selectedPresort);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1084);
			match(KeywordPresort);
			setState(1085);
			match(Colon);
			setState(1086);
			goalRankingPresort();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class GoalRankingPresortContext extends ParserRuleContext {
		public TerminalNode GoalRankingIdentifierNoOracle() { return getToken(TamarinParser.GoalRankingIdentifierNoOracle, 0); }
		public GoalRankingPresortContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_goalRankingPresort; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterGoalRankingPresort(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitGoalRankingPresort(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitGoalRankingPresort(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GoalRankingPresortContext goalRankingPresort() throws RecognitionException {
		GoalRankingPresortContext _localctx = new GoalRankingPresortContext(_ctx, getState());
		enterRule(_localctx, 148, RULE_goalRankingPresort);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1088);
			match(GoalRankingIdentifierNoOracle);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TacticFunctionNameContext extends ParserRuleContext {
		public TerminalNode TacticFunctionName() { return getToken(TamarinParser.TacticFunctionName, 0); }
		public TacticFunctionNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tacticFunctionName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTacticFunctionName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTacticFunctionName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTacticFunctionName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TacticFunctionNameContext tacticFunctionName() throws RecognitionException {
		TacticFunctionNameContext _localctx = new TacticFunctionNameContext(_ctx, getState());
		enterRule(_localctx, 150, RULE_tacticFunctionName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1090);
			match(TacticFunctionName);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class RankingIdentifierContext extends ParserRuleContext {
		public TerminalNode KeywordSmallest() { return getToken(TamarinParser.KeywordSmallest, 0); }
		public TerminalNode KeywordId() { return getToken(TamarinParser.KeywordId, 0); }
		public RankingIdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rankingIdentifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterRankingIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitRankingIdentifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitRankingIdentifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RankingIdentifierContext rankingIdentifier() throws RecognitionException {
		RankingIdentifierContext _localctx = new RankingIdentifierContext(_ctx, getState());
		enterRule(_localctx, 152, RULE_rankingIdentifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1092);
			_la = _input.LA(1);
			if ( !(_la==KeywordSmallest || _la==KeywordId) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TacticNameContext extends ParserRuleContext {
		public TerminalNode KeywordTactic() { return getToken(TamarinParser.KeywordTactic, 0); }
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TacticNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tacticName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTacticName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTacticName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTacticName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TacticNameContext tacticName() throws RecognitionException {
		TacticNameContext _localctx = new TacticNameContext(_ctx, getState());
		enterRule(_localctx, 154, RULE_tacticName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1094);
			match(KeywordTactic);
			setState(1095);
			match(Colon);
			setState(1096);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class HeuristicContext extends ParserRuleContext {
		public TerminalNode KeywordHeuristic() { return getToken(TamarinParser.KeywordHeuristic, 0); }
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public List<GoalRankingContext> goalRanking() {
			return getRuleContexts(GoalRankingContext.class);
		}
		public GoalRankingContext goalRanking(int i) {
			return getRuleContext(GoalRankingContext.class,i);
		}
		public HeuristicContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_heuristic; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterHeuristic(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitHeuristic(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitHeuristic(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HeuristicContext heuristic() throws RecognitionException {
		HeuristicContext _localctx = new HeuristicContext(_ctx, getState());
		enterRule(_localctx, 156, RULE_heuristic);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1098);
			match(KeywordHeuristic);
			setState(1099);
			match(Colon);
			setState(1101); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(1100);
				goalRanking();
				}
				}
				setState(1103); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( ((((_la - 59)) & ~0x3f) == 0 && ((1L << (_la - 59)) & 1125899906842631L) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class GoalRankingContext extends ParserRuleContext {
		public OracleRankingContext oracleRanking() {
			return getRuleContext(OracleRankingContext.class,0);
		}
		public InternalTacticRankingContext internalTacticRanking() {
			return getRuleContext(InternalTacticRankingContext.class,0);
		}
		public RegularRankingContext regularRanking() {
			return getRuleContext(RegularRankingContext.class,0);
		}
		public GoalRankingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_goalRanking; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterGoalRanking(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitGoalRanking(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitGoalRanking(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GoalRankingContext goalRanking() throws RecognitionException {
		GoalRankingContext _localctx = new GoalRankingContext(_ctx, getState());
		enterRule(_localctx, 158, RULE_goalRanking);
		try {
			setState(1108);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case GoalRankingIdentifierOracle:
				enterOuterAlt(_localctx, 1);
				{
				setState(1105);
				oracleRanking();
				}
				break;
			case LeftBrace:
				enterOuterAlt(_localctx, 2);
				{
				setState(1106);
				internalTacticRanking();
				}
				break;
			case GoalRankingIdentifierNoOracle:
			case GoalRankingIdentifiersNoOracle:
				enterOuterAlt(_localctx, 3);
				{
				setState(1107);
				regularRanking();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class RegularRankingContext extends ParserRuleContext {
		public TerminalNode GoalRankingIdentifiersNoOracle() { return getToken(TamarinParser.GoalRankingIdentifiersNoOracle, 0); }
		public TerminalNode GoalRankingIdentifierNoOracle() { return getToken(TamarinParser.GoalRankingIdentifierNoOracle, 0); }
		public RegularRankingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_regularRanking; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterRegularRanking(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitRegularRanking(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitRegularRanking(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RegularRankingContext regularRanking() throws RecognitionException {
		RegularRankingContext _localctx = new RegularRankingContext(_ctx, getState());
		enterRule(_localctx, 160, RULE_regularRanking);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1110);
			_la = _input.LA(1);
			if ( !(_la==GoalRankingIdentifierNoOracle || _la==GoalRankingIdentifiersNoOracle) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class InternalTacticRankingContext extends ParserRuleContext {
		public TerminalNode LeftBrace() { return getToken(TamarinParser.LeftBrace, 0); }
		public InternalTacticNameContext internalTacticName() {
			return getRuleContext(InternalTacticNameContext.class,0);
		}
		public TerminalNode RightBrace() { return getToken(TamarinParser.RightBrace, 0); }
		public InternalTacticRankingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_internalTacticRanking; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterInternalTacticRanking(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitInternalTacticRanking(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitInternalTacticRanking(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InternalTacticRankingContext internalTacticRanking() throws RecognitionException {
		InternalTacticRankingContext _localctx = new InternalTacticRankingContext(_ctx, getState());
		enterRule(_localctx, 162, RULE_internalTacticRanking);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1112);
			match(LeftBrace);
			setState(1113);
			internalTacticName();
			setState(1114);
			match(RightBrace);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class OracleRankingContext extends ParserRuleContext {
		public TerminalNode GoalRankingIdentifierOracle() { return getToken(TamarinParser.GoalRankingIdentifierOracle, 0); }
		public List<TerminalNode> DoubleQuote() { return getTokens(TamarinParser.DoubleQuote); }
		public TerminalNode DoubleQuote(int i) {
			return getToken(TamarinParser.DoubleQuote, i);
		}
		public OracleRelativePathContext oracleRelativePath() {
			return getRuleContext(OracleRelativePathContext.class,0);
		}
		public OracleRankingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oracleRanking; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterOracleRanking(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitOracleRanking(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitOracleRanking(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OracleRankingContext oracleRanking() throws RecognitionException {
		OracleRankingContext _localctx = new OracleRankingContext(_ctx, getState());
		enterRule(_localctx, 164, RULE_oracleRanking);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1116);
			match(GoalRankingIdentifierOracle);
			setState(1121);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DoubleQuote) {
				{
				setState(1117);
				match(DoubleQuote);
				setState(1118);
				oracleRelativePath();
				setState(1119);
				match(DoubleQuote);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ExportContext extends ParserRuleContext {
		public TerminalNode KeywordExport() { return getToken(TamarinParser.KeywordExport, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public List<TerminalNode> DoubleQuote() { return getTokens(TamarinParser.DoubleQuote); }
		public TerminalNode DoubleQuote(int i) {
			return getToken(TamarinParser.DoubleQuote, i);
		}
		public ExportBodyCharsContext exportBodyChars() {
			return getRuleContext(ExportBodyCharsContext.class,0);
		}
		public ExportContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_export; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterExport(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitExport(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitExport(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExportContext export() throws RecognitionException {
		ExportContext _localctx = new ExportContext(_ctx, getState());
		enterRule(_localctx, 166, RULE_export);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1123);
			match(KeywordExport);
			setState(1124);
			identifier();
			setState(1125);
			match(Colon);
			setState(1126);
			match(DoubleQuote);
			setState(1127);
			exportBodyChars();
			setState(1128);
			match(DoubleQuote);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class PredicateDeclarationContext extends ParserRuleContext {
		public TerminalNode KeywordPredicate() { return getToken(TamarinParser.KeywordPredicate, 0); }
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public List<PredicateContext> predicate() {
			return getRuleContexts(PredicateContext.class);
		}
		public PredicateContext predicate(int i) {
			return getRuleContext(PredicateContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public PredicateDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicateDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterPredicateDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitPredicateDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitPredicateDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PredicateDeclarationContext predicateDeclaration() throws RecognitionException {
		PredicateDeclarationContext _localctx = new PredicateDeclarationContext(_ctx, getState());
		enterRule(_localctx, 168, RULE_predicateDeclaration);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1130);
			match(KeywordPredicate);
			setState(1131);
			match(Colon);
			setState(1132);
			predicate();
			setState(1137);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,94,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1133);
					match(Comma);
					setState(1134);
					predicate();
					}
					} 
				}
				setState(1139);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,94,_ctx);
			}
			setState(1141);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Comma) {
				{
				setState(1140);
				match(Comma);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class PredicateContext extends ParserRuleContext {
		public FactContext fact() {
			return getRuleContext(FactContext.class,0);
		}
		public TerminalNode EquivOp() { return getToken(TamarinParser.EquivOp, 0); }
		public StandardFormulaContext standardFormula() {
			return getRuleContext(StandardFormulaContext.class,0);
		}
		public PredicateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterPredicate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitPredicate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitPredicate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PredicateContext predicate() throws RecognitionException {
		PredicateContext _localctx = new PredicateContext(_ctx, getState());
		enterRule(_localctx, 170, RULE_predicate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1143);
			fact("logVar");
			setState(1144);
			match(EquivOp);
			setState(1145);
			standardFormula("msgVar", "nodeVar");
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SignatureOptionsContext extends ParserRuleContext {
		public TerminalNode KeywordOptions() { return getToken(TamarinParser.KeywordOptions, 0); }
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public List<BuiltinOptionsContext> builtinOptions() {
			return getRuleContexts(BuiltinOptionsContext.class);
		}
		public BuiltinOptionsContext builtinOptions(int i) {
			return getRuleContext(BuiltinOptionsContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public SignatureOptionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_signatureOptions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSignatureOptions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSignatureOptions(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSignatureOptions(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SignatureOptionsContext signatureOptions() throws RecognitionException {
		SignatureOptionsContext _localctx = new SignatureOptionsContext(_ctx, getState());
		enterRule(_localctx, 172, RULE_signatureOptions);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1147);
			match(KeywordOptions);
			setState(1148);
			match(Colon);
			setState(1149);
			builtinOptions();
			setState(1154);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,96,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1150);
					match(Comma);
					setState(1151);
					builtinOptions();
					}
					} 
				}
				setState(1156);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,96,_ctx);
			}
			setState(1158);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Comma) {
				{
				setState(1157);
				match(Comma);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinOptionsContext extends ParserRuleContext {
		public TerminalNode BuiltinOptions() { return getToken(TamarinParser.BuiltinOptions, 0); }
		public BuiltinOptionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinOptions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinOptions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinOptions(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinOptions(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinOptionsContext builtinOptions() throws RecognitionException {
		BuiltinOptionsContext _localctx = new BuiltinOptionsContext(_ctx, getState());
		enterRule(_localctx, 174, RULE_builtinOptions);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1160);
			match(BuiltinOptions);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class EquationsContext extends ParserRuleContext {
		public TerminalNode KeywordEquations() { return getToken(TamarinParser.KeywordEquations, 0); }
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public List<EquationContext> equation() {
			return getRuleContexts(EquationContext.class);
		}
		public EquationContext equation(int i) {
			return getRuleContext(EquationContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public EquationsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equations; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterEquations(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitEquations(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitEquations(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EquationsContext equations() throws RecognitionException {
		EquationsContext _localctx = new EquationsContext(_ctx, getState());
		enterRule(_localctx, 176, RULE_equations);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1162);
			match(KeywordEquations);
			setState(1163);
			match(Colon);
			setState(1164);
			equation();
			setState(1169);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,98,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1165);
					match(Comma);
					setState(1166);
					equation();
					}
					} 
				}
				setState(1171);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,98,_ctx);
			}
			setState(1173);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Comma) {
				{
				setState(1172);
				match(Comma);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class EquationContext extends ParserRuleContext {
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public EqOpContext eqOp() {
			return getRuleContext(EqOpContext.class,0);
		}
		public EquationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterEquation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitEquation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitEquation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EquationContext equation() throws RecognitionException {
		EquationContext _localctx = new EquationContext(_ctx, getState());
		enterRule(_localctx, 178, RULE_equation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1175);
			term("logicalLiteralNoPub");
			setState(1176);
			eqOp();
			setState(1177);
			term("logicalLiteralNoPub");
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SignatureFunctionsContext extends ParserRuleContext {
		public TerminalNode KeywordFunctions() { return getToken(TamarinParser.KeywordFunctions, 0); }
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public List<SignatureFunctionContext> signatureFunction() {
			return getRuleContexts(SignatureFunctionContext.class);
		}
		public SignatureFunctionContext signatureFunction(int i) {
			return getRuleContext(SignatureFunctionContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public SignatureFunctionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_signatureFunctions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSignatureFunctions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSignatureFunctions(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSignatureFunctions(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SignatureFunctionsContext signatureFunctions() throws RecognitionException {
		SignatureFunctionsContext _localctx = new SignatureFunctionsContext(_ctx, getState());
		enterRule(_localctx, 180, RULE_signatureFunctions);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1179);
			match(KeywordFunctions);
			setState(1180);
			match(Colon);
			setState(1181);
			signatureFunction();
			setState(1186);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,100,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1182);
					match(Comma);
					setState(1183);
					signatureFunction();
					}
					} 
				}
				setState(1188);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,100,_ctx);
			}
			setState(1190);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Comma) {
				{
				setState(1189);
				match(Comma);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SignatureFunctionContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public FunctionTypeContext functionType() {
			return getRuleContext(FunctionTypeContext.class,0);
		}
		public TerminalNode LeftBracket() { return getToken(TamarinParser.LeftBracket, 0); }
		public TerminalNode RightBracket() { return getToken(TamarinParser.RightBracket, 0); }
		public List<FunctionAttributeContext> functionAttribute() {
			return getRuleContexts(FunctionAttributeContext.class);
		}
		public FunctionAttributeContext functionAttribute(int i) {
			return getRuleContext(FunctionAttributeContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public SignatureFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_signatureFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSignatureFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSignatureFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSignatureFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SignatureFunctionContext signatureFunction() throws RecognitionException {
		SignatureFunctionContext _localctx = new SignatureFunctionContext(_ctx, getState());
		enterRule(_localctx, 182, RULE_signatureFunction);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1192);
			identifier();
			setState(1193);
			functionType();
			setState(1209);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LeftBracket) {
				{
				setState(1194);
				match(LeftBracket);
				setState(1206);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==FunctionAttribute) {
					{
					setState(1195);
					functionAttribute();
					setState(1200);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,102,_ctx);
					while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
						if ( _alt==1 ) {
							{
							{
							setState(1196);
							match(Comma);
							setState(1197);
							functionAttribute();
							}
							} 
						}
						setState(1202);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,102,_ctx);
					}
					setState(1204);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==Comma) {
						{
						setState(1203);
						match(Comma);
						}
					}

					}
				}

				setState(1208);
				match(RightBracket);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FunctionAttributeContext extends ParserRuleContext {
		public TerminalNode FunctionAttribute() { return getToken(TamarinParser.FunctionAttribute, 0); }
		public FunctionAttributeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionAttribute; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFunctionAttribute(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFunctionAttribute(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFunctionAttribute(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionAttributeContext functionAttribute() throws RecognitionException {
		FunctionAttributeContext _localctx = new FunctionAttributeContext(_ctx, getState());
		enterRule(_localctx, 184, RULE_functionAttribute);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1211);
			match(FunctionAttribute);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FunctionTypeContext extends ParserRuleContext {
		public FunctionTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionType; }
	 
		public FunctionTypeContext() { }
		public void copyFrom(FunctionTypeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Function_type_naturalContext extends FunctionTypeContext {
		public TerminalNode Slash() { return getToken(TamarinParser.Slash, 0); }
		public NaturalContext natural() {
			return getRuleContext(NaturalContext.class,0);
		}
		public Function_type_naturalContext(FunctionTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFunction_type_natural(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFunction_type_natural(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFunction_type_natural(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Function_type_sapicContext extends FunctionTypeContext {
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public List<SapicTypeContext> sapicType() {
			return getRuleContexts(SapicTypeContext.class);
		}
		public SapicTypeContext sapicType(int i) {
			return getRuleContext(SapicTypeContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public Function_type_sapicContext(FunctionTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFunction_type_sapic(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFunction_type_sapic(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFunction_type_sapic(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionTypeContext functionType() throws RecognitionException {
		FunctionTypeContext _localctx = new FunctionTypeContext(_ctx, getState());
		enterRule(_localctx, 186, RULE_functionType);
		int _la;
		try {
			int _alt;
			setState(1232);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case Slash:
				_localctx = new Function_type_naturalContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(1213);
				match(Slash);
				setState(1214);
				natural();
				}
				break;
			case LeftParenthesis:
				_localctx = new Function_type_sapicContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(1215);
				match(LeftParenthesis);
				setState(1227);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & -16L) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & -4611685984067649537L) != 0) || ((((_la - 146)) & ~0x3f) == 0 && ((1L << (_la - 146)) & 17179865203L) != 0)) {
					{
					setState(1216);
					sapicType();
					setState(1221);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,106,_ctx);
					while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
						if ( _alt==1 ) {
							{
							{
							setState(1217);
							match(Comma);
							setState(1218);
							sapicType();
							}
							} 
						}
						setState(1223);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,106,_ctx);
					}
					setState(1225);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==Comma) {
						{
						setState(1224);
						match(Comma);
						}
					}

					}
				}

				setState(1229);
				match(RightParenthesis);
				setState(1230);
				match(Colon);
				setState(1231);
				sapicType();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinsContext extends ParserRuleContext {
		public TerminalNode KeywordBuiltins() { return getToken(TamarinParser.KeywordBuiltins, 0); }
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public List<BuiltinNamesContext> builtinNames() {
			return getRuleContexts(BuiltinNamesContext.class);
		}
		public BuiltinNamesContext builtinNames(int i) {
			return getRuleContext(BuiltinNamesContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public BuiltinsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtins; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltins(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltins(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltins(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinsContext builtins() throws RecognitionException {
		BuiltinsContext _localctx = new BuiltinsContext(_ctx, getState());
		enterRule(_localctx, 188, RULE_builtins);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1234);
			match(KeywordBuiltins);
			setState(1235);
			match(Colon);
			setState(1236);
			builtinNames();
			setState(1241);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,110,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1237);
					match(Comma);
					setState(1238);
					builtinNames();
					}
					} 
				}
				setState(1243);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,110,_ctx);
			}
			setState(1245);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Comma) {
				{
				setState(1244);
				match(Comma);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNamesContext extends ParserRuleContext {
		public BuiltinNameLocationsReportContext builtinNameLocationsReport() {
			return getRuleContext(BuiltinNameLocationsReportContext.class,0);
		}
		public BuiltinNameReliableChannelContext builtinNameReliableChannel() {
			return getRuleContext(BuiltinNameReliableChannelContext.class,0);
		}
		public BuiltinNameDHContext builtinNameDH() {
			return getRuleContext(BuiltinNameDHContext.class,0);
		}
		public BuiltinNameBilinearPairingContext builtinNameBilinearPairing() {
			return getRuleContext(BuiltinNameBilinearPairingContext.class,0);
		}
		public BuiltinNameMultisetContext builtinNameMultiset() {
			return getRuleContext(BuiltinNameMultisetContext.class,0);
		}
		public BuiltinNameSymmetricEncryptionContext builtinNameSymmetricEncryption() {
			return getRuleContext(BuiltinNameSymmetricEncryptionContext.class,0);
		}
		public BuiltinNameAsymmetricEncryptionContext builtinNameAsymmetricEncryption() {
			return getRuleContext(BuiltinNameAsymmetricEncryptionContext.class,0);
		}
		public BuiltinNameSigningContext builtinNameSigning() {
			return getRuleContext(BuiltinNameSigningContext.class,0);
		}
		public BuiltinNameDestPairingContext builtinNameDestPairing() {
			return getRuleContext(BuiltinNameDestPairingContext.class,0);
		}
		public BuiltinNameDestSymmetricEncryptionContext builtinNameDestSymmetricEncryption() {
			return getRuleContext(BuiltinNameDestSymmetricEncryptionContext.class,0);
		}
		public BuiltinNameDestAsymmetricEncryptionContext builtinNameDestAsymmetricEncryption() {
			return getRuleContext(BuiltinNameDestAsymmetricEncryptionContext.class,0);
		}
		public BuiltinNameDestSigningContext builtinNameDestSigning() {
			return getRuleContext(BuiltinNameDestSigningContext.class,0);
		}
		public BuiltinNameRevealingSigningContext builtinNameRevealingSigning() {
			return getRuleContext(BuiltinNameRevealingSigningContext.class,0);
		}
		public BuiltinNameHashingContext builtinNameHashing() {
			return getRuleContext(BuiltinNameHashingContext.class,0);
		}
		public BuiltinNameXorContext builtinNameXor() {
			return getRuleContext(BuiltinNameXorContext.class,0);
		}
		public BuiltinNamesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNames; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNames(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNames(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNames(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNamesContext builtinNames() throws RecognitionException {
		BuiltinNamesContext _localctx = new BuiltinNamesContext(_ctx, getState());
		enterRule(_localctx, 190, RULE_builtinNames);
		try {
			setState(1262);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BuiltinNameLocationsReport:
				enterOuterAlt(_localctx, 1);
				{
				setState(1247);
				builtinNameLocationsReport();
				}
				break;
			case BuiltinNameReliableChannel:
				enterOuterAlt(_localctx, 2);
				{
				setState(1248);
				builtinNameReliableChannel();
				}
				break;
			case BuiltinNameDH:
				enterOuterAlt(_localctx, 3);
				{
				setState(1249);
				builtinNameDH();
				}
				break;
			case BuiltinNameBilinearPairing:
				enterOuterAlt(_localctx, 4);
				{
				setState(1250);
				builtinNameBilinearPairing();
				}
				break;
			case BuiltinNameMultiset:
				enterOuterAlt(_localctx, 5);
				{
				setState(1251);
				builtinNameMultiset();
				}
				break;
			case BuiltinNameSymmetricEncryption:
				enterOuterAlt(_localctx, 6);
				{
				setState(1252);
				builtinNameSymmetricEncryption();
				}
				break;
			case BuiltinNameAsymmetricEncryption:
				enterOuterAlt(_localctx, 7);
				{
				setState(1253);
				builtinNameAsymmetricEncryption();
				}
				break;
			case BuiltinNameSigning:
				enterOuterAlt(_localctx, 8);
				{
				setState(1254);
				builtinNameSigning();
				}
				break;
			case BuiltinNameDestPairing:
				enterOuterAlt(_localctx, 9);
				{
				setState(1255);
				builtinNameDestPairing();
				}
				break;
			case BuiltinNameDestSymmetricEncryption:
				enterOuterAlt(_localctx, 10);
				{
				setState(1256);
				builtinNameDestSymmetricEncryption();
				}
				break;
			case BuiltinNameDestAsymmetricEncryption:
				enterOuterAlt(_localctx, 11);
				{
				setState(1257);
				builtinNameDestAsymmetricEncryption();
				}
				break;
			case BuiltinNameDestSigning:
				enterOuterAlt(_localctx, 12);
				{
				setState(1258);
				builtinNameDestSigning();
				}
				break;
			case BuiltinNameRevealingSigning:
				enterOuterAlt(_localctx, 13);
				{
				setState(1259);
				builtinNameRevealingSigning();
				}
				break;
			case BuiltinNameHashing:
				enterOuterAlt(_localctx, 14);
				{
				setState(1260);
				builtinNameHashing();
				}
				break;
			case KeywordXor:
				enterOuterAlt(_localctx, 15);
				{
				setState(1261);
				builtinNameXor();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameLocationsReportContext extends ParserRuleContext {
		public TerminalNode BuiltinNameLocationsReport() { return getToken(TamarinParser.BuiltinNameLocationsReport, 0); }
		public BuiltinNameLocationsReportContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameLocationsReport; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameLocationsReport(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameLocationsReport(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameLocationsReport(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameLocationsReportContext builtinNameLocationsReport() throws RecognitionException {
		BuiltinNameLocationsReportContext _localctx = new BuiltinNameLocationsReportContext(_ctx, getState());
		enterRule(_localctx, 192, RULE_builtinNameLocationsReport);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1264);
			match(BuiltinNameLocationsReport);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameReliableChannelContext extends ParserRuleContext {
		public TerminalNode BuiltinNameReliableChannel() { return getToken(TamarinParser.BuiltinNameReliableChannel, 0); }
		public BuiltinNameReliableChannelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameReliableChannel; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameReliableChannel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameReliableChannel(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameReliableChannel(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameReliableChannelContext builtinNameReliableChannel() throws RecognitionException {
		BuiltinNameReliableChannelContext _localctx = new BuiltinNameReliableChannelContext(_ctx, getState());
		enterRule(_localctx, 194, RULE_builtinNameReliableChannel);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1266);
			match(BuiltinNameReliableChannel);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameDHContext extends ParserRuleContext {
		public TerminalNode BuiltinNameDH() { return getToken(TamarinParser.BuiltinNameDH, 0); }
		public BuiltinNameDHContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameDH; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameDH(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameDH(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameDH(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameDHContext builtinNameDH() throws RecognitionException {
		BuiltinNameDHContext _localctx = new BuiltinNameDHContext(_ctx, getState());
		enterRule(_localctx, 196, RULE_builtinNameDH);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1268);
			match(BuiltinNameDH);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameBilinearPairingContext extends ParserRuleContext {
		public TerminalNode BuiltinNameBilinearPairing() { return getToken(TamarinParser.BuiltinNameBilinearPairing, 0); }
		public BuiltinNameBilinearPairingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameBilinearPairing; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameBilinearPairing(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameBilinearPairing(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameBilinearPairing(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameBilinearPairingContext builtinNameBilinearPairing() throws RecognitionException {
		BuiltinNameBilinearPairingContext _localctx = new BuiltinNameBilinearPairingContext(_ctx, getState());
		enterRule(_localctx, 198, RULE_builtinNameBilinearPairing);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1270);
			match(BuiltinNameBilinearPairing);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameMultisetContext extends ParserRuleContext {
		public TerminalNode BuiltinNameMultiset() { return getToken(TamarinParser.BuiltinNameMultiset, 0); }
		public BuiltinNameMultisetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameMultiset; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameMultiset(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameMultiset(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameMultiset(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameMultisetContext builtinNameMultiset() throws RecognitionException {
		BuiltinNameMultisetContext _localctx = new BuiltinNameMultisetContext(_ctx, getState());
		enterRule(_localctx, 200, RULE_builtinNameMultiset);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1272);
			match(BuiltinNameMultiset);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameSymmetricEncryptionContext extends ParserRuleContext {
		public TerminalNode BuiltinNameSymmetricEncryption() { return getToken(TamarinParser.BuiltinNameSymmetricEncryption, 0); }
		public BuiltinNameSymmetricEncryptionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameSymmetricEncryption; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameSymmetricEncryption(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameSymmetricEncryption(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameSymmetricEncryption(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameSymmetricEncryptionContext builtinNameSymmetricEncryption() throws RecognitionException {
		BuiltinNameSymmetricEncryptionContext _localctx = new BuiltinNameSymmetricEncryptionContext(_ctx, getState());
		enterRule(_localctx, 202, RULE_builtinNameSymmetricEncryption);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1274);
			match(BuiltinNameSymmetricEncryption);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameAsymmetricEncryptionContext extends ParserRuleContext {
		public TerminalNode BuiltinNameAsymmetricEncryption() { return getToken(TamarinParser.BuiltinNameAsymmetricEncryption, 0); }
		public BuiltinNameAsymmetricEncryptionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameAsymmetricEncryption; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameAsymmetricEncryption(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameAsymmetricEncryption(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameAsymmetricEncryption(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameAsymmetricEncryptionContext builtinNameAsymmetricEncryption() throws RecognitionException {
		BuiltinNameAsymmetricEncryptionContext _localctx = new BuiltinNameAsymmetricEncryptionContext(_ctx, getState());
		enterRule(_localctx, 204, RULE_builtinNameAsymmetricEncryption);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1276);
			match(BuiltinNameAsymmetricEncryption);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameSigningContext extends ParserRuleContext {
		public TerminalNode BuiltinNameSigning() { return getToken(TamarinParser.BuiltinNameSigning, 0); }
		public BuiltinNameSigningContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameSigning; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameSigning(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameSigning(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameSigning(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameSigningContext builtinNameSigning() throws RecognitionException {
		BuiltinNameSigningContext _localctx = new BuiltinNameSigningContext(_ctx, getState());
		enterRule(_localctx, 206, RULE_builtinNameSigning);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1278);
			match(BuiltinNameSigning);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameDestPairingContext extends ParserRuleContext {
		public TerminalNode BuiltinNameDestPairing() { return getToken(TamarinParser.BuiltinNameDestPairing, 0); }
		public BuiltinNameDestPairingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameDestPairing; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameDestPairing(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameDestPairing(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameDestPairing(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameDestPairingContext builtinNameDestPairing() throws RecognitionException {
		BuiltinNameDestPairingContext _localctx = new BuiltinNameDestPairingContext(_ctx, getState());
		enterRule(_localctx, 208, RULE_builtinNameDestPairing);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1280);
			match(BuiltinNameDestPairing);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameDestSymmetricEncryptionContext extends ParserRuleContext {
		public TerminalNode BuiltinNameDestSymmetricEncryption() { return getToken(TamarinParser.BuiltinNameDestSymmetricEncryption, 0); }
		public BuiltinNameDestSymmetricEncryptionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameDestSymmetricEncryption; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameDestSymmetricEncryption(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameDestSymmetricEncryption(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameDestSymmetricEncryption(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameDestSymmetricEncryptionContext builtinNameDestSymmetricEncryption() throws RecognitionException {
		BuiltinNameDestSymmetricEncryptionContext _localctx = new BuiltinNameDestSymmetricEncryptionContext(_ctx, getState());
		enterRule(_localctx, 210, RULE_builtinNameDestSymmetricEncryption);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1282);
			match(BuiltinNameDestSymmetricEncryption);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameDestAsymmetricEncryptionContext extends ParserRuleContext {
		public TerminalNode BuiltinNameDestAsymmetricEncryption() { return getToken(TamarinParser.BuiltinNameDestAsymmetricEncryption, 0); }
		public BuiltinNameDestAsymmetricEncryptionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameDestAsymmetricEncryption; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameDestAsymmetricEncryption(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameDestAsymmetricEncryption(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameDestAsymmetricEncryption(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameDestAsymmetricEncryptionContext builtinNameDestAsymmetricEncryption() throws RecognitionException {
		BuiltinNameDestAsymmetricEncryptionContext _localctx = new BuiltinNameDestAsymmetricEncryptionContext(_ctx, getState());
		enterRule(_localctx, 212, RULE_builtinNameDestAsymmetricEncryption);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1284);
			match(BuiltinNameDestAsymmetricEncryption);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameDestSigningContext extends ParserRuleContext {
		public TerminalNode BuiltinNameDestSigning() { return getToken(TamarinParser.BuiltinNameDestSigning, 0); }
		public BuiltinNameDestSigningContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameDestSigning; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameDestSigning(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameDestSigning(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameDestSigning(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameDestSigningContext builtinNameDestSigning() throws RecognitionException {
		BuiltinNameDestSigningContext _localctx = new BuiltinNameDestSigningContext(_ctx, getState());
		enterRule(_localctx, 214, RULE_builtinNameDestSigning);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1286);
			match(BuiltinNameDestSigning);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameRevealingSigningContext extends ParserRuleContext {
		public TerminalNode BuiltinNameRevealingSigning() { return getToken(TamarinParser.BuiltinNameRevealingSigning, 0); }
		public BuiltinNameRevealingSigningContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameRevealingSigning; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameRevealingSigning(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameRevealingSigning(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameRevealingSigning(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameRevealingSigningContext builtinNameRevealingSigning() throws RecognitionException {
		BuiltinNameRevealingSigningContext _localctx = new BuiltinNameRevealingSigningContext(_ctx, getState());
		enterRule(_localctx, 216, RULE_builtinNameRevealingSigning);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1288);
			match(BuiltinNameRevealingSigning);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameHashingContext extends ParserRuleContext {
		public TerminalNode BuiltinNameHashing() { return getToken(TamarinParser.BuiltinNameHashing, 0); }
		public BuiltinNameHashingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameHashing; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameHashing(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameHashing(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameHashing(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameHashingContext builtinNameHashing() throws RecognitionException {
		BuiltinNameHashingContext _localctx = new BuiltinNameHashingContext(_ctx, getState());
		enterRule(_localctx, 218, RULE_builtinNameHashing);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1290);
			match(BuiltinNameHashing);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinNameXorContext extends ParserRuleContext {
		public TerminalNode KeywordXor() { return getToken(TamarinParser.KeywordXor, 0); }
		public BuiltinNameXorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinNameXor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinNameXor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinNameXor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinNameXor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinNameXorContext builtinNameXor() throws RecognitionException {
		BuiltinNameXorContext _localctx = new BuiltinNameXorContext(_ctx, getState());
		enterRule(_localctx, 220, RULE_builtinNameXor);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1292);
			match(KeywordXor);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ProofSkeletonContext extends ParserRuleContext {
		public SolvedProofContext solvedProof() {
			return getRuleContext(SolvedProofContext.class,0);
		}
		public FinalProofContext finalProof() {
			return getRuleContext(FinalProofContext.class,0);
		}
		public InterProofContext interProof() {
			return getRuleContext(InterProofContext.class,0);
		}
		public ProofSkeletonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_proofSkeleton; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterProofSkeleton(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitProofSkeleton(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitProofSkeleton(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProofSkeletonContext proofSkeleton() throws RecognitionException {
		ProofSkeletonContext _localctx = new ProofSkeletonContext(_ctx, getState());
		enterRule(_localctx, 222, RULE_proofSkeleton);
		try {
			setState(1297);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case SolvedProof:
				enterOuterAlt(_localctx, 1);
				{
				setState(1294);
				solvedProof();
				}
				break;
			case KeywordBy:
				enterOuterAlt(_localctx, 2);
				{
				setState(1295);
				finalProof();
				}
				break;
			case ProofMethodSorry:
			case ProofMethodSimplify:
			case ProofMethodSolve:
			case ProofMethodContradiction:
			case ProofMethodInduction:
				enterOuterAlt(_localctx, 3);
				{
				setState(1296);
				interProof();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StartProofSkeletonContext extends ParserRuleContext {
		public FinalProofContext finalProof() {
			return getRuleContext(FinalProofContext.class,0);
		}
		public InterProofContext interProof() {
			return getRuleContext(InterProofContext.class,0);
		}
		public StartProofSkeletonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_startProofSkeleton; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterStartProofSkeleton(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitStartProofSkeleton(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitStartProofSkeleton(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StartProofSkeletonContext startProofSkeleton() throws RecognitionException {
		StartProofSkeletonContext _localctx = new StartProofSkeletonContext(_ctx, getState());
		enterRule(_localctx, 224, RULE_startProofSkeleton);
		try {
			setState(1301);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case KeywordBy:
				enterOuterAlt(_localctx, 1);
				{
				setState(1299);
				finalProof();
				}
				break;
			case ProofMethodSorry:
			case ProofMethodSimplify:
			case ProofMethodSolve:
			case ProofMethodContradiction:
			case ProofMethodInduction:
				enterOuterAlt(_localctx, 2);
				{
				setState(1300);
				interProof();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SolvedProofContext extends ParserRuleContext {
		public TerminalNode SolvedProof() { return getToken(TamarinParser.SolvedProof, 0); }
		public SolvedProofContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_solvedProof; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSolvedProof(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSolvedProof(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSolvedProof(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SolvedProofContext solvedProof() throws RecognitionException {
		SolvedProofContext _localctx = new SolvedProofContext(_ctx, getState());
		enterRule(_localctx, 226, RULE_solvedProof);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1303);
			match(SolvedProof);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FinalProofContext extends ParserRuleContext {
		public TerminalNode KeywordBy() { return getToken(TamarinParser.KeywordBy, 0); }
		public ProofMethodContext proofMethod() {
			return getRuleContext(ProofMethodContext.class,0);
		}
		public FinalProofContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_finalProof; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFinalProof(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFinalProof(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFinalProof(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FinalProofContext finalProof() throws RecognitionException {
		FinalProofContext _localctx = new FinalProofContext(_ctx, getState());
		enterRule(_localctx, 228, RULE_finalProof);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1305);
			match(KeywordBy);
			setState(1306);
			proofMethod();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class InterProofContext extends ParserRuleContext {
		public ProofMethodContext proofMethod() {
			return getRuleContext(ProofMethodContext.class,0);
		}
		public List<ProofCaseContext> proofCase() {
			return getRuleContexts(ProofCaseContext.class);
		}
		public ProofCaseContext proofCase(int i) {
			return getRuleContext(ProofCaseContext.class,i);
		}
		public TerminalNode KeywordQED() { return getToken(TamarinParser.KeywordQED, 0); }
		public ProofSkeletonContext proofSkeleton() {
			return getRuleContext(ProofSkeletonContext.class,0);
		}
		public List<TerminalNode> KeywordNext() { return getTokens(TamarinParser.KeywordNext); }
		public TerminalNode KeywordNext(int i) {
			return getToken(TamarinParser.KeywordNext, i);
		}
		public InterProofContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_interProof; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterInterProof(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitInterProof(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitInterProof(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InterProofContext interProof() throws RecognitionException {
		InterProofContext _localctx = new InterProofContext(_ctx, getState());
		enterRule(_localctx, 230, RULE_interProof);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1308);
			proofMethod();
			setState(1320);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case KeywordCase:
				{
				setState(1309);
				proofCase();
				setState(1314);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==KeywordNext) {
					{
					{
					setState(1310);
					match(KeywordNext);
					setState(1311);
					proofCase();
					}
					}
					setState(1316);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1317);
				match(KeywordQED);
				}
				break;
			case KeywordBy:
			case SolvedProof:
			case ProofMethodSorry:
			case ProofMethodSimplify:
			case ProofMethodSolve:
			case ProofMethodContradiction:
			case ProofMethodInduction:
				{
				setState(1319);
				proofSkeleton();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ProofCaseContext extends ParserRuleContext {
		public TerminalNode KeywordCase() { return getToken(TamarinParser.KeywordCase, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public ProofSkeletonContext proofSkeleton() {
			return getRuleContext(ProofSkeletonContext.class,0);
		}
		public ProofCaseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_proofCase; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterProofCase(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitProofCase(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitProofCase(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProofCaseContext proofCase() throws RecognitionException {
		ProofCaseContext _localctx = new ProofCaseContext(_ctx, getState());
		enterRule(_localctx, 232, RULE_proofCase);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1322);
			match(KeywordCase);
			setState(1323);
			identifier();
			setState(1324);
			proofSkeleton();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ProofMethodContext extends ParserRuleContext {
		public TerminalNode ProofMethodSorry() { return getToken(TamarinParser.ProofMethodSorry, 0); }
		public TerminalNode ProofMethodSimplify() { return getToken(TamarinParser.ProofMethodSimplify, 0); }
		public TerminalNode ProofMethodSolve() { return getToken(TamarinParser.ProofMethodSolve, 0); }
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public GoalContext goal() {
			return getRuleContext(GoalContext.class,0);
		}
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public TerminalNode ProofMethodContradiction() { return getToken(TamarinParser.ProofMethodContradiction, 0); }
		public TerminalNode ProofMethodInduction() { return getToken(TamarinParser.ProofMethodInduction, 0); }
		public ProofMethodContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_proofMethod; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterProofMethod(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitProofMethod(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitProofMethod(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProofMethodContext proofMethod() throws RecognitionException {
		ProofMethodContext _localctx = new ProofMethodContext(_ctx, getState());
		enterRule(_localctx, 234, RULE_proofMethod);
		try {
			setState(1335);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ProofMethodSorry:
				enterOuterAlt(_localctx, 1);
				{
				setState(1326);
				match(ProofMethodSorry);
				}
				break;
			case ProofMethodSimplify:
				enterOuterAlt(_localctx, 2);
				{
				setState(1327);
				match(ProofMethodSimplify);
				}
				break;
			case ProofMethodSolve:
				enterOuterAlt(_localctx, 3);
				{
				setState(1328);
				match(ProofMethodSolve);
				setState(1329);
				match(LeftParenthesis);
				setState(1330);
				goal();
				setState(1331);
				match(RightParenthesis);
				}
				break;
			case ProofMethodContradiction:
				enterOuterAlt(_localctx, 4);
				{
				setState(1333);
				match(ProofMethodContradiction);
				}
				break;
			case ProofMethodInduction:
				enterOuterAlt(_localctx, 5);
				{
				setState(1334);
				match(ProofMethodInduction);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class GoalContext extends ParserRuleContext {
		public ActionGoalContext actionGoal() {
			return getRuleContext(ActionGoalContext.class,0);
		}
		public PremiseGoalContext premiseGoal() {
			return getRuleContext(PremiseGoalContext.class,0);
		}
		public ChainGoalContext chainGoal() {
			return getRuleContext(ChainGoalContext.class,0);
		}
		public DisjSplitGoalContext disjSplitGoal() {
			return getRuleContext(DisjSplitGoalContext.class,0);
		}
		public EqSplitGoalContext eqSplitGoal() {
			return getRuleContext(EqSplitGoalContext.class,0);
		}
		public GoalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_goal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterGoal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitGoal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitGoal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GoalContext goal() throws RecognitionException {
		GoalContext _localctx = new GoalContext(_ctx, getState());
		enterRule(_localctx, 236, RULE_goal);
		try {
			setState(1342);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,118,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1337);
				actionGoal();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1338);
				premiseGoal();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1339);
				chainGoal();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1340);
				disjSplitGoal();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1341);
				eqSplitGoal();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ActionGoalContext extends ParserRuleContext {
		public FactContext fact() {
			return getRuleContext(FactContext.class,0);
		}
		public TerminalNode AtOp() { return getToken(TamarinParser.AtOp, 0); }
		public NodeVarContext nodeVar() {
			return getRuleContext(NodeVarContext.class,0);
		}
		public ActionGoalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_actionGoal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterActionGoal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitActionGoal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitActionGoal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ActionGoalContext actionGoal() throws RecognitionException {
		ActionGoalContext _localctx = new ActionGoalContext(_ctx, getState());
		enterRule(_localctx, 238, RULE_actionGoal);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1344);
			fact("logicalLiteral");
			setState(1345);
			match(AtOp);
			setState(1346);
			nodeVar();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class PremiseGoalContext extends ParserRuleContext {
		public FactContext fact() {
			return getRuleContext(FactContext.class,0);
		}
		public TerminalNode RequiresOp() { return getToken(TamarinParser.RequiresOp, 0); }
		public TerminalNode NaturalSubscript() { return getToken(TamarinParser.NaturalSubscript, 0); }
		public NodeVarContext nodeVar() {
			return getRuleContext(NodeVarContext.class,0);
		}
		public PremiseGoalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_premiseGoal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterPremiseGoal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitPremiseGoal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitPremiseGoal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PremiseGoalContext premiseGoal() throws RecognitionException {
		PremiseGoalContext _localctx = new PremiseGoalContext(_ctx, getState());
		enterRule(_localctx, 240, RULE_premiseGoal);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1348);
			fact("logicalLiteral");
			setState(1349);
			match(RequiresOp);
			setState(1350);
			match(NaturalSubscript);
			setState(1351);
			nodeVar();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ChainGoalContext extends ParserRuleContext {
		public NodeConclusionContext nodeConclusion() {
			return getRuleContext(NodeConclusionContext.class,0);
		}
		public TerminalNode ChainOp() { return getToken(TamarinParser.ChainOp, 0); }
		public NodePremiseContext nodePremise() {
			return getRuleContext(NodePremiseContext.class,0);
		}
		public ChainGoalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chainGoal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterChainGoal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitChainGoal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitChainGoal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ChainGoalContext chainGoal() throws RecognitionException {
		ChainGoalContext _localctx = new ChainGoalContext(_ctx, getState());
		enterRule(_localctx, 242, RULE_chainGoal);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1353);
			nodeConclusion();
			setState(1354);
			match(ChainOp);
			setState(1355);
			nodePremise();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DisjSplitGoalContext extends ParserRuleContext {
		public List<StandardFormulaContext> standardFormula() {
			return getRuleContexts(StandardFormulaContext.class);
		}
		public StandardFormulaContext standardFormula(int i) {
			return getRuleContext(StandardFormulaContext.class,i);
		}
		public List<TerminalNode> SplitOp() { return getTokens(TamarinParser.SplitOp); }
		public TerminalNode SplitOp(int i) {
			return getToken(TamarinParser.SplitOp, i);
		}
		public DisjSplitGoalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_disjSplitGoal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterDisjSplitGoal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitDisjSplitGoal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitDisjSplitGoal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DisjSplitGoalContext disjSplitGoal() throws RecognitionException {
		DisjSplitGoalContext _localctx = new DisjSplitGoalContext(_ctx, getState());
		enterRule(_localctx, 244, RULE_disjSplitGoal);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1357);
			standardFormula("msgVar", "nodeVar");
			setState(1362);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SplitOp) {
				{
				{
				setState(1358);
				match(SplitOp);
				setState(1359);
				standardFormula("msgVar", "nodeVar");
				}
				}
				setState(1364);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class EqSplitGoalContext extends ParserRuleContext {
		public TerminalNode KeywordSplitEqs() { return getToken(TamarinParser.KeywordSplitEqs, 0); }
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public NaturalContext natural() {
			return getRuleContext(NaturalContext.class,0);
		}
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public EqSplitGoalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_eqSplitGoal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterEqSplitGoal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitEqSplitGoal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitEqSplitGoal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EqSplitGoalContext eqSplitGoal() throws RecognitionException {
		EqSplitGoalContext _localctx = new EqSplitGoalContext(_ctx, getState());
		enterRule(_localctx, 246, RULE_eqSplitGoal);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1365);
			match(KeywordSplitEqs);
			setState(1366);
			match(LeftParenthesis);
			setState(1367);
			natural();
			setState(1368);
			match(RightParenthesis);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class NodePremiseContext extends ParserRuleContext {
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public NodeVarContext nodeVar() {
			return getRuleContext(NodeVarContext.class,0);
		}
		public TerminalNode Comma() { return getToken(TamarinParser.Comma, 0); }
		public NaturalContext natural() {
			return getRuleContext(NaturalContext.class,0);
		}
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public NodePremiseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nodePremise; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterNodePremise(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitNodePremise(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitNodePremise(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NodePremiseContext nodePremise() throws RecognitionException {
		NodePremiseContext _localctx = new NodePremiseContext(_ctx, getState());
		enterRule(_localctx, 248, RULE_nodePremise);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1370);
			match(LeftParenthesis);
			setState(1371);
			nodeVar();
			setState(1372);
			match(Comma);
			setState(1373);
			natural();
			setState(1374);
			match(RightParenthesis);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class NodeConclusionContext extends ParserRuleContext {
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public NodeVarContext nodeVar() {
			return getRuleContext(NodeVarContext.class,0);
		}
		public TerminalNode Comma() { return getToken(TamarinParser.Comma, 0); }
		public NaturalContext natural() {
			return getRuleContext(NaturalContext.class,0);
		}
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public NodeConclusionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nodeConclusion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterNodeConclusion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitNodeConclusion(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitNodeConclusion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NodeConclusionContext nodeConclusion() throws RecognitionException {
		NodeConclusionContext _localctx = new NodeConclusionContext(_ctx, getState());
		enterRule(_localctx, 250, RULE_nodeConclusion);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1376);
			match(LeftParenthesis);
			setState(1377);
			nodeVar();
			setState(1378);
			match(Comma);
			setState(1379);
			natural();
			setState(1380);
			match(RightParenthesis);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DiffEquivLemmaContext extends ParserRuleContext {
		public TerminalNode KeywordDiffEquivLemma() { return getToken(TamarinParser.KeywordDiffEquivLemma, 0); }
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public ProcessContext process() {
			return getRuleContext(ProcessContext.class,0);
		}
		public DiffEquivLemmaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_diffEquivLemma; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterDiffEquivLemma(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitDiffEquivLemma(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitDiffEquivLemma(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DiffEquivLemmaContext diffEquivLemma() throws RecognitionException {
		DiffEquivLemmaContext _localctx = new DiffEquivLemmaContext(_ctx, getState());
		enterRule(_localctx, 252, RULE_diffEquivLemma);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1382);
			match(KeywordDiffEquivLemma);
			setState(1383);
			match(Colon);
			setState(1384);
			process();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class EquivLemmaContext extends ParserRuleContext {
		public TerminalNode KeywordEquivLemma() { return getToken(TamarinParser.KeywordEquivLemma, 0); }
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public List<ProcessContext> process() {
			return getRuleContexts(ProcessContext.class);
		}
		public ProcessContext process(int i) {
			return getRuleContext(ProcessContext.class,i);
		}
		public EquivLemmaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equivLemma; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterEquivLemma(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitEquivLemma(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitEquivLemma(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EquivLemmaContext equivLemma() throws RecognitionException {
		EquivLemmaContext _localctx = new EquivLemmaContext(_ctx, getState());
		enterRule(_localctx, 254, RULE_equivLemma);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1386);
			match(KeywordEquivLemma);
			setState(1387);
			match(Colon);
			setState(1388);
			process();
			setState(1389);
			process();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ProcessContext extends ParserRuleContext {
		public List<ActionProcessContext> actionProcess() {
			return getRuleContexts(ActionProcessContext.class);
		}
		public ActionProcessContext actionProcess(int i) {
			return getRuleContext(ActionProcessContext.class,i);
		}
		public List<TerminalNode> PlusOp() { return getTokens(TamarinParser.PlusOp); }
		public TerminalNode PlusOp(int i) {
			return getToken(TamarinParser.PlusOp, i);
		}
		public List<TerminalNode> DoublePipeOp() { return getTokens(TamarinParser.DoublePipeOp); }
		public TerminalNode DoublePipeOp(int i) {
			return getToken(TamarinParser.DoublePipeOp, i);
		}
		public List<TerminalNode> PipeOp() { return getTokens(TamarinParser.PipeOp); }
		public TerminalNode PipeOp(int i) {
			return getToken(TamarinParser.PipeOp, i);
		}
		public ProcessContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_process; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterProcess(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitProcess(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitProcess(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProcessContext process() throws RecognitionException {
		ProcessContext _localctx = new ProcessContext(_ctx, getState());
		enterRule(_localctx, 256, RULE_process);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1391);
			actionProcess();
			setState(1396);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,120,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1392);
					_la = _input.LA(1);
					if ( !(((((_la - 115)) & ~0x3f) == 0 && ((1L << (_la - 115)) & 19L) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(1393);
					actionProcess();
					}
					} 
				}
				setState(1398);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,120,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ElseProcessContext extends ParserRuleContext {
		public TerminalNode KeywordElse() { return getToken(TamarinParser.KeywordElse, 0); }
		public ProcessContext process() {
			return getRuleContext(ProcessContext.class,0);
		}
		public ElseProcessContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elseProcess; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterElseProcess(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitElseProcess(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitElseProcess(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ElseProcessContext elseProcess() throws RecognitionException {
		ElseProcessContext _localctx = new ElseProcessContext(_ctx, getState());
		enterRule(_localctx, 258, RULE_elseProcess);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1399);
			match(KeywordElse);
			setState(1400);
			process();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TopLevelProcessContext extends ParserRuleContext {
		public TerminalNode KeywordProcess() { return getToken(TamarinParser.KeywordProcess, 0); }
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public ProcessContext process() {
			return getRuleContext(ProcessContext.class,0);
		}
		public TopLevelProcessContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_topLevelProcess; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTopLevelProcess(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTopLevelProcess(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTopLevelProcess(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TopLevelProcessContext topLevelProcess() throws RecognitionException {
		TopLevelProcessContext _localctx = new TopLevelProcessContext(_ctx, getState());
		enterRule(_localctx, 260, RULE_topLevelProcess);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1402);
			match(KeywordProcess);
			setState(1403);
			match(Colon);
			setState(1404);
			process();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ActionProcessContext extends ParserRuleContext {
		public TerminalNode BangOp() { return getToken(TamarinParser.BangOp, 0); }
		public ProcessContext process() {
			return getRuleContext(ProcessContext.class,0);
		}
		public TerminalNode KeywordLookup() { return getToken(TamarinParser.KeywordLookup, 0); }
		public List<SapicTermContext> sapicTerm() {
			return getRuleContexts(SapicTermContext.class);
		}
		public SapicTermContext sapicTerm(int i) {
			return getRuleContext(SapicTermContext.class,i);
		}
		public TerminalNode KeywordAs() { return getToken(TamarinParser.KeywordAs, 0); }
		public SapicVarContext sapicVar() {
			return getRuleContext(SapicVarContext.class,0);
		}
		public TerminalNode KeywordIn() { return getToken(TamarinParser.KeywordIn, 0); }
		public ElseProcessContext elseProcess() {
			return getRuleContext(ElseProcessContext.class,0);
		}
		public TerminalNode KeywordIf() { return getToken(TamarinParser.KeywordIf, 0); }
		public TerminalNode KeywordThen() { return getToken(TamarinParser.KeywordThen, 0); }
		public List<TerminalNode> EqOp() { return getTokens(TamarinParser.EqOp); }
		public TerminalNode EqOp(int i) {
			return getToken(TamarinParser.EqOp, i);
		}
		public StandardFormulaContext standardFormula() {
			return getRuleContext(StandardFormulaContext.class,0);
		}
		public TerminalNode KeywordLet() { return getToken(TamarinParser.KeywordLet, 0); }
		public List<SapicPatternTermContext> sapicPatternTerm() {
			return getRuleContexts(SapicPatternTermContext.class);
		}
		public SapicPatternTermContext sapicPatternTerm(int i) {
			return getRuleContext(SapicPatternTermContext.class,i);
		}
		public TerminalNode NullOp() { return getToken(TamarinParser.NullOp, 0); }
		public SapicActionContext sapicAction() {
			return getRuleContext(SapicActionContext.class,0);
		}
		public TerminalNode Semicolon() { return getToken(TamarinParser.Semicolon, 0); }
		public ActionProcessContext actionProcess() {
			return getRuleContext(ActionProcessContext.class,0);
		}
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public TerminalNode AtOp() { return getToken(TamarinParser.AtOp, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public List<MsettermContext> msetterm() {
			return getRuleContexts(MsettermContext.class);
		}
		public MsettermContext msetterm(int i) {
			return getRuleContext(MsettermContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public ActionProcessContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_actionProcess; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterActionProcess(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitActionProcess(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitActionProcess(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ActionProcessContext actionProcess() throws RecognitionException {
		ActionProcessContext _localctx = new ActionProcessContext(_ctx, getState());
		enterRule(_localctx, 262, RULE_actionProcess);
		int _la;
		try {
			int _alt;
			setState(1475);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,132,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1406);
				match(BangOp);
				setState(1407);
				process();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1408);
				match(KeywordLookup);
				setState(1409);
				sapicTerm();
				setState(1410);
				match(KeywordAs);
				setState(1411);
				sapicVar();
				setState(1412);
				match(KeywordIn);
				setState(1413);
				process();
				setState(1415);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,121,_ctx) ) {
				case 1:
					{
					setState(1414);
					elseProcess();
					}
					break;
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1417);
				match(KeywordIf);
				setState(1423);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,122,_ctx) ) {
				case 1:
					{
					setState(1418);
					sapicTerm();
					setState(1419);
					match(EqOp);
					setState(1420);
					sapicTerm();
					}
					break;
				case 2:
					{
					setState(1422);
					standardFormula("sapicVar", "sapicNodeVar");
					}
					break;
				}
				setState(1425);
				match(KeywordThen);
				setState(1426);
				process();
				setState(1428);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,123,_ctx) ) {
				case 1:
					{
					setState(1427);
					elseProcess();
					}
					break;
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1430);
				match(KeywordLet);
				setState(1435); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(1431);
						sapicPatternTerm();
						setState(1432);
						match(EqOp);
						setState(1433);
						sapicTerm();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(1437); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,124,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(1439);
				match(KeywordIn);
				setState(1440);
				process();
				setState(1442);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,125,_ctx) ) {
				case 1:
					{
					setState(1441);
					elseProcess();
					}
					break;
				}
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1444);
				match(NullOp);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1445);
				sapicAction();
				setState(1448);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Semicolon) {
					{
					setState(1446);
					match(Semicolon);
					setState(1447);
					actionProcess();
					}
				}

				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1450);
				match(LeftParenthesis);
				setState(1451);
				process();
				setState(1452);
				match(RightParenthesis);
				setState(1455);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==AtOp) {
					{
					setState(1453);
					match(AtOp);
					setState(1454);
					sapicTerm();
					}
				}

				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(1457);
				identifier();
				setState(1473);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,131,_ctx) ) {
				case 1:
					{
					setState(1458);
					match(LeftParenthesis);
					setState(1470);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,130,_ctx) ) {
					case 1:
						{
						setState(1459);
						msetterm("logicalTypedLiteral");
						setState(1464);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,128,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(1460);
								match(Comma);
								setState(1461);
								msetterm("logicalTypedLiteral");
								}
								} 
							}
							setState(1466);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,128,_ctx);
						}
						setState(1468);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==Comma) {
							{
							setState(1467);
							match(Comma);
							}
						}

						}
						break;
					}
					setState(1472);
					match(RightParenthesis);
					}
					break;
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SapicActionContext extends ParserRuleContext {
		public TerminalNode SapicActionNew() { return getToken(TamarinParser.SapicActionNew, 0); }
		public SapicVarContext sapicVar() {
			return getRuleContext(SapicVarContext.class,0);
		}
		public TerminalNode SapicActionInsert() { return getToken(TamarinParser.SapicActionInsert, 0); }
		public List<MsettermContext> msetterm() {
			return getRuleContexts(MsettermContext.class);
		}
		public MsettermContext msetterm(int i) {
			return getRuleContext(MsettermContext.class,i);
		}
		public TerminalNode Comma() { return getToken(TamarinParser.Comma, 0); }
		public SapicActionInContext sapicActionIn() {
			return getRuleContext(SapicActionInContext.class,0);
		}
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public SapicActionOutContext sapicActionOut() {
			return getRuleContext(SapicActionOutContext.class,0);
		}
		public TerminalNode SapicActionDelete() { return getToken(TamarinParser.SapicActionDelete, 0); }
		public TerminalNode SapicActionLock() { return getToken(TamarinParser.SapicActionLock, 0); }
		public TerminalNode SapicActionUnlock() { return getToken(TamarinParser.SapicActionUnlock, 0); }
		public TerminalNode SapicActionEvent() { return getToken(TamarinParser.SapicActionEvent, 0); }
		public FactContext fact() {
			return getRuleContext(FactContext.class,0);
		}
		public GenericRuleContext genericRule() {
			return getRuleContext(GenericRuleContext.class,0);
		}
		public SapicActionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sapicAction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSapicAction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSapicAction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSapicAction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SapicActionContext sapicAction() throws RecognitionException {
		SapicActionContext _localctx = new SapicActionContext(_ctx, getState());
		enterRule(_localctx, 264, RULE_sapicAction);
		try {
			setState(1517);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case SapicActionNew:
				enterOuterAlt(_localctx, 1);
				{
				setState(1477);
				match(SapicActionNew);
				setState(1478);
				sapicVar();
				}
				break;
			case SapicActionInsert:
				enterOuterAlt(_localctx, 2);
				{
				setState(1479);
				match(SapicActionInsert);
				setState(1480);
				msetterm("logicalTypedLiteral");
				setState(1481);
				match(Comma);
				setState(1482);
				msetterm("logicalTypedLiteral");
				}
				break;
			case KeywordIn:
				enterOuterAlt(_localctx, 3);
				{
				setState(1484);
				sapicActionIn();
				setState(1485);
				match(LeftParenthesis);
				setState(1494);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,133,_ctx) ) {
				case 1:
					{
					setState(1486);
					msetterm("logicalTypedPatternLiteral");
					setState(1487);
					match(RightParenthesis);
					}
					break;
				case 2:
					{
					setState(1489);
					msetterm("logicalTypedLiteral");
					setState(1490);
					match(Comma);
					setState(1491);
					msetterm("logicalTypedPatternLiteral");
					setState(1492);
					match(RightParenthesis);
					}
					break;
				}
				}
				break;
			case KeywordOut:
				enterOuterAlt(_localctx, 4);
				{
				setState(1496);
				sapicActionOut();
				setState(1497);
				match(LeftParenthesis);
				setState(1506);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,134,_ctx) ) {
				case 1:
					{
					setState(1498);
					msetterm("logicalTypedPatternLiteral");
					setState(1499);
					match(RightParenthesis);
					}
					break;
				case 2:
					{
					setState(1501);
					msetterm("logicalTypedLiteral");
					setState(1502);
					match(Comma);
					setState(1503);
					msetterm("logicalTypedPatternLiteral");
					setState(1504);
					match(RightParenthesis);
					}
					break;
				}
				}
				break;
			case SapicActionDelete:
				enterOuterAlt(_localctx, 5);
				{
				setState(1508);
				match(SapicActionDelete);
				setState(1509);
				msetterm("logicalTypedLiteral");
				}
				break;
			case SapicActionLock:
				enterOuterAlt(_localctx, 6);
				{
				setState(1510);
				match(SapicActionLock);
				setState(1511);
				msetterm("logicalTypedLiteral");
				}
				break;
			case SapicActionUnlock:
				enterOuterAlt(_localctx, 7);
				{
				setState(1512);
				match(SapicActionUnlock);
				setState(1513);
				msetterm("logicalTypedLiteral");
				}
				break;
			case SapicActionEvent:
				enterOuterAlt(_localctx, 8);
				{
				setState(1514);
				match(SapicActionEvent);
				setState(1515);
				fact("logicalTypedLiteral");
				}
				break;
			case LeftBracket:
				enterOuterAlt(_localctx, 9);
				{
				setState(1516);
				genericRule("sapicPatternVar", "sapicNodeVar");
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SapicActionInContext extends ParserRuleContext {
		public TerminalNode KeywordIn() { return getToken(TamarinParser.KeywordIn, 0); }
		public SapicActionInContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sapicActionIn; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSapicActionIn(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSapicActionIn(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSapicActionIn(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SapicActionInContext sapicActionIn() throws RecognitionException {
		SapicActionInContext _localctx = new SapicActionInContext(_ctx, getState());
		enterRule(_localctx, 266, RULE_sapicActionIn);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1519);
			match(KeywordIn);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SapicActionOutContext extends ParserRuleContext {
		public TerminalNode KeywordOut() { return getToken(TamarinParser.KeywordOut, 0); }
		public SapicActionOutContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sapicActionOut; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSapicActionOut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSapicActionOut(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSapicActionOut(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SapicActionOutContext sapicActionOut() throws RecognitionException {
		SapicActionOutContext _localctx = new SapicActionOutContext(_ctx, getState());
		enterRule(_localctx, 268, RULE_sapicActionOut);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1521);
			match(KeywordOut);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ProcessDefContext extends ParserRuleContext {
		public TerminalNode KeywordLet() { return getToken(TamarinParser.KeywordLet, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode EqOp() { return getToken(TamarinParser.EqOp, 0); }
		public ProcessContext process() {
			return getRuleContext(ProcessContext.class,0);
		}
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public List<SapicVarContext> sapicVar() {
			return getRuleContexts(SapicVarContext.class);
		}
		public SapicVarContext sapicVar(int i) {
			return getRuleContext(SapicVarContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public ProcessDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_processDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterProcessDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitProcessDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitProcessDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProcessDefContext processDef() throws RecognitionException {
		ProcessDefContext _localctx = new ProcessDefContext(_ctx, getState());
		enterRule(_localctx, 270, RULE_processDef);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1523);
			match(KeywordLet);
			setState(1524);
			identifier();
			setState(1540);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LeftParenthesis) {
				{
				setState(1525);
				match(LeftParenthesis);
				setState(1537);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & -16L) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & -4611685984067649537L) != 0) || ((((_la - 131)) & ~0x3f) == 0 && ((1L << (_la - 131)) & 562949822973441L) != 0)) {
					{
					setState(1526);
					sapicVar();
					setState(1531);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,136,_ctx);
					while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
						if ( _alt==1 ) {
							{
							{
							setState(1527);
							match(Comma);
							setState(1528);
							sapicVar();
							}
							} 
						}
						setState(1533);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,136,_ctx);
					}
					setState(1535);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==Comma) {
						{
						setState(1534);
						match(Comma);
						}
					}

					}
				}

				setState(1539);
				match(RightParenthesis);
				}
			}

			setState(1542);
			match(EqOp);
			setState(1543);
			process();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SapicPatternTermContext extends ParserRuleContext {
		public MsettermContext msetterm() {
			return getRuleContext(MsettermContext.class,0);
		}
		public SapicPatternTermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sapicPatternTerm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSapicPatternTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSapicPatternTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSapicPatternTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SapicPatternTermContext sapicPatternTerm() throws RecognitionException {
		SapicPatternTermContext _localctx = new SapicPatternTermContext(_ctx, getState());
		enterRule(_localctx, 272, RULE_sapicPatternTerm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1545);
			msetterm("logicalTypedPatternLiteral");
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SapicTermContext extends ParserRuleContext {
		public MsettermContext msetterm() {
			return getRuleContext(MsettermContext.class,0);
		}
		public SapicTermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sapicTerm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSapicTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSapicTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSapicTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SapicTermContext sapicTerm() throws RecognitionException {
		SapicTermContext _localctx = new SapicTermContext(_ctx, getState());
		enterRule(_localctx, 274, RULE_sapicTerm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1547);
			msetterm("logicalTypedLiteral");
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LogicalTypedPatternLiteralContext extends ParserRuleContext {
		public FreshNameContext freshName() {
			return getRuleContext(FreshNameContext.class,0);
		}
		public PubNameContext pubName() {
			return getRuleContext(PubNameContext.class,0);
		}
		public SapicPatternVarContext sapicPatternVar() {
			return getRuleContext(SapicPatternVarContext.class,0);
		}
		public LogicalTypedPatternLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalTypedPatternLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLogicalTypedPatternLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLogicalTypedPatternLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLogicalTypedPatternLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalTypedPatternLiteralContext logicalTypedPatternLiteral() throws RecognitionException {
		LogicalTypedPatternLiteralContext _localctx = new LogicalTypedPatternLiteralContext(_ctx, getState());
		enterRule(_localctx, 276, RULE_logicalTypedPatternLiteral);
		try {
			setState(1552);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,140,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1549);
				freshName();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1550);
				pubName();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1551);
				sapicPatternVar();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LogicalTypedLiteralContext extends ParserRuleContext {
		public FreshNameContext freshName() {
			return getRuleContext(FreshNameContext.class,0);
		}
		public PubNameContext pubName() {
			return getRuleContext(PubNameContext.class,0);
		}
		public SapicVarContext sapicVar() {
			return getRuleContext(SapicVarContext.class,0);
		}
		public LogicalTypedLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalTypedLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLogicalTypedLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLogicalTypedLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLogicalTypedLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalTypedLiteralContext logicalTypedLiteral() throws RecognitionException {
		LogicalTypedLiteralContext _localctx = new LogicalTypedLiteralContext(_ctx, getState());
		enterRule(_localctx, 278, RULE_logicalTypedLiteral);
		try {
			setState(1557);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,141,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1554);
				freshName();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1555);
				pubName();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1556);
				sapicVar();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SapicNodeVarContext extends ParserRuleContext {
		public NodeVarContext nodeVar() {
			return getRuleContext(NodeVarContext.class,0);
		}
		public SapicNodeVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sapicNodeVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSapicNodeVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSapicNodeVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSapicNodeVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SapicNodeVarContext sapicNodeVar() throws RecognitionException {
		SapicNodeVarContext _localctx = new SapicNodeVarContext(_ctx, getState());
		enterRule(_localctx, 280, RULE_sapicNodeVar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1559);
			nodeVar();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SapicPatternVarContext extends ParserRuleContext {
		public SapicVarContext sapicVar() {
			return getRuleContext(SapicVarContext.class,0);
		}
		public EqOpContext eqOp() {
			return getRuleContext(EqOpContext.class,0);
		}
		public SapicPatternVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sapicPatternVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSapicPatternVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSapicPatternVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSapicPatternVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SapicPatternVarContext sapicPatternVar() throws RecognitionException {
		SapicPatternVarContext _localctx = new SapicPatternVarContext(_ctx, getState());
		enterRule(_localctx, 282, RULE_sapicPatternVar);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1562);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==EqOp) {
				{
				setState(1561);
				eqOp();
				}
			}

			setState(1564);
			sapicVar();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SapicVarContext extends ParserRuleContext {
		public LogVarContext logVar() {
			return getRuleContext(LogVarContext.class,0);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public SapicTypeContext sapicType() {
			return getRuleContext(SapicTypeContext.class,0);
		}
		public SapicVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sapicVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSapicVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSapicVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSapicVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SapicVarContext sapicVar() throws RecognitionException {
		SapicVarContext _localctx = new SapicVarContext(_ctx, getState());
		enterRule(_localctx, 284, RULE_sapicVar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1566);
			logVar();
			setState(1569);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,143,_ctx) ) {
			case 1:
				{
				setState(1567);
				match(Colon);
				setState(1568);
				sapicType();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SapicTypeContext extends ParserRuleContext {
		public TerminalNode KeywordAny() { return getToken(TamarinParser.KeywordAny, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public SapicTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sapicType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterSapicType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitSapicType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitSapicType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SapicTypeContext sapicType() throws RecognitionException {
		SapicTypeContext _localctx = new SapicTypeContext(_ctx, getState());
		enterRule(_localctx, 286, RULE_sapicType);
		try {
			setState(1573);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,144,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1571);
				match(KeywordAny);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1572);
				identifier();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class RestrictionContext extends ParserRuleContext {
		public TerminalNode KeywordRestriction() { return getToken(TamarinParser.KeywordRestriction, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public List<TerminalNode> DoubleQuote() { return getTokens(TamarinParser.DoubleQuote); }
		public TerminalNode DoubleQuote(int i) {
			return getToken(TamarinParser.DoubleQuote, i);
		}
		public StandardFormulaContext standardFormula() {
			return getRuleContext(StandardFormulaContext.class,0);
		}
		public RestrictionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_restriction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterRestriction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitRestriction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitRestriction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RestrictionContext restriction() throws RecognitionException {
		RestrictionContext _localctx = new RestrictionContext(_ctx, getState());
		enterRule(_localctx, 288, RULE_restriction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1575);
			match(KeywordRestriction);
			setState(1576);
			identifier();
			setState(1577);
			match(Colon);
			setState(1578);
			match(DoubleQuote);
			setState(1579);
			standardFormula("msgVar", "nodeVar");
			setState(1580);
			match(DoubleQuote);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class RestrictionAttributesContext extends ParserRuleContext {
		public TerminalNode LeftBracket() { return getToken(TamarinParser.LeftBracket, 0); }
		public TerminalNode RightBracket() { return getToken(TamarinParser.RightBracket, 0); }
		public List<RestrictionAttributeContext> restrictionAttribute() {
			return getRuleContexts(RestrictionAttributeContext.class);
		}
		public RestrictionAttributeContext restrictionAttribute(int i) {
			return getRuleContext(RestrictionAttributeContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(TamarinParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(TamarinParser.Comma, i);
		}
		public RestrictionAttributesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_restrictionAttributes; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterRestrictionAttributes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitRestrictionAttributes(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitRestrictionAttributes(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RestrictionAttributesContext restrictionAttributes() throws RecognitionException {
		RestrictionAttributesContext _localctx = new RestrictionAttributesContext(_ctx, getState());
		enterRule(_localctx, 290, RULE_restrictionAttributes);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1582);
			match(LeftBracket);
			setState(1594);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 150)) & ~0x3f) == 0 && ((1L << (_la - 150)) & 7L) != 0)) {
				{
				setState(1583);
				restrictionAttribute();
				setState(1588);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,145,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1584);
						match(Comma);
						setState(1585);
						restrictionAttribute();
						}
						} 
					}
					setState(1590);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,145,_ctx);
				}
				setState(1592);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma) {
					{
					setState(1591);
					match(Comma);
					}
				}

				}
			}

			setState(1596);
			match(RightBracket);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class RestrictionAttributeContext extends ParserRuleContext {
		public TerminalNode Left() { return getToken(TamarinParser.Left, 0); }
		public TerminalNode Right() { return getToken(TamarinParser.Right, 0); }
		public TerminalNode Both() { return getToken(TamarinParser.Both, 0); }
		public RestrictionAttributeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_restrictionAttribute; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterRestrictionAttribute(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitRestrictionAttribute(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitRestrictionAttribute(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RestrictionAttributeContext restrictionAttribute() throws RecognitionException {
		RestrictionAttributeContext _localctx = new RestrictionAttributeContext(_ctx, getState());
		enterRule(_localctx, 292, RULE_restrictionAttribute);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1598);
			_la = _input.LA(1);
			if ( !(((((_la - 150)) & ~0x3f) == 0 && ((1L << (_la - 150)) & 7L) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LegacyAxiomContext extends ParserRuleContext {
		public TerminalNode KeywordAxiom() { return getToken(TamarinParser.KeywordAxiom, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public List<TerminalNode> DoubleQuote() { return getTokens(TamarinParser.DoubleQuote); }
		public TerminalNode DoubleQuote(int i) {
			return getToken(TamarinParser.DoubleQuote, i);
		}
		public StandardFormulaContext standardFormula() {
			return getRuleContext(StandardFormulaContext.class,0);
		}
		public LegacyAxiomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_legacyAxiom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLegacyAxiom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLegacyAxiom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLegacyAxiom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LegacyAxiomContext legacyAxiom() throws RecognitionException {
		LegacyAxiomContext _localctx = new LegacyAxiomContext(_ctx, getState());
		enterRule(_localctx, 294, RULE_legacyAxiom);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1600);
			match(KeywordAxiom);
			setState(1601);
			identifier();
			setState(1602);
			match(Colon);
			setState(1603);
			match(DoubleQuote);
			setState(1604);
			standardFormula("msgVar", "nodeVar");
			setState(1605);
			match(DoubleQuote);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LetBlockContext extends ParserRuleContext {
		public TerminalNode KeywordLet() { return getToken(TamarinParser.KeywordLet, 0); }
		public TerminalNode KeywordIn() { return getToken(TamarinParser.KeywordIn, 0); }
		public List<LetAssignmentContext> letAssignment() {
			return getRuleContexts(LetAssignmentContext.class);
		}
		public LetAssignmentContext letAssignment(int i) {
			return getRuleContext(LetAssignmentContext.class,i);
		}
		public LetBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_letBlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLetBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLetBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLetBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LetBlockContext letBlock() throws RecognitionException {
		LetBlockContext _localctx = new LetBlockContext(_ctx, getState());
		enterRule(_localctx, 296, RULE_letBlock);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1607);
			match(KeywordLet);
			setState(1609); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(1608);
				letAssignment();
				}
				}
				setState(1611); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & -16L) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & -4611685984067649537L) != 0) || ((((_la - 146)) & ~0x3f) == 0 && ((1L << (_la - 146)) & 17179865203L) != 0) );
			setState(1613);
			match(KeywordIn);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LetAssignmentContext extends ParserRuleContext {
		public LogMsgVarContext logMsgVar() {
			return getRuleContext(LogMsgVarContext.class,0);
		}
		public EqOpContext eqOp() {
			return getRuleContext(EqOpContext.class,0);
		}
		public MsettermContext msetterm() {
			return getRuleContext(MsettermContext.class,0);
		}
		public LetAssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_letAssignment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLetAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLetAssignment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLetAssignment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LetAssignmentContext letAssignment() throws RecognitionException {
		LetAssignmentContext _localctx = new LetAssignmentContext(_ctx, getState());
		enterRule(_localctx, 298, RULE_letAssignment);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1615);
			logMsgVar();
			setState(1616);
			eqOp();
			setState(1617);
			msetterm("logicalLiteral");
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ProtoFactIdentifierContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public ProtoFactIdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_protoFactIdentifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterProtoFactIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitProtoFactIdentifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitProtoFactIdentifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProtoFactIdentifierContext protoFactIdentifier() throws RecognitionException {
		ProtoFactIdentifierContext _localctx = new ProtoFactIdentifierContext(_ctx, getState());
		enterRule(_localctx, 300, RULE_protoFactIdentifier);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1619);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ReservedFactIdentifierContext extends ParserRuleContext {
		public ReservedFactIdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reservedFactIdentifier; }
	 
		public ReservedFactIdentifierContext() { }
		public void copyFrom(ReservedFactIdentifierContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Reserved_fact_outContext extends ReservedFactIdentifierContext {
		public TerminalNode OutFactIdent() { return getToken(TamarinParser.OutFactIdent, 0); }
		public Reserved_fact_outContext(ReservedFactIdentifierContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterReserved_fact_out(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitReserved_fact_out(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitReserved_fact_out(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Reserved_fact_dedContext extends ReservedFactIdentifierContext {
		public TerminalNode DedLogFactIdent() { return getToken(TamarinParser.DedLogFactIdent, 0); }
		public Reserved_fact_dedContext(ReservedFactIdentifierContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterReserved_fact_ded(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitReserved_fact_ded(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitReserved_fact_ded(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Reserved_fact_kuContext extends ReservedFactIdentifierContext {
		public TerminalNode KuFactIdent() { return getToken(TamarinParser.KuFactIdent, 0); }
		public Reserved_fact_kuContext(ReservedFactIdentifierContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterReserved_fact_ku(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitReserved_fact_ku(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitReserved_fact_ku(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Reserved_fact_kdContext extends ReservedFactIdentifierContext {
		public TerminalNode KdFactIdent() { return getToken(TamarinParser.KdFactIdent, 0); }
		public Reserved_fact_kdContext(ReservedFactIdentifierContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterReserved_fact_kd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitReserved_fact_kd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitReserved_fact_kd(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Reserved_fact_inContext extends ReservedFactIdentifierContext {
		public TerminalNode InFactIdent() { return getToken(TamarinParser.InFactIdent, 0); }
		public Reserved_fact_inContext(ReservedFactIdentifierContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterReserved_fact_in(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitReserved_fact_in(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitReserved_fact_in(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReservedFactIdentifierContext reservedFactIdentifier() throws RecognitionException {
		ReservedFactIdentifierContext _localctx = new ReservedFactIdentifierContext(_ctx, getState());
		enterRule(_localctx, 302, RULE_reservedFactIdentifier);
		try {
			setState(1626);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OutFactIdent:
				_localctx = new Reserved_fact_outContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(1621);
				match(OutFactIdent);
				}
				break;
			case InFactIdent:
				_localctx = new Reserved_fact_inContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(1622);
				match(InFactIdent);
				}
				break;
			case KuFactIdent:
				_localctx = new Reserved_fact_kuContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(1623);
				match(KuFactIdent);
				}
				break;
			case KdFactIdent:
				_localctx = new Reserved_fact_kdContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(1624);
				match(KdFactIdent);
				}
				break;
			case DedLogFactIdent:
				_localctx = new Reserved_fact_dedContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(1625);
				match(DedLogFactIdent);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class IdentifierContext extends ParserRuleContext {
		public AllowedIdentifierKeywordsContext allowedIdentifierKeywords() {
			return getRuleContext(AllowedIdentifierKeywordsContext.class,0);
		}
		public NaturalContext natural() {
			return getRuleContext(NaturalContext.class,0);
		}
		public TerminalNode Letters() { return getToken(TamarinParser.Letters, 0); }
		public TerminalNode AlphaNums() { return getToken(TamarinParser.AlphaNums, 0); }
		public TerminalNode IDLower() { return getToken(TamarinParser.IDLower, 0); }
		public TerminalNode IDUpper() { return getToken(TamarinParser.IDUpper, 0); }
		public TerminalNode IDNum() { return getToken(TamarinParser.IDNum, 0); }
		public IdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitIdentifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitIdentifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdentifierContext identifier() throws RecognitionException {
		IdentifierContext _localctx = new IdentifierContext(_ctx, getState());
		enterRule(_localctx, 304, RULE_identifier);
		try {
			setState(1635);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case KeywordDiff:
			case KeywordBy:
			case KeywordLemma:
			case KeywordIf:
			case KeywordElse:
			case KeywordProcess:
			case KeywordAs:
			case KeywordLookup:
			case KeywordLast:
			case KeywordPrio:
			case KeywordDeprio:
			case KeywordPresort:
			case KeywordSmallest:
			case KeywordId:
			case KeywordTactic:
			case KeywordRestriction:
			case KeywordAxiom:
			case KeywordAny:
			case KeywordTest:
			case KeywordNoPrecomp:
			case KeywordThen:
			case KeywordOut:
			case KeywordFresh:
			case KeywordPub:
			case KeywordXor:
			case KeywordOutput:
			case KeywordAccountsFor:
			case KeywordModulo:
			case KeywordTheory:
			case KeywordBegin:
			case KeywordEnd:
			case KeywordVariants:
			case KeywordRestrict:
			case KeywordHeuristic:
			case KeywordExport:
			case KeywordPredicate:
			case KeywordOptions:
			case KeywordEquations:
			case KeywordFunctions:
			case KeywordBuiltins:
			case KeywordCase:
			case KeywordQED:
			case KeywordNext:
			case KeywordSplitEqs:
			case KeywordEquivLemma:
			case KeywordDiffEquivLemma:
			case E:
			case AC:
			case RuleFresh:
			case RuleIrecv:
			case RuleIsend:
			case RuleCoerce:
			case RuleIequality:
			case SuffixMsg:
			case SuffixNode:
			case GoalRankingIdentifierNoOracle:
			case GoalRankingIdentifierOracle:
			case GoalRankingIdentifiersNoOracle:
			case DHneutral:
			case BuiltinMun:
			case BuiltinOne:
			case BuiltinExp:
			case BuiltinMult:
			case BuiltinInv:
			case BuiltinPmult:
			case BuiltinEm:
			case BuiltinZero:
			case SolvedProof:
			case ProofMethodSorry:
			case ProofMethodSimplify:
			case ProofMethodSolve:
			case ProofMethodContradiction:
			case ProofMethodInduction:
			case BuiltinOptions:
			case BuiltinNameLocationsReport:
			case BuiltinNameReliableChannel:
			case BuiltinNameDH:
			case BuiltinNameBilinearPairing:
			case BuiltinNameMultiset:
			case BuiltinNameSymmetricEncryption:
			case BuiltinNameAsymmetricEncryption:
			case BuiltinNameSigning:
			case BuiltinNameDestPairing:
			case BuiltinNameDestSymmetricEncryption:
			case BuiltinNameDestAsymmetricEncryption:
			case BuiltinNameDestSigning:
			case BuiltinNameRevealingSigning:
			case BuiltinNameHashing:
			case TacticFunctionName:
			case SapicActionNew:
			case SapicActionInsert:
			case SapicActionDelete:
			case SapicActionLock:
			case SapicActionUnlock:
			case SapicActionEvent:
			case TOp:
			case FOp:
			case Left:
			case Right:
			case Both:
			case ForallTraces:
			case ExistsTrace:
			case OutFactIdent:
			case InFactIdent:
			case KuFactIdent:
			case KdFactIdent:
			case DedLogFactIdent:
			case FreshFactIdent:
			case RuleAttributeColor:
			case LemmaAttributeTyping:
			case LemmaAttributeSources:
			case LemmaAttributeReuse:
			case LemmaAttributeDiffReuse:
			case LemmaAttributeInduction:
			case LemmaAttributeHideLemma:
			case FunctionAttribute:
				enterOuterAlt(_localctx, 1);
				{
				setState(1628);
				allowedIdentifierKeywords();
				}
				break;
			case OneOp:
			case NullOp:
			case Natural:
				enterOuterAlt(_localctx, 2);
				{
				setState(1629);
				natural();
				}
				break;
			case Letters:
				enterOuterAlt(_localctx, 3);
				{
				setState(1630);
				match(Letters);
				}
				break;
			case AlphaNums:
				enterOuterAlt(_localctx, 4);
				{
				setState(1631);
				match(AlphaNums);
				}
				break;
			case IDLower:
				enterOuterAlt(_localctx, 5);
				{
				setState(1632);
				match(IDLower);
				}
				break;
			case IDUpper:
				enterOuterAlt(_localctx, 6);
				{
				setState(1633);
				match(IDUpper);
				}
				break;
			case IDNum:
				enterOuterAlt(_localctx, 7);
				{
				setState(1634);
				match(IDNum);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class NaturalContext extends ParserRuleContext {
		public TerminalNode NullOp() { return getToken(TamarinParser.NullOp, 0); }
		public TerminalNode OneOp() { return getToken(TamarinParser.OneOp, 0); }
		public TerminalNode Natural() { return getToken(TamarinParser.Natural, 0); }
		public NaturalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_natural; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterNatural(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitNatural(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitNatural(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NaturalContext natural() throws RecognitionException {
		NaturalContext _localctx = new NaturalContext(_ctx, getState());
		enterRule(_localctx, 306, RULE_natural);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1637);
			_la = _input.LA(1);
			if ( !(((((_la - 126)) & ~0x3f) == 0 && ((1L << (_la - 126)) & 281474976710659L) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class IndexedIdentifierContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode Dot() { return getToken(TamarinParser.Dot, 0); }
		public NaturalContext natural() {
			return getRuleContext(NaturalContext.class,0);
		}
		public IndexedIdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_indexedIdentifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterIndexedIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitIndexedIdentifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitIndexedIdentifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IndexedIdentifierContext indexedIdentifier() throws RecognitionException {
		IndexedIdentifierContext _localctx = new IndexedIdentifierContext(_ctx, getState());
		enterRule(_localctx, 308, RULE_indexedIdentifier);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1639);
			identifier();
			setState(1642);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,151,_ctx) ) {
			case 1:
				{
				setState(1640);
				match(Dot);
				setState(1641);
				natural();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FilePathContext extends ParserRuleContext {
		public List<TerminalNode> DoubleQuote() { return getTokens(TamarinParser.DoubleQuote); }
		public TerminalNode DoubleQuote(int i) {
			return getToken(TamarinParser.DoubleQuote, i);
		}
		public CharDirContext charDir() {
			return getRuleContext(CharDirContext.class,0);
		}
		public FilePathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_filePath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFilePath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFilePath(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFilePath(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FilePathContext filePath() throws RecognitionException {
		FilePathContext _localctx = new FilePathContext(_ctx, getState());
		enterRule(_localctx, 310, RULE_filePath);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1644);
			match(DoubleQuote);
			setState(1645);
			charDir();
			setState(1646);
			match(DoubleQuote);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class KeywordsContext extends ParserRuleContext {
		public TerminalNode KeywordLet() { return getToken(TamarinParser.KeywordLet, 0); }
		public TerminalNode KeywordIn() { return getToken(TamarinParser.KeywordIn, 0); }
		public TerminalNode KeywordDiff() { return getToken(TamarinParser.KeywordDiff, 0); }
		public KeywordsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keywords; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterKeywords(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitKeywords(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitKeywords(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KeywordsContext keywords() throws RecognitionException {
		KeywordsContext _localctx = new KeywordsContext(_ctx, getState());
		enterRule(_localctx, 312, RULE_keywords);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1648);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 22L) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LogMsgVarContext extends ParserRuleContext {
		public IndexedIdentifierContext indexedIdentifier() {
			return getRuleContext(IndexedIdentifierContext.class,0);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public TerminalNode SuffixMsg() { return getToken(TamarinParser.SuffixMsg, 0); }
		public LogMsgVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logMsgVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLogMsgVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLogMsgVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLogMsgVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogMsgVarContext logMsgVar() throws RecognitionException {
		LogMsgVarContext _localctx = new LogMsgVarContext(_ctx, getState());
		enterRule(_localctx, 314, RULE_logMsgVar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1650);
			indexedIdentifier();
			setState(1653);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,152,_ctx) ) {
			case 1:
				{
				setState(1651);
				match(Colon);
				setState(1652);
				match(SuffixMsg);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LogFreshVarContext extends ParserRuleContext {
		public IndexedIdentifierContext indexedIdentifier() {
			return getRuleContext(IndexedIdentifierContext.class,0);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public TerminalNode KeywordFresh() { return getToken(TamarinParser.KeywordFresh, 0); }
		public TerminalNode TildeOp() { return getToken(TamarinParser.TildeOp, 0); }
		public LogFreshVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logFreshVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLogFreshVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLogFreshVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLogFreshVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogFreshVarContext logFreshVar() throws RecognitionException {
		LogFreshVarContext _localctx = new LogFreshVarContext(_ctx, getState());
		enterRule(_localctx, 316, RULE_logFreshVar);
		try {
			setState(1661);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case KeywordDiff:
			case KeywordBy:
			case KeywordLemma:
			case KeywordIf:
			case KeywordElse:
			case KeywordProcess:
			case KeywordAs:
			case KeywordLookup:
			case KeywordLast:
			case KeywordPrio:
			case KeywordDeprio:
			case KeywordPresort:
			case KeywordSmallest:
			case KeywordId:
			case KeywordTactic:
			case KeywordRestriction:
			case KeywordAxiom:
			case KeywordAny:
			case KeywordTest:
			case KeywordNoPrecomp:
			case KeywordThen:
			case KeywordOut:
			case KeywordFresh:
			case KeywordPub:
			case KeywordXor:
			case KeywordOutput:
			case KeywordAccountsFor:
			case KeywordModulo:
			case KeywordTheory:
			case KeywordBegin:
			case KeywordEnd:
			case KeywordVariants:
			case KeywordRestrict:
			case KeywordHeuristic:
			case KeywordExport:
			case KeywordPredicate:
			case KeywordOptions:
			case KeywordEquations:
			case KeywordFunctions:
			case KeywordBuiltins:
			case KeywordCase:
			case KeywordQED:
			case KeywordNext:
			case KeywordSplitEqs:
			case KeywordEquivLemma:
			case KeywordDiffEquivLemma:
			case E:
			case AC:
			case RuleFresh:
			case RuleIrecv:
			case RuleIsend:
			case RuleCoerce:
			case RuleIequality:
			case SuffixMsg:
			case SuffixNode:
			case GoalRankingIdentifierNoOracle:
			case GoalRankingIdentifierOracle:
			case GoalRankingIdentifiersNoOracle:
			case DHneutral:
			case BuiltinMun:
			case BuiltinOne:
			case BuiltinExp:
			case BuiltinMult:
			case BuiltinInv:
			case BuiltinPmult:
			case BuiltinEm:
			case BuiltinZero:
			case SolvedProof:
			case ProofMethodSorry:
			case ProofMethodSimplify:
			case ProofMethodSolve:
			case ProofMethodContradiction:
			case ProofMethodInduction:
			case BuiltinOptions:
			case BuiltinNameLocationsReport:
			case BuiltinNameReliableChannel:
			case BuiltinNameDH:
			case BuiltinNameBilinearPairing:
			case BuiltinNameMultiset:
			case BuiltinNameSymmetricEncryption:
			case BuiltinNameAsymmetricEncryption:
			case BuiltinNameSigning:
			case BuiltinNameDestPairing:
			case BuiltinNameDestSymmetricEncryption:
			case BuiltinNameDestAsymmetricEncryption:
			case BuiltinNameDestSigning:
			case BuiltinNameRevealingSigning:
			case BuiltinNameHashing:
			case TacticFunctionName:
			case SapicActionNew:
			case SapicActionInsert:
			case SapicActionDelete:
			case SapicActionLock:
			case SapicActionUnlock:
			case SapicActionEvent:
			case OneOp:
			case NullOp:
			case TOp:
			case FOp:
			case Left:
			case Right:
			case Both:
			case ForallTraces:
			case ExistsTrace:
			case OutFactIdent:
			case InFactIdent:
			case KuFactIdent:
			case KdFactIdent:
			case DedLogFactIdent:
			case FreshFactIdent:
			case RuleAttributeColor:
			case LemmaAttributeTyping:
			case LemmaAttributeSources:
			case LemmaAttributeReuse:
			case LemmaAttributeDiffReuse:
			case LemmaAttributeInduction:
			case LemmaAttributeHideLemma:
			case FunctionAttribute:
			case Natural:
			case Letters:
			case AlphaNums:
			case IDLower:
			case IDUpper:
			case IDNum:
				enterOuterAlt(_localctx, 1);
				{
				setState(1655);
				indexedIdentifier();
				setState(1656);
				match(Colon);
				setState(1657);
				match(KeywordFresh);
				}
				break;
			case TildeOp:
				enterOuterAlt(_localctx, 2);
				{
				setState(1659);
				match(TildeOp);
				setState(1660);
				indexedIdentifier();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LogPubVarContext extends ParserRuleContext {
		public IndexedIdentifierContext indexedIdentifier() {
			return getRuleContext(IndexedIdentifierContext.class,0);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public TerminalNode KeywordPub() { return getToken(TamarinParser.KeywordPub, 0); }
		public TerminalNode DollarOp() { return getToken(TamarinParser.DollarOp, 0); }
		public LogPubVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logPubVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLogPubVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLogPubVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLogPubVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogPubVarContext logPubVar() throws RecognitionException {
		LogPubVarContext _localctx = new LogPubVarContext(_ctx, getState());
		enterRule(_localctx, 318, RULE_logPubVar);
		try {
			setState(1669);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case KeywordDiff:
			case KeywordBy:
			case KeywordLemma:
			case KeywordIf:
			case KeywordElse:
			case KeywordProcess:
			case KeywordAs:
			case KeywordLookup:
			case KeywordLast:
			case KeywordPrio:
			case KeywordDeprio:
			case KeywordPresort:
			case KeywordSmallest:
			case KeywordId:
			case KeywordTactic:
			case KeywordRestriction:
			case KeywordAxiom:
			case KeywordAny:
			case KeywordTest:
			case KeywordNoPrecomp:
			case KeywordThen:
			case KeywordOut:
			case KeywordFresh:
			case KeywordPub:
			case KeywordXor:
			case KeywordOutput:
			case KeywordAccountsFor:
			case KeywordModulo:
			case KeywordTheory:
			case KeywordBegin:
			case KeywordEnd:
			case KeywordVariants:
			case KeywordRestrict:
			case KeywordHeuristic:
			case KeywordExport:
			case KeywordPredicate:
			case KeywordOptions:
			case KeywordEquations:
			case KeywordFunctions:
			case KeywordBuiltins:
			case KeywordCase:
			case KeywordQED:
			case KeywordNext:
			case KeywordSplitEqs:
			case KeywordEquivLemma:
			case KeywordDiffEquivLemma:
			case E:
			case AC:
			case RuleFresh:
			case RuleIrecv:
			case RuleIsend:
			case RuleCoerce:
			case RuleIequality:
			case SuffixMsg:
			case SuffixNode:
			case GoalRankingIdentifierNoOracle:
			case GoalRankingIdentifierOracle:
			case GoalRankingIdentifiersNoOracle:
			case DHneutral:
			case BuiltinMun:
			case BuiltinOne:
			case BuiltinExp:
			case BuiltinMult:
			case BuiltinInv:
			case BuiltinPmult:
			case BuiltinEm:
			case BuiltinZero:
			case SolvedProof:
			case ProofMethodSorry:
			case ProofMethodSimplify:
			case ProofMethodSolve:
			case ProofMethodContradiction:
			case ProofMethodInduction:
			case BuiltinOptions:
			case BuiltinNameLocationsReport:
			case BuiltinNameReliableChannel:
			case BuiltinNameDH:
			case BuiltinNameBilinearPairing:
			case BuiltinNameMultiset:
			case BuiltinNameSymmetricEncryption:
			case BuiltinNameAsymmetricEncryption:
			case BuiltinNameSigning:
			case BuiltinNameDestPairing:
			case BuiltinNameDestSymmetricEncryption:
			case BuiltinNameDestAsymmetricEncryption:
			case BuiltinNameDestSigning:
			case BuiltinNameRevealingSigning:
			case BuiltinNameHashing:
			case TacticFunctionName:
			case SapicActionNew:
			case SapicActionInsert:
			case SapicActionDelete:
			case SapicActionLock:
			case SapicActionUnlock:
			case SapicActionEvent:
			case OneOp:
			case NullOp:
			case TOp:
			case FOp:
			case Left:
			case Right:
			case Both:
			case ForallTraces:
			case ExistsTrace:
			case OutFactIdent:
			case InFactIdent:
			case KuFactIdent:
			case KdFactIdent:
			case DedLogFactIdent:
			case FreshFactIdent:
			case RuleAttributeColor:
			case LemmaAttributeTyping:
			case LemmaAttributeSources:
			case LemmaAttributeReuse:
			case LemmaAttributeDiffReuse:
			case LemmaAttributeInduction:
			case LemmaAttributeHideLemma:
			case FunctionAttribute:
			case Natural:
			case Letters:
			case AlphaNums:
			case IDLower:
			case IDUpper:
			case IDNum:
				enterOuterAlt(_localctx, 1);
				{
				setState(1663);
				indexedIdentifier();
				setState(1664);
				match(Colon);
				setState(1665);
				match(KeywordPub);
				}
				break;
			case DollarOp:
				enterOuterAlt(_localctx, 2);
				{
				setState(1667);
				match(DollarOp);
				setState(1668);
				indexedIdentifier();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LogNodeVarContext extends ParserRuleContext {
		public IndexedIdentifierContext indexedIdentifier() {
			return getRuleContext(IndexedIdentifierContext.class,0);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public TerminalNode SuffixNode() { return getToken(TamarinParser.SuffixNode, 0); }
		public TerminalNode HashtagOp() { return getToken(TamarinParser.HashtagOp, 0); }
		public LogNodeVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logNodeVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLogNodeVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLogNodeVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLogNodeVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogNodeVarContext logNodeVar() throws RecognitionException {
		LogNodeVarContext _localctx = new LogNodeVarContext(_ctx, getState());
		enterRule(_localctx, 320, RULE_logNodeVar);
		try {
			setState(1677);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case KeywordDiff:
			case KeywordBy:
			case KeywordLemma:
			case KeywordIf:
			case KeywordElse:
			case KeywordProcess:
			case KeywordAs:
			case KeywordLookup:
			case KeywordLast:
			case KeywordPrio:
			case KeywordDeprio:
			case KeywordPresort:
			case KeywordSmallest:
			case KeywordId:
			case KeywordTactic:
			case KeywordRestriction:
			case KeywordAxiom:
			case KeywordAny:
			case KeywordTest:
			case KeywordNoPrecomp:
			case KeywordThen:
			case KeywordOut:
			case KeywordFresh:
			case KeywordPub:
			case KeywordXor:
			case KeywordOutput:
			case KeywordAccountsFor:
			case KeywordModulo:
			case KeywordTheory:
			case KeywordBegin:
			case KeywordEnd:
			case KeywordVariants:
			case KeywordRestrict:
			case KeywordHeuristic:
			case KeywordExport:
			case KeywordPredicate:
			case KeywordOptions:
			case KeywordEquations:
			case KeywordFunctions:
			case KeywordBuiltins:
			case KeywordCase:
			case KeywordQED:
			case KeywordNext:
			case KeywordSplitEqs:
			case KeywordEquivLemma:
			case KeywordDiffEquivLemma:
			case E:
			case AC:
			case RuleFresh:
			case RuleIrecv:
			case RuleIsend:
			case RuleCoerce:
			case RuleIequality:
			case SuffixMsg:
			case SuffixNode:
			case GoalRankingIdentifierNoOracle:
			case GoalRankingIdentifierOracle:
			case GoalRankingIdentifiersNoOracle:
			case DHneutral:
			case BuiltinMun:
			case BuiltinOne:
			case BuiltinExp:
			case BuiltinMult:
			case BuiltinInv:
			case BuiltinPmult:
			case BuiltinEm:
			case BuiltinZero:
			case SolvedProof:
			case ProofMethodSorry:
			case ProofMethodSimplify:
			case ProofMethodSolve:
			case ProofMethodContradiction:
			case ProofMethodInduction:
			case BuiltinOptions:
			case BuiltinNameLocationsReport:
			case BuiltinNameReliableChannel:
			case BuiltinNameDH:
			case BuiltinNameBilinearPairing:
			case BuiltinNameMultiset:
			case BuiltinNameSymmetricEncryption:
			case BuiltinNameAsymmetricEncryption:
			case BuiltinNameSigning:
			case BuiltinNameDestPairing:
			case BuiltinNameDestSymmetricEncryption:
			case BuiltinNameDestAsymmetricEncryption:
			case BuiltinNameDestSigning:
			case BuiltinNameRevealingSigning:
			case BuiltinNameHashing:
			case TacticFunctionName:
			case SapicActionNew:
			case SapicActionInsert:
			case SapicActionDelete:
			case SapicActionLock:
			case SapicActionUnlock:
			case SapicActionEvent:
			case OneOp:
			case NullOp:
			case TOp:
			case FOp:
			case Left:
			case Right:
			case Both:
			case ForallTraces:
			case ExistsTrace:
			case OutFactIdent:
			case InFactIdent:
			case KuFactIdent:
			case KdFactIdent:
			case DedLogFactIdent:
			case FreshFactIdent:
			case RuleAttributeColor:
			case LemmaAttributeTyping:
			case LemmaAttributeSources:
			case LemmaAttributeReuse:
			case LemmaAttributeDiffReuse:
			case LemmaAttributeInduction:
			case LemmaAttributeHideLemma:
			case FunctionAttribute:
			case Natural:
			case Letters:
			case AlphaNums:
			case IDLower:
			case IDUpper:
			case IDNum:
				enterOuterAlt(_localctx, 1);
				{
				setState(1671);
				indexedIdentifier();
				setState(1672);
				match(Colon);
				setState(1673);
				match(SuffixNode);
				}
				break;
			case HashtagOp:
				enterOuterAlt(_localctx, 2);
				{
				setState(1675);
				match(HashtagOp);
				setState(1676);
				indexedIdentifier();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LogVarContext extends ParserRuleContext {
		public LogMsgVarContext logMsgVar() {
			return getRuleContext(LogMsgVarContext.class,0);
		}
		public LogFreshVarContext logFreshVar() {
			return getRuleContext(LogFreshVarContext.class,0);
		}
		public LogPubVarContext logPubVar() {
			return getRuleContext(LogPubVarContext.class,0);
		}
		public LogNodeVarContext logNodeVar() {
			return getRuleContext(LogNodeVarContext.class,0);
		}
		public LogVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLogVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLogVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLogVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogVarContext logVar() throws RecognitionException {
		LogVarContext _localctx = new LogVarContext(_ctx, getState());
		enterRule(_localctx, 322, RULE_logVar);
		try {
			setState(1683);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,156,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1679);
				logMsgVar();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1680);
				logFreshVar();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1681);
				logPubVar();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1682);
				logNodeVar();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class MsgVarContext extends ParserRuleContext {
		public LogMsgVarContext logMsgVar() {
			return getRuleContext(LogMsgVarContext.class,0);
		}
		public LogFreshVarContext logFreshVar() {
			return getRuleContext(LogFreshVarContext.class,0);
		}
		public LogPubVarContext logPubVar() {
			return getRuleContext(LogPubVarContext.class,0);
		}
		public MsgVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_msgVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterMsgVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitMsgVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitMsgVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MsgVarContext msgVar() throws RecognitionException {
		MsgVarContext _localctx = new MsgVarContext(_ctx, getState());
		enterRule(_localctx, 324, RULE_msgVar);
		try {
			setState(1688);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,157,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1685);
				logMsgVar();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1686);
				logFreshVar();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1687);
				logPubVar();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LogicalLiteralContext extends ParserRuleContext {
		public FreshNameContext freshName() {
			return getRuleContext(FreshNameContext.class,0);
		}
		public PubNameContext pubName() {
			return getRuleContext(PubNameContext.class,0);
		}
		public MsgVarContext msgVar() {
			return getRuleContext(MsgVarContext.class,0);
		}
		public LogicalLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLogicalLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLogicalLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLogicalLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalLiteralContext logicalLiteral() throws RecognitionException {
		LogicalLiteralContext _localctx = new LogicalLiteralContext(_ctx, getState());
		enterRule(_localctx, 326, RULE_logicalLiteral);
		try {
			setState(1693);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,158,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1690);
				freshName();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1691);
				pubName();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1692);
				msgVar();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LogicalLiteralWithNodeContext extends ParserRuleContext {
		public FreshNameContext freshName() {
			return getRuleContext(FreshNameContext.class,0);
		}
		public PubNameContext pubName() {
			return getRuleContext(PubNameContext.class,0);
		}
		public LogVarContext logVar() {
			return getRuleContext(LogVarContext.class,0);
		}
		public LogicalLiteralWithNodeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalLiteralWithNode; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLogicalLiteralWithNode(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLogicalLiteralWithNode(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLogicalLiteralWithNode(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalLiteralWithNodeContext logicalLiteralWithNode() throws RecognitionException {
		LogicalLiteralWithNodeContext _localctx = new LogicalLiteralWithNodeContext(_ctx, getState());
		enterRule(_localctx, 328, RULE_logicalLiteralWithNode);
		try {
			setState(1698);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,159,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1695);
				freshName();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1696);
				pubName();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1697);
				logVar();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LogicalLiteralNoPubContext extends ParserRuleContext {
		public FreshNameContext freshName() {
			return getRuleContext(FreshNameContext.class,0);
		}
		public MsgVarContext msgVar() {
			return getRuleContext(MsgVarContext.class,0);
		}
		public LogicalLiteralNoPubContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalLiteralNoPub; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLogicalLiteralNoPub(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLogicalLiteralNoPub(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLogicalLiteralNoPub(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalLiteralNoPubContext logicalLiteralNoPub() throws RecognitionException {
		LogicalLiteralNoPubContext _localctx = new LogicalLiteralNoPubContext(_ctx, getState());
		enterRule(_localctx, 330, RULE_logicalLiteralNoPub);
		try {
			setState(1702);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,160,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1700);
				freshName();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1701);
				msgVar();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LiteralsContext extends ParserRuleContext {
		public String lit;
		public List<LiteralContext> literal() {
			return getRuleContexts(LiteralContext.class);
		}
		public LiteralContext literal(int i) {
			return getRuleContext(LiteralContext.class,i);
		}
		public LiteralsContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public LiteralsContext(ParserRuleContext parent, int invokingState, String lit) {
			super(parent, invokingState);
			this.lit = lit;
		}
		@Override public int getRuleIndex() { return RULE_literals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralsContext literals(String lit) throws RecognitionException {
		LiteralsContext _localctx = new LiteralsContext(_ctx, getState(), lit);
		enterRule(_localctx, 332, RULE_literals);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1705); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(1704);
					literal(_localctx.lit);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(1707); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,161,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LiteralContext extends ParserRuleContext {
		public String lit;
		public FreshNameContext freshName() {
			return getRuleContext(FreshNameContext.class,0);
		}
		public PubNameContext pubName() {
			return getRuleContext(PubNameContext.class,0);
		}
		public LogVarContext logVar() {
			return getRuleContext(LogVarContext.class,0);
		}
		public MsgVarContext msgVar() {
			return getRuleContext(MsgVarContext.class,0);
		}
		public NodeVarContext nodeVar() {
			return getRuleContext(NodeVarContext.class,0);
		}
		public LogicalLiteralContext logicalLiteral() {
			return getRuleContext(LogicalLiteralContext.class,0);
		}
		public LogicalLiteralNoPubContext logicalLiteralNoPub() {
			return getRuleContext(LogicalLiteralNoPubContext.class,0);
		}
		public SapicVarContext sapicVar() {
			return getRuleContext(SapicVarContext.class,0);
		}
		public SapicNodeVarContext sapicNodeVar() {
			return getRuleContext(SapicNodeVarContext.class,0);
		}
		public LogicalTypedLiteralContext logicalTypedLiteral() {
			return getRuleContext(LogicalTypedLiteralContext.class,0);
		}
		public SapicPatternVarContext sapicPatternVar() {
			return getRuleContext(SapicPatternVarContext.class,0);
		}
		public LogicalTypedPatternLiteralContext logicalTypedPatternLiteral() {
			return getRuleContext(LogicalTypedPatternLiteralContext.class,0);
		}
		public LiteralContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public LiteralContext(ParserRuleContext parent, int invokingState, String lit) {
			super(parent, invokingState);
			this.lit = lit;
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralContext literal(String lit) throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState(), lit);
		enterRule(_localctx, 334, RULE_literal);
		try {
			setState(1734);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,163,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1709);
				if (!(_localctx.lit.contains("vlit"))) throw new FailedPredicateException(this, "$lit.contains(\"vlit\")");
				setState(1712);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case TildeOp:
					{
					setState(1710);
					freshName();
					}
					break;
				case SingleQuote:
					{
					setState(1711);
					pubName();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1714);
				if (!(_localctx.lit.contains("logVar"))) throw new FailedPredicateException(this, "$lit.contains(\"logVar\")");
				setState(1715);
				logVar();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1716);
				if (!(_localctx.lit.contains("msgVar"))) throw new FailedPredicateException(this, "$lit.contains(\"msgVar\")");
				setState(1717);
				msgVar();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1718);
				if (!(_localctx.lit.contains("nodeVar"))) throw new FailedPredicateException(this, "$lit.contains(\"nodeVar\")");
				setState(1719);
				nodeVar();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1720);
				if (!(_localctx.lit.contains("logicalLiteral") && !_localctx.lit.contains("logicalLiteralNoPub"))) throw new FailedPredicateException(this, "$lit.contains(\"logicalLiteral\") && !$lit.contains(\"logicalLiteralNoPub\")");
				setState(1721);
				logicalLiteral();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1722);
				if (!(_localctx.lit.contains("logicalLiteralNoPub"))) throw new FailedPredicateException(this, "$lit.contains(\"logicalLiteralNoPub\")");
				setState(1723);
				logicalLiteralNoPub();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1724);
				if (!(_localctx.lit.contains("sapicVar"))) throw new FailedPredicateException(this, "$lit.contains(\"sapicVar\")");
				setState(1725);
				sapicVar();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(1726);
				if (!(_localctx.lit.contains("sapicNodeVar"))) throw new FailedPredicateException(this, "$lit.contains(\"sapicNodeVar\")");
				setState(1727);
				sapicNodeVar();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(1728);
				if (!(_localctx.lit.contains("logicalTypedLiteral"))) throw new FailedPredicateException(this, "$lit.contains(\"logicalTypedLiteral\")");
				setState(1729);
				logicalTypedLiteral();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(1730);
				if (!(_localctx.lit.contains("sapicPatternVar"))) throw new FailedPredicateException(this, "$lit.contains(\"sapicPatternVar\")");
				setState(1731);
				sapicPatternVar();
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(1732);
				if (!(_localctx.lit.contains("logicalTypedPatternLiteral"))) throw new FailedPredicateException(this, "$lit.contains(\"logicalTypedPatternLiteral\")");
				setState(1733);
				logicalTypedPatternLiteral();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class NodeVarContext extends ParserRuleContext {
		public IndexedIdentifierContext indexedIdentifier() {
			return getRuleContext(IndexedIdentifierContext.class,0);
		}
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public TerminalNode SuffixNode() { return getToken(TamarinParser.SuffixNode, 0); }
		public TerminalNode HashtagOp() { return getToken(TamarinParser.HashtagOp, 0); }
		public NodeVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nodeVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterNodeVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitNodeVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitNodeVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NodeVarContext nodeVar() throws RecognitionException {
		NodeVarContext _localctx = new NodeVarContext(_ctx, getState());
		enterRule(_localctx, 336, RULE_nodeVar);
		int _la;
		try {
			setState(1744);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,165,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1736);
				indexedIdentifier();
				setState(1737);
				match(Colon);
				setState(1738);
				match(SuffixNode);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1741);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==HashtagOp) {
					{
					setState(1740);
					match(HashtagOp);
					}
				}

				setState(1743);
				indexedIdentifier();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FreshNameContext extends ParserRuleContext {
		public TerminalNode TildeOp() { return getToken(TamarinParser.TildeOp, 0); }
		public List<TerminalNode> SingleQuote() { return getTokens(TamarinParser.SingleQuote); }
		public TerminalNode SingleQuote(int i) {
			return getToken(TamarinParser.SingleQuote, i);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public FreshNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_freshName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFreshName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFreshName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFreshName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FreshNameContext freshName() throws RecognitionException {
		FreshNameContext _localctx = new FreshNameContext(_ctx, getState());
		enterRule(_localctx, 338, RULE_freshName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1746);
			match(TildeOp);
			setState(1747);
			match(SingleQuote);
			setState(1748);
			identifier();
			setState(1749);
			match(SingleQuote);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class PubNameContext extends ParserRuleContext {
		public List<TerminalNode> SingleQuote() { return getTokens(TamarinParser.SingleQuote); }
		public TerminalNode SingleQuote(int i) {
			return getToken(TamarinParser.SingleQuote, i);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public PubNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pubName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterPubName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitPubName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitPubName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PubNameContext pubName() throws RecognitionException {
		PubNameContext _localctx = new PubNameContext(_ctx, getState());
		enterRule(_localctx, 340, RULE_pubName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1751);
			match(SingleQuote);
			setState(1752);
			identifier();
			setState(1753);
			match(SingleQuote);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FormalCommentContext extends ParserRuleContext {
		public TerminalNode Letters() { return getToken(TamarinParser.Letters, 0); }
		public TerminalNode FormalComment() { return getToken(TamarinParser.FormalComment, 0); }
		public FormalCommentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalComment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFormalComment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFormalComment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFormalComment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FormalCommentContext formalComment() throws RecognitionException {
		FormalCommentContext _localctx = new FormalCommentContext(_ctx, getState());
		enterRule(_localctx, 342, RULE_formalComment);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1755);
			match(Letters);
			setState(1756);
			match(FormalComment);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TacticFunctionIdentifierContext extends ParserRuleContext {
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public List<KeywordsContext> keywords() {
			return getRuleContexts(KeywordsContext.class);
		}
		public KeywordsContext keywords(int i) {
			return getRuleContext(KeywordsContext.class,i);
		}
		public List<PunctuationContext> punctuation() {
			return getRuleContexts(PunctuationContext.class);
		}
		public PunctuationContext punctuation(int i) {
			return getRuleContext(PunctuationContext.class,i);
		}
		public List<TerminalNode> Backslash() { return getTokens(TamarinParser.Backslash); }
		public TerminalNode Backslash(int i) {
			return getToken(TamarinParser.Backslash, i);
		}
		public List<TerminalNode> LeftBrace() { return getTokens(TamarinParser.LeftBrace); }
		public TerminalNode LeftBrace(int i) {
			return getToken(TamarinParser.LeftBrace, i);
		}
		public List<TerminalNode> RightBrace() { return getTokens(TamarinParser.RightBrace); }
		public TerminalNode RightBrace(int i) {
			return getToken(TamarinParser.RightBrace, i);
		}
		public List<OperatorsContext> operators() {
			return getRuleContexts(OperatorsContext.class);
		}
		public OperatorsContext operators(int i) {
			return getRuleContext(OperatorsContext.class,i);
		}
		public List<TerminalNode> BodyChar() { return getTokens(TamarinParser.BodyChar); }
		public TerminalNode BodyChar(int i) {
			return getToken(TamarinParser.BodyChar, i);
		}
		public TacticFunctionIdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tacticFunctionIdentifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTacticFunctionIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTacticFunctionIdentifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTacticFunctionIdentifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TacticFunctionIdentifierContext tacticFunctionIdentifier() throws RecognitionException {
		TacticFunctionIdentifierContext _localctx = new TacticFunctionIdentifierContext(_ctx, getState());
		enterRule(_localctx, 344, RULE_tacticFunctionIdentifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1768);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & -10L) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & -4398046511105L) != 0) || ((((_la - 128)) & ~0x3f) == 0 && ((1L << (_la - 128)) & 148618786663038975L) != 0)) {
				{
				setState(1766);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,166,_ctx) ) {
				case 1:
					{
					setState(1758);
					identifier();
					}
					break;
				case 2:
					{
					setState(1759);
					keywords();
					}
					break;
				case 3:
					{
					setState(1760);
					punctuation();
					}
					break;
				case 4:
					{
					setState(1761);
					match(Backslash);
					}
					break;
				case 5:
					{
					setState(1762);
					match(LeftBrace);
					}
					break;
				case 6:
					{
					setState(1763);
					match(RightBrace);
					}
					break;
				case 7:
					{
					setState(1764);
					operators();
					}
					break;
				case 8:
					{
					setState(1765);
					match(BodyChar);
					}
					break;
				}
				}
				setState(1770);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class InternalTacticNameContext extends ParserRuleContext {
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public List<KeywordsContext> keywords() {
			return getRuleContexts(KeywordsContext.class);
		}
		public KeywordsContext keywords(int i) {
			return getRuleContext(KeywordsContext.class,i);
		}
		public List<PunctuationContext> punctuation() {
			return getRuleContexts(PunctuationContext.class);
		}
		public PunctuationContext punctuation(int i) {
			return getRuleContext(PunctuationContext.class,i);
		}
		public List<TerminalNode> Backslash() { return getTokens(TamarinParser.Backslash); }
		public TerminalNode Backslash(int i) {
			return getToken(TamarinParser.Backslash, i);
		}
		public List<OperatorsContext> operators() {
			return getRuleContexts(OperatorsContext.class);
		}
		public OperatorsContext operators(int i) {
			return getRuleContext(OperatorsContext.class,i);
		}
		public List<TerminalNode> BodyChar() { return getTokens(TamarinParser.BodyChar); }
		public TerminalNode BodyChar(int i) {
			return getToken(TamarinParser.BodyChar, i);
		}
		public InternalTacticNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_internalTacticName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterInternalTacticName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitInternalTacticName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitInternalTacticName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InternalTacticNameContext internalTacticName() throws RecognitionException {
		InternalTacticNameContext _localctx = new InternalTacticNameContext(_ctx, getState());
		enterRule(_localctx, 346, RULE_internalTacticName);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1777); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					setState(1777);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,168,_ctx) ) {
					case 1:
						{
						setState(1771);
						identifier();
						}
						break;
					case 2:
						{
						setState(1772);
						keywords();
						}
						break;
					case 3:
						{
						setState(1773);
						punctuation();
						}
						break;
					case 4:
						{
						setState(1774);
						match(Backslash);
						}
						break;
					case 5:
						{
						setState(1775);
						operators();
						}
						break;
					case 6:
						{
						setState(1776);
						match(BodyChar);
						}
						break;
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(1779); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,169,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class OracleRelativePathContext extends ParserRuleContext {
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public List<KeywordsContext> keywords() {
			return getRuleContexts(KeywordsContext.class);
		}
		public KeywordsContext keywords(int i) {
			return getRuleContext(KeywordsContext.class,i);
		}
		public List<PunctuationContext> punctuation() {
			return getRuleContexts(PunctuationContext.class);
		}
		public PunctuationContext punctuation(int i) {
			return getRuleContext(PunctuationContext.class,i);
		}
		public List<TerminalNode> Backslash() { return getTokens(TamarinParser.Backslash); }
		public TerminalNode Backslash(int i) {
			return getToken(TamarinParser.Backslash, i);
		}
		public List<TerminalNode> LeftBrace() { return getTokens(TamarinParser.LeftBrace); }
		public TerminalNode LeftBrace(int i) {
			return getToken(TamarinParser.LeftBrace, i);
		}
		public List<TerminalNode> RightBrace() { return getTokens(TamarinParser.RightBrace); }
		public TerminalNode RightBrace(int i) {
			return getToken(TamarinParser.RightBrace, i);
		}
		public List<OperatorsContext> operators() {
			return getRuleContexts(OperatorsContext.class);
		}
		public OperatorsContext operators(int i) {
			return getRuleContext(OperatorsContext.class,i);
		}
		public List<TerminalNode> BodyChar() { return getTokens(TamarinParser.BodyChar); }
		public TerminalNode BodyChar(int i) {
			return getToken(TamarinParser.BodyChar, i);
		}
		public OracleRelativePathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oracleRelativePath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterOracleRelativePath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitOracleRelativePath(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitOracleRelativePath(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OracleRelativePathContext oracleRelativePath() throws RecognitionException {
		OracleRelativePathContext _localctx = new OracleRelativePathContext(_ctx, getState());
		enterRule(_localctx, 348, RULE_oracleRelativePath);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1789); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(1789);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,170,_ctx) ) {
				case 1:
					{
					setState(1781);
					identifier();
					}
					break;
				case 2:
					{
					setState(1782);
					keywords();
					}
					break;
				case 3:
					{
					setState(1783);
					punctuation();
					}
					break;
				case 4:
					{
					setState(1784);
					match(Backslash);
					}
					break;
				case 5:
					{
					setState(1785);
					match(LeftBrace);
					}
					break;
				case 6:
					{
					setState(1786);
					match(RightBrace);
					}
					break;
				case 7:
					{
					setState(1787);
					operators();
					}
					break;
				case 8:
					{
					setState(1788);
					match(BodyChar);
					}
					break;
				}
				}
				setState(1791); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & -10L) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & -4398046511105L) != 0) || ((((_la - 128)) & ~0x3f) == 0 && ((1L << (_la - 128)) & 148618786663038975L) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ExportBodyCharsContext extends ParserRuleContext {
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public List<KeywordsContext> keywords() {
			return getRuleContexts(KeywordsContext.class);
		}
		public KeywordsContext keywords(int i) {
			return getRuleContext(KeywordsContext.class,i);
		}
		public List<PunctuationContext> punctuation() {
			return getRuleContexts(PunctuationContext.class);
		}
		public PunctuationContext punctuation(int i) {
			return getRuleContext(PunctuationContext.class,i);
		}
		public List<TerminalNode> LeftBrace() { return getTokens(TamarinParser.LeftBrace); }
		public TerminalNode LeftBrace(int i) {
			return getToken(TamarinParser.LeftBrace, i);
		}
		public List<TerminalNode> RightBrace() { return getTokens(TamarinParser.RightBrace); }
		public TerminalNode RightBrace(int i) {
			return getToken(TamarinParser.RightBrace, i);
		}
		public List<OperatorsContext> operators() {
			return getRuleContexts(OperatorsContext.class);
		}
		public OperatorsContext operators(int i) {
			return getRuleContext(OperatorsContext.class,i);
		}
		public List<TerminalNode> BodyChar() { return getTokens(TamarinParser.BodyChar); }
		public TerminalNode BodyChar(int i) {
			return getToken(TamarinParser.BodyChar, i);
		}
		public ExportBodyCharsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exportBodyChars; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterExportBodyChars(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitExportBodyChars(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitExportBodyChars(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExportBodyCharsContext exportBodyChars() throws RecognitionException {
		ExportBodyCharsContext _localctx = new ExportBodyCharsContext(_ctx, getState());
		enterRule(_localctx, 350, RULE_exportBodyChars);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1802);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & -10L) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & -4398046511105L) != 0) || ((((_la - 128)) & ~0x3f) == 0 && ((1L << (_la - 128)) & 148618786663038975L) != 0)) {
				{
				setState(1800);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,172,_ctx) ) {
				case 1:
					{
					setState(1793);
					identifier();
					}
					break;
				case 2:
					{
					setState(1794);
					keywords();
					}
					break;
				case 3:
					{
					setState(1795);
					punctuation();
					}
					break;
				case 4:
					{
					setState(1796);
					match(LeftBrace);
					}
					break;
				case 5:
					{
					setState(1797);
					match(RightBrace);
					}
					break;
				case 6:
					{
					setState(1798);
					operators();
					}
					break;
				case 7:
					{
					setState(1799);
					match(BodyChar);
					}
					break;
				}
				}
				setState(1804);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class CharDirContext extends ParserRuleContext {
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public List<TerminalNode> Letters() { return getTokens(TamarinParser.Letters); }
		public TerminalNode Letters(int i) {
			return getToken(TamarinParser.Letters, i);
		}
		public List<TerminalNode> Natural() { return getTokens(TamarinParser.Natural); }
		public TerminalNode Natural(int i) {
			return getToken(TamarinParser.Natural, i);
		}
		public List<TerminalNode> AlphaNums() { return getTokens(TamarinParser.AlphaNums); }
		public TerminalNode AlphaNums(int i) {
			return getToken(TamarinParser.AlphaNums, i);
		}
		public List<TerminalNode> Backslash() { return getTokens(TamarinParser.Backslash); }
		public TerminalNode Backslash(int i) {
			return getToken(TamarinParser.Backslash, i);
		}
		public List<TerminalNode> Slash() { return getTokens(TamarinParser.Slash); }
		public TerminalNode Slash(int i) {
			return getToken(TamarinParser.Slash, i);
		}
		public List<TerminalNode> Dot() { return getTokens(TamarinParser.Dot); }
		public TerminalNode Dot(int i) {
			return getToken(TamarinParser.Dot, i);
		}
		public CharDirContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_charDir; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterCharDir(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitCharDir(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitCharDir(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CharDirContext charDir() throws RecognitionException {
		CharDirContext _localctx = new CharDirContext(_ctx, getState());
		enterRule(_localctx, 352, RULE_charDir);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1814);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & -16L) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & -4611684059922300929L) != 0) || ((((_la - 146)) & ~0x3f) == 0 && ((1L << (_la - 146)) & 17179865203L) != 0)) {
				{
				setState(1812);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,174,_ctx) ) {
				case 1:
					{
					setState(1805);
					identifier();
					}
					break;
				case 2:
					{
					setState(1806);
					match(Letters);
					}
					break;
				case 3:
					{
					setState(1807);
					match(Natural);
					}
					break;
				case 4:
					{
					setState(1808);
					match(AlphaNums);
					}
					break;
				case 5:
					{
					setState(1809);
					match(Backslash);
					}
					break;
				case 6:
					{
					setState(1810);
					match(Slash);
					}
					break;
				case 7:
					{
					setState(1811);
					match(Dot);
					}
					break;
				}
				}
				setState(1816);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class EquivOpContext extends ParserRuleContext {
		public TerminalNode EquivOp() { return getToken(TamarinParser.EquivOp, 0); }
		public TerminalNode EquivSignOp() { return getToken(TamarinParser.EquivSignOp, 0); }
		public EquivOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equivOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterEquivOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitEquivOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitEquivOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EquivOpContext equivOp() throws RecognitionException {
		EquivOpContext _localctx = new EquivOpContext(_ctx, getState());
		enterRule(_localctx, 354, RULE_equivOp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1817);
			_la = _input.LA(1);
			if ( !(_la==EquivOp || _la==EquivSignOp) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class OrOpContext extends ParserRuleContext {
		public TerminalNode OrOp() { return getToken(TamarinParser.OrOp, 0); }
		public TerminalNode PipeOp() { return getToken(TamarinParser.PipeOp, 0); }
		public OrOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterOrOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitOrOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitOrOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrOpContext orOp() throws RecognitionException {
		OrOpContext _localctx = new OrOpContext(_ctx, getState());
		enterRule(_localctx, 356, RULE_orOp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1819);
			_la = _input.LA(1);
			if ( !(_la==OrOp || _la==PipeOp) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class NotOpContext extends ParserRuleContext {
		public TerminalNode NotOp() { return getToken(TamarinParser.NotOp, 0); }
		public NotOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterNotOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitNotOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitNotOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NotOpContext notOp() throws RecognitionException {
		NotOpContext _localctx = new NotOpContext(_ctx, getState());
		enterRule(_localctx, 358, RULE_notOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1821);
			match(NotOp);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class AndOpContext extends ParserRuleContext {
		public TerminalNode AndOp() { return getToken(TamarinParser.AndOp, 0); }
		public AndOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_andOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterAndOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitAndOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitAndOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AndOpContext andOp() throws RecognitionException {
		AndOpContext _localctx = new AndOpContext(_ctx, getState());
		enterRule(_localctx, 360, RULE_andOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1823);
			match(AndOp);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class EqOpContext extends ParserRuleContext {
		public TerminalNode EqOp() { return getToken(TamarinParser.EqOp, 0); }
		public EqOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_eqOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterEqOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitEqOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitEqOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EqOpContext eqOp() throws RecognitionException {
		EqOpContext _localctx = new EqOpContext(_ctx, getState());
		enterRule(_localctx, 362, RULE_eqOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1825);
			match(EqOp);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BotOpContext extends ParserRuleContext {
		public TerminalNode BotOp() { return getToken(TamarinParser.BotOp, 0); }
		public BotOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_botOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBotOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBotOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBotOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BotOpContext botOp() throws RecognitionException {
		BotOpContext _localctx = new BotOpContext(_ctx, getState());
		enterRule(_localctx, 364, RULE_botOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1827);
			match(BotOp);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FOpContext extends ParserRuleContext {
		public TerminalNode FOp() { return getToken(TamarinParser.FOp, 0); }
		public FOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterFOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitFOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitFOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FOpContext fOp() throws RecognitionException {
		FOpContext _localctx = new FOpContext(_ctx, getState());
		enterRule(_localctx, 366, RULE_fOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1829);
			match(FOp);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TOpContext extends ParserRuleContext {
		public TerminalNode TOp() { return getToken(TamarinParser.TOp, 0); }
		public TOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TOpContext tOp() throws RecognitionException {
		TOpContext _localctx = new TOpContext(_ctx, getState());
		enterRule(_localctx, 368, RULE_tOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1831);
			match(TOp);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TopOpContext extends ParserRuleContext {
		public TerminalNode TopOp() { return getToken(TamarinParser.TopOp, 0); }
		public TopOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_topOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterTopOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitTopOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitTopOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TopOpContext topOp() throws RecognitionException {
		TopOpContext _localctx = new TopOpContext(_ctx, getState());
		enterRule(_localctx, 370, RULE_topOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1833);
			match(TopOp);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ForallOpContext extends ParserRuleContext {
		public TerminalNode ForallOp() { return getToken(TamarinParser.ForallOp, 0); }
		public ForallOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forallOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterForallOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitForallOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitForallOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForallOpContext forallOp() throws RecognitionException {
		ForallOpContext _localctx = new ForallOpContext(_ctx, getState());
		enterRule(_localctx, 372, RULE_forallOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1835);
			match(ForallOp);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ExistsOpContext extends ParserRuleContext {
		public TerminalNode ExistsOp() { return getToken(TamarinParser.ExistsOp, 0); }
		public ExistsOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_existsOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterExistsOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitExistsOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitExistsOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExistsOpContext existsOp() throws RecognitionException {
		ExistsOpContext _localctx = new ExistsOpContext(_ctx, getState());
		enterRule(_localctx, 374, RULE_existsOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1837);
			match(ExistsOp);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinXorContext extends ParserRuleContext {
		public TerminalNode KeywordXor() { return getToken(TamarinParser.KeywordXor, 0); }
		public BuiltinXorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinXor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinXor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinXor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinXor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinXorContext builtinXor() throws RecognitionException {
		BuiltinXorContext _localctx = new BuiltinXorContext(_ctx, getState());
		enterRule(_localctx, 376, RULE_builtinXor);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1839);
			match(KeywordXor);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinMunContext extends ParserRuleContext {
		public TerminalNode BuiltinMun() { return getToken(TamarinParser.BuiltinMun, 0); }
		public BuiltinMunContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinMun; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinMun(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinMun(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinMun(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinMunContext builtinMun() throws RecognitionException {
		BuiltinMunContext _localctx = new BuiltinMunContext(_ctx, getState());
		enterRule(_localctx, 378, RULE_builtinMun);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1841);
			match(BuiltinMun);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinOneContext extends ParserRuleContext {
		public TerminalNode BuiltinOne() { return getToken(TamarinParser.BuiltinOne, 0); }
		public BuiltinOneContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinOne; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinOne(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinOne(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinOne(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinOneContext builtinOne() throws RecognitionException {
		BuiltinOneContext _localctx = new BuiltinOneContext(_ctx, getState());
		enterRule(_localctx, 380, RULE_builtinOne);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1843);
			match(BuiltinOne);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinExpContext extends ParserRuleContext {
		public TerminalNode BuiltinExp() { return getToken(TamarinParser.BuiltinExp, 0); }
		public BuiltinExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinExp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinExp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinExp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinExpContext builtinExp() throws RecognitionException {
		BuiltinExpContext _localctx = new BuiltinExpContext(_ctx, getState());
		enterRule(_localctx, 382, RULE_builtinExp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1845);
			match(BuiltinExp);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinMultContext extends ParserRuleContext {
		public TerminalNode BuiltinMult() { return getToken(TamarinParser.BuiltinMult, 0); }
		public BuiltinMultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinMult; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinMult(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinMult(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinMult(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinMultContext builtinMult() throws RecognitionException {
		BuiltinMultContext _localctx = new BuiltinMultContext(_ctx, getState());
		enterRule(_localctx, 384, RULE_builtinMult);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1847);
			match(BuiltinMult);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinInvContext extends ParserRuleContext {
		public TerminalNode BuiltinInv() { return getToken(TamarinParser.BuiltinInv, 0); }
		public BuiltinInvContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinInv; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinInv(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinInv(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinInv(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinInvContext builtinInv() throws RecognitionException {
		BuiltinInvContext _localctx = new BuiltinInvContext(_ctx, getState());
		enterRule(_localctx, 386, RULE_builtinInv);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1849);
			match(BuiltinInv);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinPmultContext extends ParserRuleContext {
		public TerminalNode BuiltinPmult() { return getToken(TamarinParser.BuiltinPmult, 0); }
		public BuiltinPmultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinPmult; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinPmult(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinPmult(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinPmult(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinPmultContext builtinPmult() throws RecognitionException {
		BuiltinPmultContext _localctx = new BuiltinPmultContext(_ctx, getState());
		enterRule(_localctx, 388, RULE_builtinPmult);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1851);
			match(BuiltinPmult);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinEmContext extends ParserRuleContext {
		public TerminalNode BuiltinEm() { return getToken(TamarinParser.BuiltinEm, 0); }
		public BuiltinEmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinEm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinEm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinEm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinEm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinEmContext builtinEm() throws RecognitionException {
		BuiltinEmContext _localctx = new BuiltinEmContext(_ctx, getState());
		enterRule(_localctx, 390, RULE_builtinEm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1853);
			match(BuiltinEm);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class BuiltinZeroContext extends ParserRuleContext {
		public TerminalNode BuiltinZero() { return getToken(TamarinParser.BuiltinZero, 0); }
		public BuiltinZeroContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinZero; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterBuiltinZero(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitBuiltinZero(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitBuiltinZero(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltinZeroContext builtinZero() throws RecognitionException {
		BuiltinZeroContext _localctx = new BuiltinZeroContext(_ctx, getState());
		enterRule(_localctx, 392, RULE_builtinZero);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1855);
			match(BuiltinZero);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DhNeutralContext extends ParserRuleContext {
		public TerminalNode DHneutral() { return getToken(TamarinParser.DHneutral, 0); }
		public DhNeutralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dhNeutral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterDhNeutral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitDhNeutral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitDhNeutral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DhNeutralContext dhNeutral() throws RecognitionException {
		DhNeutralContext _localctx = new DhNeutralContext(_ctx, getState());
		enterRule(_localctx, 394, RULE_dhNeutral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1857);
			match(DHneutral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class OperatorsContext extends ParserRuleContext {
		public TerminalNode OrOp() { return getToken(TamarinParser.OrOp, 0); }
		public TerminalNode PipeOp() { return getToken(TamarinParser.PipeOp, 0); }
		public TerminalNode DoublePipeOp() { return getToken(TamarinParser.DoublePipeOp, 0); }
		public TerminalNode AndOp() { return getToken(TamarinParser.AndOp, 0); }
		public TerminalNode NotOp() { return getToken(TamarinParser.NotOp, 0); }
		public TerminalNode PlusOp() { return getToken(TamarinParser.PlusOp, 0); }
		public TerminalNode MinusOp() { return getToken(TamarinParser.MinusOp, 0); }
		public TerminalNode XorOp() { return getToken(TamarinParser.XorOp, 0); }
		public TerminalNode MultOp() { return getToken(TamarinParser.MultOp, 0); }
		public TerminalNode ExpOp() { return getToken(TamarinParser.ExpOp, 0); }
		public TerminalNode EqOp() { return getToken(TamarinParser.EqOp, 0); }
		public TerminalNode AtOp() { return getToken(TamarinParser.AtOp, 0); }
		public TerminalNode OneOp() { return getToken(TamarinParser.OneOp, 0); }
		public TerminalNode NullOp() { return getToken(TamarinParser.NullOp, 0); }
		public TerminalNode RequiresOp() { return getToken(TamarinParser.RequiresOp, 0); }
		public TerminalNode SplitOp() { return getToken(TamarinParser.SplitOp, 0); }
		public TerminalNode BangOp() { return getToken(TamarinParser.BangOp, 0); }
		public TerminalNode HashtagOp() { return getToken(TamarinParser.HashtagOp, 0); }
		public TerminalNode LessOp() { return getToken(TamarinParser.LessOp, 0); }
		public TerminalNode LessTermOp() { return getToken(TamarinParser.LessTermOp, 0); }
		public TerminalNode GreaterOp() { return getToken(TamarinParser.GreaterOp, 0); }
		public TerminalNode ForallOp() { return getToken(TamarinParser.ForallOp, 0); }
		public TerminalNode ExistsOp() { return getToken(TamarinParser.ExistsOp, 0); }
		public TerminalNode ImpliesOp() { return getToken(TamarinParser.ImpliesOp, 0); }
		public TerminalNode EquivOp() { return getToken(TamarinParser.EquivOp, 0); }
		public TerminalNode EquivSignOp() { return getToken(TamarinParser.EquivSignOp, 0); }
		public TerminalNode TildeOp() { return getToken(TamarinParser.TildeOp, 0); }
		public TerminalNode DollarOp() { return getToken(TamarinParser.DollarOp, 0); }
		public TerminalNode ChainOp() { return getToken(TamarinParser.ChainOp, 0); }
		public TerminalNode ActionArrowStart() { return getToken(TamarinParser.ActionArrowStart, 0); }
		public TerminalNode ActionArrowTip() { return getToken(TamarinParser.ActionArrowTip, 0); }
		public TerminalNode LongRightArrowOp() { return getToken(TamarinParser.LongRightArrowOp, 0); }
		public TerminalNode TOp() { return getToken(TamarinParser.TOp, 0); }
		public TerminalNode FOp() { return getToken(TamarinParser.FOp, 0); }
		public TerminalNode TopOp() { return getToken(TamarinParser.TopOp, 0); }
		public TerminalNode BotOp() { return getToken(TamarinParser.BotOp, 0); }
		public OperatorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterOperators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitOperators(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitOperators(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperatorsContext operators() throws RecognitionException {
		OperatorsContext _localctx = new OperatorsContext(_ctx, getState());
		enterRule(_localctx, 396, RULE_operators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1859);
			_la = _input.LA(1);
			if ( !(((((_la - 114)) & ~0x3f) == 0 && ((1L << (_la - 114)) & 68719476735L) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class PunctuationContext extends ParserRuleContext {
		public TerminalNode Colon() { return getToken(TamarinParser.Colon, 0); }
		public TerminalNode Semicolon() { return getToken(TamarinParser.Semicolon, 0); }
		public TerminalNode Comma() { return getToken(TamarinParser.Comma, 0); }
		public TerminalNode Dot() { return getToken(TamarinParser.Dot, 0); }
		public TerminalNode Slash() { return getToken(TamarinParser.Slash, 0); }
		public TerminalNode Backslash() { return getToken(TamarinParser.Backslash, 0); }
		public TerminalNode SingleQuote() { return getToken(TamarinParser.SingleQuote, 0); }
		public TerminalNode LeftParenthesis() { return getToken(TamarinParser.LeftParenthesis, 0); }
		public TerminalNode RightParenthesis() { return getToken(TamarinParser.RightParenthesis, 0); }
		public TerminalNode LeftBrace() { return getToken(TamarinParser.LeftBrace, 0); }
		public TerminalNode RightBrace() { return getToken(TamarinParser.RightBrace, 0); }
		public TerminalNode LeftBracket() { return getToken(TamarinParser.LeftBracket, 0); }
		public TerminalNode RightBracket() { return getToken(TamarinParser.RightBracket, 0); }
		public TerminalNode Underscore() { return getToken(TamarinParser.Underscore, 0); }
		public PunctuationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_punctuation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterPunctuation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitPunctuation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitPunctuation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PunctuationContext punctuation() throws RecognitionException {
		PunctuationContext _localctx = new PunctuationContext(_ctx, getState());
		enterRule(_localctx, 398, RULE_punctuation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1861);
			_la = _input.LA(1);
			if ( !(((((_la - 99)) & ~0x3f) == 0 && ((1L << (_la - 99)) & 32639L) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class AllowedIdentifierKeywordsContext extends ParserRuleContext {
		public TerminalNode E() { return getToken(TamarinParser.E, 0); }
		public TerminalNode AC() { return getToken(TamarinParser.AC, 0); }
		public TerminalNode GoalRankingIdentifierNoOracle() { return getToken(TamarinParser.GoalRankingIdentifierNoOracle, 0); }
		public TerminalNode GoalRankingIdentifiersNoOracle() { return getToken(TamarinParser.GoalRankingIdentifiersNoOracle, 0); }
		public TerminalNode GoalRankingIdentifierOracle() { return getToken(TamarinParser.GoalRankingIdentifierOracle, 0); }
		public TerminalNode KeywordBy() { return getToken(TamarinParser.KeywordBy, 0); }
		public TerminalNode KeywordDiff() { return getToken(TamarinParser.KeywordDiff, 0); }
		public TerminalNode KeywordLemma() { return getToken(TamarinParser.KeywordLemma, 0); }
		public TerminalNode KeywordIf() { return getToken(TamarinParser.KeywordIf, 0); }
		public TerminalNode KeywordElse() { return getToken(TamarinParser.KeywordElse, 0); }
		public TerminalNode KeywordProcess() { return getToken(TamarinParser.KeywordProcess, 0); }
		public TerminalNode KeywordAs() { return getToken(TamarinParser.KeywordAs, 0); }
		public TerminalNode KeywordLookup() { return getToken(TamarinParser.KeywordLookup, 0); }
		public TerminalNode KeywordLast() { return getToken(TamarinParser.KeywordLast, 0); }
		public TerminalNode KeywordPrio() { return getToken(TamarinParser.KeywordPrio, 0); }
		public TerminalNode KeywordDeprio() { return getToken(TamarinParser.KeywordDeprio, 0); }
		public TerminalNode KeywordPresort() { return getToken(TamarinParser.KeywordPresort, 0); }
		public TerminalNode KeywordSmallest() { return getToken(TamarinParser.KeywordSmallest, 0); }
		public TerminalNode KeywordId() { return getToken(TamarinParser.KeywordId, 0); }
		public TerminalNode KeywordTactic() { return getToken(TamarinParser.KeywordTactic, 0); }
		public TerminalNode KeywordRestriction() { return getToken(TamarinParser.KeywordRestriction, 0); }
		public TerminalNode KeywordAxiom() { return getToken(TamarinParser.KeywordAxiom, 0); }
		public TerminalNode KeywordAny() { return getToken(TamarinParser.KeywordAny, 0); }
		public TerminalNode KeywordTest() { return getToken(TamarinParser.KeywordTest, 0); }
		public TerminalNode KeywordNoPrecomp() { return getToken(TamarinParser.KeywordNoPrecomp, 0); }
		public TerminalNode KeywordThen() { return getToken(TamarinParser.KeywordThen, 0); }
		public TerminalNode KeywordOut() { return getToken(TamarinParser.KeywordOut, 0); }
		public TerminalNode KeywordFresh() { return getToken(TamarinParser.KeywordFresh, 0); }
		public TerminalNode KeywordPub() { return getToken(TamarinParser.KeywordPub, 0); }
		public TerminalNode KeywordXor() { return getToken(TamarinParser.KeywordXor, 0); }
		public TerminalNode KeywordOutput() { return getToken(TamarinParser.KeywordOutput, 0); }
		public TerminalNode KeywordAccountsFor() { return getToken(TamarinParser.KeywordAccountsFor, 0); }
		public TerminalNode KeywordModulo() { return getToken(TamarinParser.KeywordModulo, 0); }
		public TerminalNode KeywordTheory() { return getToken(TamarinParser.KeywordTheory, 0); }
		public TerminalNode KeywordBegin() { return getToken(TamarinParser.KeywordBegin, 0); }
		public TerminalNode KeywordEnd() { return getToken(TamarinParser.KeywordEnd, 0); }
		public TerminalNode KeywordVariants() { return getToken(TamarinParser.KeywordVariants, 0); }
		public TerminalNode KeywordRestrict() { return getToken(TamarinParser.KeywordRestrict, 0); }
		public TerminalNode KeywordHeuristic() { return getToken(TamarinParser.KeywordHeuristic, 0); }
		public TerminalNode KeywordExport() { return getToken(TamarinParser.KeywordExport, 0); }
		public TerminalNode KeywordPredicate() { return getToken(TamarinParser.KeywordPredicate, 0); }
		public TerminalNode KeywordOptions() { return getToken(TamarinParser.KeywordOptions, 0); }
		public TerminalNode KeywordEquations() { return getToken(TamarinParser.KeywordEquations, 0); }
		public TerminalNode KeywordFunctions() { return getToken(TamarinParser.KeywordFunctions, 0); }
		public TerminalNode KeywordBuiltins() { return getToken(TamarinParser.KeywordBuiltins, 0); }
		public TerminalNode KeywordCase() { return getToken(TamarinParser.KeywordCase, 0); }
		public TerminalNode KeywordQED() { return getToken(TamarinParser.KeywordQED, 0); }
		public TerminalNode KeywordNext() { return getToken(TamarinParser.KeywordNext, 0); }
		public TerminalNode KeywordSplitEqs() { return getToken(TamarinParser.KeywordSplitEqs, 0); }
		public TerminalNode KeywordEquivLemma() { return getToken(TamarinParser.KeywordEquivLemma, 0); }
		public TerminalNode KeywordDiffEquivLemma() { return getToken(TamarinParser.KeywordDiffEquivLemma, 0); }
		public TerminalNode RuleFresh() { return getToken(TamarinParser.RuleFresh, 0); }
		public TerminalNode RuleIrecv() { return getToken(TamarinParser.RuleIrecv, 0); }
		public TerminalNode RuleIsend() { return getToken(TamarinParser.RuleIsend, 0); }
		public TerminalNode RuleCoerce() { return getToken(TamarinParser.RuleCoerce, 0); }
		public TerminalNode RuleIequality() { return getToken(TamarinParser.RuleIequality, 0); }
		public TerminalNode SuffixMsg() { return getToken(TamarinParser.SuffixMsg, 0); }
		public TerminalNode SuffixNode() { return getToken(TamarinParser.SuffixNode, 0); }
		public TerminalNode DHneutral() { return getToken(TamarinParser.DHneutral, 0); }
		public TerminalNode BuiltinMun() { return getToken(TamarinParser.BuiltinMun, 0); }
		public TerminalNode BuiltinOne() { return getToken(TamarinParser.BuiltinOne, 0); }
		public TerminalNode BuiltinExp() { return getToken(TamarinParser.BuiltinExp, 0); }
		public TerminalNode BuiltinMult() { return getToken(TamarinParser.BuiltinMult, 0); }
		public TerminalNode BuiltinInv() { return getToken(TamarinParser.BuiltinInv, 0); }
		public TerminalNode BuiltinPmult() { return getToken(TamarinParser.BuiltinPmult, 0); }
		public TerminalNode BuiltinEm() { return getToken(TamarinParser.BuiltinEm, 0); }
		public TerminalNode BuiltinZero() { return getToken(TamarinParser.BuiltinZero, 0); }
		public TerminalNode SolvedProof() { return getToken(TamarinParser.SolvedProof, 0); }
		public TerminalNode ProofMethodSorry() { return getToken(TamarinParser.ProofMethodSorry, 0); }
		public TerminalNode ProofMethodSimplify() { return getToken(TamarinParser.ProofMethodSimplify, 0); }
		public TerminalNode ProofMethodSolve() { return getToken(TamarinParser.ProofMethodSolve, 0); }
		public TerminalNode ProofMethodContradiction() { return getToken(TamarinParser.ProofMethodContradiction, 0); }
		public TerminalNode ProofMethodInduction() { return getToken(TamarinParser.ProofMethodInduction, 0); }
		public TerminalNode BuiltinOptions() { return getToken(TamarinParser.BuiltinOptions, 0); }
		public TerminalNode BuiltinNameLocationsReport() { return getToken(TamarinParser.BuiltinNameLocationsReport, 0); }
		public TerminalNode BuiltinNameReliableChannel() { return getToken(TamarinParser.BuiltinNameReliableChannel, 0); }
		public TerminalNode BuiltinNameDH() { return getToken(TamarinParser.BuiltinNameDH, 0); }
		public TerminalNode BuiltinNameBilinearPairing() { return getToken(TamarinParser.BuiltinNameBilinearPairing, 0); }
		public TerminalNode BuiltinNameMultiset() { return getToken(TamarinParser.BuiltinNameMultiset, 0); }
		public TerminalNode BuiltinNameSymmetricEncryption() { return getToken(TamarinParser.BuiltinNameSymmetricEncryption, 0); }
		public TerminalNode BuiltinNameAsymmetricEncryption() { return getToken(TamarinParser.BuiltinNameAsymmetricEncryption, 0); }
		public TerminalNode BuiltinNameSigning() { return getToken(TamarinParser.BuiltinNameSigning, 0); }
		public TerminalNode BuiltinNameDestPairing() { return getToken(TamarinParser.BuiltinNameDestPairing, 0); }
		public TerminalNode BuiltinNameDestSymmetricEncryption() { return getToken(TamarinParser.BuiltinNameDestSymmetricEncryption, 0); }
		public TerminalNode BuiltinNameDestAsymmetricEncryption() { return getToken(TamarinParser.BuiltinNameDestAsymmetricEncryption, 0); }
		public TerminalNode BuiltinNameDestSigning() { return getToken(TamarinParser.BuiltinNameDestSigning, 0); }
		public TerminalNode BuiltinNameRevealingSigning() { return getToken(TamarinParser.BuiltinNameRevealingSigning, 0); }
		public TerminalNode BuiltinNameHashing() { return getToken(TamarinParser.BuiltinNameHashing, 0); }
		public TerminalNode TacticFunctionName() { return getToken(TamarinParser.TacticFunctionName, 0); }
		public TerminalNode SapicActionNew() { return getToken(TamarinParser.SapicActionNew, 0); }
		public TerminalNode SapicActionInsert() { return getToken(TamarinParser.SapicActionInsert, 0); }
		public TerminalNode SapicActionDelete() { return getToken(TamarinParser.SapicActionDelete, 0); }
		public TerminalNode SapicActionLock() { return getToken(TamarinParser.SapicActionLock, 0); }
		public TerminalNode SapicActionUnlock() { return getToken(TamarinParser.SapicActionUnlock, 0); }
		public TerminalNode SapicActionEvent() { return getToken(TamarinParser.SapicActionEvent, 0); }
		public TerminalNode TOp() { return getToken(TamarinParser.TOp, 0); }
		public TerminalNode FOp() { return getToken(TamarinParser.FOp, 0); }
		public TerminalNode Left() { return getToken(TamarinParser.Left, 0); }
		public TerminalNode Right() { return getToken(TamarinParser.Right, 0); }
		public TerminalNode Both() { return getToken(TamarinParser.Both, 0); }
		public TerminalNode ForallTraces() { return getToken(TamarinParser.ForallTraces, 0); }
		public TerminalNode ExistsTrace() { return getToken(TamarinParser.ExistsTrace, 0); }
		public TerminalNode OutFactIdent() { return getToken(TamarinParser.OutFactIdent, 0); }
		public TerminalNode InFactIdent() { return getToken(TamarinParser.InFactIdent, 0); }
		public TerminalNode KuFactIdent() { return getToken(TamarinParser.KuFactIdent, 0); }
		public TerminalNode KdFactIdent() { return getToken(TamarinParser.KdFactIdent, 0); }
		public TerminalNode DedLogFactIdent() { return getToken(TamarinParser.DedLogFactIdent, 0); }
		public TerminalNode FreshFactIdent() { return getToken(TamarinParser.FreshFactIdent, 0); }
		public TerminalNode RuleAttributeColor() { return getToken(TamarinParser.RuleAttributeColor, 0); }
		public TerminalNode LemmaAttributeTyping() { return getToken(TamarinParser.LemmaAttributeTyping, 0); }
		public TerminalNode LemmaAttributeSources() { return getToken(TamarinParser.LemmaAttributeSources, 0); }
		public TerminalNode LemmaAttributeReuse() { return getToken(TamarinParser.LemmaAttributeReuse, 0); }
		public TerminalNode LemmaAttributeDiffReuse() { return getToken(TamarinParser.LemmaAttributeDiffReuse, 0); }
		public TerminalNode LemmaAttributeInduction() { return getToken(TamarinParser.LemmaAttributeInduction, 0); }
		public TerminalNode LemmaAttributeHideLemma() { return getToken(TamarinParser.LemmaAttributeHideLemma, 0); }
		public TerminalNode FunctionAttribute() { return getToken(TamarinParser.FunctionAttribute, 0); }
		public AllowedIdentifierKeywordsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_allowedIdentifierKeywords; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).enterAllowedIdentifierKeywords(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TamarinParserListener ) ((TamarinParserListener)listener).exitAllowedIdentifierKeywords(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TamarinParserVisitor ) return ((TamarinParserVisitor<? extends T>)visitor).visitAllowedIdentifierKeywords(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AllowedIdentifierKeywordsContext allowedIdentifierKeywords() throws RecognitionException {
		AllowedIdentifierKeywordsContext _localctx = new AllowedIdentifierKeywordsContext(_ctx, getState());
		enterRule(_localctx, 400, RULE_allowedIdentifierKeywords);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1863);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & -16L) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & 34359738367L) != 0) || ((((_la - 146)) & ~0x3f) == 0 && ((1L << (_la - 146)) & 268431475L) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 167:
			return literal_sempred((LiteralContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean literal_sempred(LiteralContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return _localctx.lit.contains("vlit");
		case 1:
			return _localctx.lit.contains("logVar");
		case 2:
			return _localctx.lit.contains("msgVar");
		case 3:
			return _localctx.lit.contains("nodeVar");
		case 4:
			return _localctx.lit.contains("logicalLiteral") && !_localctx.lit.contains("logicalLiteralNoPub");
		case 5:
			return _localctx.lit.contains("logicalLiteralNoPub");
		case 6:
			return _localctx.lit.contains("sapicVar");
		case 7:
			return _localctx.lit.contains("sapicNodeVar");
		case 8:
			return _localctx.lit.contains("logicalTypedLiteral");
		case 9:
			return _localctx.lit.contains("sapicPatternVar");
		case 10:
			return _localctx.lit.contains("logicalTypedPatternLiteral");
		}
		return true;
	}

	public static final String _serializedATN =
		"\u0004\u0001\u00b9\u074a\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001"+
		"\u0002\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004"+
		"\u0002\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007"+
		"\u0002\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b\u0007\u000b"+
		"\u0002\f\u0007\f\u0002\r\u0007\r\u0002\u000e\u0007\u000e\u0002\u000f\u0007"+
		"\u000f\u0002\u0010\u0007\u0010\u0002\u0011\u0007\u0011\u0002\u0012\u0007"+
		"\u0012\u0002\u0013\u0007\u0013\u0002\u0014\u0007\u0014\u0002\u0015\u0007"+
		"\u0015\u0002\u0016\u0007\u0016\u0002\u0017\u0007\u0017\u0002\u0018\u0007"+
		"\u0018\u0002\u0019\u0007\u0019\u0002\u001a\u0007\u001a\u0002\u001b\u0007"+
		"\u001b\u0002\u001c\u0007\u001c\u0002\u001d\u0007\u001d\u0002\u001e\u0007"+
		"\u001e\u0002\u001f\u0007\u001f\u0002 \u0007 \u0002!\u0007!\u0002\"\u0007"+
		"\"\u0002#\u0007#\u0002$\u0007$\u0002%\u0007%\u0002&\u0007&\u0002\'\u0007"+
		"\'\u0002(\u0007(\u0002)\u0007)\u0002*\u0007*\u0002+\u0007+\u0002,\u0007"+
		",\u0002-\u0007-\u0002.\u0007.\u0002/\u0007/\u00020\u00070\u00021\u0007"+
		"1\u00022\u00072\u00023\u00073\u00024\u00074\u00025\u00075\u00026\u0007"+
		"6\u00027\u00077\u00028\u00078\u00029\u00079\u0002:\u0007:\u0002;\u0007"+
		";\u0002<\u0007<\u0002=\u0007=\u0002>\u0007>\u0002?\u0007?\u0002@\u0007"+
		"@\u0002A\u0007A\u0002B\u0007B\u0002C\u0007C\u0002D\u0007D\u0002E\u0007"+
		"E\u0002F\u0007F\u0002G\u0007G\u0002H\u0007H\u0002I\u0007I\u0002J\u0007"+
		"J\u0002K\u0007K\u0002L\u0007L\u0002M\u0007M\u0002N\u0007N\u0002O\u0007"+
		"O\u0002P\u0007P\u0002Q\u0007Q\u0002R\u0007R\u0002S\u0007S\u0002T\u0007"+
		"T\u0002U\u0007U\u0002V\u0007V\u0002W\u0007W\u0002X\u0007X\u0002Y\u0007"+
		"Y\u0002Z\u0007Z\u0002[\u0007[\u0002\\\u0007\\\u0002]\u0007]\u0002^\u0007"+
		"^\u0002_\u0007_\u0002`\u0007`\u0002a\u0007a\u0002b\u0007b\u0002c\u0007"+
		"c\u0002d\u0007d\u0002e\u0007e\u0002f\u0007f\u0002g\u0007g\u0002h\u0007"+
		"h\u0002i\u0007i\u0002j\u0007j\u0002k\u0007k\u0002l\u0007l\u0002m\u0007"+
		"m\u0002n\u0007n\u0002o\u0007o\u0002p\u0007p\u0002q\u0007q\u0002r\u0007"+
		"r\u0002s\u0007s\u0002t\u0007t\u0002u\u0007u\u0002v\u0007v\u0002w\u0007"+
		"w\u0002x\u0007x\u0002y\u0007y\u0002z\u0007z\u0002{\u0007{\u0002|\u0007"+
		"|\u0002}\u0007}\u0002~\u0007~\u0002\u007f\u0007\u007f\u0002\u0080\u0007"+
		"\u0080\u0002\u0081\u0007\u0081\u0002\u0082\u0007\u0082\u0002\u0083\u0007"+
		"\u0083\u0002\u0084\u0007\u0084\u0002\u0085\u0007\u0085\u0002\u0086\u0007"+
		"\u0086\u0002\u0087\u0007\u0087\u0002\u0088\u0007\u0088\u0002\u0089\u0007"+
		"\u0089\u0002\u008a\u0007\u008a\u0002\u008b\u0007\u008b\u0002\u008c\u0007"+
		"\u008c\u0002\u008d\u0007\u008d\u0002\u008e\u0007\u008e\u0002\u008f\u0007"+
		"\u008f\u0002\u0090\u0007\u0090\u0002\u0091\u0007\u0091\u0002\u0092\u0007"+
		"\u0092\u0002\u0093\u0007\u0093\u0002\u0094\u0007\u0094\u0002\u0095\u0007"+
		"\u0095\u0002\u0096\u0007\u0096\u0002\u0097\u0007\u0097\u0002\u0098\u0007"+
		"\u0098\u0002\u0099\u0007\u0099\u0002\u009a\u0007\u009a\u0002\u009b\u0007"+
		"\u009b\u0002\u009c\u0007\u009c\u0002\u009d\u0007\u009d\u0002\u009e\u0007"+
		"\u009e\u0002\u009f\u0007\u009f\u0002\u00a0\u0007\u00a0\u0002\u00a1\u0007"+
		"\u00a1\u0002\u00a2\u0007\u00a2\u0002\u00a3\u0007\u00a3\u0002\u00a4\u0007"+
		"\u00a4\u0002\u00a5\u0007\u00a5\u0002\u00a6\u0007\u00a6\u0002\u00a7\u0007"+
		"\u00a7\u0002\u00a8\u0007\u00a8\u0002\u00a9\u0007\u00a9\u0002\u00aa\u0007"+
		"\u00aa\u0002\u00ab\u0007\u00ab\u0002\u00ac\u0007\u00ac\u0002\u00ad\u0007"+
		"\u00ad\u0002\u00ae\u0007\u00ae\u0002\u00af\u0007\u00af\u0002\u00b0\u0007"+
		"\u00b0\u0002\u00b1\u0007\u00b1\u0002\u00b2\u0007\u00b2\u0002\u00b3\u0007"+
		"\u00b3\u0002\u00b4\u0007\u00b4\u0002\u00b5\u0007\u00b5\u0002\u00b6\u0007"+
		"\u00b6\u0002\u00b7\u0007\u00b7\u0002\u00b8\u0007\u00b8\u0002\u00b9\u0007"+
		"\u00b9\u0002\u00ba\u0007\u00ba\u0002\u00bb\u0007\u00bb\u0002\u00bc\u0007"+
		"\u00bc\u0002\u00bd\u0007\u00bd\u0002\u00be\u0007\u00be\u0002\u00bf\u0007"+
		"\u00bf\u0002\u00c0\u0007\u00c0\u0002\u00c1\u0007\u00c1\u0002\u00c2\u0007"+
		"\u00c2\u0002\u00c3\u0007\u00c3\u0002\u00c4\u0007\u00c4\u0002\u00c5\u0007"+
		"\u00c5\u0002\u00c6\u0007\u00c6\u0002\u00c7\u0007\u00c7\u0002\u00c8\u0007"+
		"\u00c8\u0001\u0000\u0001\u0000\u0001\u0001\u0001\u0001\u0001\u0001\u0001"+
		"\u0001\u0005\u0001\u0199\b\u0001\n\u0001\f\u0001\u019c\t\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0003\u0002\u01b7\b\u0002\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0005\u0001\u0005\u0001\u0005\u0005\u0005"+
		"\u01c2\b\u0005\n\u0005\f\u0005\u01c5\t\u0005\u0001\u0005\u0001\u0005\u0005"+
		"\u0005\u01c9\b\u0005\n\u0005\f\u0005\u01cc\t\u0005\u0003\u0005\u01ce\b"+
		"\u0005\u0001\u0005\u0001\u0005\u0001\u0006\u0001\u0006\u0001\u0006\u0001"+
		"\u0006\u0005\u0006\u01d6\b\u0006\n\u0006\f\u0006\u01d9\t\u0006\u0001\u0007"+
		"\u0001\u0007\u0001\u0007\u0001\u0007\u0005\u0007\u01df\b\u0007\n\u0007"+
		"\f\u0007\u01e2\t\u0007\u0001\b\u0003\b\u01e5\b\b\u0001\b\u0001\b\u0001"+
		"\t\u0001\t\u0001\n\u0001\n\u0001\n\u0005\n\u01ee\b\n\n\n\f\n\u01f1\t\n"+
		"\u0001\u000b\u0001\u000b\u0001\u000b\u0005\u000b\u01f6\b\u000b\n\u000b"+
		"\f\u000b\u01f9\t\u000b\u0001\f\u0001\f\u0001\f\u0005\f\u01fe\b\f\n\f\f"+
		"\f\u0201\t\f\u0001\f\u0003\f\u0204\b\f\u0001\r\u0001\r\u0001\r\u0005\r"+
		"\u0209\b\r\n\r\f\r\u020c\t\r\u0001\u000e\u0001\u000e\u0001\u000e\u0005"+
		"\u000e\u0211\b\u000e\n\u000e\f\u000e\u0214\t\u000e\u0001\u000f\u0001\u000f"+
		"\u0001\u000f\u0005\u000f\u0219\b\u000f\n\u000f\f\u000f\u021c\t\u000f\u0001"+
		"\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0001"+
		"\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0003"+
		"\u0010\u022a\b\u0010\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001"+
		"\u0011\u0003\u0011\u0231\b\u0011\u0001\u0012\u0001\u0012\u0001\u0012\u0001"+
		"\u0012\u0001\u0012\u0001\u0013\u0001\u0013\u0001\u0013\u0003\u0013\u023b"+
		"\b\u0013\u0001\u0013\u0001\u0013\u0001\u0014\u0001\u0014\u0001\u0014\u0001"+
		"\u0014\u0001\u0014\u0001\u0014\u0001\u0015\u0001\u0015\u0001\u0015\u0003"+
		"\u0015\u0248\b\u0015\u0001\u0016\u0001\u0016\u0001\u0016\u0001\u0016\u0001"+
		"\u0016\u0001\u0016\u0001\u0016\u0001\u0016\u0001\u0016\u0003\u0016\u0253"+
		"\b\u0016\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0003\u0017\u0259"+
		"\b\u0017\u0001\u0017\u0003\u0017\u025c\b\u0017\u0001\u0017\u0001\u0017"+
		"\u0001\u0018\u0001\u0018\u0001\u0018\u0001\u0018\u0001\u0018\u0001\u0018"+
		"\u0001\u0019\u0001\u0019\u0003\u0019\u0268\b\u0019\u0001\u001a\u0001\u001a"+
		"\u0003\u001a\u026c\b\u001a\u0001\u001a\u0001\u001a\u0001\u001b\u0001\u001b"+
		"\u0001\u001b\u0001\u001b\u0003\u001b\u0274\b\u001b\u0001\u001b\u0001\u001b"+
		"\u0001\u001c\u0001\u001c\u0003\u001c\u027a\b\u001c\u0001\u001c\u0001\u001c"+
		"\u0001\u001c\u0001\u001c\u0001\u001c\u0005\u001c\u0281\b\u001c\n\u001c"+
		"\f\u001c\u0284\t\u001c\u0001\u001c\u0003\u001c\u0287\b\u001c\u0003\u001c"+
		"\u0289\b\u001c\u0001\u001d\u0001\u001d\u0001\u001d\u0001\u001d\u0003\u001d"+
		"\u028f\b\u001d\u0001\u001d\u0001\u001d\u0001\u001e\u0001\u001e\u0001\u001e"+
		"\u0001\u001e\u0005\u001e\u0297\b\u001e\n\u001e\f\u001e\u029a\t\u001e\u0001"+
		"\u001e\u0003\u001e\u029d\b\u001e\u0003\u001e\u029f\b\u001e\u0001\u001e"+
		"\u0001\u001e\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f"+
		"\u0001\u001f\u0001\u001f\u0005\u001f\u02aa\b\u001f\n\u001f\f\u001f\u02ad"+
		"\t\u001f\u0001\u001f\u0003\u001f\u02b0\b\u001f\u0001 \u0001 \u0001 \u0001"+
		" \u0001 \u0001!\u0001!\u0003!\u02b9\b!\u0001\"\u0001\"\u0001\"\u0005\""+
		"\u02be\b\"\n\"\f\"\u02c1\t\"\u0001\"\u0003\"\u02c4\b\"\u0001#\u0001#\u0001"+
		"#\u0001#\u0001#\u0001#\u0003#\u02cc\b#\u0001$\u0001$\u0001%\u0001%\u0001"+
		"&\u0001&\u0003&\u02d4\b&\u0001&\u0001&\u0001&\u0003&\u02d9\b&\u0001&\u0001"+
		"&\u0003&\u02dd\b&\u0001&\u0001&\u0001&\u0003&\u02e2\b&\u0003&\u02e4\b"+
		"&\u0001\'\u0001\'\u0001\'\u0001\'\u0003\'\u02ea\b\'\u0001(\u0001(\u0001"+
		"(\u0001(\u0001(\u0001)\u0001)\u0001)\u0001)\u0005)\u02f5\b)\n)\f)\u02f8"+
		"\t)\u0001)\u0003)\u02fb\b)\u0003)\u02fd\b)\u0001)\u0001)\u0001*\u0001"+
		"*\u0001*\u0003*\u0304\b*\u0001+\u0001+\u0001+\u0001+\u0001+\u0001+\u0001"+
		",\u0003,\u030d\b,\u0001,\u0001,\u0001,\u0001,\u0001,\u0001,\u0001-\u0003"+
		"-\u0316\b-\u0001-\u0001-\u0001-\u0003-\u031b\b-\u0001-\u0001-\u0001-\u0001"+
		".\u0001.\u0001/\u0001/\u0001/\u0001/\u0005/\u0326\b/\n/\f/\u0329\t/\u0001"+
		"/\u0003/\u032c\b/\u0003/\u032e\b/\u0001/\u0003/\u0331\b/\u00010\u0001"+
		"0\u00011\u00011\u00011\u00011\u00051\u0339\b1\n1\f1\u033c\t1\u00011\u0003"+
		"1\u033f\b1\u00031\u0341\b1\u00011\u00011\u00012\u00012\u00012\u00012\u0001"+
		"2\u00012\u00012\u00012\u00012\u00012\u00012\u00012\u00012\u00012\u0001"+
		"2\u00012\u00012\u00052\u0356\b2\n2\f2\u0359\t2\u00012\u00032\u035c\b2"+
		"\u00032\u035e\b2\u00012\u00012\u00012\u00032\u0363\b2\u00013\u00013\u0003"+
		"3\u0367\b3\u00014\u00014\u00015\u00015\u00016\u00016\u00016\u00016\u0003"+
		"6\u0371\b6\u00016\u00016\u00036\u0375\b6\u00016\u00016\u00016\u00016\u0003"+
		"6\u037b\b6\u00017\u00017\u00017\u00017\u00037\u0381\b7\u00018\u00018\u0001"+
		"8\u00058\u0386\b8\n8\f8\u0389\t8\u00019\u00019\u00019\u00019\u00059\u038f"+
		"\b9\n9\f9\u0392\t9\u0001:\u0001:\u0001:\u0001:\u0005:\u0398\b:\n:\f:\u039b"+
		"\t:\u0001;\u0003;\u039e\b;\u0001;\u0001;\u0001<\u0001<\u0003<\u03a4\b"+
		"<\u0001<\u0001<\u0003<\u03a8\b<\u0001<\u0001<\u0001<\u0001<\u0001<\u0001"+
		"<\u0003<\u03b0\b<\u0001=\u0001=\u0001=\u0001=\u0001=\u0001=\u0001=\u0001"+
		"=\u0001=\u0001=\u0001=\u0001=\u0001=\u0001=\u0001=\u0001=\u0001=\u0001"+
		"=\u0001=\u0001=\u0001=\u0001=\u0001=\u0003=\u03c9\b=\u0001>\u0001>\u0003"+
		">\u03cd\b>\u0001>\u0001>\u0001>\u0001>\u0001?\u0001?\u0001?\u0001?\u0001"+
		"@\u0001@\u0001@\u0001@\u0001@\u0001@\u0001@\u0001A\u0001A\u0001A\u0003"+
		"A\u03e1\bA\u0001A\u0001A\u0001A\u0001A\u0005A\u03e7\bA\nA\fA\u03ea\tA"+
		"\u0001A\u0003A\u03ed\bA\u0001A\u0001A\u0001A\u0001A\u0001A\u0001B\u0001"+
		"B\u0003B\u03f6\bB\u0001B\u0005B\u03f9\bB\nB\fB\u03fc\tB\u0001B\u0005B"+
		"\u03ff\bB\nB\fB\u0402\tB\u0001C\u0001C\u0001C\u0001C\u0001C\u0001C\u0003"+
		"C\u040a\bC\u0001C\u0004C\u040d\bC\u000bC\fC\u040e\u0001D\u0001D\u0001"+
		"D\u0001D\u0001D\u0001D\u0003D\u0417\bD\u0001D\u0004D\u041a\bD\u000bD\f"+
		"D\u041b\u0001E\u0001E\u0001E\u0001E\u0005E\u0422\bE\nE\fE\u0425\tE\u0001"+
		"F\u0001F\u0001F\u0005F\u042a\bF\nF\fF\u042d\tF\u0001G\u0003G\u0430\bG"+
		"\u0001G\u0001G\u0001H\u0001H\u0001H\u0001H\u0001H\u0004H\u0439\bH\u000b"+
		"H\fH\u043a\u0001I\u0001I\u0001I\u0001I\u0001J\u0001J\u0001K\u0001K\u0001"+
		"L\u0001L\u0001M\u0001M\u0001M\u0001M\u0001N\u0001N\u0001N\u0004N\u044e"+
		"\bN\u000bN\fN\u044f\u0001O\u0001O\u0001O\u0003O\u0455\bO\u0001P\u0001"+
		"P\u0001Q\u0001Q\u0001Q\u0001Q\u0001R\u0001R\u0001R\u0001R\u0001R\u0003"+
		"R\u0462\bR\u0001S\u0001S\u0001S\u0001S\u0001S\u0001S\u0001S\u0001T\u0001"+
		"T\u0001T\u0001T\u0001T\u0005T\u0470\bT\nT\fT\u0473\tT\u0001T\u0003T\u0476"+
		"\bT\u0001U\u0001U\u0001U\u0001U\u0001V\u0001V\u0001V\u0001V\u0001V\u0005"+
		"V\u0481\bV\nV\fV\u0484\tV\u0001V\u0003V\u0487\bV\u0001W\u0001W\u0001X"+
		"\u0001X\u0001X\u0001X\u0001X\u0005X\u0490\bX\nX\fX\u0493\tX\u0001X\u0003"+
		"X\u0496\bX\u0001Y\u0001Y\u0001Y\u0001Y\u0001Z\u0001Z\u0001Z\u0001Z\u0001"+
		"Z\u0005Z\u04a1\bZ\nZ\fZ\u04a4\tZ\u0001Z\u0003Z\u04a7\bZ\u0001[\u0001["+
		"\u0001[\u0001[\u0001[\u0001[\u0005[\u04af\b[\n[\f[\u04b2\t[\u0001[\u0003"+
		"[\u04b5\b[\u0003[\u04b7\b[\u0001[\u0003[\u04ba\b[\u0001\\\u0001\\\u0001"+
		"]\u0001]\u0001]\u0001]\u0001]\u0001]\u0005]\u04c4\b]\n]\f]\u04c7\t]\u0001"+
		"]\u0003]\u04ca\b]\u0003]\u04cc\b]\u0001]\u0001]\u0001]\u0003]\u04d1\b"+
		"]\u0001^\u0001^\u0001^\u0001^\u0001^\u0005^\u04d8\b^\n^\f^\u04db\t^\u0001"+
		"^\u0003^\u04de\b^\u0001_\u0001_\u0001_\u0001_\u0001_\u0001_\u0001_\u0001"+
		"_\u0001_\u0001_\u0001_\u0001_\u0001_\u0001_\u0001_\u0003_\u04ef\b_\u0001"+
		"`\u0001`\u0001a\u0001a\u0001b\u0001b\u0001c\u0001c\u0001d\u0001d\u0001"+
		"e\u0001e\u0001f\u0001f\u0001g\u0001g\u0001h\u0001h\u0001i\u0001i\u0001"+
		"j\u0001j\u0001k\u0001k\u0001l\u0001l\u0001m\u0001m\u0001n\u0001n\u0001"+
		"o\u0001o\u0001o\u0003o\u0512\bo\u0001p\u0001p\u0003p\u0516\bp\u0001q\u0001"+
		"q\u0001r\u0001r\u0001r\u0001s\u0001s\u0001s\u0001s\u0005s\u0521\bs\ns"+
		"\fs\u0524\ts\u0001s\u0001s\u0001s\u0003s\u0529\bs\u0001t\u0001t\u0001"+
		"t\u0001t\u0001u\u0001u\u0001u\u0001u\u0001u\u0001u\u0001u\u0001u\u0001"+
		"u\u0003u\u0538\bu\u0001v\u0001v\u0001v\u0001v\u0001v\u0003v\u053f\bv\u0001"+
		"w\u0001w\u0001w\u0001w\u0001x\u0001x\u0001x\u0001x\u0001x\u0001y\u0001"+
		"y\u0001y\u0001y\u0001z\u0001z\u0001z\u0005z\u0551\bz\nz\fz\u0554\tz\u0001"+
		"{\u0001{\u0001{\u0001{\u0001{\u0001|\u0001|\u0001|\u0001|\u0001|\u0001"+
		"|\u0001}\u0001}\u0001}\u0001}\u0001}\u0001}\u0001~\u0001~\u0001~\u0001"+
		"~\u0001\u007f\u0001\u007f\u0001\u007f\u0001\u007f\u0001\u007f\u0001\u0080"+
		"\u0001\u0080\u0001\u0080\u0005\u0080\u0573\b\u0080\n\u0080\f\u0080\u0576"+
		"\t\u0080\u0001\u0081\u0001\u0081\u0001\u0081\u0001\u0082\u0001\u0082\u0001"+
		"\u0082\u0001\u0082\u0001\u0083\u0001\u0083\u0001\u0083\u0001\u0083\u0001"+
		"\u0083\u0001\u0083\u0001\u0083\u0001\u0083\u0001\u0083\u0003\u0083\u0588"+
		"\b\u0083\u0001\u0083\u0001\u0083\u0001\u0083\u0001\u0083\u0001\u0083\u0001"+
		"\u0083\u0003\u0083\u0590\b\u0083\u0001\u0083\u0001\u0083\u0001\u0083\u0003"+
		"\u0083\u0595\b\u0083\u0001\u0083\u0001\u0083\u0001\u0083\u0001\u0083\u0001"+
		"\u0083\u0004\u0083\u059c\b\u0083\u000b\u0083\f\u0083\u059d\u0001\u0083"+
		"\u0001\u0083\u0001\u0083\u0003\u0083\u05a3\b\u0083\u0001\u0083\u0001\u0083"+
		"\u0001\u0083\u0001\u0083\u0003\u0083\u05a9\b\u0083\u0001\u0083\u0001\u0083"+
		"\u0001\u0083\u0001\u0083\u0001\u0083\u0003\u0083\u05b0\b\u0083\u0001\u0083"+
		"\u0001\u0083\u0001\u0083\u0001\u0083\u0001\u0083\u0005\u0083\u05b7\b\u0083"+
		"\n\u0083\f\u0083\u05ba\t\u0083\u0001\u0083\u0003\u0083\u05bd\b\u0083\u0003"+
		"\u0083\u05bf\b\u0083\u0001\u0083\u0003\u0083\u05c2\b\u0083\u0003\u0083"+
		"\u05c4\b\u0083\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084"+
		"\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084"+
		"\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084"+
		"\u0003\u0084\u05d7\b\u0084\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084"+
		"\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084"+
		"\u0003\u0084\u05e3\b\u0084\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084"+
		"\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084\u0001\u0084\u0003\u0084"+
		"\u05ee\b\u0084\u0001\u0085\u0001\u0085\u0001\u0086\u0001\u0086\u0001\u0087"+
		"\u0001\u0087\u0001\u0087\u0001\u0087\u0001\u0087\u0001\u0087\u0005\u0087"+
		"\u05fa\b\u0087\n\u0087\f\u0087\u05fd\t\u0087\u0001\u0087\u0003\u0087\u0600"+
		"\b\u0087\u0003\u0087\u0602\b\u0087\u0001\u0087\u0003\u0087\u0605\b\u0087"+
		"\u0001\u0087\u0001\u0087\u0001\u0087\u0001\u0088\u0001\u0088\u0001\u0089"+
		"\u0001\u0089\u0001\u008a\u0001\u008a\u0001\u008a\u0003\u008a\u0611\b\u008a"+
		"\u0001\u008b\u0001\u008b\u0001\u008b\u0003\u008b\u0616\b\u008b\u0001\u008c"+
		"\u0001\u008c\u0001\u008d\u0003\u008d\u061b\b\u008d\u0001\u008d\u0001\u008d"+
		"\u0001\u008e\u0001\u008e\u0001\u008e\u0003\u008e\u0622\b\u008e\u0001\u008f"+
		"\u0001\u008f\u0003\u008f\u0626\b\u008f\u0001\u0090\u0001\u0090\u0001\u0090"+
		"\u0001\u0090\u0001\u0090\u0001\u0090\u0001\u0090\u0001\u0091\u0001\u0091"+
		"\u0001\u0091\u0001\u0091\u0005\u0091\u0633\b\u0091\n\u0091\f\u0091\u0636"+
		"\t\u0091\u0001\u0091\u0003\u0091\u0639\b\u0091\u0003\u0091\u063b\b\u0091"+
		"\u0001\u0091\u0001\u0091\u0001\u0092\u0001\u0092\u0001\u0093\u0001\u0093"+
		"\u0001\u0093\u0001\u0093\u0001\u0093\u0001\u0093\u0001\u0093\u0001\u0094"+
		"\u0001\u0094\u0004\u0094\u064a\b\u0094\u000b\u0094\f\u0094\u064b\u0001"+
		"\u0094\u0001\u0094\u0001\u0095\u0001\u0095\u0001\u0095\u0001\u0095\u0001"+
		"\u0096\u0001\u0096\u0001\u0097\u0001\u0097\u0001\u0097\u0001\u0097\u0001"+
		"\u0097\u0003\u0097\u065b\b\u0097\u0001\u0098\u0001\u0098\u0001\u0098\u0001"+
		"\u0098\u0001\u0098\u0001\u0098\u0001\u0098\u0003\u0098\u0664\b\u0098\u0001"+
		"\u0099\u0001\u0099\u0001\u009a\u0001\u009a\u0001\u009a\u0003\u009a\u066b"+
		"\b\u009a\u0001\u009b\u0001\u009b\u0001\u009b\u0001\u009b\u0001\u009c\u0001"+
		"\u009c\u0001\u009d\u0001\u009d\u0001\u009d\u0003\u009d\u0676\b\u009d\u0001"+
		"\u009e\u0001\u009e\u0001\u009e\u0001\u009e\u0001\u009e\u0001\u009e\u0003"+
		"\u009e\u067e\b\u009e\u0001\u009f\u0001\u009f\u0001\u009f\u0001\u009f\u0001"+
		"\u009f\u0001\u009f\u0003\u009f\u0686\b\u009f\u0001\u00a0\u0001\u00a0\u0001"+
		"\u00a0\u0001\u00a0\u0001\u00a0\u0001\u00a0\u0003\u00a0\u068e\b\u00a0\u0001"+
		"\u00a1\u0001\u00a1\u0001\u00a1\u0001\u00a1\u0003\u00a1\u0694\b\u00a1\u0001"+
		"\u00a2\u0001\u00a2\u0001\u00a2\u0003\u00a2\u0699\b\u00a2\u0001\u00a3\u0001"+
		"\u00a3\u0001\u00a3\u0003\u00a3\u069e\b\u00a3\u0001\u00a4\u0001\u00a4\u0001"+
		"\u00a4\u0003\u00a4\u06a3\b\u00a4\u0001\u00a5\u0001\u00a5\u0003\u00a5\u06a7"+
		"\b\u00a5\u0001\u00a6\u0004\u00a6\u06aa\b\u00a6\u000b\u00a6\f\u00a6\u06ab"+
		"\u0001\u00a7\u0001\u00a7\u0001\u00a7\u0003\u00a7\u06b1\b\u00a7\u0001\u00a7"+
		"\u0001\u00a7\u0001\u00a7\u0001\u00a7\u0001\u00a7\u0001\u00a7\u0001\u00a7"+
		"\u0001\u00a7\u0001\u00a7\u0001\u00a7\u0001\u00a7\u0001\u00a7\u0001\u00a7"+
		"\u0001\u00a7\u0001\u00a7\u0001\u00a7\u0001\u00a7\u0001\u00a7\u0001\u00a7"+
		"\u0001\u00a7\u0003\u00a7\u06c7\b\u00a7\u0001\u00a8\u0001\u00a8\u0001\u00a8"+
		"\u0001\u00a8\u0001\u00a8\u0003\u00a8\u06ce\b\u00a8\u0001\u00a8\u0003\u00a8"+
		"\u06d1\b\u00a8\u0001\u00a9\u0001\u00a9\u0001\u00a9\u0001\u00a9\u0001\u00a9"+
		"\u0001\u00aa\u0001\u00aa\u0001\u00aa\u0001\u00aa\u0001\u00ab\u0001\u00ab"+
		"\u0001\u00ab\u0001\u00ac\u0001\u00ac\u0001\u00ac\u0001\u00ac\u0001\u00ac"+
		"\u0001\u00ac\u0001\u00ac\u0001\u00ac\u0005\u00ac\u06e7\b\u00ac\n\u00ac"+
		"\f\u00ac\u06ea\t\u00ac\u0001\u00ad\u0001\u00ad\u0001\u00ad\u0001\u00ad"+
		"\u0001\u00ad\u0001\u00ad\u0004\u00ad\u06f2\b\u00ad\u000b\u00ad\f\u00ad"+
		"\u06f3\u0001\u00ae\u0001\u00ae\u0001\u00ae\u0001\u00ae\u0001\u00ae\u0001"+
		"\u00ae\u0001\u00ae\u0001\u00ae\u0004\u00ae\u06fe\b\u00ae\u000b\u00ae\f"+
		"\u00ae\u06ff\u0001\u00af\u0001\u00af\u0001\u00af\u0001\u00af\u0001\u00af"+
		"\u0001\u00af\u0001\u00af\u0005\u00af\u0709\b\u00af\n\u00af\f\u00af\u070c"+
		"\t\u00af\u0001\u00b0\u0001\u00b0\u0001\u00b0\u0001\u00b0\u0001\u00b0\u0001"+
		"\u00b0\u0001\u00b0\u0005\u00b0\u0715\b\u00b0\n\u00b0\f\u00b0\u0718\t\u00b0"+
		"\u0001\u00b1\u0001\u00b1\u0001\u00b2\u0001\u00b2\u0001\u00b3\u0001\u00b3"+
		"\u0001\u00b4\u0001\u00b4\u0001\u00b5\u0001\u00b5\u0001\u00b6\u0001\u00b6"+
		"\u0001\u00b7\u0001\u00b7\u0001\u00b8\u0001\u00b8\u0001\u00b9\u0001\u00b9"+
		"\u0001\u00ba\u0001\u00ba\u0001\u00bb\u0001\u00bb\u0001\u00bc\u0001\u00bc"+
		"\u0001\u00bd\u0001\u00bd\u0001\u00be\u0001\u00be\u0001\u00bf\u0001\u00bf"+
		"\u0001\u00c0\u0001\u00c0\u0001\u00c1\u0001\u00c1\u0001\u00c2\u0001\u00c2"+
		"\u0001\u00c3\u0001\u00c3\u0001\u00c4\u0001\u00c4\u0001\u00c5\u0001\u00c5"+
		"\u0001\u00c6\u0001\u00c6\u0001\u00c7\u0001\u00c7\u0001\u00c8\u0001\u00c8"+
		"\u0001\u00c8\u0001\u02ab\u0000\u00c9\u0000\u0002\u0004\u0006\b\n\f\u000e"+
		"\u0010\u0012\u0014\u0016\u0018\u001a\u001c\u001e \"$&(*,.02468:<>@BDF"+
		"HJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080\u0082\u0084\u0086\u0088\u008a\u008c"+
		"\u008e\u0090\u0092\u0094\u0096\u0098\u009a\u009c\u009e\u00a0\u00a2\u00a4"+
		"\u00a6\u00a8\u00aa\u00ac\u00ae\u00b0\u00b2\u00b4\u00b6\u00b8\u00ba\u00bc"+
		"\u00be\u00c0\u00c2\u00c4\u00c6\u00c8\u00ca\u00cc\u00ce\u00d0\u00d2\u00d4"+
		"\u00d6\u00d8\u00da\u00dc\u00de\u00e0\u00e2\u00e4\u00e6\u00e8\u00ea\u00ec"+
		"\u00ee\u00f0\u00f2\u00f4\u00f6\u00f8\u00fa\u00fc\u00fe\u0100\u0102\u0104"+
		"\u0106\u0108\u010a\u010c\u010e\u0110\u0112\u0114\u0116\u0118\u011a\u011c"+
		"\u011e\u0120\u0122\u0124\u0126\u0128\u012a\u012c\u012e\u0130\u0132\u0134"+
		"\u0136\u0138\u013a\u013c\u013e\u0140\u0142\u0144\u0146\u0148\u014a\u014c"+
		"\u014e\u0150\u0152\u0154\u0156\u0158\u015a\u015c\u015e\u0160\u0162\u0164"+
		"\u0166\u0168\u016a\u016c\u016e\u0170\u0172\u0174\u0176\u0178\u017a\u017c"+
		"\u017e\u0180\u0182\u0184\u0186\u0188\u018a\u018c\u018e\u0190\u0000\r\u0002"+
		"\u0000\u001a\u001a44\u0002\u0000\u0017\u0017wx\u0001\u0000\u0010\u0011"+
		"\u0002\u0000;;==\u0002\u0000stww\u0001\u0000\u0096\u0098\u0002\u0000~"+
		"\u007f\u00ae\u00ae\u0002\u0000\u0001\u0002\u0004\u0004\u0001\u0000\u008a"+
		"\u008b\u0001\u0000rs\u0001\u0000r\u0095\u0002\u0000cikq\u0004\u0000\u0004"+
		"b\u0092\u0093\u0096\u0098\u009e\u00ad\u07bf\u0000\u0192\u0001\u0000\u0000"+
		"\u0000\u0002\u0194\u0001\u0000\u0000\u0000\u0004\u01b6\u0001\u0000\u0000"+
		"\u0000\u0006\u01b8\u0001\u0000\u0000\u0000\b\u01bb\u0001\u0000\u0000\u0000"+
		"\n\u01be\u0001\u0000\u0000\u0000\f\u01d1\u0001\u0000\u0000\u0000\u000e"+
		"\u01da\u0001\u0000\u0000\u0000\u0010\u01e4\u0001\u0000\u0000\u0000\u0012"+
		"\u01e8\u0001\u0000\u0000\u0000\u0014\u01ea\u0001\u0000\u0000\u0000\u0016"+
		"\u01f2\u0001\u0000\u0000\u0000\u0018\u01fa\u0001\u0000\u0000\u0000\u001a"+
		"\u0205\u0001\u0000\u0000\u0000\u001c\u020d\u0001\u0000\u0000\u0000\u001e"+
		"\u0215\u0001\u0000\u0000\u0000 \u0229\u0001\u0000\u0000\u0000\"\u0230"+
		"\u0001\u0000\u0000\u0000$\u0232\u0001\u0000\u0000\u0000&\u0237\u0001\u0000"+
		"\u0000\u0000(\u023e\u0001\u0000\u0000\u0000*\u0247\u0001\u0000\u0000\u0000"+
		",\u0252\u0001\u0000\u0000\u0000.\u0254\u0001\u0000\u0000\u00000\u025f"+
		"\u0001\u0000\u0000\u00002\u0265\u0001\u0000\u0000\u00004\u0269\u0001\u0000"+
		"\u0000\u00006\u026f\u0001\u0000\u0000\u00008\u0277\u0001\u0000\u0000\u0000"+
		":\u028a\u0001\u0000\u0000\u0000<\u0292\u0001\u0000\u0000\u0000>\u02af"+
		"\u0001\u0000\u0000\u0000@\u02b1\u0001\u0000\u0000\u0000B\u02b8\u0001\u0000"+
		"\u0000\u0000D\u02ba\u0001\u0000\u0000\u0000F\u02cb\u0001\u0000\u0000\u0000"+
		"H\u02cd\u0001\u0000\u0000\u0000J\u02cf\u0001\u0000\u0000\u0000L\u02e3"+
		"\u0001\u0000\u0000\u0000N\u02e9\u0001\u0000\u0000\u0000P\u02eb\u0001\u0000"+
		"\u0000\u0000R\u02f0\u0001\u0000\u0000\u0000T\u0303\u0001\u0000\u0000\u0000"+
		"V\u0305\u0001\u0000\u0000\u0000X\u030c\u0001\u0000\u0000\u0000Z\u0315"+
		"\u0001\u0000\u0000\u0000\\\u031f\u0001\u0000\u0000\u0000^\u0330\u0001"+
		"\u0000\u0000\u0000`\u0332\u0001\u0000\u0000\u0000b\u0334\u0001\u0000\u0000"+
		"\u0000d\u0362\u0001\u0000\u0000\u0000f\u0366\u0001\u0000\u0000\u0000h"+
		"\u0368\u0001\u0000\u0000\u0000j\u036a\u0001\u0000\u0000\u0000l\u036c\u0001"+
		"\u0000\u0000\u0000n\u037c\u0001\u0000\u0000\u0000p\u0382\u0001\u0000\u0000"+
		"\u0000r\u038a\u0001\u0000\u0000\u0000t\u0393\u0001\u0000\u0000\u0000v"+
		"\u039d\u0001\u0000\u0000\u0000x\u03af\u0001\u0000\u0000\u0000z\u03c8\u0001"+
		"\u0000\u0000\u0000|\u03cc\u0001\u0000\u0000\u0000~\u03d2\u0001\u0000\u0000"+
		"\u0000\u0080\u03d6\u0001\u0000\u0000\u0000\u0082\u03dd\u0001\u0000\u0000"+
		"\u0000\u0084\u03f3\u0001\u0000\u0000\u0000\u0086\u0403\u0001\u0000\u0000"+
		"\u0000\u0088\u0410\u0001\u0000\u0000\u0000\u008a\u041d\u0001\u0000\u0000"+
		"\u0000\u008c\u0426\u0001\u0000\u0000\u0000\u008e\u042f\u0001\u0000\u0000"+
		"\u0000\u0090\u0433\u0001\u0000\u0000\u0000\u0092\u043c\u0001\u0000\u0000"+
		"\u0000\u0094\u0440\u0001\u0000\u0000\u0000\u0096\u0442\u0001\u0000\u0000"+
		"\u0000\u0098\u0444\u0001\u0000\u0000\u0000\u009a\u0446\u0001\u0000\u0000"+
		"\u0000\u009c\u044a\u0001\u0000\u0000\u0000\u009e\u0454\u0001\u0000\u0000"+
		"\u0000\u00a0\u0456\u0001\u0000\u0000\u0000\u00a2\u0458\u0001\u0000\u0000"+
		"\u0000\u00a4\u045c\u0001\u0000\u0000\u0000\u00a6\u0463\u0001\u0000\u0000"+
		"\u0000\u00a8\u046a\u0001\u0000\u0000\u0000\u00aa\u0477\u0001\u0000\u0000"+
		"\u0000\u00ac\u047b\u0001\u0000\u0000\u0000\u00ae\u0488\u0001\u0000\u0000"+
		"\u0000\u00b0\u048a\u0001\u0000\u0000\u0000\u00b2\u0497\u0001\u0000\u0000"+
		"\u0000\u00b4\u049b\u0001\u0000\u0000\u0000\u00b6\u04a8\u0001\u0000\u0000"+
		"\u0000\u00b8\u04bb\u0001\u0000\u0000\u0000\u00ba\u04d0\u0001\u0000\u0000"+
		"\u0000\u00bc\u04d2\u0001\u0000\u0000\u0000\u00be\u04ee\u0001\u0000\u0000"+
		"\u0000\u00c0\u04f0\u0001\u0000\u0000\u0000\u00c2\u04f2\u0001\u0000\u0000"+
		"\u0000\u00c4\u04f4\u0001\u0000\u0000\u0000\u00c6\u04f6\u0001\u0000\u0000"+
		"\u0000\u00c8\u04f8\u0001\u0000\u0000\u0000\u00ca\u04fa\u0001\u0000\u0000"+
		"\u0000\u00cc\u04fc\u0001\u0000\u0000\u0000\u00ce\u04fe\u0001\u0000\u0000"+
		"\u0000\u00d0\u0500\u0001\u0000\u0000\u0000\u00d2\u0502\u0001\u0000\u0000"+
		"\u0000\u00d4\u0504\u0001\u0000\u0000\u0000\u00d6\u0506\u0001\u0000\u0000"+
		"\u0000\u00d8\u0508\u0001\u0000\u0000\u0000\u00da\u050a\u0001\u0000\u0000"+
		"\u0000\u00dc\u050c\u0001\u0000\u0000\u0000\u00de\u0511\u0001\u0000\u0000"+
		"\u0000\u00e0\u0515\u0001\u0000\u0000\u0000\u00e2\u0517\u0001\u0000\u0000"+
		"\u0000\u00e4\u0519\u0001\u0000\u0000\u0000\u00e6\u051c\u0001\u0000\u0000"+
		"\u0000\u00e8\u052a\u0001\u0000\u0000\u0000\u00ea\u0537\u0001\u0000\u0000"+
		"\u0000\u00ec\u053e\u0001\u0000\u0000\u0000\u00ee\u0540\u0001\u0000\u0000"+
		"\u0000\u00f0\u0544\u0001\u0000\u0000\u0000\u00f2\u0549\u0001\u0000\u0000"+
		"\u0000\u00f4\u054d\u0001\u0000\u0000\u0000\u00f6\u0555\u0001\u0000\u0000"+
		"\u0000\u00f8\u055a\u0001\u0000\u0000\u0000\u00fa\u0560\u0001\u0000\u0000"+
		"\u0000\u00fc\u0566\u0001\u0000\u0000\u0000\u00fe\u056a\u0001\u0000\u0000"+
		"\u0000\u0100\u056f\u0001\u0000\u0000\u0000\u0102\u0577\u0001\u0000\u0000"+
		"\u0000\u0104\u057a\u0001\u0000\u0000\u0000\u0106\u05c3\u0001\u0000\u0000"+
		"\u0000\u0108\u05ed\u0001\u0000\u0000\u0000\u010a\u05ef\u0001\u0000\u0000"+
		"\u0000\u010c\u05f1\u0001\u0000\u0000\u0000\u010e\u05f3\u0001\u0000\u0000"+
		"\u0000\u0110\u0609\u0001\u0000\u0000\u0000\u0112\u060b\u0001\u0000\u0000"+
		"\u0000\u0114\u0610\u0001\u0000\u0000\u0000\u0116\u0615\u0001\u0000\u0000"+
		"\u0000\u0118\u0617\u0001\u0000\u0000\u0000\u011a\u061a\u0001\u0000\u0000"+
		"\u0000\u011c\u061e\u0001\u0000\u0000\u0000\u011e\u0625\u0001\u0000\u0000"+
		"\u0000\u0120\u0627\u0001\u0000\u0000\u0000\u0122\u062e\u0001\u0000\u0000"+
		"\u0000\u0124\u063e\u0001\u0000\u0000\u0000\u0126\u0640\u0001\u0000\u0000"+
		"\u0000\u0128\u0647\u0001\u0000\u0000\u0000\u012a\u064f\u0001\u0000\u0000"+
		"\u0000\u012c\u0653\u0001\u0000\u0000\u0000\u012e\u065a\u0001\u0000\u0000"+
		"\u0000\u0130\u0663\u0001\u0000\u0000\u0000\u0132\u0665\u0001\u0000\u0000"+
		"\u0000\u0134\u0667\u0001\u0000\u0000\u0000\u0136\u066c\u0001\u0000\u0000"+
		"\u0000\u0138\u0670\u0001\u0000\u0000\u0000\u013a\u0672\u0001\u0000\u0000"+
		"\u0000\u013c\u067d\u0001\u0000\u0000\u0000\u013e\u0685\u0001\u0000\u0000"+
		"\u0000\u0140\u068d\u0001\u0000\u0000\u0000\u0142\u0693\u0001\u0000\u0000"+
		"\u0000\u0144\u0698\u0001\u0000\u0000\u0000\u0146\u069d\u0001\u0000\u0000"+
		"\u0000\u0148\u06a2\u0001\u0000\u0000\u0000\u014a\u06a6\u0001\u0000\u0000"+
		"\u0000\u014c\u06a9\u0001\u0000\u0000\u0000\u014e\u06c6\u0001\u0000\u0000"+
		"\u0000\u0150\u06d0\u0001\u0000\u0000\u0000\u0152\u06d2\u0001\u0000\u0000"+
		"\u0000\u0154\u06d7\u0001\u0000\u0000\u0000\u0156\u06db\u0001\u0000\u0000"+
		"\u0000\u0158\u06e8\u0001\u0000\u0000\u0000\u015a\u06f1\u0001\u0000\u0000"+
		"\u0000\u015c\u06fd\u0001\u0000\u0000\u0000\u015e\u070a\u0001\u0000\u0000"+
		"\u0000\u0160\u0716\u0001\u0000\u0000\u0000\u0162\u0719\u0001\u0000\u0000"+
		"\u0000\u0164\u071b\u0001\u0000\u0000\u0000\u0166\u071d\u0001\u0000\u0000"+
		"\u0000\u0168\u071f\u0001\u0000\u0000\u0000\u016a\u0721\u0001\u0000\u0000"+
		"\u0000\u016c\u0723\u0001\u0000\u0000\u0000\u016e\u0725\u0001\u0000\u0000"+
		"\u0000\u0170\u0727\u0001\u0000\u0000\u0000\u0172\u0729\u0001\u0000\u0000"+
		"\u0000\u0174\u072b\u0001\u0000\u0000\u0000\u0176\u072d\u0001\u0000\u0000"+
		"\u0000\u0178\u072f\u0001\u0000\u0000\u0000\u017a\u0731\u0001\u0000\u0000"+
		"\u0000\u017c\u0733\u0001\u0000\u0000\u0000\u017e\u0735\u0001\u0000\u0000"+
		"\u0000\u0180\u0737\u0001\u0000\u0000\u0000\u0182\u0739\u0001\u0000\u0000"+
		"\u0000\u0184\u073b\u0001\u0000\u0000\u0000\u0186\u073d\u0001\u0000\u0000"+
		"\u0000\u0188\u073f\u0001\u0000\u0000\u0000\u018a\u0741\u0001\u0000\u0000"+
		"\u0000\u018c\u0743\u0001\u0000\u0000\u0000\u018e\u0745\u0001\u0000\u0000"+
		"\u0000\u0190\u0747\u0001\u0000\u0000\u0000\u0192\u0193\u0003\u0002\u0001"+
		"\u0000\u0193\u0001\u0001\u0000\u0000\u0000\u0194\u0195\u0005 \u0000\u0000"+
		"\u0195\u0196\u0003\u0130\u0098\u0000\u0196\u019a\u0005!\u0000\u0000\u0197"+
		"\u0199\u0003\u0004\u0002\u0000\u0198\u0197\u0001\u0000\u0000\u0000\u0199"+
		"\u019c\u0001\u0000\u0000\u0000\u019a\u0198\u0001\u0000\u0000\u0000\u019a"+
		"\u019b\u0001\u0000\u0000\u0000\u019b\u019d\u0001\u0000\u0000\u0000\u019c"+
		"\u019a\u0001\u0000\u0000\u0000\u019d\u019e\u0005\"\u0000\u0000\u019e\u0003"+
		"\u0001\u0000\u0000\u0000\u019f\u01b7\u0003\u009cN\u0000\u01a0\u01b7\u0003"+
		"\u0084B\u0000\u01a1\u01b7\u0003\u00bc^\u0000\u01a2\u01b7\u0003\u00acV"+
		"\u0000\u01a3\u01b7\u0003\u00b4Z\u0000\u01a4\u01b7\u0003\u00b0X\u0000\u01a5"+
		"\u01b7\u0003\u0120\u0090\u0000\u01a6\u01b7\u0003\u0126\u0093\u0000\u01a7"+
		"\u01b7\u0003\u0080@\u0000\u01a8\u01b7\u0003\u0082A\u0000\u01a9\u01b7\u0003"+
		"l6\u0000\u01aa\u01b7\u00038\u001c\u0000\u01ab\u01b7\u00030\u0018\u0000"+
		"\u01ac\u01b7\u0003\u0104\u0082\u0000\u01ad\u01b7\u0003\u010e\u0087\u0000"+
		"\u01ae\u01b7\u0003\u00fe\u007f\u0000\u01af\u01b7\u0003\u00fc~\u0000\u01b0"+
		"\u01b7\u0003\u00a8T\u0000\u01b1\u01b7\u0003\u00a6S\u0000\u01b2\u01b7\u0003"+
		"\n\u0005\u0000\u01b3\u01b7\u0003\u0006\u0003\u0000\u01b4\u01b7\u0003\b"+
		"\u0004\u0000\u01b5\u01b7\u0003\u0156\u00ab\u0000\u01b6\u019f\u0001\u0000"+
		"\u0000\u0000\u01b6\u01a0\u0001\u0000\u0000\u0000\u01b6\u01a1\u0001\u0000"+
		"\u0000\u0000\u01b6\u01a2\u0001\u0000\u0000\u0000\u01b6\u01a3\u0001\u0000"+
		"\u0000\u0000\u01b6\u01a4\u0001\u0000\u0000\u0000\u01b6\u01a5\u0001\u0000"+
		"\u0000\u0000\u01b6\u01a6\u0001\u0000\u0000\u0000\u01b6\u01a7\u0001\u0000"+
		"\u0000\u0000\u01b6\u01a8\u0001\u0000\u0000\u0000\u01b6\u01a9\u0001\u0000"+
		"\u0000\u0000\u01b6\u01aa\u0001\u0000\u0000\u0000\u01b6\u01ab\u0001\u0000"+
		"\u0000\u0000\u01b6\u01ac\u0001\u0000\u0000\u0000\u01b6\u01ad\u0001\u0000"+
		"\u0000\u0000\u01b6\u01ae\u0001\u0000\u0000\u0000\u01b6\u01af\u0001\u0000"+
		"\u0000\u0000\u01b6\u01b0\u0001\u0000\u0000\u0000\u01b6\u01b1\u0001\u0000"+
		"\u0000\u0000\u01b6\u01b2\u0001\u0000\u0000\u0000\u01b6\u01b3\u0001\u0000"+
		"\u0000\u0000\u01b6\u01b4\u0001\u0000\u0000\u0000\u01b6\u01b5\u0001\u0000"+
		"\u0000\u0000\u01b7\u0005\u0001\u0000\u0000\u0000\u01b8\u01b9\u0005\u0099"+
		"\u0000\u0000\u01b9\u01ba\u0003\u0130\u0098\u0000\u01ba\u0007\u0001\u0000"+
		"\u0000\u0000\u01bb\u01bc\u0005\u009a\u0000\u0000\u01bc\u01bd\u0003\u0136"+
		"\u009b\u0000\u01bd\t\u0001\u0000\u0000\u0000\u01be\u01bf\u0005\u009b\u0000"+
		"\u0000\u01bf\u01c3\u0003\f\u0006\u0000\u01c0\u01c2\u0003\u0004\u0002\u0000"+
		"\u01c1\u01c0\u0001\u0000\u0000\u0000\u01c2\u01c5\u0001\u0000\u0000\u0000"+
		"\u01c3\u01c1\u0001\u0000\u0000\u0000\u01c3\u01c4\u0001\u0000\u0000\u0000"+
		"\u01c4\u01cd\u0001\u0000\u0000\u0000\u01c5\u01c3\u0001\u0000\u0000\u0000"+
		"\u01c6\u01ca\u0005\u009c\u0000\u0000\u01c7\u01c9\u0003\u0004\u0002\u0000"+
		"\u01c8\u01c7\u0001\u0000\u0000\u0000\u01c9\u01cc\u0001\u0000\u0000\u0000"+
		"\u01ca\u01c8\u0001\u0000\u0000\u0000\u01ca\u01cb\u0001\u0000\u0000\u0000"+
		"\u01cb\u01ce\u0001\u0000\u0000\u0000\u01cc\u01ca\u0001\u0000\u0000\u0000"+
		"\u01cd\u01c6\u0001\u0000\u0000\u0000\u01cd\u01ce\u0001\u0000\u0000\u0000"+
		"\u01ce\u01cf\u0001\u0000\u0000\u0000\u01cf\u01d0\u0005\u009d\u0000\u0000"+
		"\u01d0\u000b\u0001\u0000\u0000\u0000\u01d1\u01d7\u0003\u000e\u0007\u0000"+
		"\u01d2\u01d3\u0003\u0164\u00b2\u0000\u01d3\u01d4\u0003\u000e\u0007\u0000"+
		"\u01d4\u01d6\u0001\u0000\u0000\u0000\u01d5\u01d2\u0001\u0000\u0000\u0000"+
		"\u01d6\u01d9\u0001\u0000\u0000\u0000\u01d7\u01d5\u0001\u0000\u0000\u0000"+
		"\u01d7\u01d8\u0001\u0000\u0000\u0000\u01d8\r\u0001\u0000\u0000\u0000\u01d9"+
		"\u01d7\u0001\u0000\u0000\u0000\u01da\u01e0\u0003\u0010\b\u0000\u01db\u01dc"+
		"\u0003\u0168\u00b4\u0000\u01dc\u01dd\u0003\u0010\b\u0000\u01dd\u01df\u0001"+
		"\u0000\u0000\u0000\u01de\u01db\u0001\u0000\u0000\u0000\u01df\u01e2\u0001"+
		"\u0000\u0000\u0000\u01e0\u01de\u0001\u0000\u0000\u0000\u01e0\u01e1\u0001"+
		"\u0000\u0000\u0000\u01e1\u000f\u0001\u0000\u0000\u0000\u01e2\u01e0\u0001"+
		"\u0000\u0000\u0000\u01e3\u01e5\u0003\u0166\u00b3\u0000\u01e4\u01e3\u0001"+
		"\u0000\u0000\u0000\u01e4\u01e5\u0001\u0000\u0000\u0000\u01e5\u01e6\u0001"+
		"\u0000\u0000\u0000\u01e6\u01e7\u0003\u0012\t\u0000\u01e7\u0011\u0001\u0000"+
		"\u0000\u0000\u01e8\u01e9\u0003\u0130\u0098\u0000\u01e9\u0013\u0001\u0000"+
		"\u0000\u0000\u01ea\u01ef\u0003\u0016\u000b\u0000\u01eb\u01ec\u0005e\u0000"+
		"\u0000\u01ec\u01ee\u0003\u0016\u000b\u0000\u01ed\u01eb\u0001\u0000\u0000"+
		"\u0000\u01ee\u01f1\u0001\u0000\u0000\u0000\u01ef\u01ed\u0001\u0000\u0000"+
		"\u0000\u01ef\u01f0\u0001\u0000\u0000\u0000\u01f0\u0015\u0001\u0000\u0000"+
		"\u0000\u01f1\u01ef\u0001\u0000\u0000\u0000\u01f2\u01f7\u0003\u001a\r\u0000"+
		"\u01f3\u01f4\u0005w\u0000\u0000\u01f4\u01f6\u0003\u001a\r\u0000\u01f5"+
		"\u01f3\u0001\u0000\u0000\u0000\u01f6\u01f9\u0001\u0000\u0000\u0000\u01f7"+
		"\u01f5\u0001\u0000\u0000\u0000\u01f7\u01f8\u0001\u0000\u0000\u0000\u01f8"+
		"\u0017\u0001\u0000\u0000\u0000\u01f9\u01f7\u0001\u0000\u0000\u0000\u01fa"+
		"\u01ff\u0003\u0016\u000b\u0000\u01fb\u01fc\u0005e\u0000\u0000\u01fc\u01fe"+
		"\u0003\u0016\u000b\u0000\u01fd\u01fb\u0001\u0000\u0000\u0000\u01fe\u0201"+
		"\u0001\u0000\u0000\u0000\u01ff\u01fd\u0001\u0000\u0000\u0000\u01ff\u0200"+
		"\u0001\u0000\u0000\u0000\u0200\u0203\u0001\u0000\u0000\u0000\u0201\u01ff"+
		"\u0001\u0000\u0000\u0000\u0202\u0204\u0005e\u0000\u0000\u0203\u0202\u0001"+
		"\u0000\u0000\u0000\u0203\u0204\u0001\u0000\u0000\u0000\u0204\u0019\u0001"+
		"\u0000\u0000\u0000\u0205\u020a\u0003\u001c\u000e\u0000\u0206\u0207\u0005"+
		"y\u0000\u0000\u0207\u0209\u0003\u001c\u000e\u0000\u0208\u0206\u0001\u0000"+
		"\u0000\u0000\u0209\u020c\u0001\u0000\u0000\u0000\u020a\u0208\u0001\u0000"+
		"\u0000\u0000\u020a\u020b\u0001\u0000\u0000\u0000\u020b\u001b\u0001\u0000"+
		"\u0000\u0000\u020c\u020a\u0001\u0000\u0000\u0000\u020d\u0212\u0003\u001e"+
		"\u000f\u0000\u020e\u020f\u0005z\u0000\u0000\u020f\u0211\u0003\u001e\u000f"+
		"\u0000\u0210\u020e\u0001\u0000\u0000\u0000\u0211\u0214\u0001\u0000\u0000"+
		"\u0000\u0212\u0210\u0001\u0000\u0000\u0000\u0212\u0213\u0001\u0000\u0000"+
		"\u0000\u0213\u001d\u0001\u0000\u0000\u0000\u0214\u0212\u0001\u0000\u0000"+
		"\u0000\u0215\u021a\u0003 \u0010\u0000\u0216\u0217\u0005{\u0000\u0000\u0217"+
		"\u0219\u0003 \u0010\u0000\u0218\u0216\u0001\u0000\u0000\u0000\u0219\u021c"+
		"\u0001\u0000\u0000\u0000\u021a\u0218\u0001\u0000\u0000\u0000\u021a\u021b"+
		"\u0001\u0000\u0000\u0000\u021b\u001f\u0001\u0000\u0000\u0000\u021c\u021a"+
		"\u0001\u0000\u0000\u0000\u021d\u021e\u0005\u0084\u0000\u0000\u021e\u021f"+
		"\u0003\u0014\n\u0000\u021f\u0220\u0005\u0086\u0000\u0000\u0220\u022a\u0001"+
		"\u0000\u0000\u0000\u0221\u0222\u0005k\u0000\u0000\u0222\u0223\u0003\u0016"+
		"\u000b\u0000\u0223\u0224\u0005l\u0000\u0000\u0224\u022a\u0001\u0000\u0000"+
		"\u0000\u0225\u022a\u0005~\u0000\u0000\u0226\u022a\u0003\u018a\u00c5\u0000"+
		"\u0227\u022a\u0003\"\u0011\u0000\u0228\u022a\u0003\u014e\u00a7\u0000\u0229"+
		"\u021d\u0001\u0000\u0000\u0000\u0229\u0221\u0001\u0000\u0000\u0000\u0229"+
		"\u0225\u0001\u0000\u0000\u0000\u0229\u0226\u0001\u0000\u0000\u0000\u0229"+
		"\u0227\u0001\u0000\u0000\u0000\u0229\u0228\u0001\u0000\u0000\u0000\u022a"+
		"!\u0001\u0000\u0000\u0000\u022b\u0231\u0001\u0000\u0000\u0000\u022c\u0231"+
		"\u0003$\u0012\u0000\u022d\u0231\u0003&\u0013\u0000\u022e\u0231\u0003("+
		"\u0014\u0000\u022f\u0231\u0003*\u0015\u0000\u0230\u022b\u0001\u0000\u0000"+
		"\u0000\u0230\u022c\u0001\u0000\u0000\u0000\u0230\u022d\u0001\u0000\u0000"+
		"\u0000\u0230\u022e\u0001\u0000\u0000\u0000\u0230\u022f\u0001\u0000\u0000"+
		"\u0000\u0231#\u0001\u0000\u0000\u0000\u0232\u0233\u0003\u0130\u0098\u0000"+
		"\u0233\u0234\u0005k\u0000\u0000\u0234\u0235\u0003\u0014\n\u0000\u0235"+
		"\u0236\u0005l\u0000\u0000\u0236%\u0001\u0000\u0000\u0000\u0237\u0238\u0003"+
		"\u0130\u0098\u0000\u0238\u023a\u0005k\u0000\u0000\u0239\u023b\u0003\u0018"+
		"\f\u0000\u023a\u0239\u0001\u0000\u0000\u0000\u023a\u023b\u0001\u0000\u0000"+
		"\u0000\u023b\u023c\u0001\u0000\u0000\u0000\u023c\u023d\u0005l\u0000\u0000"+
		"\u023d\'\u0001\u0000\u0000\u0000\u023e\u023f\u0003\u0130\u0098\u0000\u023f"+
		"\u0240\u0005m\u0000\u0000\u0240\u0241\u0003\u0014\n\u0000\u0241\u0242"+
		"\u0005n\u0000\u0000\u0242\u0243\u0003 \u0010\u0000\u0243)\u0001\u0000"+
		"\u0000\u0000\u0244\u0248\u0003\u017c\u00be\u0000\u0245\u0248\u0003\u018a"+
		"\u00c5\u0000\u0246\u0248\u0003\u0188\u00c4\u0000\u0247\u0244\u0001\u0000"+
		"\u0000\u0000\u0247\u0245\u0001\u0000\u0000\u0000\u0247\u0246\u0001\u0000"+
		"\u0000\u0000\u0248+\u0001\u0000\u0000\u0000\u0249\u0253\u0003\u017a\u00bd"+
		"\u0000\u024a\u0253\u0003\u017c\u00be\u0000\u024b\u0253\u0003\u017e\u00bf"+
		"\u0000\u024c\u0253\u0003\u0180\u00c0\u0000\u024d\u0253\u0003\u0182\u00c1"+
		"\u0000\u024e\u0253\u0003\u0184\u00c2\u0000\u024f\u0253\u0003\u0186\u00c3"+
		"\u0000\u0250\u0253\u0003\u0188\u00c4\u0000\u0251\u0253\u0003\u0178\u00bc"+
		"\u0000\u0252\u0249\u0001\u0000\u0000\u0000\u0252\u024a\u0001\u0000\u0000"+
		"\u0000\u0252\u024b\u0001\u0000\u0000\u0000\u0252\u024c\u0001\u0000\u0000"+
		"\u0000\u0252\u024d\u0001\u0000\u0000\u0000\u0252\u024e\u0001\u0000\u0000"+
		"\u0000\u0252\u024f\u0001\u0000\u0000\u0000\u0252\u0250\u0001\u0000\u0000"+
		"\u0000\u0252\u0251\u0001\u0000\u0000\u0000\u0253-\u0001\u0000\u0000\u0000"+
		"\u0254\u025b\u0003R)\u0000\u0255\u025c\u0005\u0091\u0000\u0000\u0256\u0258"+
		"\u0005\u008f\u0000\u0000\u0257\u0259\u0003D\"\u0000\u0258\u0257\u0001"+
		"\u0000\u0000\u0000\u0258\u0259\u0001\u0000\u0000\u0000\u0259\u025a\u0001"+
		"\u0000\u0000\u0000\u025a\u025c\u0005\u0090\u0000\u0000\u025b\u0255\u0001"+
		"\u0000\u0000\u0000\u025b\u0256\u0001\u0000\u0000\u0000\u025c\u025d\u0001"+
		"\u0000\u0000\u0000\u025d\u025e\u0003R)\u0000\u025e/\u0001\u0000\u0000"+
		"\u0000\u025f\u0260\u0005\u0003\u0000\u0000\u0260\u0261\u0003P(\u0000\u0261"+
		"\u0262\u00032\u0019\u0000\u0262\u0263\u0005c\u0000\u0000\u0263\u0264\u0003"+
		".\u0017\u0000\u02641\u0001\u0000\u0000\u0000\u0265\u0267\u0003\u0130\u0098"+
		"\u0000\u0266\u0268\u0003\u0132\u0099\u0000\u0267\u0266\u0001\u0000\u0000"+
		"\u0000\u0267\u0268\u0001\u0000\u0000\u0000\u02683\u0001\u0000\u0000\u0000"+
		"\u0269\u026b\u00036\u001b\u0000\u026a\u026c\u0003\u0128\u0094\u0000\u026b"+
		"\u026a\u0001\u0000\u0000\u0000\u026b\u026c\u0001\u0000\u0000\u0000\u026c"+
		"\u026d\u0001\u0000\u0000\u0000\u026d\u026e\u0003.\u0017\u0000\u026e5\u0001"+
		"\u0000\u0000\u0000\u026f\u0270\u0005\u0003\u0000\u0000\u0270\u0271\u0003"+
		"P(\u0000\u0271\u0273\u0003\u0130\u0098\u0000\u0272\u0274\u0003<\u001e"+
		"\u0000\u0273\u0272\u0001\u0000\u0000\u0000\u0273\u0274\u0001\u0000\u0000"+
		"\u0000\u0274\u0275\u0001\u0000\u0000\u0000\u0275\u0276\u0005c\u0000\u0000"+
		"\u02767\u0001\u0000\u0000\u0000\u0277\u0279\u0003:\u001d\u0000\u0278\u027a"+
		"\u0003\u0128\u0094\u0000\u0279\u0278\u0001\u0000\u0000\u0000\u0279\u027a"+
		"\u0001\u0000\u0000\u0000\u027a\u027b\u0001\u0000\u0000\u0000\u027b\u0288"+
		"\u0003.\u0017\u0000\u027c\u027d\u0005#\u0000\u0000\u027d\u0282\u00034"+
		"\u001a\u0000\u027e\u027f\u0005e\u0000\u0000\u027f\u0281\u00034\u001a\u0000"+
		"\u0280\u027e\u0001\u0000\u0000\u0000\u0281\u0284\u0001\u0000\u0000\u0000"+
		"\u0282\u0280\u0001\u0000\u0000\u0000\u0282\u0283\u0001\u0000\u0000\u0000"+
		"\u0283\u0286\u0001\u0000\u0000\u0000\u0284\u0282\u0001\u0000\u0000\u0000"+
		"\u0285\u0287\u0005e\u0000\u0000\u0286\u0285\u0001\u0000\u0000\u0000\u0286"+
		"\u0287\u0001\u0000\u0000\u0000\u0287\u0289\u0001\u0000\u0000\u0000\u0288"+
		"\u027c\u0001\u0000\u0000\u0000\u0288\u0289\u0001\u0000\u0000\u0000\u0289"+
		"9\u0001\u0000\u0000\u0000\u028a\u028b\u0005\u0003\u0000\u0000\u028b\u028c"+
		"\u0003N\'\u0000\u028c\u028e\u0003\u0130\u0098\u0000\u028d\u028f\u0003"+
		"<\u001e\u0000\u028e\u028d\u0001\u0000\u0000\u0000\u028e\u028f\u0001\u0000"+
		"\u0000\u0000\u028f\u0290\u0001\u0000\u0000\u0000\u0290\u0291\u0005c\u0000"+
		"\u0000\u0291;\u0001\u0000\u0000\u0000\u0292\u029e\u0005o\u0000\u0000\u0293"+
		"\u0298\u0003>\u001f\u0000\u0294\u0295\u0005e\u0000\u0000\u0295\u0297\u0003"+
		">\u001f\u0000\u0296\u0294\u0001\u0000\u0000\u0000\u0297\u029a\u0001\u0000"+
		"\u0000\u0000\u0298\u0296\u0001\u0000\u0000\u0000\u0298\u0299\u0001\u0000"+
		"\u0000\u0000\u0299\u029c\u0001\u0000\u0000\u0000\u029a\u0298\u0001\u0000"+
		"\u0000\u0000\u029b\u029d\u0005e\u0000\u0000\u029c\u029b\u0001\u0000\u0000"+
		"\u0000\u029c\u029d\u0001\u0000\u0000\u0000\u029d\u029f\u0001\u0000\u0000"+
		"\u0000\u029e\u0293\u0001\u0000\u0000\u0000\u029e\u029f\u0001\u0000\u0000"+
		"\u0000\u029f\u02a0\u0001\u0000\u0000\u0000\u02a0\u02a1\u0005p\u0000\u0000"+
		"\u02a1=\u0001\u0000\u0000\u0000\u02a2\u02a3\u0005\u00a6\u0000\u0000\u02a3"+
		"\u02a4\u0005|\u0000\u0000\u02a4\u02b0\u0003L&\u0000\u02a5\u02a6\u0005"+
		"\t\u0000\u0000\u02a6\u02a7\u0005|\u0000\u0000\u02a7\u02ab\u0005j\u0000"+
		"\u0000\u02a8\u02aa\t\u0000\u0000\u0000\u02a9\u02a8\u0001\u0000\u0000\u0000"+
		"\u02aa\u02ad\u0001\u0000\u0000\u0000\u02ab\u02ac\u0001\u0000\u0000\u0000"+
		"\u02ab\u02a9\u0001\u0000\u0000\u0000\u02ac\u02ae\u0001\u0000\u0000\u0000"+
		"\u02ad\u02ab\u0001\u0000\u0000\u0000\u02ae\u02b0\u0005j\u0000\u0000\u02af"+
		"\u02a2\u0001\u0000\u0000\u0000\u02af\u02a5\u0001\u0000\u0000\u0000\u02b0"+
		"?\u0001\u0000\u0000\u0000\u02b1\u02b2\u0005$\u0000\u0000\u02b2\u02b3\u0005"+
		"k\u0000\u0000\u02b3\u02b4\u0003n7\u0000\u02b4\u02b5\u0005l\u0000\u0000"+
		"\u02b5A\u0001\u0000\u0000\u0000\u02b6\u02b9\u0003T*\u0000\u02b7\u02b9"+
		"\u0003@ \u0000\u02b8\u02b6\u0001\u0000\u0000\u0000\u02b8\u02b7\u0001\u0000"+
		"\u0000\u0000\u02b9C\u0001\u0000\u0000\u0000\u02ba\u02bf\u0003B!\u0000"+
		"\u02bb\u02bc\u0005e\u0000\u0000\u02bc\u02be\u0003B!\u0000\u02bd\u02bb"+
		"\u0001\u0000\u0000\u0000\u02be\u02c1\u0001\u0000\u0000\u0000\u02bf\u02bd"+
		"\u0001\u0000\u0000\u0000\u02bf\u02c0\u0001\u0000\u0000\u0000\u02c0\u02c3"+
		"\u0001\u0000\u0000\u0000\u02c1\u02bf\u0001\u0000\u0000\u0000\u02c2\u02c4"+
		"\u0005e\u0000\u0000\u02c3\u02c2\u0001\u0000\u0000\u0000\u02c3\u02c4\u0001"+
		"\u0000\u0000\u0000\u02c4E\u0001\u0000\u0000\u0000\u02c5\u02cc\u0003H$"+
		"\u0000\u02c6\u02cc\u00055\u0000\u0000\u02c7\u02cc\u00056\u0000\u0000\u02c8"+
		"\u02cc\u00057\u0000\u0000\u02c9\u02cc\u0003J%\u0000\u02ca\u02cc\u0005"+
		"8\u0000\u0000\u02cb\u02c5\u0001\u0000\u0000\u0000\u02cb\u02c6\u0001\u0000"+
		"\u0000\u0000\u02cb\u02c7\u0001\u0000\u0000\u0000\u02cb\u02c8\u0001\u0000"+
		"\u0000\u0000\u02cb\u02c9\u0001\u0000\u0000\u0000\u02cb\u02ca\u0001\u0000"+
		"\u0000\u0000\u02ccG\u0001\u0000\u0000\u0000\u02cd\u02ce\u0007\u0000\u0000"+
		"\u0000\u02ceI\u0001\u0000\u0000\u0000\u02cf\u02d0\u0005\u001b\u0000\u0000"+
		"\u02d0K\u0001\u0000\u0000\u0000\u02d1\u02d3\u0005i\u0000\u0000\u02d2\u02d4"+
		"\u0005\u0083\u0000\u0000\u02d3\u02d2\u0001\u0000\u0000\u0000\u02d3\u02d4"+
		"\u0001\u0000\u0000\u0000\u02d4\u02d8\u0001\u0000\u0000\u0000\u02d5\u02d9"+
		"\u0003\u0132\u0099\u0000\u02d6\u02d9\u0005\u00b0\u0000\u0000\u02d7\u02d9"+
		"\u0005\u00af\u0000\u0000\u02d8\u02d5\u0001\u0000\u0000\u0000\u02d8\u02d6"+
		"\u0001\u0000\u0000\u0000\u02d8\u02d7\u0001\u0000\u0000\u0000\u02d9\u02da"+
		"\u0001\u0000\u0000\u0000\u02da\u02e4\u0005i\u0000\u0000\u02db\u02dd\u0005"+
		"\u0083\u0000\u0000\u02dc\u02db\u0001\u0000\u0000\u0000\u02dc\u02dd\u0001"+
		"\u0000\u0000\u0000\u02dd\u02e1\u0001\u0000\u0000\u0000\u02de\u02e2\u0003"+
		"\u0132\u0099\u0000\u02df\u02e2\u0005\u00b0\u0000\u0000\u02e0\u02e2\u0005"+
		"\u00af\u0000\u0000\u02e1\u02de\u0001\u0000\u0000\u0000\u02e1\u02df\u0001"+
		"\u0000\u0000\u0000\u02e1\u02e0\u0001\u0000\u0000\u0000\u02e2\u02e4\u0001"+
		"\u0000\u0000\u0000\u02e3\u02d1\u0001\u0000\u0000\u0000\u02e3\u02dc\u0001"+
		"\u0000\u0000\u0000\u02e4M\u0001\u0000\u0000\u0000\u02e5\u02e6\u0005k\u0000"+
		"\u0000\u02e6\u02e7\u0005\u001f\u0000\u0000\u02e7\u02e8\u00052\u0000\u0000"+
		"\u02e8\u02ea\u0005l\u0000\u0000\u02e9\u02e5\u0001\u0000\u0000\u0000\u02e9"+
		"\u02ea\u0001\u0000\u0000\u0000\u02eaO\u0001\u0000\u0000\u0000\u02eb\u02ec"+
		"\u0005k\u0000\u0000\u02ec\u02ed\u0005\u001f\u0000\u0000\u02ed\u02ee\u0005"+
		"3\u0000\u0000\u02ee\u02ef\u0005l\u0000\u0000\u02efQ\u0001\u0000\u0000"+
		"\u0000\u02f0\u02fc\u0005o\u0000\u0000\u02f1\u02f6\u0003T*\u0000\u02f2"+
		"\u02f3\u0005e\u0000\u0000\u02f3\u02f5\u0003T*\u0000\u02f4\u02f2\u0001"+
		"\u0000\u0000\u0000\u02f5\u02f8\u0001\u0000\u0000\u0000\u02f6\u02f4\u0001"+
		"\u0000\u0000\u0000\u02f6\u02f7\u0001\u0000\u0000\u0000\u02f7\u02fa\u0001"+
		"\u0000\u0000\u0000\u02f8\u02f6\u0001\u0000\u0000\u0000\u02f9\u02fb\u0005"+
		"e\u0000\u0000\u02fa\u02f9\u0001\u0000\u0000\u0000\u02fa\u02fb\u0001\u0000"+
		"\u0000\u0000\u02fb\u02fd\u0001\u0000\u0000\u0000\u02fc\u02f1\u0001\u0000"+
		"\u0000\u0000\u02fc\u02fd\u0001\u0000\u0000\u0000\u02fd\u02fe\u0001\u0000"+
		"\u0000\u0000\u02fe\u02ff\u0005p\u0000\u0000\u02ffS\u0001\u0000\u0000\u0000"+
		"\u0300\u0304\u0003V+\u0000\u0301\u0304\u0003X,\u0000\u0302\u0304\u0003"+
		"Z-\u0000\u0303\u0300\u0001\u0000\u0000\u0000\u0303\u0301\u0001\u0000\u0000"+
		"\u0000\u0303\u0302\u0001\u0000\u0000\u0000\u0304U\u0001\u0000\u0000\u0000"+
		"\u0305\u0306\u0005\u00a5\u0000\u0000\u0306\u0307\u0005k\u0000\u0000\u0307"+
		"\u0308\u0003\u0016\u000b\u0000\u0308\u0309\u0005l\u0000\u0000\u0309\u030a"+
		"\u0003^/\u0000\u030aW\u0001\u0000\u0000\u0000\u030b\u030d\u0003\\.\u0000"+
		"\u030c\u030b\u0001\u0000\u0000\u0000\u030c\u030d\u0001\u0000\u0000\u0000"+
		"\u030d\u030e\u0001\u0000\u0000\u0000\u030e\u030f\u0003\u012e\u0097\u0000"+
		"\u030f\u0310\u0005k\u0000\u0000\u0310\u0311\u0003\u0016\u000b\u0000\u0311"+
		"\u0312\u0005l\u0000\u0000\u0312\u0313\u0003^/\u0000\u0313Y\u0001\u0000"+
		"\u0000\u0000\u0314\u0316\u0003\\.\u0000\u0315\u0314\u0001\u0000\u0000"+
		"\u0000\u0315\u0316\u0001\u0000\u0000\u0000\u0316\u0317\u0001\u0000\u0000"+
		"\u0000\u0317\u0318\u0003\u012c\u0096\u0000\u0318\u031a\u0005k\u0000\u0000"+
		"\u0319\u031b\u0003\u0018\f\u0000\u031a\u0319\u0001\u0000\u0000\u0000\u031a"+
		"\u031b\u0001\u0000\u0000\u0000\u031b\u031c\u0001\u0000\u0000\u0000\u031c"+
		"\u031d\u0005l\u0000\u0000\u031d\u031e\u0003^/\u0000\u031e[\u0001\u0000"+
		"\u0000\u0000\u031f\u0320\u0005\u0082\u0000\u0000\u0320]\u0001\u0000\u0000"+
		"\u0000\u0321\u032d\u0005o\u0000\u0000\u0322\u0327\u0003`0\u0000\u0323"+
		"\u0324\u0005e\u0000\u0000\u0324\u0326\u0003`0\u0000\u0325\u0323\u0001"+
		"\u0000\u0000\u0000\u0326\u0329\u0001\u0000\u0000\u0000\u0327\u0325\u0001"+
		"\u0000\u0000\u0000\u0327\u0328\u0001\u0000\u0000\u0000\u0328\u032b\u0001"+
		"\u0000\u0000\u0000\u0329\u0327\u0001\u0000\u0000\u0000\u032a\u032c\u0005"+
		"e\u0000\u0000\u032b\u032a\u0001\u0000\u0000\u0000\u032b\u032c\u0001\u0000"+
		"\u0000\u0000\u032c\u032e\u0001\u0000\u0000\u0000\u032d\u0322\u0001\u0000"+
		"\u0000\u0000\u032d\u032e\u0001\u0000\u0000\u0000\u032e\u032f\u0001\u0000"+
		"\u0000\u0000\u032f\u0331\u0005p\u0000\u0000\u0330\u0321\u0001\u0000\u0000"+
		"\u0000\u0330\u0331\u0001\u0000\u0000\u0000\u0331_\u0001\u0000\u0000\u0000"+
		"\u0332\u0333\u0007\u0001\u0000\u0000\u0333a\u0001\u0000\u0000\u0000\u0334"+
		"\u0340\u0005o\u0000\u0000\u0335\u033a\u0003d2\u0000\u0336\u0337\u0005"+
		"e\u0000\u0000\u0337\u0339\u0003d2\u0000\u0338\u0336\u0001\u0000\u0000"+
		"\u0000\u0339\u033c\u0001\u0000\u0000\u0000\u033a\u0338\u0001\u0000\u0000"+
		"\u0000\u033a\u033b\u0001\u0000\u0000\u0000\u033b\u033e\u0001\u0000\u0000"+
		"\u0000\u033c\u033a\u0001\u0000\u0000\u0000\u033d\u033f\u0005e\u0000\u0000"+
		"\u033e\u033d\u0001\u0000\u0000\u0000\u033e\u033f\u0001\u0000\u0000\u0000"+
		"\u033f\u0341\u0001\u0000\u0000\u0000\u0340\u0335\u0001\u0000\u0000\u0000"+
		"\u0340\u0341\u0001\u0000\u0000\u0000\u0341\u0342\u0001\u0000\u0000\u0000"+
		"\u0342\u0343\u0005p\u0000\u0000\u0343c\u0001\u0000\u0000\u0000\u0344\u0363"+
		"\u0005\u00a7\u0000\u0000\u0345\u0363\u0005\u00a8\u0000\u0000\u0346\u0363"+
		"\u0005\u00a9\u0000\u0000\u0347\u0363\u0005\u00aa\u0000\u0000\u0348\u0363"+
		"\u0005\u00ab\u0000\u0000\u0349\u034a\u0005\u00ac\u0000\u0000\u034a\u034b"+
		"\u0005|\u0000\u0000\u034b\u0363\u0003\u0130\u0098\u0000\u034c\u034d\u0005"+
		"%\u0000\u0000\u034d\u034e\u0005|\u0000\u0000\u034e\u0363\u0003\u009eO"+
		"\u0000\u034f\u0350\u0005\u001d\u0000\u0000\u0350\u0351\u0005|\u0000\u0000"+
		"\u0351\u035d\u0005o\u0000\u0000\u0352\u0357\u0003\u0130\u0098\u0000\u0353"+
		"\u0354\u0005e\u0000\u0000\u0354\u0356\u0003\u0130\u0098\u0000\u0355\u0353"+
		"\u0001\u0000\u0000\u0000\u0356\u0359\u0001\u0000\u0000\u0000\u0357\u0355"+
		"\u0001\u0000\u0000\u0000\u0357\u0358\u0001\u0000\u0000\u0000\u0358\u035b"+
		"\u0001\u0000\u0000\u0000\u0359\u0357\u0001\u0000\u0000\u0000\u035a\u035c"+
		"\u0005e\u0000\u0000\u035b\u035a\u0001\u0000\u0000\u0000\u035b\u035c\u0001"+
		"\u0000\u0000\u0000\u035c\u035e\u0001\u0000\u0000\u0000\u035d\u0352\u0001"+
		"\u0000\u0000\u0000\u035d\u035e\u0001\u0000\u0000\u0000\u035e\u035f\u0001"+
		"\u0000\u0000\u0000\u035f\u0363\u0005p\u0000\u0000\u0360\u0363\u0005\u0096"+
		"\u0000\u0000\u0361\u0363\u0005\u0097\u0000\u0000\u0362\u0344\u0001\u0000"+
		"\u0000\u0000\u0362\u0345\u0001\u0000\u0000\u0000\u0362\u0346\u0001\u0000"+
		"\u0000\u0000\u0362\u0347\u0001\u0000\u0000\u0000\u0362\u0348\u0001\u0000"+
		"\u0000\u0000\u0362\u0349\u0001\u0000\u0000\u0000\u0362\u034c\u0001\u0000"+
		"\u0000\u0000\u0362\u034f\u0001\u0000\u0000\u0000\u0362\u0360\u0001\u0000"+
		"\u0000\u0000\u0362\u0361\u0001\u0000\u0000\u0000\u0363e\u0001\u0000\u0000"+
		"\u0000\u0364\u0367\u0003h4\u0000\u0365\u0367\u0003j5\u0000\u0366\u0364"+
		"\u0001\u0000\u0000\u0000\u0366\u0365\u0001\u0000\u0000\u0000\u0367g\u0001"+
		"\u0000\u0000\u0000\u0368\u0369\u0005\u009e\u0000\u0000\u0369i\u0001\u0000"+
		"\u0000\u0000\u036a\u036b\u0005\u009f\u0000\u0000\u036bk\u0001\u0000\u0000"+
		"\u0000\u036c\u036d\u0005\u0006\u0000\u0000\u036d\u036e\u0003N\'\u0000"+
		"\u036e\u0370\u0003\u0130\u0098\u0000\u036f\u0371\u0003b1\u0000\u0370\u036f"+
		"\u0001\u0000\u0000\u0000\u0370\u0371\u0001\u0000\u0000\u0000\u0371\u0372"+
		"\u0001\u0000\u0000\u0000\u0372\u0374\u0005c\u0000\u0000\u0373\u0375\u0003"+
		"f3\u0000\u0374\u0373\u0001\u0000\u0000\u0000\u0374\u0375\u0001\u0000\u0000"+
		"\u0000\u0375\u0376\u0001\u0000\u0000\u0000\u0376\u0377\u0005j\u0000\u0000"+
		"\u0377\u0378\u0003n7\u0000\u0378\u037a\u0005j\u0000\u0000\u0379\u037b"+
		"\u0003\u00e0p\u0000\u037a\u0379\u0001\u0000\u0000\u0000\u037a\u037b\u0001"+
		"\u0000\u0000\u0000\u037bm\u0001\u0000\u0000\u0000\u037c\u0380\u0003p8"+
		"\u0000\u037d\u037e\u0003\u0162\u00b1\u0000\u037e\u037f\u0003p8\u0000\u037f"+
		"\u0381\u0001\u0000\u0000\u0000\u0380\u037d\u0001\u0000\u0000\u0000\u0380"+
		"\u0381\u0001\u0000\u0000\u0000\u0381o\u0001\u0000\u0000\u0000\u0382\u0387"+
		"\u0003r9\u0000\u0383\u0384\u0005\u0089\u0000\u0000\u0384\u0386\u0003r"+
		"9\u0000\u0385\u0383\u0001\u0000\u0000\u0000\u0386\u0389\u0001\u0000\u0000"+
		"\u0000\u0387\u0385\u0001\u0000\u0000\u0000\u0387\u0388\u0001\u0000\u0000"+
		"\u0000\u0388q\u0001\u0000\u0000\u0000\u0389\u0387\u0001\u0000\u0000\u0000"+
		"\u038a\u0390\u0003t:\u0000\u038b\u038c\u0003\u0164\u00b2\u0000\u038c\u038d"+
		"\u0003t:\u0000\u038d\u038f\u0001\u0000\u0000\u0000\u038e\u038b\u0001\u0000"+
		"\u0000\u0000\u038f\u0392\u0001\u0000\u0000\u0000\u0390\u038e\u0001\u0000"+
		"\u0000\u0000\u0390\u0391\u0001\u0000\u0000\u0000\u0391s\u0001\u0000\u0000"+
		"\u0000\u0392\u0390\u0001\u0000\u0000\u0000\u0393\u0399\u0003v;\u0000\u0394"+
		"\u0395\u0003\u0168\u00b4\u0000\u0395\u0396\u0003v;\u0000\u0396\u0398\u0001"+
		"\u0000\u0000\u0000\u0397\u0394\u0001\u0000\u0000\u0000\u0398\u039b\u0001"+
		"\u0000\u0000\u0000\u0399\u0397\u0001\u0000\u0000\u0000\u0399\u039a\u0001"+
		"\u0000\u0000\u0000\u039au\u0001\u0000\u0000\u0000\u039b\u0399\u0001\u0000"+
		"\u0000\u0000\u039c\u039e\u0003\u0166\u00b3\u0000\u039d\u039c\u0001\u0000"+
		"\u0000\u0000\u039d\u039e\u0001\u0000\u0000\u0000\u039e\u039f\u0001\u0000"+
		"\u0000\u0000\u039f\u03a0\u0003x<\u0000\u03a0w\u0001\u0000\u0000\u0000"+
		"\u03a1\u03a4\u0003\u016c\u00b6\u0000\u03a2\u03a4\u0003\u016e\u00b7\u0000"+
		"\u03a3\u03a1\u0001\u0000\u0000\u0000\u03a3\u03a2\u0001\u0000\u0000\u0000"+
		"\u03a4\u03b0\u0001\u0000\u0000\u0000\u03a5\u03a8\u0003\u0172\u00b9\u0000"+
		"\u03a6\u03a8\u0003\u0170\u00b8\u0000\u03a7\u03a5\u0001\u0000\u0000\u0000"+
		"\u03a7\u03a6\u0001\u0000\u0000\u0000\u03a8\u03b0\u0001\u0000\u0000\u0000"+
		"\u03a9\u03b0\u0003z=\u0000\u03aa\u03b0\u0003|>\u0000\u03ab\u03ac\u0005"+
		"k\u0000\u0000\u03ac\u03ad\u0003n7\u0000\u03ad\u03ae\u0005l\u0000\u0000"+
		"\u03ae\u03b0\u0001\u0000\u0000\u0000\u03af\u03a3\u0001\u0000\u0000\u0000"+
		"\u03af\u03a7\u0001\u0000\u0000\u0000\u03af\u03a9\u0001\u0000\u0000\u0000"+
		"\u03af\u03aa\u0001\u0000\u0000\u0000\u03af\u03ab\u0001\u0000\u0000\u0000"+
		"\u03b0y\u0001\u0000\u0000\u0000\u03b1\u03b2\u0005\f\u0000\u0000\u03b2"+
		"\u03b3\u0005k\u0000\u0000\u03b3\u03b4\u0003\u014e\u00a7\u0000\u03b4\u03b5"+
		"\u0005l\u0000\u0000\u03b5\u03c9\u0001\u0000\u0000\u0000\u03b6\u03b7\u0003"+
		"T*\u0000\u03b7\u03b8\u0005}\u0000\u0000\u03b8\u03b9\u0003\u014e\u00a7"+
		"\u0000\u03b9\u03c9\u0001\u0000\u0000\u0000\u03ba\u03c9\u0003T*\u0000\u03bb"+
		"\u03bc\u0003\u014e\u00a7\u0000\u03bc\u03bd\u0005\u0084\u0000\u0000\u03bd"+
		"\u03be\u0003\u014e\u00a7\u0000\u03be\u03c9\u0001\u0000\u0000\u0000\u03bf"+
		"\u03c9\u0003~?\u0000\u03c0\u03c1\u0003\u014e\u00a7\u0000\u03c1\u03c2\u0003"+
		"\u016a\u00b5\u0000\u03c2\u03c3\u0003\u014e\u00a7\u0000\u03c3\u03c9\u0001"+
		"\u0000\u0000\u0000\u03c4\u03c5\u0003\u0016\u000b\u0000\u03c5\u03c6\u0003"+
		"\u016a\u00b5\u0000\u03c6\u03c7\u0003\u0016\u000b\u0000\u03c7\u03c9\u0001"+
		"\u0000\u0000\u0000\u03c8\u03b1\u0001\u0000\u0000\u0000\u03c8\u03b6\u0001"+
		"\u0000\u0000\u0000\u03c8\u03ba\u0001\u0000\u0000\u0000\u03c8\u03bb\u0001"+
		"\u0000\u0000\u0000\u03c8\u03bf\u0001\u0000\u0000\u0000\u03c8\u03c0\u0001"+
		"\u0000\u0000\u0000\u03c8\u03c4\u0001\u0000\u0000\u0000\u03c9{\u0001\u0000"+
		"\u0000\u0000\u03ca\u03cd\u0003\u0174\u00ba\u0000\u03cb\u03cd\u0003\u0176"+
		"\u00bb\u0000\u03cc\u03ca\u0001\u0000\u0000\u0000\u03cc\u03cb\u0001\u0000"+
		"\u0000\u0000\u03cd\u03ce\u0001\u0000\u0000\u0000\u03ce\u03cf\u0003\u014c"+
		"\u00a6\u0000\u03cf\u03d0\u0005f\u0000\u0000\u03d0\u03d1\u0003n7\u0000"+
		"\u03d1}\u0001\u0000\u0000\u0000\u03d2\u03d3\u0003\u0016\u000b\u0000\u03d3"+
		"\u03d4\u0005\u0085\u0000\u0000\u03d4\u03d5\u0003\u0016\u000b\u0000\u03d5"+
		"\u007f\u0001\u0000\u0000\u0000\u03d6\u03d7\u0005\u0016\u0000\u0000\u03d7"+
		"\u03d8\u0003\u0130\u0098\u0000\u03d8\u03d9\u0005c\u0000\u0000\u03d9\u03da"+
		"\u0005j\u0000\u0000\u03da\u03db\u0003n7\u0000\u03db\u03dc\u0005j\u0000"+
		"\u0000\u03dc\u0081\u0001\u0000\u0000\u0000\u03dd\u03de\u0005\u0006\u0000"+
		"\u0000\u03de\u03e0\u0003\u0130\u0098\u0000\u03df\u03e1\u0003b1\u0000\u03e0"+
		"\u03df\u0001\u0000\u0000\u0000\u03e0\u03e1\u0001\u0000\u0000\u0000\u03e1"+
		"\u03e2\u0001\u0000\u0000\u0000\u03e2\u03e3\u0005c\u0000\u0000\u03e3\u03e8"+
		"\u0003\u0130\u0098\u0000\u03e4\u03e5\u0005e\u0000\u0000\u03e5\u03e7\u0003"+
		"\u0130\u0098\u0000\u03e6\u03e4\u0001\u0000\u0000\u0000\u03e7\u03ea\u0001"+
		"\u0000\u0000\u0000\u03e8\u03e6\u0001\u0000\u0000\u0000\u03e8\u03e9\u0001"+
		"\u0000\u0000\u0000\u03e9\u03ec\u0001\u0000\u0000\u0000\u03ea\u03e8\u0001"+
		"\u0000\u0000\u0000\u03eb\u03ed\u0005e\u0000\u0000\u03ec\u03eb\u0001\u0000"+
		"\u0000\u0000\u03ec\u03ed\u0001\u0000\u0000\u0000\u03ed\u03ee\u0001\u0000"+
		"\u0000\u0000\u03ee\u03ef\u0005\u001e\u0000\u0000\u03ef\u03f0\u0005j\u0000"+
		"\u0000\u03f0\u03f1\u0003n7\u0000\u03f1\u03f2\u0005j\u0000\u0000\u03f2"+
		"\u0083\u0001\u0000\u0000\u0000\u03f3\u03f5\u0003\u009aM\u0000\u03f4\u03f6"+
		"\u0003\u0092I\u0000\u03f5\u03f4\u0001\u0000\u0000\u0000\u03f5\u03f6\u0001"+
		"\u0000\u0000\u0000\u03f6\u03fa\u0001\u0000\u0000\u0000\u03f7\u03f9\u0003"+
		"\u0088D\u0000\u03f8\u03f7\u0001\u0000\u0000\u0000\u03f9\u03fc\u0001\u0000"+
		"\u0000\u0000\u03fa\u03f8\u0001\u0000\u0000\u0000\u03fa\u03fb\u0001\u0000"+
		"\u0000\u0000\u03fb\u0400\u0001\u0000\u0000\u0000\u03fc\u03fa\u0001\u0000"+
		"\u0000\u0000\u03fd\u03ff\u0003\u0086C\u0000\u03fe\u03fd\u0001\u0000\u0000"+
		"\u0000\u03ff\u0402\u0001\u0000\u0000\u0000\u0400\u03fe\u0001\u0000\u0000"+
		"\u0000\u0400\u0401\u0001\u0000\u0000\u0000\u0401\u0085\u0001\u0000\u0000"+
		"\u0000\u0402\u0400\u0001\u0000\u0000\u0000\u0403\u0404\u0005\u000e\u0000"+
		"\u0000\u0404\u0409\u0005c\u0000\u0000\u0405\u0406\u0005m\u0000\u0000\u0406"+
		"\u0407\u0003\u0098L\u0000\u0407\u0408\u0005n\u0000\u0000\u0408\u040a\u0001"+
		"\u0000\u0000\u0000\u0409\u0405\u0001\u0000\u0000\u0000\u0409\u040a\u0001"+
		"\u0000\u0000\u0000\u040a\u040c\u0001\u0000\u0000\u0000\u040b\u040d\u0003"+
		"\u008aE\u0000\u040c\u040b\u0001\u0000\u0000\u0000\u040d\u040e\u0001\u0000"+
		"\u0000\u0000\u040e\u040c\u0001\u0000\u0000\u0000\u040e\u040f\u0001\u0000"+
		"\u0000\u0000\u040f\u0087\u0001\u0000\u0000\u0000\u0410\u0411\u0005\r\u0000"+
		"\u0000\u0411\u0416\u0005c\u0000\u0000\u0412\u0413\u0005m\u0000\u0000\u0413"+
		"\u0414\u0003\u0098L\u0000\u0414\u0415\u0005n\u0000\u0000\u0415\u0417\u0001"+
		"\u0000\u0000\u0000\u0416\u0412\u0001\u0000\u0000\u0000\u0416\u0417\u0001"+
		"\u0000\u0000\u0000\u0417\u0419\u0001\u0000\u0000\u0000\u0418\u041a\u0003"+
		"\u008aE\u0000\u0419\u0418\u0001\u0000\u0000\u0000\u041a\u041b\u0001\u0000"+
		"\u0000\u0000\u041b\u0419\u0001\u0000\u0000\u0000\u041b\u041c\u0001\u0000"+
		"\u0000\u0000\u041c\u0089\u0001\u0000\u0000\u0000\u041d\u0423\u0003\u008c"+
		"F\u0000\u041e\u041f\u0003\u0164\u00b2\u0000\u041f\u0420\u0003\u008cF\u0000"+
		"\u0420\u0422\u0001\u0000\u0000\u0000\u0421\u041e\u0001\u0000\u0000\u0000"+
		"\u0422\u0425\u0001\u0000\u0000\u0000\u0423\u0421\u0001\u0000\u0000\u0000"+
		"\u0423\u0424\u0001\u0000\u0000\u0000\u0424\u008b\u0001\u0000\u0000\u0000"+
		"\u0425\u0423\u0001\u0000\u0000\u0000\u0426\u042b\u0003\u008eG\u0000\u0427"+
		"\u0428\u0005u\u0000\u0000\u0428\u042a\u0003\u008eG\u0000\u0429\u0427\u0001"+
		"\u0000\u0000\u0000\u042a\u042d\u0001\u0000\u0000\u0000\u042b\u0429\u0001"+
		"\u0000\u0000\u0000\u042b\u042c\u0001\u0000\u0000\u0000\u042c\u008d\u0001"+
		"\u0000\u0000\u0000\u042d\u042b\u0001\u0000\u0000\u0000\u042e\u0430\u0003"+
		"\u0166\u00b3\u0000\u042f\u042e\u0001\u0000\u0000\u0000\u042f\u0430\u0001"+
		"\u0000\u0000\u0000\u0430\u0431\u0001\u0000\u0000\u0000\u0431\u0432\u0003"+
		"\u0090H\u0000\u0432\u008f\u0001\u0000\u0000\u0000\u0433\u0438\u0003\u0096"+
		"K\u0000\u0434\u0435\u0005j\u0000\u0000\u0435\u0436\u0003\u0158\u00ac\u0000"+
		"\u0436\u0437\u0005j\u0000\u0000\u0437\u0439\u0001\u0000\u0000\u0000\u0438"+
		"\u0434\u0001\u0000\u0000\u0000\u0439\u043a\u0001\u0000\u0000\u0000\u043a"+
		"\u0438\u0001\u0000\u0000\u0000\u043a\u043b\u0001\u0000\u0000\u0000\u043b"+
		"\u0091\u0001\u0000\u0000\u0000\u043c\u043d\u0005\u000f\u0000\u0000\u043d"+
		"\u043e\u0005c\u0000\u0000\u043e\u043f\u0003\u0094J\u0000\u043f\u0093\u0001"+
		"\u0000\u0000\u0000\u0440\u0441\u0005;\u0000\u0000\u0441\u0095\u0001\u0000"+
		"\u0000\u0000\u0442\u0443\u0005\\\u0000\u0000\u0443\u0097\u0001\u0000\u0000"+
		"\u0000\u0444\u0445\u0007\u0002\u0000\u0000\u0445\u0099\u0001\u0000\u0000"+
		"\u0000\u0446\u0447\u0005\u0012\u0000\u0000\u0447\u0448\u0005c\u0000\u0000"+
		"\u0448\u0449\u0003\u0130\u0098\u0000\u0449\u009b\u0001\u0000\u0000\u0000"+
		"\u044a\u044b\u0005%\u0000\u0000\u044b\u044d\u0005c\u0000\u0000\u044c\u044e"+
		"\u0003\u009eO\u0000\u044d\u044c\u0001\u0000\u0000\u0000\u044e\u044f\u0001"+
		"\u0000\u0000\u0000\u044f\u044d\u0001\u0000\u0000\u0000\u044f\u0450\u0001"+
		"\u0000\u0000\u0000\u0450\u009d\u0001\u0000\u0000\u0000\u0451\u0455\u0003"+
		"\u00a4R\u0000\u0452\u0455\u0003\u00a2Q\u0000\u0453\u0455\u0003\u00a0P"+
		"\u0000\u0454\u0451\u0001\u0000\u0000\u0000\u0454\u0452\u0001\u0000\u0000"+
		"\u0000\u0454\u0453\u0001\u0000\u0000\u0000\u0455\u009f\u0001\u0000\u0000"+
		"\u0000\u0456\u0457\u0007\u0003\u0000\u0000\u0457\u00a1\u0001\u0000\u0000"+
		"\u0000\u0458\u0459\u0005m\u0000\u0000\u0459\u045a\u0003\u015a\u00ad\u0000"+
		"\u045a\u045b\u0005n\u0000\u0000\u045b\u00a3\u0001\u0000\u0000\u0000\u045c"+
		"\u0461\u0005<\u0000\u0000\u045d\u045e\u0005j\u0000\u0000\u045e\u045f\u0003"+
		"\u015c\u00ae\u0000\u045f\u0460\u0005j\u0000\u0000\u0460\u0462\u0001\u0000"+
		"\u0000\u0000\u0461\u045d\u0001\u0000\u0000\u0000\u0461\u0462\u0001\u0000"+
		"\u0000\u0000\u0462\u00a5\u0001\u0000\u0000\u0000\u0463\u0464\u0005&\u0000"+
		"\u0000\u0464\u0465\u0003\u0130\u0098\u0000\u0465\u0466\u0005c\u0000\u0000"+
		"\u0466\u0467\u0005j\u0000\u0000\u0467\u0468\u0003\u015e\u00af\u0000\u0468"+
		"\u0469\u0005j\u0000\u0000\u0469\u00a7\u0001\u0000\u0000\u0000\u046a\u046b"+
		"\u0005\'\u0000\u0000\u046b\u046c\u0005c\u0000\u0000\u046c\u0471\u0003"+
		"\u00aaU\u0000\u046d\u046e\u0005e\u0000\u0000\u046e\u0470\u0003\u00aaU"+
		"\u0000\u046f\u046d\u0001\u0000\u0000\u0000\u0470\u0473\u0001\u0000\u0000"+
		"\u0000\u0471\u046f\u0001\u0000\u0000\u0000\u0471\u0472\u0001\u0000\u0000"+
		"\u0000\u0472\u0475\u0001\u0000\u0000\u0000\u0473\u0471\u0001\u0000\u0000"+
		"\u0000\u0474\u0476\u0005e\u0000\u0000\u0475\u0474\u0001\u0000\u0000\u0000"+
		"\u0475\u0476\u0001\u0000\u0000\u0000\u0476\u00a9\u0001\u0000\u0000\u0000"+
		"\u0477\u0478\u0003T*\u0000\u0478\u0479\u0005\u008a\u0000\u0000\u0479\u047a"+
		"\u0003n7\u0000\u047a\u00ab\u0001\u0000\u0000\u0000\u047b\u047c\u0005("+
		"\u0000\u0000\u047c\u047d\u0005c\u0000\u0000\u047d\u0482\u0003\u00aeW\u0000"+
		"\u047e\u047f\u0005e\u0000\u0000\u047f\u0481\u0003\u00aeW\u0000\u0480\u047e"+
		"\u0001\u0000\u0000\u0000\u0481\u0484\u0001\u0000\u0000\u0000\u0482\u0480"+
		"\u0001\u0000\u0000\u0000\u0482\u0483\u0001\u0000\u0000\u0000\u0483\u0486"+
		"\u0001\u0000\u0000\u0000\u0484\u0482\u0001\u0000\u0000\u0000\u0485\u0487"+
		"\u0005e\u0000\u0000\u0486\u0485\u0001\u0000\u0000\u0000\u0486\u0487\u0001"+
		"\u0000\u0000\u0000\u0487\u00ad\u0001\u0000\u0000\u0000\u0488\u0489\u0005"+
		"M\u0000\u0000\u0489\u00af\u0001\u0000\u0000\u0000\u048a\u048b\u0005)\u0000"+
		"\u0000\u048b\u048c\u0005c\u0000\u0000\u048c\u0491\u0003\u00b2Y\u0000\u048d"+
		"\u048e\u0005e\u0000\u0000\u048e\u0490\u0003\u00b2Y\u0000\u048f\u048d\u0001"+
		"\u0000\u0000\u0000\u0490\u0493\u0001\u0000\u0000\u0000\u0491\u048f\u0001"+
		"\u0000\u0000\u0000\u0491\u0492\u0001\u0000\u0000\u0000\u0492\u0495\u0001"+
		"\u0000\u0000\u0000\u0493\u0491\u0001\u0000\u0000\u0000\u0494\u0496\u0005"+
		"e\u0000\u0000\u0495\u0494\u0001\u0000\u0000\u0000\u0495\u0496\u0001\u0000"+
		"\u0000\u0000\u0496\u00b1\u0001\u0000\u0000\u0000\u0497\u0498\u0003 \u0010"+
		"\u0000\u0498\u0499\u0003\u016a\u00b5\u0000\u0499\u049a\u0003 \u0010\u0000"+
		"\u049a\u00b3\u0001\u0000\u0000\u0000\u049b\u049c\u0005*\u0000\u0000\u049c"+
		"\u049d\u0005c\u0000\u0000\u049d\u04a2\u0003\u00b6[\u0000\u049e\u049f\u0005"+
		"e\u0000\u0000\u049f\u04a1\u0003\u00b6[\u0000\u04a0\u049e\u0001\u0000\u0000"+
		"\u0000\u04a1\u04a4\u0001\u0000\u0000\u0000\u04a2\u04a0\u0001\u0000\u0000"+
		"\u0000\u04a2\u04a3\u0001\u0000\u0000\u0000\u04a3\u04a6\u0001\u0000\u0000"+
		"\u0000\u04a4\u04a2\u0001\u0000\u0000\u0000\u04a5\u04a7\u0005e\u0000\u0000"+
		"\u04a6\u04a5\u0001\u0000\u0000\u0000\u04a6\u04a7\u0001\u0000\u0000\u0000"+
		"\u04a7\u00b5\u0001\u0000\u0000\u0000\u04a8\u04a9\u0003\u0130\u0098\u0000"+
		"\u04a9\u04b9\u0003\u00ba]\u0000\u04aa\u04b6\u0005o\u0000\u0000\u04ab\u04b0"+
		"\u0003\u00b8\\\u0000\u04ac\u04ad\u0005e\u0000\u0000\u04ad\u04af\u0003"+
		"\u00b8\\\u0000\u04ae\u04ac\u0001\u0000\u0000\u0000\u04af\u04b2\u0001\u0000"+
		"\u0000\u0000\u04b0\u04ae\u0001\u0000\u0000\u0000\u04b0\u04b1\u0001\u0000"+
		"\u0000\u0000\u04b1\u04b4\u0001\u0000\u0000\u0000\u04b2\u04b0\u0001\u0000"+
		"\u0000\u0000\u04b3\u04b5\u0005e\u0000\u0000\u04b4\u04b3\u0001\u0000\u0000"+
		"\u0000\u04b4\u04b5\u0001\u0000\u0000\u0000\u04b5\u04b7\u0001\u0000\u0000"+
		"\u0000\u04b6\u04ab\u0001\u0000\u0000\u0000\u04b6\u04b7\u0001\u0000\u0000"+
		"\u0000\u04b7\u04b8\u0001\u0000\u0000\u0000\u04b8\u04ba\u0005p\u0000\u0000"+
		"\u04b9\u04aa\u0001\u0000\u0000\u0000\u04b9\u04ba\u0001\u0000\u0000\u0000"+
		"\u04ba\u00b7\u0001\u0000\u0000\u0000\u04bb\u04bc\u0005\u00ad\u0000\u0000"+
		"\u04bc\u00b9\u0001\u0000\u0000\u0000\u04bd\u04be\u0005g\u0000\u0000\u04be"+
		"\u04d1\u0003\u0132\u0099\u0000\u04bf\u04cb\u0005k\u0000\u0000\u04c0\u04c5"+
		"\u0003\u011e\u008f\u0000\u04c1\u04c2\u0005e\u0000\u0000\u04c2\u04c4\u0003"+
		"\u011e\u008f\u0000\u04c3\u04c1\u0001\u0000\u0000\u0000\u04c4\u04c7\u0001"+
		"\u0000\u0000\u0000\u04c5\u04c3\u0001\u0000\u0000\u0000\u04c5\u04c6\u0001"+
		"\u0000\u0000\u0000\u04c6\u04c9\u0001\u0000\u0000\u0000\u04c7\u04c5\u0001"+
		"\u0000\u0000\u0000\u04c8\u04ca\u0005e\u0000\u0000\u04c9\u04c8\u0001\u0000"+
		"\u0000\u0000\u04c9\u04ca\u0001\u0000\u0000\u0000\u04ca\u04cc\u0001\u0000"+
		"\u0000\u0000\u04cb\u04c0\u0001\u0000\u0000\u0000\u04cb\u04cc\u0001\u0000"+
		"\u0000\u0000\u04cc\u04cd\u0001\u0000\u0000\u0000\u04cd\u04ce\u0005l\u0000"+
		"\u0000\u04ce\u04cf\u0005c\u0000\u0000\u04cf\u04d1\u0003\u011e\u008f\u0000"+
		"\u04d0\u04bd\u0001\u0000\u0000\u0000\u04d0\u04bf\u0001\u0000\u0000\u0000"+
		"\u04d1\u00bb\u0001\u0000\u0000\u0000\u04d2\u04d3\u0005+\u0000\u0000\u04d3"+
		"\u04d4\u0005c\u0000\u0000\u04d4\u04d9\u0003\u00be_\u0000\u04d5\u04d6\u0005"+
		"e\u0000\u0000\u04d6\u04d8\u0003\u00be_\u0000\u04d7\u04d5\u0001\u0000\u0000"+
		"\u0000\u04d8\u04db\u0001\u0000\u0000\u0000\u04d9\u04d7\u0001\u0000\u0000"+
		"\u0000\u04d9\u04da\u0001\u0000\u0000\u0000\u04da\u04dd\u0001\u0000\u0000"+
		"\u0000\u04db\u04d9\u0001\u0000\u0000\u0000\u04dc\u04de\u0005e\u0000\u0000"+
		"\u04dd\u04dc\u0001\u0000\u0000\u0000\u04dd\u04de\u0001\u0000\u0000\u0000"+
		"\u04de\u00bd\u0001\u0000\u0000\u0000\u04df\u04ef\u0003\u00c0`\u0000\u04e0"+
		"\u04ef\u0003\u00c2a\u0000\u04e1\u04ef\u0003\u00c4b\u0000\u04e2\u04ef\u0003"+
		"\u00c6c\u0000\u04e3\u04ef\u0003\u00c8d\u0000\u04e4\u04ef\u0003\u00cae"+
		"\u0000\u04e5\u04ef\u0003\u00ccf\u0000\u04e6\u04ef\u0003\u00ceg\u0000\u04e7"+
		"\u04ef\u0003\u00d0h\u0000\u04e8\u04ef\u0003\u00d2i\u0000\u04e9\u04ef\u0003"+
		"\u00d4j\u0000\u04ea\u04ef\u0003\u00d6k\u0000\u04eb\u04ef\u0003\u00d8l"+
		"\u0000\u04ec\u04ef\u0003\u00dam\u0000\u04ed\u04ef\u0003\u00dcn\u0000\u04ee"+
		"\u04df\u0001\u0000\u0000\u0000\u04ee\u04e0\u0001\u0000\u0000\u0000\u04ee"+
		"\u04e1\u0001\u0000\u0000\u0000\u04ee\u04e2\u0001\u0000\u0000\u0000\u04ee"+
		"\u04e3\u0001\u0000\u0000\u0000\u04ee\u04e4\u0001\u0000\u0000\u0000\u04ee"+
		"\u04e5\u0001\u0000\u0000\u0000\u04ee\u04e6\u0001\u0000\u0000\u0000\u04ee"+
		"\u04e7\u0001\u0000\u0000\u0000\u04ee\u04e8\u0001\u0000\u0000\u0000\u04ee"+
		"\u04e9\u0001\u0000\u0000\u0000\u04ee\u04ea\u0001\u0000\u0000\u0000\u04ee"+
		"\u04eb\u0001\u0000\u0000\u0000\u04ee\u04ec\u0001\u0000\u0000\u0000\u04ee"+
		"\u04ed\u0001\u0000\u0000\u0000\u04ef\u00bf\u0001\u0000\u0000\u0000\u04f0"+
		"\u04f1\u0005N\u0000\u0000\u04f1\u00c1\u0001\u0000\u0000\u0000\u04f2\u04f3"+
		"\u0005O\u0000\u0000\u04f3\u00c3\u0001\u0000\u0000\u0000\u04f4\u04f5\u0005"+
		"P\u0000\u0000\u04f5\u00c5\u0001\u0000\u0000\u0000\u04f6\u04f7\u0005Q\u0000"+
		"\u0000\u04f7\u00c7\u0001\u0000\u0000\u0000\u04f8\u04f9\u0005R\u0000\u0000"+
		"\u04f9\u00c9\u0001\u0000\u0000\u0000\u04fa\u04fb\u0005S\u0000\u0000\u04fb"+
		"\u00cb\u0001\u0000\u0000\u0000\u04fc\u04fd\u0005T\u0000\u0000\u04fd\u00cd"+
		"\u0001\u0000\u0000\u0000\u04fe\u04ff\u0005U\u0000\u0000\u04ff\u00cf\u0001"+
		"\u0000\u0000\u0000\u0500\u0501\u0005V\u0000\u0000\u0501\u00d1\u0001\u0000"+
		"\u0000\u0000\u0502\u0503\u0005W\u0000\u0000\u0503\u00d3\u0001\u0000\u0000"+
		"\u0000\u0504\u0505\u0005X\u0000\u0000\u0505\u00d5\u0001\u0000\u0000\u0000"+
		"\u0506\u0507\u0005Y\u0000\u0000\u0507\u00d7\u0001\u0000\u0000\u0000\u0508"+
		"\u0509\u0005Z\u0000\u0000\u0509\u00d9\u0001\u0000\u0000\u0000\u050a\u050b"+
		"\u0005[\u0000\u0000\u050b\u00db\u0001\u0000\u0000\u0000\u050c\u050d\u0005"+
		"\u001c\u0000\u0000\u050d\u00dd\u0001\u0000\u0000\u0000\u050e\u0512\u0003"+
		"\u00e2q\u0000\u050f\u0512\u0003\u00e4r\u0000\u0510\u0512\u0003\u00e6s"+
		"\u0000\u0511\u050e\u0001\u0000\u0000\u0000\u0511\u050f\u0001\u0000\u0000"+
		"\u0000\u0511\u0510\u0001\u0000\u0000\u0000\u0512\u00df\u0001\u0000\u0000"+
		"\u0000\u0513\u0516\u0003\u00e4r\u0000\u0514\u0516\u0003\u00e6s\u0000\u0515"+
		"\u0513\u0001\u0000\u0000\u0000\u0515\u0514\u0001\u0000\u0000\u0000\u0516"+
		"\u00e1\u0001\u0000\u0000\u0000\u0517\u0518\u0005G\u0000\u0000\u0518\u00e3"+
		"\u0001\u0000\u0000\u0000\u0519\u051a\u0005\u0005\u0000\u0000\u051a\u051b"+
		"\u0003\u00eau\u0000\u051b\u00e5\u0001\u0000\u0000\u0000\u051c\u0528\u0003"+
		"\u00eau\u0000\u051d\u0522\u0003\u00e8t\u0000\u051e\u051f\u0005.\u0000"+
		"\u0000\u051f\u0521\u0003\u00e8t\u0000\u0520\u051e\u0001\u0000\u0000\u0000"+
		"\u0521\u0524\u0001\u0000\u0000\u0000\u0522\u0520\u0001\u0000\u0000\u0000"+
		"\u0522\u0523\u0001\u0000\u0000\u0000\u0523\u0525\u0001\u0000\u0000\u0000"+
		"\u0524\u0522\u0001\u0000\u0000\u0000\u0525\u0526\u0005-\u0000\u0000\u0526"+
		"\u0529\u0001\u0000\u0000\u0000\u0527\u0529\u0003\u00deo\u0000\u0528\u051d"+
		"\u0001\u0000\u0000\u0000\u0528\u0527\u0001\u0000\u0000\u0000\u0529\u00e7"+
		"\u0001\u0000\u0000\u0000\u052a\u052b\u0005,\u0000\u0000\u052b\u052c\u0003"+
		"\u0130\u0098\u0000\u052c\u052d\u0003\u00deo\u0000\u052d\u00e9\u0001\u0000"+
		"\u0000\u0000\u052e\u0538\u0005H\u0000\u0000\u052f\u0538\u0005I\u0000\u0000"+
		"\u0530\u0531\u0005J\u0000\u0000\u0531\u0532\u0005k\u0000\u0000\u0532\u0533"+
		"\u0003\u00ecv\u0000\u0533\u0534\u0005l\u0000\u0000\u0534\u0538\u0001\u0000"+
		"\u0000\u0000\u0535\u0538\u0005K\u0000\u0000\u0536\u0538\u0005L\u0000\u0000"+
		"\u0537\u052e\u0001\u0000\u0000\u0000\u0537\u052f\u0001\u0000\u0000\u0000"+
		"\u0537\u0530\u0001\u0000\u0000\u0000\u0537\u0535\u0001\u0000\u0000\u0000"+
		"\u0537\u0536\u0001\u0000\u0000\u0000\u0538\u00eb\u0001\u0000\u0000\u0000"+
		"\u0539\u053f\u0003\u00eew\u0000\u053a\u053f\u0003\u00f0x\u0000\u053b\u053f"+
		"\u0003\u00f2y\u0000\u053c\u053f\u0003\u00f4z\u0000\u053d\u053f\u0003\u00f6"+
		"{\u0000\u053e\u0539\u0001\u0000\u0000\u0000\u053e\u053a\u0001\u0000\u0000"+
		"\u0000\u053e\u053b\u0001\u0000\u0000\u0000\u053e\u053c\u0001\u0000\u0000"+
		"\u0000\u053e\u053d\u0001\u0000\u0000\u0000\u053f\u00ed\u0001\u0000\u0000"+
		"\u0000\u0540\u0541\u0003T*\u0000\u0541\u0542\u0005}\u0000\u0000\u0542"+
		"\u0543\u0003\u0150\u00a8\u0000\u0543\u00ef\u0001\u0000\u0000\u0000\u0544"+
		"\u0545\u0003T*\u0000\u0545\u0546\u0005\u0080\u0000\u0000\u0546\u0547\u0005"+
		"\u00b4\u0000\u0000\u0547\u0548\u0003\u0150\u00a8\u0000\u0548\u00f1\u0001"+
		"\u0000\u0000\u0000\u0549\u054a\u0003\u00fa}\u0000\u054a\u054b\u0005\u008e"+
		"\u0000\u0000\u054b\u054c\u0003\u00f8|\u0000\u054c\u00f3\u0001\u0000\u0000"+
		"\u0000\u054d\u0552\u0003n7\u0000\u054e\u054f\u0005\u0081\u0000\u0000\u054f"+
		"\u0551\u0003n7\u0000\u0550\u054e\u0001\u0000\u0000\u0000\u0551\u0554\u0001"+
		"\u0000\u0000\u0000\u0552\u0550\u0001\u0000\u0000\u0000\u0552\u0553\u0001"+
		"\u0000\u0000\u0000\u0553\u00f5\u0001\u0000\u0000\u0000\u0554\u0552\u0001"+
		"\u0000\u0000\u0000\u0555\u0556\u0005/\u0000\u0000\u0556\u0557\u0005k\u0000"+
		"\u0000\u0557\u0558\u0003\u0132\u0099\u0000\u0558\u0559\u0005l\u0000\u0000"+
		"\u0559\u00f7\u0001\u0000\u0000\u0000\u055a\u055b\u0005k\u0000\u0000\u055b"+
		"\u055c\u0003\u0150\u00a8\u0000\u055c\u055d\u0005e\u0000\u0000\u055d\u055e"+
		"\u0003\u0132\u0099\u0000\u055e\u055f\u0005l\u0000\u0000\u055f\u00f9\u0001"+
		"\u0000\u0000\u0000\u0560\u0561\u0005k\u0000\u0000\u0561\u0562\u0003\u0150"+
		"\u00a8\u0000\u0562\u0563\u0005e\u0000\u0000\u0563\u0564\u0003\u0132\u0099"+
		"\u0000\u0564\u0565\u0005l\u0000\u0000\u0565\u00fb\u0001\u0000\u0000\u0000"+
		"\u0566\u0567\u00051\u0000\u0000\u0567\u0568\u0005c\u0000\u0000\u0568\u0569"+
		"\u0003\u0100\u0080\u0000\u0569\u00fd\u0001\u0000\u0000\u0000\u056a\u056b"+
		"\u00050\u0000\u0000\u056b\u056c\u0005c\u0000\u0000\u056c\u056d\u0003\u0100"+
		"\u0080\u0000\u056d\u056e\u0003\u0100\u0080\u0000\u056e\u00ff\u0001\u0000"+
		"\u0000\u0000\u056f\u0574\u0003\u0106\u0083\u0000\u0570\u0571\u0007\u0004"+
		"\u0000\u0000\u0571\u0573\u0003\u0106\u0083\u0000\u0572\u0570\u0001\u0000"+
		"\u0000\u0000\u0573\u0576\u0001\u0000\u0000\u0000\u0574\u0572\u0001\u0000"+
		"\u0000\u0000\u0574\u0575\u0001\u0000\u0000\u0000\u0575\u0101\u0001\u0000"+
		"\u0000\u0000\u0576\u0574\u0001\u0000\u0000\u0000\u0577\u0578\u0005\b\u0000"+
		"\u0000\u0578\u0579\u0003\u0100\u0080\u0000\u0579\u0103\u0001\u0000\u0000"+
		"\u0000\u057a\u057b\u0005\t\u0000\u0000\u057b\u057c\u0005c\u0000\u0000"+
		"\u057c\u057d\u0003\u0100\u0080\u0000\u057d\u0105\u0001\u0000\u0000\u0000"+
		"\u057e\u057f\u0005\u0082\u0000\u0000\u057f\u05c4\u0003\u0100\u0080\u0000"+
		"\u0580\u0581\u0005\u000b\u0000\u0000\u0581\u0582\u0003\u0112\u0089\u0000"+
		"\u0582\u0583\u0005\n\u0000\u0000\u0583\u0584\u0003\u011c\u008e\u0000\u0584"+
		"\u0585\u0005\u0002\u0000\u0000\u0585\u0587\u0003\u0100\u0080\u0000\u0586"+
		"\u0588\u0003\u0102\u0081\u0000\u0587\u0586\u0001\u0000\u0000\u0000\u0587"+
		"\u0588\u0001\u0000\u0000\u0000\u0588\u05c4\u0001\u0000\u0000\u0000\u0589"+
		"\u058f\u0005\u0007\u0000\u0000\u058a\u058b\u0003\u0112\u0089\u0000\u058b"+
		"\u058c\u0005|\u0000\u0000\u058c\u058d\u0003\u0112\u0089\u0000\u058d\u0590"+
		"\u0001\u0000\u0000\u0000\u058e\u0590\u0003n7\u0000\u058f\u058a\u0001\u0000"+
		"\u0000\u0000\u058f\u058e\u0001\u0000\u0000\u0000\u0590\u0591\u0001\u0000"+
		"\u0000\u0000\u0591\u0592\u0005\u0018\u0000\u0000\u0592\u0594\u0003\u0100"+
		"\u0080\u0000\u0593\u0595\u0003\u0102\u0081\u0000\u0594\u0593\u0001\u0000"+
		"\u0000\u0000\u0594\u0595\u0001\u0000\u0000\u0000\u0595\u05c4\u0001\u0000"+
		"\u0000\u0000\u0596\u059b\u0005\u0001\u0000\u0000\u0597\u0598\u0003\u0110"+
		"\u0088\u0000\u0598\u0599\u0005|\u0000\u0000\u0599\u059a\u0003\u0112\u0089"+
		"\u0000\u059a\u059c\u0001\u0000\u0000\u0000\u059b\u0597\u0001\u0000\u0000"+
		"\u0000\u059c\u059d\u0001\u0000\u0000\u0000\u059d\u059b\u0001\u0000\u0000"+
		"\u0000\u059d\u059e\u0001\u0000\u0000\u0000\u059e\u059f\u0001\u0000\u0000"+
		"\u0000\u059f\u05a0\u0005\u0002\u0000\u0000\u05a0\u05a2\u0003\u0100\u0080"+
		"\u0000\u05a1\u05a3\u0003\u0102\u0081\u0000\u05a2\u05a1\u0001\u0000\u0000"+
		"\u0000\u05a2\u05a3\u0001\u0000\u0000\u0000\u05a3\u05c4\u0001\u0000\u0000"+
		"\u0000\u05a4\u05c4\u0005\u007f\u0000\u0000\u05a5\u05a8\u0003\u0108\u0084"+
		"\u0000\u05a6\u05a7\u0005d\u0000\u0000\u05a7\u05a9\u0003\u0106\u0083\u0000"+
		"\u05a8\u05a6\u0001\u0000\u0000\u0000\u05a8\u05a9\u0001\u0000\u0000\u0000"+
		"\u05a9\u05c4\u0001\u0000\u0000\u0000\u05aa\u05ab\u0005k\u0000\u0000\u05ab"+
		"\u05ac\u0003\u0100\u0080\u0000\u05ac\u05af\u0005l\u0000\u0000\u05ad\u05ae"+
		"\u0005}\u0000\u0000\u05ae\u05b0\u0003\u0112\u0089\u0000\u05af\u05ad\u0001"+
		"\u0000\u0000\u0000\u05af\u05b0\u0001\u0000\u0000\u0000\u05b0\u05c4\u0001"+
		"\u0000\u0000\u0000\u05b1\u05c1\u0003\u0130\u0098\u0000\u05b2\u05be\u0005"+
		"k\u0000\u0000\u05b3\u05b8\u0003\u0016\u000b\u0000\u05b4\u05b5\u0005e\u0000"+
		"\u0000\u05b5\u05b7\u0003\u0016\u000b\u0000\u05b6\u05b4\u0001\u0000\u0000"+
		"\u0000\u05b7\u05ba\u0001\u0000\u0000\u0000\u05b8\u05b6\u0001\u0000\u0000"+
		"\u0000\u05b8\u05b9\u0001\u0000\u0000\u0000\u05b9\u05bc\u0001\u0000\u0000"+
		"\u0000\u05ba\u05b8\u0001\u0000\u0000\u0000\u05bb\u05bd\u0005e\u0000\u0000"+
		"\u05bc\u05bb\u0001\u0000\u0000\u0000\u05bc\u05bd\u0001\u0000\u0000\u0000"+
		"\u05bd\u05bf\u0001\u0000\u0000\u0000\u05be\u05b3\u0001\u0000\u0000\u0000"+
		"\u05be\u05bf\u0001\u0000\u0000\u0000\u05bf\u05c0\u0001\u0000\u0000\u0000"+
		"\u05c0\u05c2\u0005l\u0000\u0000\u05c1\u05b2\u0001\u0000\u0000\u0000\u05c1"+
		"\u05c2\u0001\u0000\u0000\u0000\u05c2\u05c4\u0001\u0000\u0000\u0000\u05c3"+
		"\u057e\u0001\u0000\u0000\u0000\u05c3\u0580\u0001\u0000\u0000\u0000\u05c3"+
		"\u0589\u0001\u0000\u0000\u0000\u05c3\u0596\u0001\u0000\u0000\u0000\u05c3"+
		"\u05a4\u0001\u0000\u0000\u0000\u05c3\u05a5\u0001\u0000\u0000\u0000\u05c3"+
		"\u05aa\u0001\u0000\u0000\u0000\u05c3\u05b1\u0001\u0000\u0000\u0000\u05c4"+
		"\u0107\u0001\u0000\u0000\u0000\u05c5\u05c6\u0005]\u0000\u0000\u05c6\u05ee"+
		"\u0003\u011c\u008e\u0000\u05c7\u05c8\u0005^\u0000\u0000\u05c8\u05c9\u0003"+
		"\u0016\u000b\u0000\u05c9\u05ca\u0005e\u0000\u0000\u05ca\u05cb\u0003\u0016"+
		"\u000b\u0000\u05cb\u05ee\u0001\u0000\u0000\u0000\u05cc\u05cd\u0003\u010a"+
		"\u0085\u0000\u05cd\u05d6\u0005k\u0000\u0000\u05ce\u05cf\u0003\u0016\u000b"+
		"\u0000\u05cf\u05d0\u0005l\u0000\u0000\u05d0\u05d7\u0001\u0000\u0000\u0000"+
		"\u05d1\u05d2\u0003\u0016\u000b\u0000\u05d2\u05d3\u0005e\u0000\u0000\u05d3"+
		"\u05d4\u0003\u0016\u000b\u0000\u05d4\u05d5\u0005l\u0000\u0000\u05d5\u05d7"+
		"\u0001\u0000\u0000\u0000\u05d6\u05ce\u0001\u0000\u0000\u0000\u05d6\u05d1"+
		"\u0001\u0000\u0000\u0000\u05d7\u05ee\u0001\u0000\u0000\u0000\u05d8\u05d9"+
		"\u0003\u010c\u0086\u0000\u05d9\u05e2\u0005k\u0000\u0000\u05da\u05db\u0003"+
		"\u0016\u000b\u0000\u05db\u05dc\u0005l\u0000\u0000\u05dc\u05e3\u0001\u0000"+
		"\u0000\u0000\u05dd\u05de\u0003\u0016\u000b\u0000\u05de\u05df\u0005e\u0000"+
		"\u0000\u05df\u05e0\u0003\u0016\u000b\u0000\u05e0\u05e1\u0005l\u0000\u0000"+
		"\u05e1\u05e3\u0001\u0000\u0000\u0000\u05e2\u05da\u0001\u0000\u0000\u0000"+
		"\u05e2\u05dd\u0001\u0000\u0000\u0000\u05e3\u05ee\u0001\u0000\u0000\u0000"+
		"\u05e4\u05e5\u0005_\u0000\u0000\u05e5\u05ee\u0003\u0016\u000b\u0000\u05e6"+
		"\u05e7\u0005`\u0000\u0000\u05e7\u05ee\u0003\u0016\u000b\u0000\u05e8\u05e9"+
		"\u0005a\u0000\u0000\u05e9\u05ee\u0003\u0016\u000b\u0000\u05ea\u05eb\u0005"+
		"b\u0000\u0000\u05eb\u05ee\u0003T*\u0000\u05ec\u05ee\u0003.\u0017\u0000"+
		"\u05ed\u05c5\u0001\u0000\u0000\u0000\u05ed\u05c7\u0001\u0000\u0000\u0000"+
		"\u05ed\u05cc\u0001\u0000\u0000\u0000\u05ed\u05d8\u0001\u0000\u0000\u0000"+
		"\u05ed\u05e4\u0001\u0000\u0000\u0000\u05ed\u05e6\u0001\u0000\u0000\u0000"+
		"\u05ed\u05e8\u0001\u0000\u0000\u0000\u05ed\u05ea\u0001\u0000\u0000\u0000"+
		"\u05ed\u05ec\u0001\u0000\u0000\u0000\u05ee\u0109\u0001\u0000\u0000\u0000"+
		"\u05ef\u05f0\u0005\u0002\u0000\u0000\u05f0\u010b\u0001\u0000\u0000\u0000"+
		"\u05f1\u05f2\u0005\u0019\u0000\u0000\u05f2\u010d\u0001\u0000\u0000\u0000"+
		"\u05f3\u05f4\u0005\u0001\u0000\u0000\u05f4\u0604\u0003\u0130\u0098\u0000"+
		"\u05f5\u0601\u0005k\u0000\u0000\u05f6\u05fb\u0003\u011c\u008e\u0000\u05f7"+
		"\u05f8\u0005e\u0000\u0000\u05f8\u05fa\u0003\u011c\u008e\u0000\u05f9\u05f7"+
		"\u0001\u0000\u0000\u0000\u05fa\u05fd\u0001\u0000\u0000\u0000\u05fb\u05f9"+
		"\u0001\u0000\u0000\u0000\u05fb\u05fc\u0001\u0000\u0000\u0000\u05fc\u05ff"+
		"\u0001\u0000\u0000\u0000\u05fd\u05fb\u0001\u0000\u0000\u0000\u05fe\u0600"+
		"\u0005e\u0000\u0000\u05ff\u05fe\u0001\u0000\u0000\u0000\u05ff\u0600\u0001"+
		"\u0000\u0000\u0000\u0600\u0602\u0001\u0000\u0000\u0000\u0601\u05f6\u0001"+
		"\u0000\u0000\u0000\u0601\u0602\u0001\u0000\u0000\u0000\u0602\u0603\u0001"+
		"\u0000\u0000\u0000\u0603\u0605\u0005l\u0000\u0000\u0604\u05f5\u0001\u0000"+
		"\u0000\u0000\u0604\u0605\u0001\u0000\u0000\u0000\u0605\u0606\u0001\u0000"+
		"\u0000\u0000\u0606\u0607\u0005|\u0000\u0000\u0607\u0608\u0003\u0100\u0080"+
		"\u0000\u0608\u010f\u0001\u0000\u0000\u0000\u0609\u060a\u0003\u0016\u000b"+
		"\u0000\u060a\u0111\u0001\u0000\u0000\u0000\u060b\u060c\u0003\u0016\u000b"+
		"\u0000\u060c\u0113\u0001\u0000\u0000\u0000\u060d\u0611\u0003\u0152\u00a9"+
		"\u0000\u060e\u0611\u0003\u0154\u00aa\u0000\u060f\u0611\u0003\u011a\u008d"+
		"\u0000\u0610\u060d\u0001\u0000\u0000\u0000\u0610\u060e\u0001\u0000\u0000"+
		"\u0000\u0610\u060f\u0001\u0000\u0000\u0000\u0611\u0115\u0001\u0000\u0000"+
		"\u0000\u0612\u0616\u0003\u0152\u00a9\u0000\u0613\u0616\u0003\u0154\u00aa"+
		"\u0000\u0614\u0616\u0003\u011c\u008e\u0000\u0615\u0612\u0001\u0000\u0000"+
		"\u0000\u0615\u0613\u0001\u0000\u0000\u0000\u0615\u0614\u0001\u0000\u0000"+
		"\u0000\u0616\u0117\u0001\u0000\u0000\u0000\u0617\u0618\u0003\u0150\u00a8"+
		"\u0000\u0618\u0119\u0001\u0000\u0000\u0000\u0619\u061b\u0003\u016a\u00b5"+
		"\u0000\u061a\u0619\u0001\u0000\u0000\u0000\u061a\u061b\u0001\u0000\u0000"+
		"\u0000\u061b\u061c\u0001\u0000\u0000\u0000\u061c\u061d\u0003\u011c\u008e"+
		"\u0000\u061d\u011b\u0001\u0000\u0000\u0000\u061e\u0621\u0003\u0142\u00a1"+
		"\u0000\u061f\u0620\u0005c\u0000\u0000\u0620\u0622\u0003\u011e\u008f\u0000"+
		"\u0621\u061f\u0001\u0000\u0000\u0000\u0621\u0622\u0001\u0000\u0000\u0000"+
		"\u0622\u011d\u0001\u0000\u0000\u0000\u0623\u0626\u0005\u0015\u0000\u0000"+
		"\u0624\u0626\u0003\u0130\u0098\u0000\u0625\u0623\u0001\u0000\u0000\u0000"+
		"\u0625\u0624\u0001\u0000\u0000\u0000\u0626\u011f\u0001\u0000\u0000\u0000"+
		"\u0627\u0628\u0005\u0013\u0000\u0000\u0628\u0629\u0003\u0130\u0098\u0000"+
		"\u0629\u062a\u0005c\u0000\u0000\u062a\u062b\u0005j\u0000\u0000\u062b\u062c"+
		"\u0003n7\u0000\u062c\u062d\u0005j\u0000\u0000\u062d\u0121\u0001\u0000"+
		"\u0000\u0000\u062e\u063a\u0005o\u0000\u0000\u062f\u0634\u0003\u0124\u0092"+
		"\u0000\u0630\u0631\u0005e\u0000\u0000\u0631\u0633\u0003\u0124\u0092\u0000"+
		"\u0632\u0630\u0001\u0000\u0000\u0000\u0633\u0636\u0001\u0000\u0000\u0000"+
		"\u0634\u0632\u0001\u0000\u0000\u0000\u0634\u0635\u0001\u0000\u0000\u0000"+
		"\u0635\u0638\u0001\u0000\u0000\u0000\u0636\u0634\u0001\u0000\u0000\u0000"+
		"\u0637\u0639\u0005e\u0000\u0000\u0638\u0637\u0001\u0000\u0000\u0000\u0638"+
		"\u0639\u0001\u0000\u0000\u0000\u0639\u063b\u0001\u0000\u0000\u0000\u063a"+
		"\u062f\u0001\u0000\u0000\u0000\u063a\u063b\u0001\u0000\u0000\u0000\u063b"+
		"\u063c\u0001\u0000\u0000\u0000\u063c\u063d\u0005p\u0000\u0000\u063d\u0123"+
		"\u0001\u0000\u0000\u0000\u063e\u063f\u0007\u0005\u0000\u0000\u063f\u0125"+
		"\u0001\u0000\u0000\u0000\u0640\u0641\u0005\u0014\u0000\u0000\u0641\u0642"+
		"\u0003\u0130\u0098\u0000\u0642\u0643\u0005c\u0000\u0000\u0643\u0644\u0005"+
		"j\u0000\u0000\u0644\u0645\u0003n7\u0000\u0645\u0646\u0005j\u0000\u0000"+
		"\u0646\u0127\u0001\u0000\u0000\u0000\u0647\u0649\u0005\u0001\u0000\u0000"+
		"\u0648\u064a\u0003\u012a\u0095\u0000\u0649\u0648\u0001\u0000\u0000\u0000"+
		"\u064a\u064b\u0001\u0000\u0000\u0000\u064b\u0649\u0001\u0000\u0000\u0000"+
		"\u064b\u064c\u0001\u0000\u0000\u0000\u064c\u064d\u0001\u0000\u0000\u0000"+
		"\u064d\u064e\u0005\u0002\u0000\u0000\u064e\u0129\u0001\u0000\u0000\u0000"+
		"\u064f\u0650\u0003\u013a\u009d\u0000\u0650\u0651\u0003\u016a\u00b5\u0000"+
		"\u0651\u0652\u0003\u0016\u000b\u0000\u0652\u012b\u0001\u0000\u0000\u0000"+
		"\u0653\u0654\u0003\u0130\u0098\u0000\u0654\u012d\u0001\u0000\u0000\u0000"+
		"\u0655\u065b\u0005\u00a0\u0000\u0000\u0656\u065b\u0005\u00a1\u0000\u0000"+
		"\u0657\u065b\u0005\u00a2\u0000\u0000\u0658\u065b\u0005\u00a3\u0000\u0000"+
		"\u0659\u065b\u0005\u00a4\u0000\u0000\u065a\u0655\u0001\u0000\u0000\u0000"+
		"\u065a\u0656\u0001\u0000\u0000\u0000\u065a\u0657\u0001\u0000\u0000\u0000"+
		"\u065a\u0658\u0001\u0000\u0000\u0000\u065a\u0659\u0001\u0000\u0000\u0000"+
		"\u065b\u012f\u0001\u0000\u0000\u0000\u065c\u0664\u0003\u0190\u00c8\u0000"+
		"\u065d\u0664\u0003\u0132\u0099\u0000\u065e\u0664\u0005\u00af\u0000\u0000"+
		"\u065f\u0664\u0005\u00b0\u0000\u0000\u0660\u0664\u0005\u00b1\u0000\u0000"+
		"\u0661\u0664\u0005\u00b2\u0000\u0000\u0662\u0664\u0005\u00b3\u0000\u0000"+
		"\u0663\u065c\u0001\u0000\u0000\u0000\u0663\u065d\u0001\u0000\u0000\u0000"+
		"\u0663\u065e\u0001\u0000\u0000\u0000\u0663\u065f\u0001\u0000\u0000\u0000"+
		"\u0663\u0660\u0001\u0000\u0000\u0000\u0663\u0661\u0001\u0000\u0000\u0000"+
		"\u0663\u0662\u0001\u0000\u0000\u0000\u0664\u0131\u0001\u0000\u0000\u0000"+
		"\u0665\u0666\u0007\u0006\u0000\u0000\u0666\u0133\u0001\u0000\u0000\u0000"+
		"\u0667\u066a\u0003\u0130\u0098\u0000\u0668\u0669\u0005f\u0000\u0000\u0669"+
		"\u066b\u0003\u0132\u0099\u0000\u066a\u0668\u0001\u0000\u0000\u0000\u066a"+
		"\u066b\u0001\u0000\u0000\u0000\u066b\u0135\u0001\u0000\u0000\u0000\u066c"+
		"\u066d\u0005j\u0000\u0000\u066d\u066e\u0003\u0160\u00b0\u0000\u066e\u066f"+
		"\u0005j\u0000\u0000\u066f\u0137\u0001\u0000\u0000\u0000\u0670\u0671\u0007"+
		"\u0007\u0000\u0000\u0671\u0139\u0001\u0000\u0000\u0000\u0672\u0675\u0003"+
		"\u0134\u009a\u0000\u0673\u0674\u0005c\u0000\u0000\u0674\u0676\u00059\u0000"+
		"\u0000\u0675\u0673\u0001\u0000\u0000\u0000\u0675\u0676\u0001\u0000\u0000"+
		"\u0000\u0676\u013b\u0001\u0000\u0000\u0000\u0677\u0678\u0003\u0134\u009a"+
		"\u0000\u0678\u0679\u0005c\u0000\u0000\u0679\u067a\u0005\u001a\u0000\u0000"+
		"\u067a\u067e\u0001\u0000\u0000\u0000\u067b\u067c\u0005\u008c\u0000\u0000"+
		"\u067c\u067e\u0003\u0134\u009a\u0000\u067d\u0677\u0001\u0000\u0000\u0000"+
		"\u067d\u067b\u0001\u0000\u0000\u0000\u067e\u013d\u0001\u0000\u0000\u0000"+
		"\u067f\u0680\u0003\u0134\u009a\u0000\u0680\u0681\u0005c\u0000\u0000\u0681"+
		"\u0682\u0005\u001b\u0000\u0000\u0682\u0686\u0001\u0000\u0000\u0000\u0683"+
		"\u0684\u0005\u008d\u0000\u0000\u0684\u0686\u0003\u0134\u009a\u0000\u0685"+
		"\u067f\u0001\u0000\u0000\u0000\u0685\u0683\u0001\u0000\u0000\u0000\u0686"+
		"\u013f\u0001\u0000\u0000\u0000\u0687\u0688\u0003\u0134\u009a\u0000\u0688"+
		"\u0689\u0005c\u0000\u0000\u0689\u068a\u0005:\u0000\u0000\u068a\u068e\u0001"+
		"\u0000\u0000\u0000\u068b\u068c\u0005\u0083\u0000\u0000\u068c\u068e\u0003"+
		"\u0134\u009a\u0000\u068d\u0687\u0001\u0000\u0000\u0000\u068d\u068b\u0001"+
		"\u0000\u0000\u0000\u068e\u0141\u0001\u0000\u0000\u0000\u068f\u0694\u0003"+
		"\u013a\u009d\u0000\u0690\u0694\u0003\u013c\u009e\u0000\u0691\u0694\u0003"+
		"\u013e\u009f\u0000\u0692\u0694\u0003\u0140\u00a0\u0000\u0693\u068f\u0001"+
		"\u0000\u0000\u0000\u0693\u0690\u0001\u0000\u0000\u0000\u0693\u0691\u0001"+
		"\u0000\u0000\u0000\u0693\u0692\u0001\u0000\u0000\u0000\u0694\u0143\u0001"+
		"\u0000\u0000\u0000\u0695\u0699\u0003\u013a\u009d\u0000\u0696\u0699\u0003"+
		"\u013c\u009e\u0000\u0697\u0699\u0003\u013e\u009f\u0000\u0698\u0695\u0001"+
		"\u0000\u0000\u0000\u0698\u0696\u0001\u0000\u0000\u0000\u0698\u0697\u0001"+
		"\u0000\u0000\u0000\u0699\u0145\u0001\u0000\u0000\u0000\u069a\u069e\u0003"+
		"\u0152\u00a9\u0000\u069b\u069e\u0003\u0154\u00aa\u0000\u069c\u069e\u0003"+
		"\u0144\u00a2\u0000\u069d\u069a\u0001\u0000\u0000\u0000\u069d\u069b\u0001"+
		"\u0000\u0000\u0000\u069d\u069c\u0001\u0000\u0000\u0000\u069e\u0147\u0001"+
		"\u0000\u0000\u0000\u069f\u06a3\u0003\u0152\u00a9\u0000\u06a0\u06a3\u0003"+
		"\u0154\u00aa\u0000\u06a1\u06a3\u0003\u0142\u00a1\u0000\u06a2\u069f\u0001"+
		"\u0000\u0000\u0000\u06a2\u06a0\u0001\u0000\u0000\u0000\u06a2\u06a1\u0001"+
		"\u0000\u0000\u0000\u06a3\u0149\u0001\u0000\u0000\u0000\u06a4\u06a7\u0003"+
		"\u0152\u00a9\u0000\u06a5\u06a7\u0003\u0144\u00a2\u0000\u06a6\u06a4\u0001"+
		"\u0000\u0000\u0000\u06a6\u06a5\u0001\u0000\u0000\u0000\u06a7\u014b\u0001"+
		"\u0000\u0000\u0000\u06a8\u06aa\u0003\u014e\u00a7\u0000\u06a9\u06a8\u0001"+
		"\u0000\u0000\u0000\u06aa\u06ab\u0001\u0000\u0000\u0000\u06ab\u06a9\u0001"+
		"\u0000\u0000\u0000\u06ab\u06ac\u0001\u0000\u0000\u0000\u06ac\u014d\u0001"+
		"\u0000\u0000\u0000\u06ad\u06b0\u0004\u00a7\u0000\u0001\u06ae\u06b1\u0003"+
		"\u0152\u00a9\u0000\u06af\u06b1\u0003\u0154\u00aa\u0000\u06b0\u06ae\u0001"+
		"\u0000\u0000\u0000\u06b0\u06af\u0001\u0000\u0000\u0000\u06b1\u06c7\u0001"+
		"\u0000\u0000\u0000\u06b2\u06b3\u0004\u00a7\u0001\u0001\u06b3\u06c7\u0003"+
		"\u0142\u00a1\u0000\u06b4\u06b5\u0004\u00a7\u0002\u0001\u06b5\u06c7\u0003"+
		"\u0144\u00a2\u0000\u06b6\u06b7\u0004\u00a7\u0003\u0001\u06b7\u06c7\u0003"+
		"\u0150\u00a8\u0000\u06b8\u06b9\u0004\u00a7\u0004\u0001\u06b9\u06c7\u0003"+
		"\u0146\u00a3\u0000\u06ba\u06bb\u0004\u00a7\u0005\u0001\u06bb\u06c7\u0003"+
		"\u014a\u00a5\u0000\u06bc\u06bd\u0004\u00a7\u0006\u0001\u06bd\u06c7\u0003"+
		"\u011c\u008e\u0000\u06be\u06bf\u0004\u00a7\u0007\u0001\u06bf\u06c7\u0003"+
		"\u0118\u008c\u0000\u06c0\u06c1\u0004\u00a7\b\u0001\u06c1\u06c7\u0003\u0116"+
		"\u008b\u0000\u06c2\u06c3\u0004\u00a7\t\u0001\u06c3\u06c7\u0003\u011a\u008d"+
		"\u0000\u06c4\u06c5\u0004\u00a7\n\u0001\u06c5\u06c7\u0003\u0114\u008a\u0000"+
		"\u06c6\u06ad\u0001\u0000\u0000\u0000\u06c6\u06b2\u0001\u0000\u0000\u0000"+
		"\u06c6\u06b4\u0001\u0000\u0000\u0000\u06c6\u06b6\u0001\u0000\u0000\u0000"+
		"\u06c6\u06b8\u0001\u0000\u0000\u0000\u06c6\u06ba\u0001\u0000\u0000\u0000"+
		"\u06c6\u06bc\u0001\u0000\u0000\u0000\u06c6\u06be\u0001\u0000\u0000\u0000"+
		"\u06c6\u06c0\u0001\u0000\u0000\u0000\u06c6\u06c2\u0001\u0000\u0000\u0000"+
		"\u06c6\u06c4\u0001\u0000\u0000\u0000\u06c7\u014f\u0001\u0000\u0000\u0000"+
		"\u06c8\u06c9\u0003\u0134\u009a\u0000\u06c9\u06ca\u0005c\u0000\u0000\u06ca"+
		"\u06cb\u0005:\u0000\u0000\u06cb\u06d1\u0001\u0000\u0000\u0000\u06cc\u06ce"+
		"\u0005\u0083\u0000\u0000\u06cd\u06cc\u0001\u0000\u0000\u0000\u06cd\u06ce"+
		"\u0001\u0000\u0000\u0000\u06ce\u06cf\u0001\u0000\u0000\u0000\u06cf\u06d1"+
		"\u0003\u0134\u009a\u0000\u06d0\u06c8\u0001\u0000\u0000\u0000\u06d0\u06cd"+
		"\u0001\u0000\u0000\u0000\u06d1\u0151\u0001\u0000\u0000\u0000\u06d2\u06d3"+
		"\u0005\u008c\u0000\u0000\u06d3\u06d4\u0005i\u0000\u0000\u06d4\u06d5\u0003"+
		"\u0130\u0098\u0000\u06d5\u06d6\u0005i\u0000\u0000\u06d6\u0153\u0001\u0000"+
		"\u0000\u0000\u06d7\u06d8\u0005i\u0000\u0000\u06d8\u06d9\u0003\u0130\u0098"+
		"\u0000\u06d9\u06da\u0005i\u0000\u0000\u06da\u0155\u0001\u0000\u0000\u0000"+
		"\u06db\u06dc\u0005\u00af\u0000\u0000\u06dc\u06dd\u0005\u00b8\u0000\u0000"+
		"\u06dd\u0157\u0001\u0000\u0000\u0000\u06de\u06e7\u0003\u0130\u0098\u0000"+
		"\u06df\u06e7\u0003\u0138\u009c\u0000\u06e0\u06e7\u0003\u018e\u00c7\u0000"+
		"\u06e1\u06e7\u0005h\u0000\u0000\u06e2\u06e7\u0005m\u0000\u0000\u06e3\u06e7"+
		"\u0005n\u0000\u0000\u06e4\u06e7\u0003\u018c\u00c6\u0000\u06e5\u06e7\u0005"+
		"\u00b9\u0000\u0000\u06e6\u06de\u0001\u0000\u0000\u0000\u06e6\u06df\u0001"+
		"\u0000\u0000\u0000\u06e6\u06e0\u0001\u0000\u0000\u0000\u06e6\u06e1\u0001"+
		"\u0000\u0000\u0000\u06e6\u06e2\u0001\u0000\u0000\u0000\u06e6\u06e3\u0001"+
		"\u0000\u0000\u0000\u06e6\u06e4\u0001\u0000\u0000\u0000\u06e6\u06e5\u0001"+
		"\u0000\u0000\u0000\u06e7\u06ea\u0001\u0000\u0000\u0000\u06e8\u06e6\u0001"+
		"\u0000\u0000\u0000\u06e8\u06e9\u0001\u0000\u0000\u0000\u06e9\u0159\u0001"+
		"\u0000\u0000\u0000\u06ea\u06e8\u0001\u0000\u0000\u0000\u06eb\u06f2\u0003"+
		"\u0130\u0098\u0000\u06ec\u06f2\u0003\u0138\u009c\u0000\u06ed\u06f2\u0003"+
		"\u018e\u00c7\u0000\u06ee\u06f2\u0005h\u0000\u0000\u06ef\u06f2\u0003\u018c"+
		"\u00c6\u0000\u06f0\u06f2\u0005\u00b9\u0000\u0000\u06f1\u06eb\u0001\u0000"+
		"\u0000\u0000\u06f1\u06ec\u0001\u0000\u0000\u0000\u06f1\u06ed\u0001\u0000"+
		"\u0000\u0000\u06f1\u06ee\u0001\u0000\u0000\u0000\u06f1\u06ef\u0001\u0000"+
		"\u0000\u0000\u06f1\u06f0\u0001\u0000\u0000\u0000\u06f2\u06f3\u0001\u0000"+
		"\u0000\u0000\u06f3\u06f1\u0001\u0000\u0000\u0000\u06f3\u06f4\u0001\u0000"+
		"\u0000\u0000\u06f4\u015b\u0001\u0000\u0000\u0000\u06f5\u06fe\u0003\u0130"+
		"\u0098\u0000\u06f6\u06fe\u0003\u0138\u009c\u0000\u06f7\u06fe\u0003\u018e"+
		"\u00c7\u0000\u06f8\u06fe\u0005h\u0000\u0000\u06f9\u06fe\u0005m\u0000\u0000"+
		"\u06fa\u06fe\u0005n\u0000\u0000\u06fb\u06fe\u0003\u018c\u00c6\u0000\u06fc"+
		"\u06fe\u0005\u00b9\u0000\u0000\u06fd\u06f5\u0001\u0000\u0000\u0000\u06fd"+
		"\u06f6\u0001\u0000\u0000\u0000\u06fd\u06f7\u0001\u0000\u0000\u0000\u06fd"+
		"\u06f8\u0001\u0000\u0000\u0000\u06fd\u06f9\u0001\u0000\u0000\u0000\u06fd"+
		"\u06fa\u0001\u0000\u0000\u0000\u06fd\u06fb\u0001\u0000\u0000\u0000\u06fd"+
		"\u06fc\u0001\u0000\u0000\u0000\u06fe\u06ff\u0001\u0000\u0000\u0000\u06ff"+
		"\u06fd\u0001\u0000\u0000\u0000\u06ff\u0700\u0001\u0000\u0000\u0000\u0700"+
		"\u015d\u0001\u0000\u0000\u0000\u0701\u0709\u0003\u0130\u0098\u0000\u0702"+
		"\u0709\u0003\u0138\u009c\u0000\u0703\u0709\u0003\u018e\u00c7\u0000\u0704"+
		"\u0709\u0005m\u0000\u0000\u0705\u0709\u0005n\u0000\u0000\u0706\u0709\u0003"+
		"\u018c\u00c6\u0000\u0707\u0709\u0005\u00b9\u0000\u0000\u0708\u0701\u0001"+
		"\u0000\u0000\u0000\u0708\u0702\u0001\u0000\u0000\u0000\u0708\u0703\u0001"+
		"\u0000\u0000\u0000\u0708\u0704\u0001\u0000\u0000\u0000\u0708\u0705\u0001"+
		"\u0000\u0000\u0000\u0708\u0706\u0001\u0000\u0000\u0000\u0708\u0707\u0001"+
		"\u0000\u0000\u0000\u0709\u070c\u0001\u0000\u0000\u0000\u070a\u0708\u0001"+
		"\u0000\u0000\u0000\u070a\u070b\u0001\u0000\u0000\u0000\u070b\u015f\u0001"+
		"\u0000\u0000\u0000\u070c\u070a\u0001\u0000\u0000\u0000\u070d\u0715\u0003"+
		"\u0130\u0098\u0000\u070e\u0715\u0005\u00af\u0000\u0000\u070f\u0715\u0005"+
		"\u00ae\u0000\u0000\u0710\u0715\u0005\u00b0\u0000\u0000\u0711\u0715\u0005"+
		"h\u0000\u0000\u0712\u0715\u0005g\u0000\u0000\u0713\u0715\u0005f\u0000"+
		"\u0000\u0714\u070d\u0001\u0000\u0000\u0000\u0714\u070e\u0001\u0000\u0000"+
		"\u0000\u0714\u070f\u0001\u0000\u0000\u0000\u0714\u0710\u0001\u0000\u0000"+
		"\u0000\u0714\u0711\u0001\u0000\u0000\u0000\u0714\u0712\u0001\u0000\u0000"+
		"\u0000\u0714\u0713\u0001\u0000\u0000\u0000\u0715\u0718\u0001\u0000\u0000"+
		"\u0000\u0716\u0714\u0001\u0000\u0000\u0000\u0716\u0717\u0001\u0000\u0000"+
		"\u0000\u0717\u0161\u0001\u0000\u0000\u0000\u0718\u0716\u0001\u0000\u0000"+
		"\u0000\u0719\u071a\u0007\b\u0000\u0000\u071a\u0163\u0001\u0000\u0000\u0000"+
		"\u071b\u071c\u0007\t\u0000\u0000\u071c\u0165\u0001\u0000\u0000\u0000\u071d"+
		"\u071e\u0005v\u0000\u0000\u071e\u0167\u0001\u0000\u0000\u0000\u071f\u0720"+
		"\u0005u\u0000\u0000\u0720\u0169\u0001\u0000\u0000\u0000\u0721\u0722\u0005"+
		"|\u0000\u0000\u0722\u016b\u0001\u0000\u0000\u0000\u0723\u0724\u0005\u0095"+
		"\u0000\u0000\u0724\u016d\u0001\u0000\u0000\u0000\u0725\u0726\u0005\u0093"+
		"\u0000\u0000\u0726\u016f\u0001\u0000\u0000\u0000\u0727\u0728\u0005\u0092"+
		"\u0000\u0000\u0728\u0171\u0001\u0000\u0000\u0000\u0729\u072a\u0005\u0094"+
		"\u0000\u0000\u072a\u0173\u0001\u0000\u0000\u0000\u072b\u072c\u0005\u0087"+
		"\u0000\u0000\u072c\u0175\u0001\u0000\u0000\u0000\u072d\u072e\u0005\u0088"+
		"\u0000\u0000\u072e\u0177\u0001\u0000\u0000\u0000\u072f\u0730\u0005\u001c"+
		"\u0000\u0000\u0730\u0179\u0001\u0000\u0000\u0000\u0731\u0732\u0005?\u0000"+
		"\u0000\u0732\u017b\u0001\u0000\u0000\u0000\u0733\u0734\u0005@\u0000\u0000"+
		"\u0734\u017d\u0001\u0000\u0000\u0000\u0735\u0736\u0005A\u0000\u0000\u0736"+
		"\u017f\u0001\u0000\u0000\u0000\u0737\u0738\u0005B\u0000\u0000\u0738\u0181"+
		"\u0001\u0000\u0000\u0000\u0739\u073a\u0005C\u0000\u0000\u073a\u0183\u0001"+
		"\u0000\u0000\u0000\u073b\u073c\u0005D\u0000\u0000\u073c\u0185\u0001\u0000"+
		"\u0000\u0000\u073d\u073e\u0005E\u0000\u0000\u073e\u0187\u0001\u0000\u0000"+
		"\u0000\u073f\u0740\u0005F\u0000\u0000\u0740\u0189\u0001\u0000\u0000\u0000"+
		"\u0741\u0742\u0005>\u0000\u0000\u0742\u018b\u0001\u0000\u0000\u0000\u0743"+
		"\u0744\u0007\n\u0000\u0000\u0744\u018d\u0001\u0000\u0000\u0000\u0745\u0746"+
		"\u0007\u000b\u0000\u0000\u0746\u018f\u0001\u0000\u0000\u0000\u0747\u0748"+
		"\u0007\f\u0000\u0000\u0748\u0191\u0001\u0000\u0000\u0000\u00b0\u019a\u01b6"+
		"\u01c3\u01ca\u01cd\u01d7\u01e0\u01e4\u01ef\u01f7\u01ff\u0203\u020a\u0212"+
		"\u021a\u0229\u0230\u023a\u0247\u0252\u0258\u025b\u0267\u026b\u0273\u0279"+
		"\u0282\u0286\u0288\u028e\u0298\u029c\u029e\u02ab\u02af\u02b8\u02bf\u02c3"+
		"\u02cb\u02d3\u02d8\u02dc\u02e1\u02e3\u02e9\u02f6\u02fa\u02fc\u0303\u030c"+
		"\u0315\u031a\u0327\u032b\u032d\u0330\u033a\u033e\u0340\u0357\u035b\u035d"+
		"\u0362\u0366\u0370\u0374\u037a\u0380\u0387\u0390\u0399\u039d\u03a3\u03a7"+
		"\u03af\u03c8\u03cc\u03e0\u03e8\u03ec\u03f5\u03fa\u0400\u0409\u040e\u0416"+
		"\u041b\u0423\u042b\u042f\u043a\u044f\u0454\u0461\u0471\u0475\u0482\u0486"+
		"\u0491\u0495\u04a2\u04a6\u04b0\u04b4\u04b6\u04b9\u04c5\u04c9\u04cb\u04d0"+
		"\u04d9\u04dd\u04ee\u0511\u0515\u0522\u0528\u0537\u053e\u0552\u0574\u0587"+
		"\u058f\u0594\u059d\u05a2\u05a8\u05af\u05b8\u05bc\u05be\u05c1\u05c3\u05d6"+
		"\u05e2\u05ed\u05fb\u05ff\u0601\u0604\u0610\u0615\u061a\u0621\u0625\u0634"+
		"\u0638\u063a\u064b\u065a\u0663\u066a\u0675\u067d\u0685\u068d\u0693\u0698"+
		"\u069d\u06a2\u06a6\u06ab\u06b0\u06c6\u06cd\u06d0\u06e6\u06e8\u06f1\u06f3"+
		"\u06fd\u06ff\u0708\u070a\u0714\u0716";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}