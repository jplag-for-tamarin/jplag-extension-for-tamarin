// Generated from /home/jastau/ownCloud - jstauffer@192.168.0.69/ETH/Bachelor Thesis/code-JPlag-Extension-Tamarin/languages/tamarin/src/main/antlr4/de/jplag/tamarin/grammar/TamarinParser.g4 by ANTLR 4.12.0
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link TamarinParser}.
 */
public interface TamarinParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link TamarinParser#tamarin_file}.
	 * @param ctx the parse tree
	 */
	void enterTamarin_file(TamarinParser.Tamarin_fileContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#tamarin_file}.
	 * @param ctx the parse tree
	 */
	void exitTamarin_file(TamarinParser.Tamarin_fileContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#theory}.
	 * @param ctx the parse tree
	 */
	void enterTheory(TamarinParser.TheoryContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#theory}.
	 * @param ctx the parse tree
	 */
	void exitTheory(TamarinParser.TheoryContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#body}.
	 * @param ctx the parse tree
	 */
	void enterBody(TamarinParser.BodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#body}.
	 * @param ctx the parse tree
	 */
	void exitBody(TamarinParser.BodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#define}.
	 * @param ctx the parse tree
	 */
	void enterDefine(TamarinParser.DefineContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#define}.
	 * @param ctx the parse tree
	 */
	void exitDefine(TamarinParser.DefineContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#include}.
	 * @param ctx the parse tree
	 */
	void enterInclude(TamarinParser.IncludeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#include}.
	 * @param ctx the parse tree
	 */
	void exitInclude(TamarinParser.IncludeContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#ifdef}.
	 * @param ctx the parse tree
	 */
	void enterIfdef(TamarinParser.IfdefContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#ifdef}.
	 * @param ctx the parse tree
	 */
	void exitIfdef(TamarinParser.IfdefContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#flagDisjuncts}.
	 * @param ctx the parse tree
	 */
	void enterFlagDisjuncts(TamarinParser.FlagDisjunctsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#flagDisjuncts}.
	 * @param ctx the parse tree
	 */
	void exitFlagDisjuncts(TamarinParser.FlagDisjunctsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#flagConjuncts}.
	 * @param ctx the parse tree
	 */
	void enterFlagConjuncts(TamarinParser.FlagConjunctsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#flagConjuncts}.
	 * @param ctx the parse tree
	 */
	void exitFlagConjuncts(TamarinParser.FlagConjunctsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#flagNegation}.
	 * @param ctx the parse tree
	 */
	void enterFlagNegation(TamarinParser.FlagNegationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#flagNegation}.
	 * @param ctx the parse tree
	 */
	void exitFlagNegation(TamarinParser.FlagNegationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#flagAtom}.
	 * @param ctx the parse tree
	 */
	void enterFlagAtom(TamarinParser.FlagAtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#flagAtom}.
	 * @param ctx the parse tree
	 */
	void exitFlagAtom(TamarinParser.FlagAtomContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#tupleTerm}.
	 * @param ctx the parse tree
	 */
	void enterTupleTerm(TamarinParser.TupleTermContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#tupleTerm}.
	 * @param ctx the parse tree
	 */
	void exitTupleTerm(TamarinParser.TupleTermContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#msetterm}.
	 * @param ctx the parse tree
	 */
	void enterMsetterm(TamarinParser.MsettermContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#msetterm}.
	 * @param ctx the parse tree
	 */
	void exitMsetterm(TamarinParser.MsettermContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#msettermList}.
	 * @param ctx the parse tree
	 */
	void enterMsettermList(TamarinParser.MsettermListContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#msettermList}.
	 * @param ctx the parse tree
	 */
	void exitMsettermList(TamarinParser.MsettermListContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#xorterm}.
	 * @param ctx the parse tree
	 */
	void enterXorterm(TamarinParser.XortermContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#xorterm}.
	 * @param ctx the parse tree
	 */
	void exitXorterm(TamarinParser.XortermContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#multterm}.
	 * @param ctx the parse tree
	 */
	void enterMultterm(TamarinParser.MulttermContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#multterm}.
	 * @param ctx the parse tree
	 */
	void exitMultterm(TamarinParser.MulttermContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#expterm}.
	 * @param ctx the parse tree
	 */
	void enterExpterm(TamarinParser.ExptermContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#expterm}.
	 * @param ctx the parse tree
	 */
	void exitExpterm(TamarinParser.ExptermContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#term}.
	 * @param ctx the parse tree
	 */
	void enterTerm(TamarinParser.TermContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#term}.
	 * @param ctx the parse tree
	 */
	void exitTerm(TamarinParser.TermContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#application}.
	 * @param ctx the parse tree
	 */
	void enterApplication(TamarinParser.ApplicationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#application}.
	 * @param ctx the parse tree
	 */
	void exitApplication(TamarinParser.ApplicationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#unaryOpApplication}.
	 * @param ctx the parse tree
	 */
	void enterUnaryOpApplication(TamarinParser.UnaryOpApplicationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#unaryOpApplication}.
	 * @param ctx the parse tree
	 */
	void exitUnaryOpApplication(TamarinParser.UnaryOpApplicationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#nonUnaryOpApplication}.
	 * @param ctx the parse tree
	 */
	void enterNonUnaryOpApplication(TamarinParser.NonUnaryOpApplicationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#nonUnaryOpApplication}.
	 * @param ctx the parse tree
	 */
	void exitNonUnaryOpApplication(TamarinParser.NonUnaryOpApplicationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#binaryAlgApplication}.
	 * @param ctx the parse tree
	 */
	void enterBinaryAlgApplication(TamarinParser.BinaryAlgApplicationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#binaryAlgApplication}.
	 * @param ctx the parse tree
	 */
	void exitBinaryAlgApplication(TamarinParser.BinaryAlgApplicationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#nullaryApplication}.
	 * @param ctx the parse tree
	 */
	void enterNullaryApplication(TamarinParser.NullaryApplicationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#nullaryApplication}.
	 * @param ctx the parse tree
	 */
	void exitNullaryApplication(TamarinParser.NullaryApplicationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#reservedBuiltins}.
	 * @param ctx the parse tree
	 */
	void enterReservedBuiltins(TamarinParser.ReservedBuiltinsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#reservedBuiltins}.
	 * @param ctx the parse tree
	 */
	void exitReservedBuiltins(TamarinParser.ReservedBuiltinsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#genericRule}.
	 * @param ctx the parse tree
	 */
	void enterGenericRule(TamarinParser.GenericRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#genericRule}.
	 * @param ctx the parse tree
	 */
	void exitGenericRule(TamarinParser.GenericRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#intruderRule}.
	 * @param ctx the parse tree
	 */
	void enterIntruderRule(TamarinParser.IntruderRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#intruderRule}.
	 * @param ctx the parse tree
	 */
	void exitIntruderRule(TamarinParser.IntruderRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#intruderInfo}.
	 * @param ctx the parse tree
	 */
	void enterIntruderInfo(TamarinParser.IntruderInfoContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#intruderInfo}.
	 * @param ctx the parse tree
	 */
	void exitIntruderInfo(TamarinParser.IntruderInfoContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#protoRuleAC}.
	 * @param ctx the parse tree
	 */
	void enterProtoRuleAC(TamarinParser.ProtoRuleACContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#protoRuleAC}.
	 * @param ctx the parse tree
	 */
	void exitProtoRuleAC(TamarinParser.ProtoRuleACContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#protoRuleACInfo}.
	 * @param ctx the parse tree
	 */
	void enterProtoRuleACInfo(TamarinParser.ProtoRuleACInfoContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#protoRuleACInfo}.
	 * @param ctx the parse tree
	 */
	void exitProtoRuleACInfo(TamarinParser.ProtoRuleACInfoContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#protoRule}.
	 * @param ctx the parse tree
	 */
	void enterProtoRule(TamarinParser.ProtoRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#protoRule}.
	 * @param ctx the parse tree
	 */
	void exitProtoRule(TamarinParser.ProtoRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#protoRuleInfo}.
	 * @param ctx the parse tree
	 */
	void enterProtoRuleInfo(TamarinParser.ProtoRuleInfoContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#protoRuleInfo}.
	 * @param ctx the parse tree
	 */
	void exitProtoRuleInfo(TamarinParser.ProtoRuleInfoContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#ruleAttributes}.
	 * @param ctx the parse tree
	 */
	void enterRuleAttributes(TamarinParser.RuleAttributesContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#ruleAttributes}.
	 * @param ctx the parse tree
	 */
	void exitRuleAttributes(TamarinParser.RuleAttributesContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#ruleAttribute}.
	 * @param ctx the parse tree
	 */
	void enterRuleAttribute(TamarinParser.RuleAttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#ruleAttribute}.
	 * @param ctx the parse tree
	 */
	void exitRuleAttribute(TamarinParser.RuleAttributeContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#embeddedRestriction}.
	 * @param ctx the parse tree
	 */
	void enterEmbeddedRestriction(TamarinParser.EmbeddedRestrictionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#embeddedRestriction}.
	 * @param ctx the parse tree
	 */
	void exitEmbeddedRestriction(TamarinParser.EmbeddedRestrictionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#factOrRestriction}.
	 * @param ctx the parse tree
	 */
	void enterFactOrRestriction(TamarinParser.FactOrRestrictionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#factOrRestriction}.
	 * @param ctx the parse tree
	 */
	void exitFactOrRestriction(TamarinParser.FactOrRestrictionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#factOrRestrictionList}.
	 * @param ctx the parse tree
	 */
	void enterFactOrRestrictionList(TamarinParser.FactOrRestrictionListContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#factOrRestrictionList}.
	 * @param ctx the parse tree
	 */
	void exitFactOrRestrictionList(TamarinParser.FactOrRestrictionListContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#reservedRuleNames}.
	 * @param ctx the parse tree
	 */
	void enterReservedRuleNames(TamarinParser.ReservedRuleNamesContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#reservedRuleNames}.
	 * @param ctx the parse tree
	 */
	void exitReservedRuleNames(TamarinParser.ReservedRuleNamesContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#ruleFresh}.
	 * @param ctx the parse tree
	 */
	void enterRuleFresh(TamarinParser.RuleFreshContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#ruleFresh}.
	 * @param ctx the parse tree
	 */
	void exitRuleFresh(TamarinParser.RuleFreshContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#rulePub}.
	 * @param ctx the parse tree
	 */
	void enterRulePub(TamarinParser.RulePubContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#rulePub}.
	 * @param ctx the parse tree
	 */
	void exitRulePub(TamarinParser.RulePubContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#hexColor}.
	 * @param ctx the parse tree
	 */
	void enterHexColor(TamarinParser.HexColorContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#hexColor}.
	 * @param ctx the parse tree
	 */
	void exitHexColor(TamarinParser.HexColorContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#moduloE}.
	 * @param ctx the parse tree
	 */
	void enterModuloE(TamarinParser.ModuloEContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#moduloE}.
	 * @param ctx the parse tree
	 */
	void exitModuloE(TamarinParser.ModuloEContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#moduloAC}.
	 * @param ctx the parse tree
	 */
	void enterModuloAC(TamarinParser.ModuloACContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#moduloAC}.
	 * @param ctx the parse tree
	 */
	void exitModuloAC(TamarinParser.ModuloACContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#factList}.
	 * @param ctx the parse tree
	 */
	void enterFactList(TamarinParser.FactListContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#factList}.
	 * @param ctx the parse tree
	 */
	void exitFactList(TamarinParser.FactListContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#fact}.
	 * @param ctx the parse tree
	 */
	void enterFact(TamarinParser.FactContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#fact}.
	 * @param ctx the parse tree
	 */
	void exitFact(TamarinParser.FactContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#freshFact}.
	 * @param ctx the parse tree
	 */
	void enterFreshFact(TamarinParser.FreshFactContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#freshFact}.
	 * @param ctx the parse tree
	 */
	void exitFreshFact(TamarinParser.FreshFactContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#reservedFact}.
	 * @param ctx the parse tree
	 */
	void enterReservedFact(TamarinParser.ReservedFactContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#reservedFact}.
	 * @param ctx the parse tree
	 */
	void exitReservedFact(TamarinParser.ReservedFactContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#protoFact}.
	 * @param ctx the parse tree
	 */
	void enterProtoFact(TamarinParser.ProtoFactContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#protoFact}.
	 * @param ctx the parse tree
	 */
	void exitProtoFact(TamarinParser.ProtoFactContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#multiplicity}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicity(TamarinParser.MultiplicityContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#multiplicity}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicity(TamarinParser.MultiplicityContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#factAnnotations}.
	 * @param ctx the parse tree
	 */
	void enterFactAnnotations(TamarinParser.FactAnnotationsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#factAnnotations}.
	 * @param ctx the parse tree
	 */
	void exitFactAnnotations(TamarinParser.FactAnnotationsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#factAnnotation}.
	 * @param ctx the parse tree
	 */
	void enterFactAnnotation(TamarinParser.FactAnnotationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#factAnnotation}.
	 * @param ctx the parse tree
	 */
	void exitFactAnnotation(TamarinParser.FactAnnotationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#lemmaAttributes}.
	 * @param ctx the parse tree
	 */
	void enterLemmaAttributes(TamarinParser.LemmaAttributesContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#lemmaAttributes}.
	 * @param ctx the parse tree
	 */
	void exitLemmaAttributes(TamarinParser.LemmaAttributesContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#lemmaAttribute}.
	 * @param ctx the parse tree
	 */
	void enterLemmaAttribute(TamarinParser.LemmaAttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#lemmaAttribute}.
	 * @param ctx the parse tree
	 */
	void exitLemmaAttribute(TamarinParser.LemmaAttributeContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#traceQuantifier}.
	 * @param ctx the parse tree
	 */
	void enterTraceQuantifier(TamarinParser.TraceQuantifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#traceQuantifier}.
	 * @param ctx the parse tree
	 */
	void exitTraceQuantifier(TamarinParser.TraceQuantifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#forallTraces}.
	 * @param ctx the parse tree
	 */
	void enterForallTraces(TamarinParser.ForallTracesContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#forallTraces}.
	 * @param ctx the parse tree
	 */
	void exitForallTraces(TamarinParser.ForallTracesContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#existsTrace}.
	 * @param ctx the parse tree
	 */
	void enterExistsTrace(TamarinParser.ExistsTraceContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#existsTrace}.
	 * @param ctx the parse tree
	 */
	void exitExistsTrace(TamarinParser.ExistsTraceContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#lemma}.
	 * @param ctx the parse tree
	 */
	void enterLemma(TamarinParser.LemmaContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#lemma}.
	 * @param ctx the parse tree
	 */
	void exitLemma(TamarinParser.LemmaContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#standardFormula}.
	 * @param ctx the parse tree
	 */
	void enterStandardFormula(TamarinParser.StandardFormulaContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#standardFormula}.
	 * @param ctx the parse tree
	 */
	void exitStandardFormula(TamarinParser.StandardFormulaContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#implication}.
	 * @param ctx the parse tree
	 */
	void enterImplication(TamarinParser.ImplicationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#implication}.
	 * @param ctx the parse tree
	 */
	void exitImplication(TamarinParser.ImplicationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#disjunction}.
	 * @param ctx the parse tree
	 */
	void enterDisjunction(TamarinParser.DisjunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#disjunction}.
	 * @param ctx the parse tree
	 */
	void exitDisjunction(TamarinParser.DisjunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#conjunction}.
	 * @param ctx the parse tree
	 */
	void enterConjunction(TamarinParser.ConjunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#conjunction}.
	 * @param ctx the parse tree
	 */
	void exitConjunction(TamarinParser.ConjunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#negation}.
	 * @param ctx the parse tree
	 */
	void enterNegation(TamarinParser.NegationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#negation}.
	 * @param ctx the parse tree
	 */
	void exitNegation(TamarinParser.NegationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#fatom}.
	 * @param ctx the parse tree
	 */
	void enterFatom(TamarinParser.FatomContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#fatom}.
	 * @param ctx the parse tree
	 */
	void exitFatom(TamarinParser.FatomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blatom_last}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 */
	void enterBlatom_last(TamarinParser.Blatom_lastContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blatom_last}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 */
	void exitBlatom_last(TamarinParser.Blatom_lastContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blatom_action}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 */
	void enterBlatom_action(TamarinParser.Blatom_actionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blatom_action}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 */
	void exitBlatom_action(TamarinParser.Blatom_actionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blatom_predicate}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 */
	void enterBlatom_predicate(TamarinParser.Blatom_predicateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blatom_predicate}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 */
	void exitBlatom_predicate(TamarinParser.Blatom_predicateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blatom_less}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 */
	void enterBlatom_less(TamarinParser.Blatom_lessContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blatom_less}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 */
	void exitBlatom_less(TamarinParser.Blatom_lessContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blatom_smallerp}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 */
	void enterBlatom_smallerp(TamarinParser.Blatom_smallerpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blatom_smallerp}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 */
	void exitBlatom_smallerp(TamarinParser.Blatom_smallerpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blatom_node_eq}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 */
	void enterBlatom_node_eq(TamarinParser.Blatom_node_eqContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blatom_node_eq}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 */
	void exitBlatom_node_eq(TamarinParser.Blatom_node_eqContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blatom_term_eq}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 */
	void enterBlatom_term_eq(TamarinParser.Blatom_term_eqContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blatom_term_eq}
	 * labeled alternative in {@link TamarinParser#blatom}.
	 * @param ctx the parse tree
	 */
	void exitBlatom_term_eq(TamarinParser.Blatom_term_eqContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#quantification}.
	 * @param ctx the parse tree
	 */
	void enterQuantification(TamarinParser.QuantificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#quantification}.
	 * @param ctx the parse tree
	 */
	void exitQuantification(TamarinParser.QuantificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#smallerp}.
	 * @param ctx the parse tree
	 */
	void enterSmallerp(TamarinParser.SmallerpContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#smallerp}.
	 * @param ctx the parse tree
	 */
	void exitSmallerp(TamarinParser.SmallerpContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#caseTest}.
	 * @param ctx the parse tree
	 */
	void enterCaseTest(TamarinParser.CaseTestContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#caseTest}.
	 * @param ctx the parse tree
	 */
	void exitCaseTest(TamarinParser.CaseTestContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#lemmaAcc}.
	 * @param ctx the parse tree
	 */
	void enterLemmaAcc(TamarinParser.LemmaAccContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#lemmaAcc}.
	 * @param ctx the parse tree
	 */
	void exitLemmaAcc(TamarinParser.LemmaAccContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#tactic}.
	 * @param ctx the parse tree
	 */
	void enterTactic(TamarinParser.TacticContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#tactic}.
	 * @param ctx the parse tree
	 */
	void exitTactic(TamarinParser.TacticContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#deprio}.
	 * @param ctx the parse tree
	 */
	void enterDeprio(TamarinParser.DeprioContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#deprio}.
	 * @param ctx the parse tree
	 */
	void exitDeprio(TamarinParser.DeprioContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#prio}.
	 * @param ctx the parse tree
	 */
	void enterPrio(TamarinParser.PrioContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#prio}.
	 * @param ctx the parse tree
	 */
	void exitPrio(TamarinParser.PrioContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#tacticDisjuncts}.
	 * @param ctx the parse tree
	 */
	void enterTacticDisjuncts(TamarinParser.TacticDisjunctsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#tacticDisjuncts}.
	 * @param ctx the parse tree
	 */
	void exitTacticDisjuncts(TamarinParser.TacticDisjunctsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#tacticConjuncts}.
	 * @param ctx the parse tree
	 */
	void enterTacticConjuncts(TamarinParser.TacticConjunctsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#tacticConjuncts}.
	 * @param ctx the parse tree
	 */
	void exitTacticConjuncts(TamarinParser.TacticConjunctsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#tacticNegation}.
	 * @param ctx the parse tree
	 */
	void enterTacticNegation(TamarinParser.TacticNegationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#tacticNegation}.
	 * @param ctx the parse tree
	 */
	void exitTacticNegation(TamarinParser.TacticNegationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#tacticFunction}.
	 * @param ctx the parse tree
	 */
	void enterTacticFunction(TamarinParser.TacticFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#tacticFunction}.
	 * @param ctx the parse tree
	 */
	void exitTacticFunction(TamarinParser.TacticFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#selectedPresort}.
	 * @param ctx the parse tree
	 */
	void enterSelectedPresort(TamarinParser.SelectedPresortContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#selectedPresort}.
	 * @param ctx the parse tree
	 */
	void exitSelectedPresort(TamarinParser.SelectedPresortContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#goalRankingPresort}.
	 * @param ctx the parse tree
	 */
	void enterGoalRankingPresort(TamarinParser.GoalRankingPresortContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#goalRankingPresort}.
	 * @param ctx the parse tree
	 */
	void exitGoalRankingPresort(TamarinParser.GoalRankingPresortContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#tacticFunctionName}.
	 * @param ctx the parse tree
	 */
	void enterTacticFunctionName(TamarinParser.TacticFunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#tacticFunctionName}.
	 * @param ctx the parse tree
	 */
	void exitTacticFunctionName(TamarinParser.TacticFunctionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#rankingIdentifier}.
	 * @param ctx the parse tree
	 */
	void enterRankingIdentifier(TamarinParser.RankingIdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#rankingIdentifier}.
	 * @param ctx the parse tree
	 */
	void exitRankingIdentifier(TamarinParser.RankingIdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#tacticName}.
	 * @param ctx the parse tree
	 */
	void enterTacticName(TamarinParser.TacticNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#tacticName}.
	 * @param ctx the parse tree
	 */
	void exitTacticName(TamarinParser.TacticNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#heuristic}.
	 * @param ctx the parse tree
	 */
	void enterHeuristic(TamarinParser.HeuristicContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#heuristic}.
	 * @param ctx the parse tree
	 */
	void exitHeuristic(TamarinParser.HeuristicContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#goalRanking}.
	 * @param ctx the parse tree
	 */
	void enterGoalRanking(TamarinParser.GoalRankingContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#goalRanking}.
	 * @param ctx the parse tree
	 */
	void exitGoalRanking(TamarinParser.GoalRankingContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#regularRanking}.
	 * @param ctx the parse tree
	 */
	void enterRegularRanking(TamarinParser.RegularRankingContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#regularRanking}.
	 * @param ctx the parse tree
	 */
	void exitRegularRanking(TamarinParser.RegularRankingContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#internalTacticRanking}.
	 * @param ctx the parse tree
	 */
	void enterInternalTacticRanking(TamarinParser.InternalTacticRankingContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#internalTacticRanking}.
	 * @param ctx the parse tree
	 */
	void exitInternalTacticRanking(TamarinParser.InternalTacticRankingContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#oracleRanking}.
	 * @param ctx the parse tree
	 */
	void enterOracleRanking(TamarinParser.OracleRankingContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#oracleRanking}.
	 * @param ctx the parse tree
	 */
	void exitOracleRanking(TamarinParser.OracleRankingContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#export}.
	 * @param ctx the parse tree
	 */
	void enterExport(TamarinParser.ExportContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#export}.
	 * @param ctx the parse tree
	 */
	void exitExport(TamarinParser.ExportContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#predicateDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterPredicateDeclaration(TamarinParser.PredicateDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#predicateDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitPredicateDeclaration(TamarinParser.PredicateDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterPredicate(TamarinParser.PredicateContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitPredicate(TamarinParser.PredicateContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#signatureOptions}.
	 * @param ctx the parse tree
	 */
	void enterSignatureOptions(TamarinParser.SignatureOptionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#signatureOptions}.
	 * @param ctx the parse tree
	 */
	void exitSignatureOptions(TamarinParser.SignatureOptionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinOptions}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinOptions(TamarinParser.BuiltinOptionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinOptions}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinOptions(TamarinParser.BuiltinOptionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#equations}.
	 * @param ctx the parse tree
	 */
	void enterEquations(TamarinParser.EquationsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#equations}.
	 * @param ctx the parse tree
	 */
	void exitEquations(TamarinParser.EquationsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#equation}.
	 * @param ctx the parse tree
	 */
	void enterEquation(TamarinParser.EquationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#equation}.
	 * @param ctx the parse tree
	 */
	void exitEquation(TamarinParser.EquationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#signatureFunctions}.
	 * @param ctx the parse tree
	 */
	void enterSignatureFunctions(TamarinParser.SignatureFunctionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#signatureFunctions}.
	 * @param ctx the parse tree
	 */
	void exitSignatureFunctions(TamarinParser.SignatureFunctionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#signatureFunction}.
	 * @param ctx the parse tree
	 */
	void enterSignatureFunction(TamarinParser.SignatureFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#signatureFunction}.
	 * @param ctx the parse tree
	 */
	void exitSignatureFunction(TamarinParser.SignatureFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#functionAttribute}.
	 * @param ctx the parse tree
	 */
	void enterFunctionAttribute(TamarinParser.FunctionAttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#functionAttribute}.
	 * @param ctx the parse tree
	 */
	void exitFunctionAttribute(TamarinParser.FunctionAttributeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code function_type_natural}
	 * labeled alternative in {@link TamarinParser#functionType}.
	 * @param ctx the parse tree
	 */
	void enterFunction_type_natural(TamarinParser.Function_type_naturalContext ctx);
	/**
	 * Exit a parse tree produced by the {@code function_type_natural}
	 * labeled alternative in {@link TamarinParser#functionType}.
	 * @param ctx the parse tree
	 */
	void exitFunction_type_natural(TamarinParser.Function_type_naturalContext ctx);
	/**
	 * Enter a parse tree produced by the {@code function_type_sapic}
	 * labeled alternative in {@link TamarinParser#functionType}.
	 * @param ctx the parse tree
	 */
	void enterFunction_type_sapic(TamarinParser.Function_type_sapicContext ctx);
	/**
	 * Exit a parse tree produced by the {@code function_type_sapic}
	 * labeled alternative in {@link TamarinParser#functionType}.
	 * @param ctx the parse tree
	 */
	void exitFunction_type_sapic(TamarinParser.Function_type_sapicContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtins}.
	 * @param ctx the parse tree
	 */
	void enterBuiltins(TamarinParser.BuiltinsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtins}.
	 * @param ctx the parse tree
	 */
	void exitBuiltins(TamarinParser.BuiltinsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNames}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNames(TamarinParser.BuiltinNamesContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNames}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNames(TamarinParser.BuiltinNamesContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameLocationsReport}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameLocationsReport(TamarinParser.BuiltinNameLocationsReportContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameLocationsReport}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameLocationsReport(TamarinParser.BuiltinNameLocationsReportContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameReliableChannel}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameReliableChannel(TamarinParser.BuiltinNameReliableChannelContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameReliableChannel}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameReliableChannel(TamarinParser.BuiltinNameReliableChannelContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameDH}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameDH(TamarinParser.BuiltinNameDHContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameDH}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameDH(TamarinParser.BuiltinNameDHContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameBilinearPairing}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameBilinearPairing(TamarinParser.BuiltinNameBilinearPairingContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameBilinearPairing}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameBilinearPairing(TamarinParser.BuiltinNameBilinearPairingContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameMultiset}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameMultiset(TamarinParser.BuiltinNameMultisetContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameMultiset}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameMultiset(TamarinParser.BuiltinNameMultisetContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameSymmetricEncryption}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameSymmetricEncryption(TamarinParser.BuiltinNameSymmetricEncryptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameSymmetricEncryption}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameSymmetricEncryption(TamarinParser.BuiltinNameSymmetricEncryptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameAsymmetricEncryption}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameAsymmetricEncryption(TamarinParser.BuiltinNameAsymmetricEncryptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameAsymmetricEncryption}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameAsymmetricEncryption(TamarinParser.BuiltinNameAsymmetricEncryptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameSigning}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameSigning(TamarinParser.BuiltinNameSigningContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameSigning}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameSigning(TamarinParser.BuiltinNameSigningContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameDestPairing}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameDestPairing(TamarinParser.BuiltinNameDestPairingContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameDestPairing}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameDestPairing(TamarinParser.BuiltinNameDestPairingContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameDestSymmetricEncryption}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameDestSymmetricEncryption(TamarinParser.BuiltinNameDestSymmetricEncryptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameDestSymmetricEncryption}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameDestSymmetricEncryption(TamarinParser.BuiltinNameDestSymmetricEncryptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameDestAsymmetricEncryption}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameDestAsymmetricEncryption(TamarinParser.BuiltinNameDestAsymmetricEncryptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameDestAsymmetricEncryption}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameDestAsymmetricEncryption(TamarinParser.BuiltinNameDestAsymmetricEncryptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameDestSigning}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameDestSigning(TamarinParser.BuiltinNameDestSigningContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameDestSigning}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameDestSigning(TamarinParser.BuiltinNameDestSigningContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameRevealingSigning}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameRevealingSigning(TamarinParser.BuiltinNameRevealingSigningContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameRevealingSigning}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameRevealingSigning(TamarinParser.BuiltinNameRevealingSigningContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameHashing}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameHashing(TamarinParser.BuiltinNameHashingContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameHashing}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameHashing(TamarinParser.BuiltinNameHashingContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinNameXor}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinNameXor(TamarinParser.BuiltinNameXorContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinNameXor}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinNameXor(TamarinParser.BuiltinNameXorContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#proofSkeleton}.
	 * @param ctx the parse tree
	 */
	void enterProofSkeleton(TamarinParser.ProofSkeletonContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#proofSkeleton}.
	 * @param ctx the parse tree
	 */
	void exitProofSkeleton(TamarinParser.ProofSkeletonContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#startProofSkeleton}.
	 * @param ctx the parse tree
	 */
	void enterStartProofSkeleton(TamarinParser.StartProofSkeletonContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#startProofSkeleton}.
	 * @param ctx the parse tree
	 */
	void exitStartProofSkeleton(TamarinParser.StartProofSkeletonContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#solvedProof}.
	 * @param ctx the parse tree
	 */
	void enterSolvedProof(TamarinParser.SolvedProofContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#solvedProof}.
	 * @param ctx the parse tree
	 */
	void exitSolvedProof(TamarinParser.SolvedProofContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#finalProof}.
	 * @param ctx the parse tree
	 */
	void enterFinalProof(TamarinParser.FinalProofContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#finalProof}.
	 * @param ctx the parse tree
	 */
	void exitFinalProof(TamarinParser.FinalProofContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#interProof}.
	 * @param ctx the parse tree
	 */
	void enterInterProof(TamarinParser.InterProofContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#interProof}.
	 * @param ctx the parse tree
	 */
	void exitInterProof(TamarinParser.InterProofContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#proofCase}.
	 * @param ctx the parse tree
	 */
	void enterProofCase(TamarinParser.ProofCaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#proofCase}.
	 * @param ctx the parse tree
	 */
	void exitProofCase(TamarinParser.ProofCaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#proofMethod}.
	 * @param ctx the parse tree
	 */
	void enterProofMethod(TamarinParser.ProofMethodContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#proofMethod}.
	 * @param ctx the parse tree
	 */
	void exitProofMethod(TamarinParser.ProofMethodContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#goal}.
	 * @param ctx the parse tree
	 */
	void enterGoal(TamarinParser.GoalContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#goal}.
	 * @param ctx the parse tree
	 */
	void exitGoal(TamarinParser.GoalContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#actionGoal}.
	 * @param ctx the parse tree
	 */
	void enterActionGoal(TamarinParser.ActionGoalContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#actionGoal}.
	 * @param ctx the parse tree
	 */
	void exitActionGoal(TamarinParser.ActionGoalContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#premiseGoal}.
	 * @param ctx the parse tree
	 */
	void enterPremiseGoal(TamarinParser.PremiseGoalContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#premiseGoal}.
	 * @param ctx the parse tree
	 */
	void exitPremiseGoal(TamarinParser.PremiseGoalContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#chainGoal}.
	 * @param ctx the parse tree
	 */
	void enterChainGoal(TamarinParser.ChainGoalContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#chainGoal}.
	 * @param ctx the parse tree
	 */
	void exitChainGoal(TamarinParser.ChainGoalContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#disjSplitGoal}.
	 * @param ctx the parse tree
	 */
	void enterDisjSplitGoal(TamarinParser.DisjSplitGoalContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#disjSplitGoal}.
	 * @param ctx the parse tree
	 */
	void exitDisjSplitGoal(TamarinParser.DisjSplitGoalContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#eqSplitGoal}.
	 * @param ctx the parse tree
	 */
	void enterEqSplitGoal(TamarinParser.EqSplitGoalContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#eqSplitGoal}.
	 * @param ctx the parse tree
	 */
	void exitEqSplitGoal(TamarinParser.EqSplitGoalContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#nodePremise}.
	 * @param ctx the parse tree
	 */
	void enterNodePremise(TamarinParser.NodePremiseContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#nodePremise}.
	 * @param ctx the parse tree
	 */
	void exitNodePremise(TamarinParser.NodePremiseContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#nodeConclusion}.
	 * @param ctx the parse tree
	 */
	void enterNodeConclusion(TamarinParser.NodeConclusionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#nodeConclusion}.
	 * @param ctx the parse tree
	 */
	void exitNodeConclusion(TamarinParser.NodeConclusionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#diffEquivLemma}.
	 * @param ctx the parse tree
	 */
	void enterDiffEquivLemma(TamarinParser.DiffEquivLemmaContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#diffEquivLemma}.
	 * @param ctx the parse tree
	 */
	void exitDiffEquivLemma(TamarinParser.DiffEquivLemmaContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#equivLemma}.
	 * @param ctx the parse tree
	 */
	void enterEquivLemma(TamarinParser.EquivLemmaContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#equivLemma}.
	 * @param ctx the parse tree
	 */
	void exitEquivLemma(TamarinParser.EquivLemmaContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#process}.
	 * @param ctx the parse tree
	 */
	void enterProcess(TamarinParser.ProcessContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#process}.
	 * @param ctx the parse tree
	 */
	void exitProcess(TamarinParser.ProcessContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#elseProcess}.
	 * @param ctx the parse tree
	 */
	void enterElseProcess(TamarinParser.ElseProcessContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#elseProcess}.
	 * @param ctx the parse tree
	 */
	void exitElseProcess(TamarinParser.ElseProcessContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#topLevelProcess}.
	 * @param ctx the parse tree
	 */
	void enterTopLevelProcess(TamarinParser.TopLevelProcessContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#topLevelProcess}.
	 * @param ctx the parse tree
	 */
	void exitTopLevelProcess(TamarinParser.TopLevelProcessContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#actionProcess}.
	 * @param ctx the parse tree
	 */
	void enterActionProcess(TamarinParser.ActionProcessContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#actionProcess}.
	 * @param ctx the parse tree
	 */
	void exitActionProcess(TamarinParser.ActionProcessContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#sapicAction}.
	 * @param ctx the parse tree
	 */
	void enterSapicAction(TamarinParser.SapicActionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#sapicAction}.
	 * @param ctx the parse tree
	 */
	void exitSapicAction(TamarinParser.SapicActionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#sapicActionIn}.
	 * @param ctx the parse tree
	 */
	void enterSapicActionIn(TamarinParser.SapicActionInContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#sapicActionIn}.
	 * @param ctx the parse tree
	 */
	void exitSapicActionIn(TamarinParser.SapicActionInContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#sapicActionOut}.
	 * @param ctx the parse tree
	 */
	void enterSapicActionOut(TamarinParser.SapicActionOutContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#sapicActionOut}.
	 * @param ctx the parse tree
	 */
	void exitSapicActionOut(TamarinParser.SapicActionOutContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#processDef}.
	 * @param ctx the parse tree
	 */
	void enterProcessDef(TamarinParser.ProcessDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#processDef}.
	 * @param ctx the parse tree
	 */
	void exitProcessDef(TamarinParser.ProcessDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#sapicPatternTerm}.
	 * @param ctx the parse tree
	 */
	void enterSapicPatternTerm(TamarinParser.SapicPatternTermContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#sapicPatternTerm}.
	 * @param ctx the parse tree
	 */
	void exitSapicPatternTerm(TamarinParser.SapicPatternTermContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#sapicTerm}.
	 * @param ctx the parse tree
	 */
	void enterSapicTerm(TamarinParser.SapicTermContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#sapicTerm}.
	 * @param ctx the parse tree
	 */
	void exitSapicTerm(TamarinParser.SapicTermContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#logicalTypedPatternLiteral}.
	 * @param ctx the parse tree
	 */
	void enterLogicalTypedPatternLiteral(TamarinParser.LogicalTypedPatternLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#logicalTypedPatternLiteral}.
	 * @param ctx the parse tree
	 */
	void exitLogicalTypedPatternLiteral(TamarinParser.LogicalTypedPatternLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#logicalTypedLiteral}.
	 * @param ctx the parse tree
	 */
	void enterLogicalTypedLiteral(TamarinParser.LogicalTypedLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#logicalTypedLiteral}.
	 * @param ctx the parse tree
	 */
	void exitLogicalTypedLiteral(TamarinParser.LogicalTypedLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#sapicNodeVar}.
	 * @param ctx the parse tree
	 */
	void enterSapicNodeVar(TamarinParser.SapicNodeVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#sapicNodeVar}.
	 * @param ctx the parse tree
	 */
	void exitSapicNodeVar(TamarinParser.SapicNodeVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#sapicPatternVar}.
	 * @param ctx the parse tree
	 */
	void enterSapicPatternVar(TamarinParser.SapicPatternVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#sapicPatternVar}.
	 * @param ctx the parse tree
	 */
	void exitSapicPatternVar(TamarinParser.SapicPatternVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#sapicVar}.
	 * @param ctx the parse tree
	 */
	void enterSapicVar(TamarinParser.SapicVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#sapicVar}.
	 * @param ctx the parse tree
	 */
	void exitSapicVar(TamarinParser.SapicVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#sapicType}.
	 * @param ctx the parse tree
	 */
	void enterSapicType(TamarinParser.SapicTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#sapicType}.
	 * @param ctx the parse tree
	 */
	void exitSapicType(TamarinParser.SapicTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#restriction}.
	 * @param ctx the parse tree
	 */
	void enterRestriction(TamarinParser.RestrictionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#restriction}.
	 * @param ctx the parse tree
	 */
	void exitRestriction(TamarinParser.RestrictionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#restrictionAttributes}.
	 * @param ctx the parse tree
	 */
	void enterRestrictionAttributes(TamarinParser.RestrictionAttributesContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#restrictionAttributes}.
	 * @param ctx the parse tree
	 */
	void exitRestrictionAttributes(TamarinParser.RestrictionAttributesContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#restrictionAttribute}.
	 * @param ctx the parse tree
	 */
	void enterRestrictionAttribute(TamarinParser.RestrictionAttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#restrictionAttribute}.
	 * @param ctx the parse tree
	 */
	void exitRestrictionAttribute(TamarinParser.RestrictionAttributeContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#legacyAxiom}.
	 * @param ctx the parse tree
	 */
	void enterLegacyAxiom(TamarinParser.LegacyAxiomContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#legacyAxiom}.
	 * @param ctx the parse tree
	 */
	void exitLegacyAxiom(TamarinParser.LegacyAxiomContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#letBlock}.
	 * @param ctx the parse tree
	 */
	void enterLetBlock(TamarinParser.LetBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#letBlock}.
	 * @param ctx the parse tree
	 */
	void exitLetBlock(TamarinParser.LetBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#letAssignment}.
	 * @param ctx the parse tree
	 */
	void enterLetAssignment(TamarinParser.LetAssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#letAssignment}.
	 * @param ctx the parse tree
	 */
	void exitLetAssignment(TamarinParser.LetAssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#protoFactIdentifier}.
	 * @param ctx the parse tree
	 */
	void enterProtoFactIdentifier(TamarinParser.ProtoFactIdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#protoFactIdentifier}.
	 * @param ctx the parse tree
	 */
	void exitProtoFactIdentifier(TamarinParser.ProtoFactIdentifierContext ctx);
	/**
	 * Enter a parse tree produced by the {@code reserved_fact_out}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 */
	void enterReserved_fact_out(TamarinParser.Reserved_fact_outContext ctx);
	/**
	 * Exit a parse tree produced by the {@code reserved_fact_out}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 */
	void exitReserved_fact_out(TamarinParser.Reserved_fact_outContext ctx);
	/**
	 * Enter a parse tree produced by the {@code reserved_fact_in}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 */
	void enterReserved_fact_in(TamarinParser.Reserved_fact_inContext ctx);
	/**
	 * Exit a parse tree produced by the {@code reserved_fact_in}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 */
	void exitReserved_fact_in(TamarinParser.Reserved_fact_inContext ctx);
	/**
	 * Enter a parse tree produced by the {@code reserved_fact_ku}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 */
	void enterReserved_fact_ku(TamarinParser.Reserved_fact_kuContext ctx);
	/**
	 * Exit a parse tree produced by the {@code reserved_fact_ku}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 */
	void exitReserved_fact_ku(TamarinParser.Reserved_fact_kuContext ctx);
	/**
	 * Enter a parse tree produced by the {@code reserved_fact_kd}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 */
	void enterReserved_fact_kd(TamarinParser.Reserved_fact_kdContext ctx);
	/**
	 * Exit a parse tree produced by the {@code reserved_fact_kd}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 */
	void exitReserved_fact_kd(TamarinParser.Reserved_fact_kdContext ctx);
	/**
	 * Enter a parse tree produced by the {@code reserved_fact_ded}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 */
	void enterReserved_fact_ded(TamarinParser.Reserved_fact_dedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code reserved_fact_ded}
	 * labeled alternative in {@link TamarinParser#reservedFactIdentifier}.
	 * @param ctx the parse tree
	 */
	void exitReserved_fact_ded(TamarinParser.Reserved_fact_dedContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(TamarinParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(TamarinParser.IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#natural}.
	 * @param ctx the parse tree
	 */
	void enterNatural(TamarinParser.NaturalContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#natural}.
	 * @param ctx the parse tree
	 */
	void exitNatural(TamarinParser.NaturalContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#indexedIdentifier}.
	 * @param ctx the parse tree
	 */
	void enterIndexedIdentifier(TamarinParser.IndexedIdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#indexedIdentifier}.
	 * @param ctx the parse tree
	 */
	void exitIndexedIdentifier(TamarinParser.IndexedIdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#filePath}.
	 * @param ctx the parse tree
	 */
	void enterFilePath(TamarinParser.FilePathContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#filePath}.
	 * @param ctx the parse tree
	 */
	void exitFilePath(TamarinParser.FilePathContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#keywords}.
	 * @param ctx the parse tree
	 */
	void enterKeywords(TamarinParser.KeywordsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#keywords}.
	 * @param ctx the parse tree
	 */
	void exitKeywords(TamarinParser.KeywordsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#logMsgVar}.
	 * @param ctx the parse tree
	 */
	void enterLogMsgVar(TamarinParser.LogMsgVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#logMsgVar}.
	 * @param ctx the parse tree
	 */
	void exitLogMsgVar(TamarinParser.LogMsgVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#logFreshVar}.
	 * @param ctx the parse tree
	 */
	void enterLogFreshVar(TamarinParser.LogFreshVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#logFreshVar}.
	 * @param ctx the parse tree
	 */
	void exitLogFreshVar(TamarinParser.LogFreshVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#logPubVar}.
	 * @param ctx the parse tree
	 */
	void enterLogPubVar(TamarinParser.LogPubVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#logPubVar}.
	 * @param ctx the parse tree
	 */
	void exitLogPubVar(TamarinParser.LogPubVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#logNodeVar}.
	 * @param ctx the parse tree
	 */
	void enterLogNodeVar(TamarinParser.LogNodeVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#logNodeVar}.
	 * @param ctx the parse tree
	 */
	void exitLogNodeVar(TamarinParser.LogNodeVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#logVar}.
	 * @param ctx the parse tree
	 */
	void enterLogVar(TamarinParser.LogVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#logVar}.
	 * @param ctx the parse tree
	 */
	void exitLogVar(TamarinParser.LogVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#msgVar}.
	 * @param ctx the parse tree
	 */
	void enterMsgVar(TamarinParser.MsgVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#msgVar}.
	 * @param ctx the parse tree
	 */
	void exitMsgVar(TamarinParser.MsgVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#logicalLiteral}.
	 * @param ctx the parse tree
	 */
	void enterLogicalLiteral(TamarinParser.LogicalLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#logicalLiteral}.
	 * @param ctx the parse tree
	 */
	void exitLogicalLiteral(TamarinParser.LogicalLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#logicalLiteralWithNode}.
	 * @param ctx the parse tree
	 */
	void enterLogicalLiteralWithNode(TamarinParser.LogicalLiteralWithNodeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#logicalLiteralWithNode}.
	 * @param ctx the parse tree
	 */
	void exitLogicalLiteralWithNode(TamarinParser.LogicalLiteralWithNodeContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#logicalLiteralNoPub}.
	 * @param ctx the parse tree
	 */
	void enterLogicalLiteralNoPub(TamarinParser.LogicalLiteralNoPubContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#logicalLiteralNoPub}.
	 * @param ctx the parse tree
	 */
	void exitLogicalLiteralNoPub(TamarinParser.LogicalLiteralNoPubContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#literals}.
	 * @param ctx the parse tree
	 */
	void enterLiterals(TamarinParser.LiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#literals}.
	 * @param ctx the parse tree
	 */
	void exitLiterals(TamarinParser.LiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(TamarinParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(TamarinParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#nodeVar}.
	 * @param ctx the parse tree
	 */
	void enterNodeVar(TamarinParser.NodeVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#nodeVar}.
	 * @param ctx the parse tree
	 */
	void exitNodeVar(TamarinParser.NodeVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#freshName}.
	 * @param ctx the parse tree
	 */
	void enterFreshName(TamarinParser.FreshNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#freshName}.
	 * @param ctx the parse tree
	 */
	void exitFreshName(TamarinParser.FreshNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#pubName}.
	 * @param ctx the parse tree
	 */
	void enterPubName(TamarinParser.PubNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#pubName}.
	 * @param ctx the parse tree
	 */
	void exitPubName(TamarinParser.PubNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#formalComment}.
	 * @param ctx the parse tree
	 */
	void enterFormalComment(TamarinParser.FormalCommentContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#formalComment}.
	 * @param ctx the parse tree
	 */
	void exitFormalComment(TamarinParser.FormalCommentContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#tacticFunctionIdentifier}.
	 * @param ctx the parse tree
	 */
	void enterTacticFunctionIdentifier(TamarinParser.TacticFunctionIdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#tacticFunctionIdentifier}.
	 * @param ctx the parse tree
	 */
	void exitTacticFunctionIdentifier(TamarinParser.TacticFunctionIdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#internalTacticName}.
	 * @param ctx the parse tree
	 */
	void enterInternalTacticName(TamarinParser.InternalTacticNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#internalTacticName}.
	 * @param ctx the parse tree
	 */
	void exitInternalTacticName(TamarinParser.InternalTacticNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#oracleRelativePath}.
	 * @param ctx the parse tree
	 */
	void enterOracleRelativePath(TamarinParser.OracleRelativePathContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#oracleRelativePath}.
	 * @param ctx the parse tree
	 */
	void exitOracleRelativePath(TamarinParser.OracleRelativePathContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#exportBodyChars}.
	 * @param ctx the parse tree
	 */
	void enterExportBodyChars(TamarinParser.ExportBodyCharsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#exportBodyChars}.
	 * @param ctx the parse tree
	 */
	void exitExportBodyChars(TamarinParser.ExportBodyCharsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#charDir}.
	 * @param ctx the parse tree
	 */
	void enterCharDir(TamarinParser.CharDirContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#charDir}.
	 * @param ctx the parse tree
	 */
	void exitCharDir(TamarinParser.CharDirContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#equivOp}.
	 * @param ctx the parse tree
	 */
	void enterEquivOp(TamarinParser.EquivOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#equivOp}.
	 * @param ctx the parse tree
	 */
	void exitEquivOp(TamarinParser.EquivOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#orOp}.
	 * @param ctx the parse tree
	 */
	void enterOrOp(TamarinParser.OrOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#orOp}.
	 * @param ctx the parse tree
	 */
	void exitOrOp(TamarinParser.OrOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#notOp}.
	 * @param ctx the parse tree
	 */
	void enterNotOp(TamarinParser.NotOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#notOp}.
	 * @param ctx the parse tree
	 */
	void exitNotOp(TamarinParser.NotOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#andOp}.
	 * @param ctx the parse tree
	 */
	void enterAndOp(TamarinParser.AndOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#andOp}.
	 * @param ctx the parse tree
	 */
	void exitAndOp(TamarinParser.AndOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#eqOp}.
	 * @param ctx the parse tree
	 */
	void enterEqOp(TamarinParser.EqOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#eqOp}.
	 * @param ctx the parse tree
	 */
	void exitEqOp(TamarinParser.EqOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#botOp}.
	 * @param ctx the parse tree
	 */
	void enterBotOp(TamarinParser.BotOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#botOp}.
	 * @param ctx the parse tree
	 */
	void exitBotOp(TamarinParser.BotOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#fOp}.
	 * @param ctx the parse tree
	 */
	void enterFOp(TamarinParser.FOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#fOp}.
	 * @param ctx the parse tree
	 */
	void exitFOp(TamarinParser.FOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#tOp}.
	 * @param ctx the parse tree
	 */
	void enterTOp(TamarinParser.TOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#tOp}.
	 * @param ctx the parse tree
	 */
	void exitTOp(TamarinParser.TOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#topOp}.
	 * @param ctx the parse tree
	 */
	void enterTopOp(TamarinParser.TopOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#topOp}.
	 * @param ctx the parse tree
	 */
	void exitTopOp(TamarinParser.TopOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#forallOp}.
	 * @param ctx the parse tree
	 */
	void enterForallOp(TamarinParser.ForallOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#forallOp}.
	 * @param ctx the parse tree
	 */
	void exitForallOp(TamarinParser.ForallOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#existsOp}.
	 * @param ctx the parse tree
	 */
	void enterExistsOp(TamarinParser.ExistsOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#existsOp}.
	 * @param ctx the parse tree
	 */
	void exitExistsOp(TamarinParser.ExistsOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinXor}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinXor(TamarinParser.BuiltinXorContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinXor}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinXor(TamarinParser.BuiltinXorContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinMun}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinMun(TamarinParser.BuiltinMunContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinMun}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinMun(TamarinParser.BuiltinMunContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinOne}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinOne(TamarinParser.BuiltinOneContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinOne}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinOne(TamarinParser.BuiltinOneContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinExp}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinExp(TamarinParser.BuiltinExpContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinExp}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinExp(TamarinParser.BuiltinExpContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinMult}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinMult(TamarinParser.BuiltinMultContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinMult}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinMult(TamarinParser.BuiltinMultContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinInv}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinInv(TamarinParser.BuiltinInvContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinInv}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinInv(TamarinParser.BuiltinInvContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinPmult}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinPmult(TamarinParser.BuiltinPmultContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinPmult}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinPmult(TamarinParser.BuiltinPmultContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinEm}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinEm(TamarinParser.BuiltinEmContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinEm}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinEm(TamarinParser.BuiltinEmContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#builtinZero}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinZero(TamarinParser.BuiltinZeroContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#builtinZero}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinZero(TamarinParser.BuiltinZeroContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#dhNeutral}.
	 * @param ctx the parse tree
	 */
	void enterDhNeutral(TamarinParser.DhNeutralContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#dhNeutral}.
	 * @param ctx the parse tree
	 */
	void exitDhNeutral(TamarinParser.DhNeutralContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#operators}.
	 * @param ctx the parse tree
	 */
	void enterOperators(TamarinParser.OperatorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#operators}.
	 * @param ctx the parse tree
	 */
	void exitOperators(TamarinParser.OperatorsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#punctuation}.
	 * @param ctx the parse tree
	 */
	void enterPunctuation(TamarinParser.PunctuationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#punctuation}.
	 * @param ctx the parse tree
	 */
	void exitPunctuation(TamarinParser.PunctuationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TamarinParser#allowedIdentifierKeywords}.
	 * @param ctx the parse tree
	 */
	void enterAllowedIdentifierKeywords(TamarinParser.AllowedIdentifierKeywordsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TamarinParser#allowedIdentifierKeywords}.
	 * @param ctx the parse tree
	 */
	void exitAllowedIdentifierKeywords(TamarinParser.AllowedIdentifierKeywordsContext ctx);
}