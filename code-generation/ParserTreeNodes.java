package de.jplag.tamarin;

abstract class ParseTreeNode implements Comparable<ParseTreeNode>{
    ArrayList<ParseTreeNode> Children;
    ParseTreeNode parent;
    ArrayList<Token> nodeTokens;
    int priority;
    ParseTreeNode() {
        Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
    }

    abstract void sort();
    List<Token> getTokens() {
        List<Token> tokens = new ArrayList<Token>();
        int childIndex = 0;
        for(int i = 0; i < nodeTokens.size(); i++) {
            if(nodeTokens.get(i).getType() == CHILD) {
                tokens.addAll(Children.get(childIndex).getTokens());
                childIndex++;
            }
            else {
                tokens.add(nodeTokens.get(i));
            }
        }
        return tokens;
    }
    void setParent(ParseTreeNode parent) {
        this.parent = parent;
    }
    void addChild(ParseTreeNode child) {
        Children.add(child);
    }
    void addToken(Token token)  {
        nodeTokens.add(token);
    }
    public int compareTo(ParseTreeNode other) {
        if (this.priority < other.priority) {
            return 1;
        }
        else if(this.priority > other.priority) {
            return -1;
        }
        else {
            if(this.Children.size() == 0 && other.Children.size() == 0) {
                return 0;
            }
            if(this.Children.size() < other.Children.size()) {
                return 1;
            }
            else if(this.Children.size() > other.Children.size()) {
                return -1;
            }
            else {
                this.sort();
                other.sort();
                for(int i = 0; i < this.Children.size(); i++) {
                    int comp = this.Children.get(0).compareTo(other.Children.get(0));
                    if(comp != 0) {
                        return comp;
                    }
                }
                return 0;
            }
        }
    }
}

class InitialNode extends ParseTreeNode {

    InitialNode() {
        Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
        priority = -1;
    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

    @Override
    void addToken(Token token)  {
        if(token.getType() == CHILD) {
            nodeTokens.add(token);
        }
        else {
            System.err.println("Trying to add token " + token.getType().getDescription() + " to initial node!");
        }
    }

}

class TheoryNode extends ParseTreeNode {
	TheoryNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 0;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BodyNode extends ParseTreeNode {
	BodyNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 1;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class DefineNode extends ParseTreeNode {
	DefineNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 2;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class IncludeNode extends ParseTreeNode {
	IncludeNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 3;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class IfdefNode extends ParseTreeNode {
	IfdefNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 4;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FlagDisjunctsNode extends ParseTreeNode {
	FlagDisjunctsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 5;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FlagConjunctsNode extends ParseTreeNode {
	FlagConjunctsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 6;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FlagNegationNode extends ParseTreeNode {
	FlagNegationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 7;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FlagAtomNode extends ParseTreeNode {
	FlagAtomNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 8;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class TupleTermNode extends ParseTreeNode {
	TupleTermNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 9;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class MsettermNode extends ParseTreeNode {
	MsettermNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 10;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class XortermNode extends ParseTreeNode {
	XortermNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 11;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class MulttermNode extends ParseTreeNode {
	MulttermNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 12;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ExptermNode extends ParseTreeNode {
	ExptermNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 13;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class TermNode extends ParseTreeNode {
	TermNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 14;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Term_tupletermNode extends ParseTreeNode {
	Term_tupletermNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 15;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Term_msettermNode extends ParseTreeNode {
	Term_msettermNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 16;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Term_oneopNode extends ParseTreeNode {
	Term_oneopNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 17;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Term_dhneutralNode extends ParseTreeNode {
	Term_dhneutralNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 18;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Term_applicationNode extends ParseTreeNode {
	Term_applicationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 19;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Term_nullaryNode extends ParseTreeNode {
	Term_nullaryNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 20;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Term_literalNode extends ParseTreeNode {
	Term_literalNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 21;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ApplicationNode extends ParseTreeNode {
	ApplicationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 22;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class NaryOpApplicationNode extends ParseTreeNode {
	NaryOpApplicationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 23;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class UnaryOpApplicationNode extends ParseTreeNode {
	UnaryOpApplicationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 24;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class NonUnaryOpApplicationNode extends ParseTreeNode {
	NonUnaryOpApplicationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 25;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BinaryAlgApplicationNode extends ParseTreeNode {
	BinaryAlgApplicationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 26;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class NullaryApplicationNode extends ParseTreeNode {
	NullaryApplicationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 27;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class NullaryApp_OneNode extends ParseTreeNode {
	NullaryApp_OneNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 28;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class NullaryApp_DHNode extends ParseTreeNode {
	NullaryApp_DHNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 29;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class NullaryApp_ZeroNode extends ParseTreeNode {
	NullaryApp_ZeroNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 30;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ReservedBuiltinsNode extends ParseTreeNode {
	ReservedBuiltinsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 31;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class GenericRuleNode extends ParseTreeNode {
	GenericRuleNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 32;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class IntruderRuleNode extends ParseTreeNode {
	IntruderRuleNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 33;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class IntruderInfoNode extends ParseTreeNode {
	IntruderInfoNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 34;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ProtoRuleACNode extends ParseTreeNode {
	ProtoRuleACNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 35;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ProtoRuleACInfoNode extends ParseTreeNode {
	ProtoRuleACInfoNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 36;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ProtoRuleNode extends ParseTreeNode {
	ProtoRuleNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 37;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ProtoRuleInfoNode extends ParseTreeNode {
	ProtoRuleInfoNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 38;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class RuleAttributesNode extends ParseTreeNode {
	RuleAttributesNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 39;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class RuleAttributeNode extends ParseTreeNode {
	RuleAttributeNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 40;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class EmbeddedRestrictionNode extends ParseTreeNode {
	EmbeddedRestrictionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 41;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FactOrRestrictionNode extends ParseTreeNode {
	FactOrRestrictionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 42;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ReservedRuleNamesNode extends ParseTreeNode {
	ReservedRuleNamesNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 43;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class RuleFreshNode extends ParseTreeNode {
	RuleFreshNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 44;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class RulePubNode extends ParseTreeNode {
	RulePubNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 45;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class HexColorNode extends ParseTreeNode {
	HexColorNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 46;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ModuloENode extends ParseTreeNode {
	ModuloENode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 47;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ModuloACNode extends ParseTreeNode {
	ModuloACNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 48;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FactListNode extends ParseTreeNode {
	FactListNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 49;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FactNode extends ParseTreeNode {
	FactNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 50;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FreshFactNode extends ParseTreeNode {
	FreshFactNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 51;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ReservedFactNode extends ParseTreeNode {
	ReservedFactNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 52;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ProtoFactNode extends ParseTreeNode {
	ProtoFactNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 53;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class MultiplicityNode extends ParseTreeNode {
	MultiplicityNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 54;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FactAnnotationsNode extends ParseTreeNode {
	FactAnnotationsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 55;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FactAnnotationNode extends ParseTreeNode {
	FactAnnotationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 56;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Fact_annotation_plusNode extends ParseTreeNode {
	Fact_annotation_plusNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 57;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Fact_annotation_minusNode extends ParseTreeNode {
	Fact_annotation_minusNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 58;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Fact_annotation_noprecompNode extends ParseTreeNode {
	Fact_annotation_noprecompNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 59;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LemmaAttributesNode extends ParseTreeNode {
	LemmaAttributesNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 60;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LemmaAttributeNode extends ParseTreeNode {
	LemmaAttributeNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 61;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Lemma_attribute_typingNode extends ParseTreeNode {
	Lemma_attribute_typingNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 62;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Lemma_attribute_sourcesNode extends ParseTreeNode {
	Lemma_attribute_sourcesNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 63;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Lemma_attribute_reuseNode extends ParseTreeNode {
	Lemma_attribute_reuseNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 64;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Lemma_attribute_diff_reuseNode extends ParseTreeNode {
	Lemma_attribute_diff_reuseNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 65;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Lemma_attribute_inductionNode extends ParseTreeNode {
	Lemma_attribute_inductionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 66;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Lemma_attribute_hide_lemmaNode extends ParseTreeNode {
	Lemma_attribute_hide_lemmaNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 67;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Lemma_attribute_heuristicNode extends ParseTreeNode {
	Lemma_attribute_heuristicNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 68;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Lemma_attribute_outputNode extends ParseTreeNode {
	Lemma_attribute_outputNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 69;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Lemma_attribute_leftNode extends ParseTreeNode {
	Lemma_attribute_leftNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 70;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Lemma_attribute_rightNode extends ParseTreeNode {
	Lemma_attribute_rightNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 71;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LemmaAttributeTypingNode extends ParseTreeNode {
	LemmaAttributeTypingNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 72;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LemmaAttributeSourcesNode extends ParseTreeNode {
	LemmaAttributeSourcesNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 73;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LemmaAttributeReuseNode extends ParseTreeNode {
	LemmaAttributeReuseNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 74;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LemmaAttributeDiffReuseNode extends ParseTreeNode {
	LemmaAttributeDiffReuseNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 75;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LemmaAttributeInductionNode extends ParseTreeNode {
	LemmaAttributeInductionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 76;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LemmaAttributeHideLemmaNode extends ParseTreeNode {
	LemmaAttributeHideLemmaNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 77;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LemmaAttributeHeuristicNode extends ParseTreeNode {
	LemmaAttributeHeuristicNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 78;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LemmaAttributeOutputNode extends ParseTreeNode {
	LemmaAttributeOutputNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 79;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class TraceQuantifierNode extends ParseTreeNode {
	TraceQuantifierNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 80;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ForallTracesNode extends ParseTreeNode {
	ForallTracesNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 81;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ExistsTraceNode extends ParseTreeNode {
	ExistsTraceNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 82;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LemmaNode extends ParseTreeNode {
	LemmaNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 83;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class StandardFormulaNode extends ParseTreeNode {
	StandardFormulaNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 84;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ImplicationNode extends ParseTreeNode {
	ImplicationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 85;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class DisjunctionNode extends ParseTreeNode {
	DisjunctionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 86;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ConjunctionNode extends ParseTreeNode {
	ConjunctionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 87;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class NegationNode extends ParseTreeNode {
	NegationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 88;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FatomNode extends ParseTreeNode {
	FatomNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 89;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Fatom_botNode extends ParseTreeNode {
	Fatom_botNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 90;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Fatom_topNode extends ParseTreeNode {
	Fatom_topNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 91;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Fatom_blatomNode extends ParseTreeNode {
	Fatom_blatomNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 92;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Fatom_quantificationNode extends ParseTreeNode {
	Fatom_quantificationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 93;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Fatom_formulaNode extends ParseTreeNode {
	Fatom_formulaNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 94;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BlatomNode extends ParseTreeNode {
	BlatomNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 95;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Blatom_lastNode extends ParseTreeNode {
	Blatom_lastNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 96;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Blatom_actionNode extends ParseTreeNode {
	Blatom_actionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 97;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Blatom_predicateNode extends ParseTreeNode {
	Blatom_predicateNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 98;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Blatom_lessNode extends ParseTreeNode {
	Blatom_lessNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 99;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Blatom_smallerpNode extends ParseTreeNode {
	Blatom_smallerpNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 100;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Blatom_node_eqNode extends ParseTreeNode {
	Blatom_node_eqNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 101;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Blatom_term_eqNode extends ParseTreeNode {
	Blatom_term_eqNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 102;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class QuantificationNode extends ParseTreeNode {
	QuantificationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 103;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SmallerpNode extends ParseTreeNode {
	SmallerpNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 104;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class CaseTestNode extends ParseTreeNode {
	CaseTestNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 105;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LemmaAccNode extends ParseTreeNode {
	LemmaAccNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 106;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class TacticNode extends ParseTreeNode {
	TacticNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 107;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class DeprioNode extends ParseTreeNode {
	DeprioNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 108;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class PrioNode extends ParseTreeNode {
	PrioNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 109;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class TacticDisjunctsNode extends ParseTreeNode {
	TacticDisjunctsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 110;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class TacticConjunctsNode extends ParseTreeNode {
	TacticConjunctsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 111;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class TacticNegationNode extends ParseTreeNode {
	TacticNegationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 112;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class TacticFunctionNode extends ParseTreeNode {
	TacticFunctionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 113;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SelectedPresortNode extends ParseTreeNode {
	SelectedPresortNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 114;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class GoalRankingPresortNode extends ParseTreeNode {
	GoalRankingPresortNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 115;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class TacticFunctionNameNode extends ParseTreeNode {
	TacticFunctionNameNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 116;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class RankingIdentifierNode extends ParseTreeNode {
	RankingIdentifierNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 117;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Ranking_smallestNode extends ParseTreeNode {
	Ranking_smallestNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 118;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Ranking_idNode extends ParseTreeNode {
	Ranking_idNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 119;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class TacticNameNode extends ParseTreeNode {
	TacticNameNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 120;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class HeuristicNode extends ParseTreeNode {
	HeuristicNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 121;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class GoalRankingNode extends ParseTreeNode {
	GoalRankingNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 122;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Goal_oracle_rankingNode extends ParseTreeNode {
	Goal_oracle_rankingNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 123;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Goal_internal_rankingNode extends ParseTreeNode {
	Goal_internal_rankingNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 124;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Goal_regular_rankingNode extends ParseTreeNode {
	Goal_regular_rankingNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 125;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class RegularRankingNode extends ParseTreeNode {
	RegularRankingNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 126;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class InternalTacticRankingNode extends ParseTreeNode {
	InternalTacticRankingNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 127;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class OracleRankingNode extends ParseTreeNode {
	OracleRankingNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 128;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ExportNode extends ParseTreeNode {
	ExportNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 129;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class PredicateDeclarationNode extends ParseTreeNode {
	PredicateDeclarationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 130;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class PredicateNode extends ParseTreeNode {
	PredicateNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 131;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SignatureOptionsNode extends ParseTreeNode {
	SignatureOptionsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 132;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinOptionsNode extends ParseTreeNode {
	BuiltinOptionsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 133;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class EquationsNode extends ParseTreeNode {
	EquationsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 134;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class EquationNode extends ParseTreeNode {
	EquationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 135;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SignatureFunctionsNode extends ParseTreeNode {
	SignatureFunctionsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 136;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SignatureFunctionNode extends ParseTreeNode {
	SignatureFunctionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 137;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FunctionAttributeNode extends ParseTreeNode {
	FunctionAttributeNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 138;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FunctionTypeNode extends ParseTreeNode {
	FunctionTypeNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 139;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Function_type_naturalNode extends ParseTreeNode {
	Function_type_naturalNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 140;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Function_type_sapicNode extends ParseTreeNode {
	Function_type_sapicNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 141;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinsNode extends ParseTreeNode {
	BuiltinsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 142;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNamesNode extends ParseTreeNode {
	BuiltinNamesNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 143;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameLocationsReportNode extends ParseTreeNode {
	BuiltinNameLocationsReportNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 144;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameReliableChannelNode extends ParseTreeNode {
	BuiltinNameReliableChannelNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 145;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameDHNode extends ParseTreeNode {
	BuiltinNameDHNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 146;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameBilinearPairingNode extends ParseTreeNode {
	BuiltinNameBilinearPairingNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 147;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameMultisetNode extends ParseTreeNode {
	BuiltinNameMultisetNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 148;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameSymmetricEncryptionNode extends ParseTreeNode {
	BuiltinNameSymmetricEncryptionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 149;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameAsymmetricEncryptionNode extends ParseTreeNode {
	BuiltinNameAsymmetricEncryptionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 150;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameSigningNode extends ParseTreeNode {
	BuiltinNameSigningNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 151;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameDestPairingNode extends ParseTreeNode {
	BuiltinNameDestPairingNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 152;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameDestSymmetricEncryptionNode extends ParseTreeNode {
	BuiltinNameDestSymmetricEncryptionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 153;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameDestAsymmetricEncryptionNode extends ParseTreeNode {
	BuiltinNameDestAsymmetricEncryptionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 154;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameDestSigningNode extends ParseTreeNode {
	BuiltinNameDestSigningNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 155;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameRevealingSigningNode extends ParseTreeNode {
	BuiltinNameRevealingSigningNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 156;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameHashingNode extends ParseTreeNode {
	BuiltinNameHashingNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 157;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinNameXorNode extends ParseTreeNode {
	BuiltinNameXorNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 158;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ProofSkeletonNode extends ParseTreeNode {
	ProofSkeletonNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 159;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class StartProofSkeletonNode extends ParseTreeNode {
	StartProofSkeletonNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 160;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SolvedProofNode extends ParseTreeNode {
	SolvedProofNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 161;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FinalProofNode extends ParseTreeNode {
	FinalProofNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 162;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class InterProofNode extends ParseTreeNode {
	InterProofNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 163;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ProofCaseNode extends ParseTreeNode {
	ProofCaseNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 164;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ProofMethodNode extends ParseTreeNode {
	ProofMethodNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 165;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Proof_method_sorryNode extends ParseTreeNode {
	Proof_method_sorryNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 166;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Proof_method_simplifyNode extends ParseTreeNode {
	Proof_method_simplifyNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 167;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Proof_method_solveNode extends ParseTreeNode {
	Proof_method_solveNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 168;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Proof_method_contradictionNode extends ParseTreeNode {
	Proof_method_contradictionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 169;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Proof_method_inductionNode extends ParseTreeNode {
	Proof_method_inductionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 170;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class GoalNode extends ParseTreeNode {
	GoalNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 171;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ActionGoalNode extends ParseTreeNode {
	ActionGoalNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 172;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class PremiseGoalNode extends ParseTreeNode {
	PremiseGoalNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 173;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ChainGoalNode extends ParseTreeNode {
	ChainGoalNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 174;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class DisjSplitGoalNode extends ParseTreeNode {
	DisjSplitGoalNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 175;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class EqSplitGoalNode extends ParseTreeNode {
	EqSplitGoalNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 176;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class NodePremiseNode extends ParseTreeNode {
	NodePremiseNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 177;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class NodeConclusionNode extends ParseTreeNode {
	NodeConclusionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 178;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class DiffEquivLemmaNode extends ParseTreeNode {
	DiffEquivLemmaNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 179;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class EquivLemmaNode extends ParseTreeNode {
	EquivLemmaNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 180;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ProcessNode extends ParseTreeNode {
	ProcessNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 181;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ElseProcessNode extends ParseTreeNode {
	ElseProcessNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 182;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class TopLevelProcessNode extends ParseTreeNode {
	TopLevelProcessNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 183;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ActionProcessNode extends ParseTreeNode {
	ActionProcessNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 184;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Action_process_replicationNode extends ParseTreeNode {
	Action_process_replicationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 185;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Action_process_lookupNode extends ParseTreeNode {
	Action_process_lookupNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 186;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Action_process_conditionalNode extends ParseTreeNode {
	Action_process_conditionalNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 187;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Action_process_let_bindingNode extends ParseTreeNode {
	Action_process_let_bindingNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 188;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Action_process_nullNode extends ParseTreeNode {
	Action_process_nullNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 189;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Action_process_sequentialNode extends ParseTreeNode {
	Action_process_sequentialNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 190;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Action_process_processNode extends ParseTreeNode {
	Action_process_processNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 191;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Action_process_identifierNode extends ParseTreeNode {
	Action_process_identifierNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 192;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SapicActionNode extends ParseTreeNode {
	SapicActionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 193;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Sapic_action_newNode extends ParseTreeNode {
	Sapic_action_newNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 194;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Sapic_action_insertNode extends ParseTreeNode {
	Sapic_action_insertNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 195;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Sapic_action_inNode extends ParseTreeNode {
	Sapic_action_inNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 196;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Sapic_action_outNode extends ParseTreeNode {
	Sapic_action_outNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 197;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Sapic_action_deleteNode extends ParseTreeNode {
	Sapic_action_deleteNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 198;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Sapic_action_lockNode extends ParseTreeNode {
	Sapic_action_lockNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 199;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Sapic_action_unlockNode extends ParseTreeNode {
	Sapic_action_unlockNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 200;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Sapic_action_eventNode extends ParseTreeNode {
	Sapic_action_eventNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 201;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Sapic_action_ruleNode extends ParseTreeNode {
	Sapic_action_ruleNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 202;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SapicActionInNode extends ParseTreeNode {
	SapicActionInNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 203;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SapicActionOutNode extends ParseTreeNode {
	SapicActionOutNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 204;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ProcessDefNode extends ParseTreeNode {
	ProcessDefNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 205;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SapicPatternTermNode extends ParseTreeNode {
	SapicPatternTermNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 206;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SapicTermNode extends ParseTreeNode {
	SapicTermNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 207;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LogicalTypedPatternLiteralNode extends ParseTreeNode {
	LogicalTypedPatternLiteralNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 208;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LogicalTypedLiteralNode extends ParseTreeNode {
	LogicalTypedLiteralNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 209;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SapicNodeVarNode extends ParseTreeNode {
	SapicNodeVarNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 210;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SapicPatternVarNode extends ParseTreeNode {
	SapicPatternVarNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 211;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SapicVarNode extends ParseTreeNode {
	SapicVarNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 212;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SapicTypeNode extends ParseTreeNode {
	SapicTypeNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 213;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class RestrictionNode extends ParseTreeNode {
	RestrictionNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 214;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class RestrictionAttributesNode extends ParseTreeNode {
	RestrictionAttributesNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 215;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class RestrictionAttributeNode extends ParseTreeNode {
	RestrictionAttributeNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 216;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LegacyAxiomNode extends ParseTreeNode {
	LegacyAxiomNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 217;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LetBlockNode extends ParseTreeNode {
	LetBlockNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 218;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ProtoFactIdentifierNode extends ParseTreeNode {
	ProtoFactIdentifierNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 219;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ReservedFactIdentifierNode extends ParseTreeNode {
	ReservedFactIdentifierNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 220;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Reserved_fact_outNode extends ParseTreeNode {
	Reserved_fact_outNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 221;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Reserved_fact_inNode extends ParseTreeNode {
	Reserved_fact_inNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 222;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Reserved_fact_kuNode extends ParseTreeNode {
	Reserved_fact_kuNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 223;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Reserved_fact_kdNode extends ParseTreeNode {
	Reserved_fact_kdNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 224;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class Reserved_fact_dedNode extends ParseTreeNode {
	Reserved_fact_dedNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 225;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class IdentifierNode extends ParseTreeNode {
	IdentifierNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 226;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class NaturalNode extends ParseTreeNode {
	NaturalNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 227;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class IndexedIdentifierNode extends ParseTreeNode {
	IndexedIdentifierNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 228;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FilePathNode extends ParseTreeNode {
	FilePathNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 229;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class KeywordsNode extends ParseTreeNode {
	KeywordsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 230;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LsortMsgNode extends ParseTreeNode {
	LsortMsgNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 231;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class MsgVarNode extends ParseTreeNode {
	MsgVarNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 232;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LogicalLiteralNode extends ParseTreeNode {
	LogicalLiteralNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 233;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LogicalLiteralWithNodeNode extends ParseTreeNode {
	LogicalLiteralWithNodeNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 234;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LogicalLiteralNoPubNode extends ParseTreeNode {
	LogicalLiteralNoPubNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 235;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LogVarNode extends ParseTreeNode {
	LogVarNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 236;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class LiteralNode extends ParseTreeNode {
	LiteralNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 237;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class NodeVarNode extends ParseTreeNode {
	NodeVarNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 238;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FreshNameNode extends ParseTreeNode {
	FreshNameNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 239;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class PubNameNode extends ParseTreeNode {
	PubNameNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 240;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FormalCommentNode extends ParseTreeNode {
	FormalCommentNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 241;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class TacticFunctionIdentifierNode extends ParseTreeNode {
	TacticFunctionIdentifierNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 242;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class InternalTacticNameNode extends ParseTreeNode {
	InternalTacticNameNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 243;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class OracleRelativePathNode extends ParseTreeNode {
	OracleRelativePathNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 244;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ExportBodyCharsNode extends ParseTreeNode {
	ExportBodyCharsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 245;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class CharDirNode extends ParseTreeNode {
	CharDirNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 246;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class EquivOpNode extends ParseTreeNode {
	EquivOpNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 247;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class OrOpNode extends ParseTreeNode {
	OrOpNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 248;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class NotOpNode extends ParseTreeNode {
	NotOpNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 249;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class AndOpNode extends ParseTreeNode {
	AndOpNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 250;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class EqOpNode extends ParseTreeNode {
	EqOpNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 251;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BotOpNode extends ParseTreeNode {
	BotOpNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 252;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class FOpNode extends ParseTreeNode {
	FOpNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 253;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class TOpNode extends ParseTreeNode {
	TOpNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 254;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class TopOpNode extends ParseTreeNode {
	TopOpNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 255;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ForallOpNode extends ParseTreeNode {
	ForallOpNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 256;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class ExistsOpNode extends ParseTreeNode {
	ExistsOpNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 257;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SuffixFreshNode extends ParseTreeNode {
	SuffixFreshNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 258;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class SuffixPubNode extends ParseTreeNode {
	SuffixPubNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 259;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinXorNode extends ParseTreeNode {
	BuiltinXorNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 260;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinMunNode extends ParseTreeNode {
	BuiltinMunNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 261;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinOneNode extends ParseTreeNode {
	BuiltinOneNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 262;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinExpNode extends ParseTreeNode {
	BuiltinExpNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 263;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinMultNode extends ParseTreeNode {
	BuiltinMultNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 264;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinInvNode extends ParseTreeNode {
	BuiltinInvNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 265;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinPmultNode extends ParseTreeNode {
	BuiltinPmultNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 266;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinEmNode extends ParseTreeNode {
	BuiltinEmNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 267;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class BuiltinZeroNode extends ParseTreeNode {
	BuiltinZeroNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 268;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class DhNeutralNode extends ParseTreeNode {
	DhNeutralNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 269;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class OperatorsNode extends ParseTreeNode {
	OperatorsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 270;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class PunctuationNode extends ParseTreeNode {
	PunctuationNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 271;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

class AllowedIdentifierKeywordsNode extends ParseTreeNode {
	AllowedIdentifierKeywordsNode() {
		Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
		priority = 272;    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}

