import re

def read_parser_rules():
    with open('/home/jastau/BT/code-JPlag-Extension-Tamarin/languages/tamarin/src/main/antlr4/de/jplag/tamarin/grammar/TamarinParser.g4', 'r') as f:
        with open('./parser_rules', 'w') as p:
            for line in f.readlines():
                split = line.split()
                for s in split:
                    if 'TODO' in s:
                        break
                    if ':' in s:
                        p.write(split[0])
                        p.write('\n')
                        break
                for s in split:
                    if '#' in s:
                        p.write(s[1:])
                        p.write('\n')
                
def read_lexer_rules():
    with open('/home/jastau/BT/code-JPlag-Extension-Tamarin/languages/tamarin/src/main/antlr4/de/jplag/tamarin/grammar/TamarinLexer.g4', 'r') as f:
        with open('./lexer_rules', 'w') as p:
            for line in f.readlines():
                split = line.split()
                if len(split) >= 2 and split[1] == ':':
                    p.write(split[0])
                    p.write('\n')

def generate_ast_nodes():
    with open('./ParserTreeNodes.java', 'w') as f:
        f.write('package de.jplag.tamarin;')
        f.write('\n\n')
        f.write('''abstract class ParseTreeNode implements Comparable<ParseTreeNode>{
    ArrayList<ParseTreeNode> Children;
    ParseTreeNode parent;
    ArrayList<Token> nodeTokens;
    int priority;
    ParseTreeNode() {
        Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
    }

    abstract void sort();
    List<Token> getTokens() {
        List<Token> tokens = new ArrayList<Token>();
        int childIndex = 0;
        for(int i = 0; i < nodeTokens.size(); i++) {
            if(nodeTokens.get(i).getType() == CHILD) {
                tokens.addAll(Children.get(childIndex).getTokens());
                childIndex++;
            }
            else {
                tokens.add(nodeTokens.get(i));
            }
        }
        return tokens;
    }
    void setParent(ParseTreeNode parent) {
        this.parent = parent;
    }
    void addChild(ParseTreeNode child) {
        Children.add(child);
    }
    void addToken(Token token)  {
        nodeTokens.add(token);
    }
    public int compareTo(ParseTreeNode other) {
        if (this.priority < other.priority) {
            return 1;
        }
        else if(this.priority > other.priority) {
            return -1;
        }
        else {
            if(this.Children.size() == 0 && other.Children.size() == 0) {
                return 0;
            }
            if(this.Children.size() < other.Children.size()) {
                return 1;
            }
            else if(this.Children.size() > other.Children.size()) {
                return -1;
            }
            else {
                this.sort();
                other.sort();
                for(int i = 0; i < this.Children.size(); i++) {
                    int comp = this.Children.get(0).compareTo(other.Children.get(0));
                    if(comp != 0) {
                        return comp;
                    }
                }
                return 0;
            }
        }
    }
}''')
        f.write('\n\n')
        f.write('''class InitialNode extends ParseTreeNode {

    InitialNode() {
        Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
        priority = -1;
    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

    @Override
    void addToken(Token token)  {
        if(token.getType() == CHILD) {
            nodeTokens.add(token);
        }
        else {
            System.err.println("Trying to add token " + token.getType().getDescription() + " to initial node!");
        }
    }

}''')
        f.write('\n\n')
        with open('./parser_rules', 'r') as p:
            for i, rule in enumerate(p.readlines()):
                rule = rule[0:1].capitalize() + rule[1:-1:] + 'Node'
                f.write('class ' + rule + ' extends ParseTreeNode {\n')
                f.write('\t' + rule + '() {\n')
                f.write('''\t\tChildren = new ArrayList<>();
        nodeTokens = new ArrayList<>();\n''')
                f.write(f'\t\tpriority = {i};')
                f.write('''    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
        Collections.sort(Children);
    }

}''')
                f.write('\n\n')

def generate_rule_tokens():
    with open('./ParserTokenTypes.java', 'w') as f:
        with open('./parser_rules', 'r') as p:
            f.write('public enum TamarinTokenType implements TokenType {\n')
            for rule in p.readlines():
                rule = rule[:-1]
                f.write('\t' + rule.upper() + '(' +  '"' + rule.upper() + '"' + '),\n')
                f.write('\t' + rule.upper() + '_BEGIN(' +  '"' + rule.upper() + '_BEGIN"' + '),\n')
                f.write('\t' + rule.upper() + '_END(' +  '"' + rule.upper() + '_END"' + '),\n')
        with open('./lexer_rules', 'r') as l:
            for token in l.readlines():
                token = token[:-1]
                f.write('\t' + 'TOKEN_' + token.upper() + '("' + token.upper() + '"),\n')
            f.write('}')

def generate_terminal_tokens():
    with open('/home/jastau/BT/code-JPlag-Extension-Tamarin/languages/tamarin/src/main/antlr4/de/jplag/tamarin/grammar/TamarinLexer.g4', 'r') as f:
        with open('./terminal_tokens', 'w') as g:
            for line in f.readlines():
                split = line.split()
                tokens = map(lambda x : x[1:-1], re.findall("'[^']*'", line))
                for t in tokens:
                    g.write(f'\t\t\tcase "{t}" -> Optional.of(TOKEN_{split[0].upper()});\n')
                


read_parser_rules()
read_lexer_rules()

generate_ast_nodes()
generate_rule_tokens()

generate_terminal_tokens()