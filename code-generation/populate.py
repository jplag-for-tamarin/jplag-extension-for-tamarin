# - Rules that have to sort
# - which tokens a rule generates
# - whether a rule should be transparent
# - whether a rule should return its children

import re
import json
from tkinter import *
from tkinter import ttk
from tkinter.filedialog import asksaveasfilename, askopenfilename
from functools import partial

def capitalize(s):
    if len(s) > 0:
        return s[0].upper() + s[1::]
    else: return ''

def generate_listener():
    with open('./JplagTamarinListener.java', 'w') as listener:
        listener.write('''package de.jplag.tamarin;

import static de.jplag.tamarin.TamarinTokenType.*;
import de.jplag.tamarin.grammar.TamarinParserBaseListener;
import de.jplag.tamarin.grammar.TamarinParser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import javax.swing.text.html.Option;
import java.util.Optional;

public class JplagTamarinListener extends TamarinParserBaseListener {
    private final Parser parser;\n\n''')
        listener.write('''    @Override public void visitTerminal(TerminalNode node) {
       Token token = node.getSymbol();
       String tokenText = token.getText();
       Optional<TamarinTokenType> type = switch (tokenText) {
           case "let" -> Optional.of(TOKEN_KEYWORDLET);
           case "in" -> Optional.of(TOKEN_KEYWORDIN);
           case "rule" -> Optional.of(TOKEN_KEYWORDRULE);
           case "diff" -> Optional.of(TOKEN_KEYWORDDIFF);
           case "by" -> Optional.of(TOKEN_KEYWORDBY);
           case "lemma" -> Optional.of(TOKEN_KEYWORDLEMMA);
           case "if" -> Optional.of(TOKEN_KEYWORDIF);
           case "else" -> Optional.of(TOKEN_KEYWORDELSE);
           case "process" -> Optional.of(TOKEN_KEYWORDPROCESS);
           case "as" -> Optional.of(TOKEN_KEYWORDAS);
           case "lookup" -> Optional.of(TOKEN_KEYWORDLOOKUP);
           case "last" -> Optional.of(TOKEN_KEYWORDLAST);
           case "prio" -> Optional.of(TOKEN_KEYWORDPRIO);
           case "deprio" -> Optional.of(TOKEN_KEYWORDDEPRIO);
           case "presort" -> Optional.of(TOKEN_KEYWORDPRESORT);
           case "smallest" -> Optional.of(TOKEN_KEYWORDSMALLEST);
           case "id" -> Optional.of(TOKEN_KEYWORDID);
           case "tactic" -> Optional.of(TOKEN_KEYWORDTACTIC);
           case "restriction" -> Optional.of(TOKEN_KEYWORDRESTRICTION);
           case "axiom" -> Optional.of(TOKEN_KEYWORDAXIOM);
           case "Any" -> Optional.of(TOKEN_KEYWORDANY);
           case "test" -> Optional.of(TOKEN_KEYWORDTEST);
           case "no_precomp" -> Optional.of(TOKEN_KEYWORDNOPRECOMP);
           case "then" -> Optional.of(TOKEN_KEYWORDTHEN);
           case "out" -> Optional.of(TOKEN_KEYWORDOUT);
           case "fresh" -> Optional.of(TOKEN_KEYWORDFRESH);
           case "pub" -> Optional.of(TOKEN_KEYWORDPUB);
           case "xor" -> Optional.of(TOKEN_KEYWORDXOR);
           case "output" -> Optional.of(TOKEN_KEYWORDOUTPUT);
           case "accounts for" -> Optional.of(TOKEN_KEYWORDACCOUNTSFOR);
           case "account for" -> Optional.of(TOKEN_KEYWORDACCOUNTSFOR);
           case "modulo" -> Optional.of(TOKEN_KEYWORDMODULO);
           case "theory" -> Optional.of(TOKEN_KEYWORDTHEORY);
           case "begin" -> Optional.of(TOKEN_KEYWORDBEGIN);
           case "end" -> Optional.of(TOKEN_KEYWORDEND);
           case "variants" -> Optional.of(TOKEN_KEYWORDVARIANTS);
           case "_restrict" -> Optional.of(TOKEN_KEYWORDRESTRICT);
           case "heuristic" -> Optional.of(TOKEN_KEYWORDHEURISTIC);
           case "export" -> Optional.of(TOKEN_KEYWORDEXPORT);
           case "predicate" -> Optional.of(TOKEN_KEYWORDPREDICATE);
           case "predicates" -> Optional.of(TOKEN_KEYWORDPREDICATE);
           case "options" -> Optional.of(TOKEN_KEYWORDOPTIONS);
           case "equations" -> Optional.of(TOKEN_KEYWORDEQUATIONS);
           case "functions" -> Optional.of(TOKEN_KEYWORDFUNCTIONS);
           case "function" -> Optional.of(TOKEN_KEYWORDFUNCTIONS);
           case "builtins" -> Optional.of(TOKEN_KEYWORDBUILTINS);
           case "case" -> Optional.of(TOKEN_KEYWORDCASE);
           case "qed" -> Optional.of(TOKEN_KEYWORDQED);
           case "next" -> Optional.of(TOKEN_KEYWORDNEXT);
           case "splitEqs" -> Optional.of(TOKEN_KEYWORDSPLITEQS);
           case "equivLemma" -> Optional.of(TOKEN_KEYWORDEQUIVLEMMA);
           case "diffEquivLemma" -> Optional.of(TOKEN_KEYWORDDIFFEQUIVLEMMA);
           case "E" -> Optional.of(TOKEN_E);
           case "AC" -> Optional.of(TOKEN_AC);
           case "Fresh" -> Optional.of(TOKEN_RULEFRESH);
           case "irecv" -> Optional.of(TOKEN_RULEIRECV);
           case "isend" -> Optional.of(TOKEN_RULEISEND);
           case "coerce" -> Optional.of(TOKEN_RULECOERCE);
           case "iequality" -> Optional.of(TOKEN_RULEIEQUALITY);
           case "DH_neutral" -> Optional.of(TOKEN_DHNEUTRAL);
           case "mun" -> Optional.of(TOKEN_BUILTINMUN);
           case "one" -> Optional.of(TOKEN_BUILTINONE);
           case "exp" -> Optional.of(TOKEN_BUILTINEXP);
           case "mult" -> Optional.of(TOKEN_BUILTINMULT);
           case "inv" -> Optional.of(TOKEN_BUILTININV);
           case "pmult" -> Optional.of(TOKEN_BUILTINPMULT);
           case "em" -> Optional.of(TOKEN_BUILTINEM);
           case "zero" -> Optional.of(TOKEN_BUILTINZERO);
           case "SOLVED" -> Optional.of(TOKEN_SOLVEDPROOF);
           case "sorry" -> Optional.of(TOKEN_PROOFMETHODSORRY);
           case "simplify" -> Optional.of(TOKEN_PROOFMETHODSIMPLIFY);
           case "solve" -> Optional.of(TOKEN_PROOFMETHODSOLVE);
           case "contradiction" -> Optional.of(TOKEN_PROOFMETHODCONTRADICTION);
           case "induction" -> Optional.of(TOKEN_PROOFMETHODINDUCTION);
           case "translation-progress" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "translation-allow-pattern-lookups" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "translation-state-optimisation" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "translation-asynchronous-channels" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "translation-compress-events" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "locations-report" -> Optional.of(TOKEN_BUILTINNAMELOCATIONSREPORT);
           case "reliable-channel" -> Optional.of(TOKEN_BUILTINNAMERELIABLECHANNEL);
           case "diffie-hellman" -> Optional.of(TOKEN_BUILTINNAMEDH);
           case "bilinear-pairing" -> Optional.of(TOKEN_BUILTINNAMEBILINEARPAIRING);
           case "multiset" -> Optional.of(TOKEN_BUILTINNAMEMULTISET);
           case "symmetric-encryption" -> Optional.of(TOKEN_BUILTINNAMESYMMETRICENCRYPTION);
           case "asymmetric-encryption" -> Optional.of(TOKEN_BUILTINNAMEASYMMETRICENCRYPTION);
           case "signing" -> Optional.of(TOKEN_BUILTINNAMESIGNING);
           case "dest-pairing" -> Optional.of(TOKEN_BUILTINNAMEDESTPAIRING);
           case "dest-symmetric-encryption" -> Optional.of(TOKEN_BUILTINNAMEDESTSYMMETRICENCRYPTION);
           case "dest-asymmetric-encryption" -> Optional.of(TOKEN_BUILTINNAMEDESTASYMMETRICENCRYPTION);
           case "dest-signing" -> Optional.of(TOKEN_BUILTINNAMEDESTSIGNING);
           case "revealing-signing" -> Optional.of(TOKEN_BUILTINNAMEREVEALINGSIGNING);
           case "hashing" -> Optional.of(TOKEN_BUILTINNAMEHASHING);
           case "regex" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "isFactName" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "isInFactTerms" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "nonAbsurdGoal" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "dhreNoise" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "defaultNoise" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "reasonableNoncesNoise" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "new" -> Optional.of(TOKEN_SAPICACTIONNEW);
           case "insert" -> Optional.of(TOKEN_SAPICACTIONINSERT);
           case "delete" -> Optional.of(TOKEN_SAPICACTIONDELETE);
           case "lock" -> Optional.of(TOKEN_SAPICACTIONLOCK);
           case "unlock" -> Optional.of(TOKEN_SAPICACTIONUNLOCK);
           case "event" -> Optional.of(TOKEN_SAPICACTIONEVENT);
           case "∨" -> Optional.of(TOKEN_OROP);
           case "|" -> Optional.of(TOKEN_PIPEOP);
           case "||" -> Optional.of(TOKEN_DOUBLEPIPEOP);
           case "&" -> Optional.of(TOKEN_ANDOP);
           case "∧" -> Optional.of(TOKEN_ANDOP);
           case "¬" -> Optional.of(TOKEN_NOTOP);
           case "not" -> Optional.of(TOKEN_NOTOP);
           case "+" -> Optional.of(TOKEN_PLUSOP);
           case "-" -> Optional.of(TOKEN_MINUSOP);
           case "XOR" -> Optional.of(TOKEN_XOROP);
           case "⊕" -> Optional.of(TOKEN_XOROP);
           case "*" -> Optional.of(TOKEN_MULTOP);
           case "^" -> Optional.of(TOKEN_EXPOP);
           case "=" -> Optional.of(TOKEN_EQOP);
           case "@" -> Optional.of(TOKEN_ATOP);
           case "1" -> Optional.of(TOKEN_ONEOP);
           case "0" -> Optional.of(TOKEN_NULLOP);
           case "▶" -> Optional.of(TOKEN_REQUIRESOP);
           case "∥" -> Optional.of(TOKEN_SPLITOP);
           case "!" -> Optional.of(TOKEN_BANGOP);
           case "<" -> Optional.of(TOKEN_LESSOP);
           case "(<)" -> Optional.of(TOKEN_LESSTERMOP);
           case ">" -> Optional.of(TOKEN_GREATEROP);
           case "All" -> Optional.of(TOKEN_FORALLOP);
           case "∀" -> Optional.of(TOKEN_FORALLOP);
           case "Ex" -> Optional.of(TOKEN_EXISTSOP);
           case "∃" -> Optional.of(TOKEN_EXISTSOP);
           case "==>" -> Optional.of(TOKEN_IMPLIESOP);
           case "⇒" -> Optional.of(TOKEN_IMPLIESOP);
           case "left" -> Optional.of(TOKEN_LEFT);
           case "right" -> Optional.of(TOKEN_RIGHT);
           case "both" -> Optional.of(TOKEN_BOTH);
           case "#define" -> Optional.of(TOKEN_DEFINE);
           case "#include" -> Optional.of(TOKEN_INCLUDE);
           case "#ifdef" -> Optional.of(TOKEN_IFDEF);
           case "#else" -> Optional.of(TOKEN_ELSE);
           case "#endif" -> Optional.of(TOKEN_ENDIF);
           case "all-traces" -> Optional.of(TOKEN_FORALLTRACES);
           case "exists-trace" -> Optional.of(TOKEN_EXISTSTRACE);
           case "OUT" -> Optional.of(TOKEN_OUTFACTIDENT);
           case "Out" -> Optional.of(TOKEN_OUTFACTIDENT);
           case "IN" -> Optional.of(TOKEN_INFACTIDENT);
           case "In" -> Optional.of(TOKEN_INFACTIDENT);
           case "KU" -> Optional.of(TOKEN_KUFACTIDENT);
           case "Ku" -> Optional.of(TOKEN_KUFACTIDENT);
           case "KD" -> Optional.of(TOKEN_KDFACTIDENT);
           case "Kd" -> Optional.of(TOKEN_KDFACTIDENT);
           case "DED" -> Optional.of(TOKEN_DEDLOGFACTIDENT);
           case "Ded" -> Optional.of(TOKEN_DEDLOGFACTIDENT);
           case "FR" -> Optional.of(TOKEN_FRESHFACTIDENT);
           case "Fr" -> Optional.of(TOKEN_FRESHFACTIDENT);
           case "typing" -> Optional.of(TOKEN_LEMMAATTRIBUTETYPING);
           case "sources" -> Optional.of(TOKEN_LEMMAATTRIBUTESOURCES);
           case "reuse" -> Optional.of(TOKEN_LEMMAATTRIBUTEREUSE);
           case "diff_reuse" -> Optional.of(TOKEN_LEMMAATTRIBUTEDIFFREUSE);
           case "use_induction" -> Optional.of(TOKEN_LEMMAATTRIBUTEINDUCTION);
           case "hide_lemma" -> Optional.of(TOKEN_LEMMAATTRIBUTEHIDELEMMA);
           case "private" -> Optional.of(TOKEN_FUNCTIONATTRIBUTE);
           case "destructor" -> Optional.of(TOKEN_FUNCTIONATTRIBUTE);
           default -> Optional.empty();
       };
       type.ifPresent(tokenType -> parser.addToken(tokenType, token));
    }

    public JplagTamarinListener(Parser parser) {
        this.parser = parser;
    }

    public void visitErrorNode(ErrorNode node) {
    }''')
        listener.write('}\n')

def get_rule_name(s):
    split = s.split()
    if '@Override' in split:
        return split[3].split('(')[0]
    else:
        return None

def extract_listener_rules(listener_file_path):
    with open(listener_file_path, 'r') as base_listener:
        rules = {}
        for line in base_listener.readlines():
            fun = get_rule_name(line)
            if fun != None:
                rules[fun] = {}
        return rules

def read_parser_rules(parser_file_path):
    with open(parser_file_path, 'r') as f:
        rules = []
        for line in f.readlines():
            split = line.split()
            for s in split:
                if 'TODO' in s:
                    break
                if ':' in s:
                    rules.append(split[0])
                    break
            for s in split:
                if '#' in s:
                    rules.append(s[1:])
        return rules

def read_lexer_rules(lexer_file_path):
    with open(lexer_file_path, 'r') as f:
        lexer_rules = []
        for line in f.readlines():
            split = line.split()
            if len(split) >= 2 and split[1] == ':':
                lexer_rules.append(split[0])
        return lexer_rules

def parser_rules_to_tokens(parser_rules):
    rule_tokens = []
    for rule in parser_rules:
        rule_tokens.append(rule.upper())
        rule_tokens.append(rule.upper() + '_BEGIN')
        rule_tokens.append(rule.upper() + '_END')
    return rule_tokens

def lexer_rules_to_tokens(lexer_rules):
    rule_tokens = []
    for rule in lexer_rules:
        rule_tokens.append('TOKEN_' + rule.upper())
    return rule_tokens

def generate_listener(listener_rules, settings, ):
    with open('./output/JplagTamarinListener.java', 'w') as listener:
        listener.write('''package de.jplag.tamarin;

import static de.jplag.tamarin.TamarinTokenType.*;
import de.jplag.tamarin.grammar.TamarinParserBaseListener;
import de.jplag.tamarin.grammar.TamarinParser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import javax.swing.text.html.Option;
import java.util.Optional;

public class JplagTamarinListener extends TamarinParserBaseListener {
    private final Parser parser;
''')
        for (lrule, prule) in listener_rules.items():
            if len(prule) > 0:
                listener.write(f"\t@Override public void {lrule}(TamarinParser.{capitalize(prule)}Context ctx) {{ \n")
                if lrule.startswith('enter'):
                    listener.write(f"\t\tparser.addChildNode(new {capitalize(prule)}Node());\n")
                    if settings[prule]['generated_token'] != '':
                        listener.write(f"\t\tparser.addToken({settings[prule]['generated_token']}{'_BEGIN' if settings[prule]['encapsulating_tokens'] else ''}, ctx.getStart());\n")
                elif lrule.startswith('exit'):
                    if settings[prule]['encapsulating_tokens'] and settings[prule]['generated_token'] != '': listener.write(f"\t\tparser.addToken({settings[prule]['generated_token']}_END, ctx.getStart());\n")
                    listener.write(f"\t\tparser.finishChild();\n")
                listener.write(f"\t}}\n")
        listener.write(f'''    @Override public void visitTerminal(TerminalNode node) {{
       Token token = node.getSymbol();
       String tokenText = token.getText();
       Optional<TamarinTokenType> type = switch (tokenText) {{
           case "let" -> Optional.of(TOKEN_KEYWORDLET);
           case "in" -> Optional.of(TOKEN_KEYWORDIN);
           case "rule" -> Optional.of(TOKEN_KEYWORDRULE);
           case "diff" -> Optional.of(TOKEN_KEYWORDDIFF);
           case "by" -> Optional.of(TOKEN_KEYWORDBY);
           case "lemma" -> Optional.of(TOKEN_KEYWORDLEMMA);
           case "if" -> Optional.of(TOKEN_KEYWORDIF);
           case "else" -> Optional.of(TOKEN_KEYWORDELSE);
           case "process" -> Optional.of(TOKEN_KEYWORDPROCESS);
           case "as" -> Optional.of(TOKEN_KEYWORDAS);
           case "lookup" -> Optional.of(TOKEN_KEYWORDLOOKUP);
           case "last" -> Optional.of(TOKEN_KEYWORDLAST);
           case "prio" -> Optional.of(TOKEN_KEYWORDPRIO);
           case "deprio" -> Optional.of(TOKEN_KEYWORDDEPRIO);
           case "presort" -> Optional.of(TOKEN_KEYWORDPRESORT);
           case "smallest" -> Optional.of(TOKEN_KEYWORDSMALLEST);
           case "id" -> Optional.of(TOKEN_KEYWORDID);
           case "tactic" -> Optional.of(TOKEN_KEYWORDTACTIC);
           case "restriction" -> Optional.of(TOKEN_KEYWORDRESTRICTION);
           case "axiom" -> Optional.of(TOKEN_KEYWORDAXIOM);
           case "Any" -> Optional.of(TOKEN_KEYWORDANY);
           case "test" -> Optional.of(TOKEN_KEYWORDTEST);
           case "no_precomp" -> Optional.of(TOKEN_KEYWORDNOPRECOMP);
           case "then" -> Optional.of(TOKEN_KEYWORDTHEN);
           case "out" -> Optional.of(TOKEN_KEYWORDOUT);
           case "fresh" -> Optional.of(TOKEN_KEYWORDFRESH);
           case "pub" -> Optional.of(TOKEN_KEYWORDPUB);
           case "xor" -> Optional.of(TOKEN_KEYWORDXOR);
           case "output" -> Optional.of(TOKEN_KEYWORDOUTPUT);
           case "accounts for" -> Optional.of(TOKEN_KEYWORDACCOUNTSFOR);
           case "account for" -> Optional.of(TOKEN_KEYWORDACCOUNTSFOR);
           case "modulo" -> Optional.of(TOKEN_KEYWORDMODULO);
           case "theory" -> Optional.of(TOKEN_KEYWORDTHEORY);
           case "begin" -> Optional.of(TOKEN_KEYWORDBEGIN);
           case "end" -> Optional.of(TOKEN_KEYWORDEND);
           case "variants" -> Optional.of(TOKEN_KEYWORDVARIANTS);
           case "_restrict" -> Optional.of(TOKEN_KEYWORDRESTRICT);
           case "heuristic" -> Optional.of(TOKEN_KEYWORDHEURISTIC);
           case "export" -> Optional.of(TOKEN_KEYWORDEXPORT);
           case "predicate" -> Optional.of(TOKEN_KEYWORDPREDICATE);
           case "predicates" -> Optional.of(TOKEN_KEYWORDPREDICATE);
           case "options" -> Optional.of(TOKEN_KEYWORDOPTIONS);
           case "equations" -> Optional.of(TOKEN_KEYWORDEQUATIONS);
           case "functions" -> Optional.of(TOKEN_KEYWORDFUNCTIONS);
           case "function" -> Optional.of(TOKEN_KEYWORDFUNCTIONS);
           case "builtins" -> Optional.of(TOKEN_KEYWORDBUILTINS);
           case "case" -> Optional.of(TOKEN_KEYWORDCASE);
           case "qed" -> Optional.of(TOKEN_KEYWORDQED);
           case "next" -> Optional.of(TOKEN_KEYWORDNEXT);
           case "splitEqs" -> Optional.of(TOKEN_KEYWORDSPLITEQS);
           case "equivLemma" -> Optional.of(TOKEN_KEYWORDEQUIVLEMMA);
           case "diffEquivLemma" -> Optional.of(TOKEN_KEYWORDDIFFEQUIVLEMMA);
           case "E" -> Optional.of(TOKEN_E);
           case "AC" -> Optional.of(TOKEN_AC);
           case "Fresh" -> Optional.of(TOKEN_RULEFRESH);
           case "irecv" -> Optional.of(TOKEN_RULEIRECV);
           case "isend" -> Optional.of(TOKEN_RULEISEND);
           case "coerce" -> Optional.of(TOKEN_RULECOERCE);
           case "iequality" -> Optional.of(TOKEN_RULEIEQUALITY);
           case "DH_neutral" -> Optional.of(TOKEN_DHNEUTRAL);
           case "mun" -> Optional.of(TOKEN_BUILTINMUN);
           case "one" -> Optional.of(TOKEN_BUILTINONE);
           case "exp" -> Optional.of(TOKEN_BUILTINEXP);
           case "mult" -> Optional.of(TOKEN_BUILTINMULT);
           case "inv" -> Optional.of(TOKEN_BUILTININV);
           case "pmult" -> Optional.of(TOKEN_BUILTINPMULT);
           case "em" -> Optional.of(TOKEN_BUILTINEM);
           case "zero" -> Optional.of(TOKEN_BUILTINZERO);
           case "SOLVED" -> Optional.of(TOKEN_SOLVEDPROOF);
           case "sorry" -> Optional.of(TOKEN_PROOFMETHODSORRY);
           case "simplify" -> Optional.of(TOKEN_PROOFMETHODSIMPLIFY);
           case "solve" -> Optional.of(TOKEN_PROOFMETHODSOLVE);
           case "contradiction" -> Optional.of(TOKEN_PROOFMETHODCONTRADICTION);
           case "induction" -> Optional.of(TOKEN_PROOFMETHODINDUCTION);
           case "translation-progress" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "translation-allow-pattern-lookups" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "translation-state-optimisation" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "translation-asynchronous-channels" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "translation-compress-events" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "locations-report" -> Optional.of(TOKEN_BUILTINNAMELOCATIONSREPORT);
           case "reliable-channel" -> Optional.of(TOKEN_BUILTINNAMERELIABLECHANNEL);
           case "diffie-hellman" -> Optional.of(TOKEN_BUILTINNAMEDH);
           case "bilinear-pairing" -> Optional.of(TOKEN_BUILTINNAMEBILINEARPAIRING);
           case "multiset" -> Optional.of(TOKEN_BUILTINNAMEMULTISET);
           case "symmetric-encryption" -> Optional.of(TOKEN_BUILTINNAMESYMMETRICENCRYPTION);
           case "asymmetric-encryption" -> Optional.of(TOKEN_BUILTINNAMEASYMMETRICENCRYPTION);
           case "signing" -> Optional.of(TOKEN_BUILTINNAMESIGNING);
           case "dest-pairing" -> Optional.of(TOKEN_BUILTINNAMEDESTPAIRING);
           case "dest-symmetric-encryption" -> Optional.of(TOKEN_BUILTINNAMEDESTSYMMETRICENCRYPTION);
           case "dest-asymmetric-encryption" -> Optional.of(TOKEN_BUILTINNAMEDESTASYMMETRICENCRYPTION);
           case "dest-signing" -> Optional.of(TOKEN_BUILTINNAMEDESTSIGNING);
           case "revealing-signing" -> Optional.of(TOKEN_BUILTINNAMEREVEALINGSIGNING);
           case "hashing" -> Optional.of(TOKEN_BUILTINNAMEHASHING);
           case "regex" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "isFactName" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "isInFactTerms" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "nonAbsurdGoal" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "dhreNoise" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "defaultNoise" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "reasonableNoncesNoise" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "new" -> Optional.of(TOKEN_SAPICACTIONNEW);
           case "insert" -> Optional.of(TOKEN_SAPICACTIONINSERT);
           case "delete" -> Optional.of(TOKEN_SAPICACTIONDELETE);
           case "lock" -> Optional.of(TOKEN_SAPICACTIONLOCK);
           case "unlock" -> Optional.of(TOKEN_SAPICACTIONUNLOCK);
           case "event" -> Optional.of(TOKEN_SAPICACTIONEVENT);
           case "∨" -> Optional.of(TOKEN_OROP);
           case "|" -> Optional.of(TOKEN_PIPEOP);
           case "||" -> Optional.of(TOKEN_DOUBLEPIPEOP);
           case "&" -> Optional.of(TOKEN_ANDOP);
           case "∧" -> Optional.of(TOKEN_ANDOP);
           case "¬" -> Optional.of(TOKEN_NOTOP);
           case "not" -> Optional.of(TOKEN_NOTOP);
           case "+" -> Optional.of(TOKEN_PLUSOP);
           case "-" -> Optional.of(TOKEN_MINUSOP);
           case "XOR" -> Optional.of(TOKEN_XOROP);
           case "⊕" -> Optional.of(TOKEN_XOROP);
           case "*" -> Optional.of(TOKEN_MULTOP);
           case "^" -> Optional.of(TOKEN_EXPOP);
           case "=" -> Optional.of(TOKEN_EQOP);
           case "@" -> Optional.of(TOKEN_ATOP);
           case "1" -> Optional.of(TOKEN_ONEOP);
           case "0" -> Optional.of(TOKEN_NULLOP);
           case "▶" -> Optional.of(TOKEN_REQUIRESOP);
           case "∥" -> Optional.of(TOKEN_SPLITOP);
           case "!" -> Optional.of(TOKEN_BANGOP);
           case "<" -> Optional.of(TOKEN_LESSOP);
           case "(<)" -> Optional.of(TOKEN_LESSTERMOP);
           case ">" -> Optional.of(TOKEN_GREATEROP);
           case "All" -> Optional.of(TOKEN_FORALLOP);
           case "∀" -> Optional.of(TOKEN_FORALLOP);
           case "Ex" -> Optional.of(TOKEN_EXISTSOP);
           case "∃" -> Optional.of(TOKEN_EXISTSOP);
           case "==>" -> Optional.of(TOKEN_IMPLIESOP);
           case "⇒" -> Optional.of(TOKEN_IMPLIESOP);

           case "left" -> Optional.of(TOKEN_LEFT);
           case "right" -> Optional.of(TOKEN_RIGHT);
           case "both" -> Optional.of(TOKEN_BOTH);
           case "#define" -> Optional.of(TOKEN_DEFINE);
           case "#include" -> Optional.of(TOKEN_INCLUDE);
           case "#ifdef" -> Optional.of(TOKEN_IFDEF);
           case "#else" -> Optional.of(TOKEN_ELSE);
           case "#endif" -> Optional.of(TOKEN_ENDIF);
           case "all-traces" -> Optional.of(TOKEN_FORALLTRACES);
           case "exists-trace" -> Optional.of(TOKEN_EXISTSTRACE);
           case "OUT" -> Optional.of(TOKEN_OUTFACTIDENT);
           case "Out" -> Optional.of(TOKEN_OUTFACTIDENT);
           case "IN" -> Optional.of(TOKEN_INFACTIDENT);
           case "In" -> Optional.of(TOKEN_INFACTIDENT);
           case "KU" -> Optional.of(TOKEN_KUFACTIDENT);
           case "Ku" -> Optional.of(TOKEN_KUFACTIDENT);
           case "KD" -> Optional.of(TOKEN_KDFACTIDENT);
           case "Kd" -> Optional.of(TOKEN_KDFACTIDENT);
           case "DED" -> Optional.of(TOKEN_DEDLOGFACTIDENT);
           case "Ded" -> Optional.of(TOKEN_DEDLOGFACTIDENT);
           case "FR" -> Optional.of(TOKEN_FRESHFACTIDENT);
           case "Fr" -> Optional.of(TOKEN_FRESHFACTIDENT);
           case "typing" -> Optional.of(TOKEN_LEMMAATTRIBUTETYPING);
           case "sources" -> Optional.of(TOKEN_LEMMAATTRIBUTESOURCES);
           case "reuse" -> Optional.of(TOKEN_LEMMAATTRIBUTEREUSE);
           case "diff_reuse" -> Optional.of(TOKEN_LEMMAATTRIBUTEDIFFREUSE);
           case "use_induction" -> Optional.of(TOKEN_LEMMAATTRIBUTEINDUCTION);
           case "hide_lemma" -> Optional.of(TOKEN_LEMMAATTRIBUTEHIDELEMMA);
           case "private" -> Optional.of(TOKEN_FUNCTIONATTRIBUTE);
           case "destructor" -> Optional.of(TOKEN_FUNCTIONATTRIBUTE);
           default -> Optional.empty();
       }};
       type.ifPresent(tokenType -> parser.addToken(tokenType, token));
                }}
                               ''')
        listener.write(f"\tpublic JplagTamarinListener(Parser parser) {{ this.parser = parser; }}\n")
        listener.write(f"\tpublic void visitErrorNode(Error node) {{ }}\n}}")

def generate_tokentype(parser_rule_tokens, lexer_rule_tokens):
    with open('./output/TamarinTokenType.java', 'w') as f:
        f.write('''package de.jplag.tamarin;

import de.jplag.TokenType;
import org.antlr.v4.runtime.Token;

public enum TamarinTokenType implements TokenType {\n
\tCHILD("CHILD"),\n''')
        for token in parser_rule_tokens:
            f.write(f'\t{token}("{token}"),\n')
        for token in lexer_rule_tokens[0:-1]:
            f.write(f'\t{token}("{token}"),\n')
        f.write(f'\t{lexer_rule_tokens[-1]}("{lexer_rule_tokens[-1]}");\n')
        f.write('''    private final String description;

    @Override
    public String getDescription() {
        return description;
    }

    TamarinTokenType(String description) {
        this.description = description;
    }
}''')

def generate_parsetree(parser_rules, settings):
    with open('./output/ParseTreeNode.java', 'w') as f:
        f.write('''package de.jplag.tamarin;

import de.jplag.Token;
import de.jplag.TokenType;
import de.jplag.tamarin.grammar.TamarinParser;
import de.jplag.tamarin.grammar.TamarinLexer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static de.jplag.tamarin.TamarinTokenType.*;



abstract class ParseTreeNode implements Comparable<ParseTreeNode>{
    ArrayList<ParseTreeNode> Children;
    ParseTreeNode parent;
    ArrayList<Token> nodeTokens;
    int priority;
    ParseTreeNode() {
        Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
    }

    abstract void sort();
    List<Token> getTokens() {
        List<Token> tokens = new ArrayList<Token>();
        int childIndex = 0;
        for(int i = 0; i < nodeTokens.size(); i++) {
            if(nodeTokens.get(i).getType() == CHILD) {
                tokens.addAll(Children.get(childIndex).getTokens());
                childIndex++;
            }
            else {
                tokens.add(nodeTokens.get(i));
            }
        }
        return tokens;
    }
    void setParent(ParseTreeNode parent) {
        this.parent = parent;
    }
    void addChild(ParseTreeNode child) {
        Children.add(child);
    }
    void addToken(Token token)  {
        nodeTokens.add(token);
    }
    public int compareTo(ParseTreeNode other) {
        if (this.priority < other.priority) {
            return -1;
        }
        else if(this.priority > other.priority) {
            return 1;
        }
        else {
            this.sort();
            other.sort();
            for(int i = 0; i < Math.max(this.Children.size(), other.Children.size()); i++) {
                int comp = this.Children.get(i).compareTo(other.Children.get(i));
                if(comp != 0) {
                    return comp;
                }
            }
            return 0;
        }
    }
}

class InitialNode extends ParseTreeNode {

    InitialNode() {
        Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
        priority = -1;
    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
    }

    @Override
    void addToken(Token token)  {
        if(token.getType() == CHILD) {
            nodeTokens.add(token);
        }
        else {
            System.err.println("Trying to add token " + token.getType().getDescription() + " to initial node!");
        }
    }

}''')
        for i, rule in enumerate(parser_rules):
            f.write(f'class {capitalize(rule)}Node extends ParseTreeNode {{\n\n')
            prio = i if settings[rule]['relevant'] else 10000
            f.write(f'''\t{capitalize(rule)}Node() {{\n
\t\tChildren = new ArrayList<>();\n
\t\tnodeTokens = new ArrayList<>();\n
\t\tpriority = {prio};\n}}\n''')
            if settings[rule]['sort']:
                f.write(f'''\t@Override void sort() {{\n
\t\tChildren.forEach(c -> sort());\n
\t\tCollections.sort(Children);\n
                        }}\n\n''')
            if not settings[rule]['sort']:
                f.write(f'\t@Override void sort() {{ Children.forEach(c -> c.sort());}}\n\n')
            if not settings[rule]['relevant']:
                f.write(f'\t@Override List<Token> getTokens() {{ return new ArrayList<>(); }}\n\n')
            f.write('}\n\n')

class Editor():
    def __init__(self, parser_file_path, lexer_file_path, listener_file_path):

        self.parser_rules = read_parser_rules(parser_file_path)
        self.parser_rule_tokens = parser_rules_to_tokens(self.parser_rules)
        self.lexer_rules = read_lexer_rules(lexer_file_path)
        self.lexer_rule_tokens = lexer_rules_to_tokens(self.lexer_rules)
        self.listener_rules = extract_listener_rules(listener_file_path)
        for rule in self.listener_rules.keys():
            self.listener_rules[rule] = [x for x in self.parser_rules if rule.lower().endswith(x.lower())]
            if len(self.listener_rules[rule]) > 0:
                self.listener_rules[rule].sort(key=len, reverse=True)
                self.listener_rules[rule] = self.listener_rules[rule][0]


        self.settings = {r : {'sort' : False, 'generated_token' : r.upper(), 'relevant' : True, 'encapsulating_tokens' : True} for r in self.parser_rules}        

        self.window = Tk()
        self.title = Label(text="Jplag Configurator")
        self.title.pack()
        self.button_frame = Frame(self.window)
        self.button_frame.pack(side='top', fill='x')
        self.save_button = Button(self.button_frame, text='Save Settings', command=self.save_file)
        self.save_button.pack(side='left', fill='x', expand=True)
        self.load_button = Button(self.button_frame, text='Load Settings', command=self.load_file)
        self.load_button.pack(side='left', fill='x', expand=True)
        self.generate_button = Button(self.button_frame, text='Generate Files', command=self.generate_files)
        self.generate_button.pack(side='right', fill='x', expand=True)

        ttk.Separator(self.window, orient='horizontal').pack(fill='x')


        self.canvas = Canvas(self.window)
        self.sb = Scrollbar(self.window, orient='vertical', command=self.canvas.yview)
        self.frame = Frame(self.canvas)
        self.frame.pack(fill='both', expand=True)
        self.canvas.bind('<Enter>', self._bound_to_mousewheel)
        self.canvas.bind('<Leave>', self._unbound_to_mousewheel)

        self.sortVars = []
        self.relevantVars = []
        self.encapsulationVars = []
        self.cbox = []
        self.cbox_strings = []
        for i, rule in enumerate(self.parser_rules):
            label = Label(self.frame, text=rule, anchor='w').pack(fill='both')

            self.sortVars.append(Checkbutton(self.frame, text='sort', anchor='w', onvalue=True, offvalue=False))
            self.sortVars[i].bind('<Button-1>', partial(self.flip_sort, rule))
            if self.settings[rule]['sort']: self.sortVars[i].select()
            self.sortVars[i].pack(fill='both', padx=5)

            self.relevantVars.append(Checkbutton(self.frame, text='relevant', anchor='w', onvalue=True, offvalue=False))
            self.relevantVars[i].bind('<Button-1>', partial(self.flip_relevant, rule))
            if self.settings[rule]['relevant']: self.relevantVars[i].select()
            self.relevantVars[i].pack(fill='both', padx=5) 

            self.encapsulationVars.append(Checkbutton(self.frame, text='encapsulate', anchor='w'))
            self.encapsulationVars[i].bind('<Button-1>', partial(self.flip_encapsulating_tokens, rule)) 
            if self.settings[rule]['encapsulating_tokens']: self.encapsulationVars[i].select()
            self.encapsulationVars[i].pack(fill='both', padx=5)

            choices = [r for r in self.parser_rule_tokens if not '_BEGIN' in r and not '_END' in r] + [""]

            self.cbox_strings.append(StringVar(self.frame, value=rule.upper()))
            self.cbox.append(ttk.Combobox(self.frame, values=choices, textvariable=self.cbox_strings[i]))
            self.cbox[i].set(rule.upper())
            self.cbox_strings[i].trace_add("write", partial(self.cbox_string_changed, rule, i))
            self.cbox[i].pack(fill='both', padx=5)
            self.canvas.create_window(0, 0, anchor='nw', window=self.frame)
            self.canvas.update_idletasks()

        self.canvas.configure(scrollregion=self.canvas.bbox('all'), yscrollcommand=self.sb.set)
        self.sb.pack(side=RIGHT, fill='both')
        self.canvas.pack(side=LEFT, fill='both', expand=True)
    
        self.window.mainloop()
    
    def generate_files(self):
        generate_listener(self.listener_rules, self.settings)
        generate_parsetree(self.parser_rules, self.settings)
        generate_tokentype(self.parser_rule_tokens, self.lexer_rule_tokens)
        print('Generated Files!')

    def save_file(self):
        f = asksaveasfilename(initialfile='settings.json', defaultextension='.json,', filetypes=[('All Files', '*.*'), ('Json Documents', '*.json')])    
        with open(f, 'w') as f:
            f.write(json.dumps(self.settings))

    def load_file(self):
        f = askopenfilename(defaultextension='.json')
        with open(f, 'r') as f:
            self.settings = json.load(f)
            for i, rule in enumerate(self.settings.keys()):
                if self.settings[rule]['sort']:
                    self.sortVars[i].select()
                else:
                    self.sortVars[i].deselect()
                if self.settings[rule]['relevant']:
                    self.relevantVars[i].select()
                else:
                    self.relevantVars[i].deselect()
                if self.settings[rule]['encapsulating_tokens']:
                    self.encapsulationVars[i].select()
                else:
                    self.encapsulationVars[i].deselect()
                self.cbox_strings[i].set(self.settings[rule]['generated_token'])

    def flip_sort(self, rule, event):
        self.settings[rule]['sort'] = not(self.settings[rule]['sort'])
        print(f"put {self.settings[rule]['sort']} for {rule} at sort")
    
    def flip_relevant(self, rule, event):
        self.settings[rule]['relevant'] = not(self.settings[rule]['relevant'])
        print(f"put {self.settings[rule]['relevant']} for {rule} at relevant")

    def flip_encapsulating_tokens(self, rule, event):
        self.settings[rule]['encapsulating_tokens'] = not(self.settings[rule]['encapsulating_tokens'])
        print(f"put {self.settings[rule]['encapsulating_tokens']} for {rule} at encapsulating_tokens")
   
    def cbox_string_changed(self, rule, i, x, y, z):
        current_value = self.cbox_strings[i].get()
        self.settings[rule]['generated_token'] = current_value.upper()
        print(f"put {current_value} for {rule} at generated_token")
        self.cbox[i].configure(values= [r for r in self.parser_rule_tokens if not '_BEGIN' in r and not '_END' in r and current_value.upper() in r] + [""])
    
    def _bound_to_mousewheel(self, event):
        self.canvas.bind("<Button-4>", partial(self._on_mousewheel, direction=-1))
        self.canvas.bind("<Button-5>", partial(self._on_mousewheel, direction=1))

    def _unbound_to_mousewheel(self, event):
        self.canvas.unbind_all("<Button-4>")
        self.canvas.unbind_all("<Button-5>")
    
    def _on_mousewheel(self, event, direction):
        self.canvas.yview_scroll(direction*2, "units")


if __name__ == "__main__":
    parser_file_path = '/home/jastau/BT/code-JPlag-Extension-Tamarin/languages/tamarin/src/main/antlr4/de/jplag/tamarin/grammar/TamarinParser.g4'
    lexer_file_path = '/home/jastau/BT/code-JPlag-Extension-Tamarin/languages/tamarin/src/main/antlr4/de/jplag/tamarin/grammar/TamarinLexer.g4'
    listener_file_path = '/home/jastau/BT/code-JPlag-Extension-Tamarin/languages/tamarin/gen/TamarinParserBaseListener.java'
    Editor(parser_file_path, lexer_file_path, listener_file_path)