package de.jplag.tamarin;

import static de.jplag.tamarin.TamarinTokenType.*;
import de.jplag.tamarin.grammar.TamarinParserBaseListener;
import de.jplag.tamarin.grammar.TamarinParser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import javax.swing.text.html.Option;
import java.util.Optional;

public class JplagTamarinListener extends TamarinParserBaseListener {
    private final Parser parser;
	@Override public void enterTheory(TamarinParser.TheoryContext ctx) { 
		parser.addChildNode(new TheoryNode());
		parser.addToken(THEORY_BEGIN, ctx.getStart());
	}
	@Override public void exitTheory(TamarinParser.TheoryContext ctx) { 
		parser.addToken(THEORY_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBody(TamarinParser.BodyContext ctx) { 
		parser.addChildNode(new BodyNode());
		parser.addToken(BODY_BEGIN, ctx.getStart());
	}
	@Override public void exitBody(TamarinParser.BodyContext ctx) { 
		parser.addToken(BODY_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterDefine(TamarinParser.DefineContext ctx) { 
		parser.addChildNode(new DefineNode());
		parser.addToken(DEFINE, ctx.getStart());
	}
	@Override public void exitDefine(TamarinParser.DefineContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterInclude(TamarinParser.IncludeContext ctx) { 
		parser.addChildNode(new IncludeNode());
		parser.addToken(INCLUDE, ctx.getStart());
	}
	@Override public void exitInclude(TamarinParser.IncludeContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterIfdef(TamarinParser.IfdefContext ctx) { 
		parser.addChildNode(new IfdefNode());
		parser.addToken(IFDEF_BEGIN, ctx.getStart());
	}
	@Override public void exitIfdef(TamarinParser.IfdefContext ctx) { 
		parser.addToken(IFDEF_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFlagDisjuncts(TamarinParser.FlagDisjunctsContext ctx) { 
		parser.addChildNode(new FlagDisjunctsNode());
		parser.addToken(FLAGDISJUNCTS_BEGIN, ctx.getStart());
	}
	@Override public void exitFlagDisjuncts(TamarinParser.FlagDisjunctsContext ctx) { 
		parser.addToken(FLAGDISJUNCTS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFlagConjuncts(TamarinParser.FlagConjunctsContext ctx) { 
		parser.addChildNode(new FlagConjunctsNode());
		parser.addToken(FLAGCONJUNCTS_BEGIN, ctx.getStart());
	}
	@Override public void exitFlagConjuncts(TamarinParser.FlagConjunctsContext ctx) { 
		parser.addToken(FLAGCONJUNCTS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFlagNegation(TamarinParser.FlagNegationContext ctx) { 
		parser.addChildNode(new FlagNegationNode());
		parser.addToken(FLAGNEGATION_BEGIN, ctx.getStart());
	}
	@Override public void exitFlagNegation(TamarinParser.FlagNegationContext ctx) { 
		parser.addToken(FLAGNEGATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFlagAtom(TamarinParser.FlagAtomContext ctx) { 
		parser.addChildNode(new FlagAtomNode());
		parser.addToken(FLAGATOM_BEGIN, ctx.getStart());
	}
	@Override public void exitFlagAtom(TamarinParser.FlagAtomContext ctx) { 
		parser.addToken(FLAGATOM_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTupleTerm(TamarinParser.TupleTermContext ctx) { 
		parser.addChildNode(new TupleTermNode());
		parser.addToken(TUPLETERM_BEGIN, ctx.getStart());
	}
	@Override public void exitTupleTerm(TamarinParser.TupleTermContext ctx) { 
		parser.addToken(TUPLETERM_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterMsetterm(TamarinParser.MsettermContext ctx) { 
		parser.addChildNode(new MsettermNode());
		parser.addToken(MSETTERM_BEGIN, ctx.getStart());
	}
	@Override public void exitMsetterm(TamarinParser.MsettermContext ctx) { 
		parser.addToken(MSETTERM_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterXorterm(TamarinParser.XortermContext ctx) { 
		parser.addChildNode(new XortermNode());
		parser.addToken(XORTERM_BEGIN, ctx.getStart());
	}
	@Override public void exitXorterm(TamarinParser.XortermContext ctx) { 
		parser.addToken(XORTERM_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterMultterm(TamarinParser.MulttermContext ctx) { 
		parser.addChildNode(new MulttermNode());
		parser.addToken(MULTTERM_BEGIN, ctx.getStart());
	}
	@Override public void exitMultterm(TamarinParser.MulttermContext ctx) { 
		parser.addToken(MULTTERM_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterExpterm(TamarinParser.ExptermContext ctx) { 
		parser.addChildNode(new ExptermNode());
		parser.addToken(EXPTERM_BEGIN, ctx.getStart());
	}
	@Override public void exitExpterm(TamarinParser.ExptermContext ctx) { 
		parser.addToken(EXPTERM_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTerm_tupleterm(TamarinParser.Term_tupletermContext ctx) { 
		parser.addChildNode(new Term_tupletermNode());
		parser.addToken(TERM_TUPLETERM_BEGIN, ctx.getStart());
	}
	@Override public void exitTerm_tupleterm(TamarinParser.Term_tupletermContext ctx) { 
		parser.addToken(TERM_TUPLETERM_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTerm_msetterm(TamarinParser.Term_msettermContext ctx) { 
		parser.addChildNode(new Term_msettermNode());
		parser.addToken(TERM_MSETTERM_BEGIN, ctx.getStart());
	}
	@Override public void exitTerm_msetterm(TamarinParser.Term_msettermContext ctx) { 
		parser.addToken(TERM_MSETTERM_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTerm_oneop(TamarinParser.Term_oneopContext ctx) { 
		parser.addChildNode(new Term_oneopNode());
		parser.addToken(TERM_ONEOP, ctx.getStart());
	}
	@Override public void exitTerm_oneop(TamarinParser.Term_oneopContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTerm_dhneutral(TamarinParser.Term_dhneutralContext ctx) { 
		parser.addChildNode(new Term_dhneutralNode());
		parser.addToken(TERM_DHNEUTRAL, ctx.getStart());
	}
	@Override public void exitTerm_dhneutral(TamarinParser.Term_dhneutralContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTerm_application(TamarinParser.Term_applicationContext ctx) { 
		parser.addChildNode(new Term_applicationNode());
		parser.addToken(TERM_APPLICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitTerm_application(TamarinParser.Term_applicationContext ctx) { 
		parser.addToken(TERM_APPLICATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTerm_nullary(TamarinParser.Term_nullaryContext ctx) { 
		parser.addChildNode(new Term_nullaryNode());
		parser.addToken(TERM_NULLARY_BEGIN, ctx.getStart());
	}
	@Override public void exitTerm_nullary(TamarinParser.Term_nullaryContext ctx) { 
		parser.addToken(TERM_NULLARY_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTerm_literal(TamarinParser.Term_literalContext ctx) { 
		parser.addChildNode(new Term_literalNode());
		parser.addToken(TERM_LITERAL_BEGIN, ctx.getStart());
	}
	@Override public void exitTerm_literal(TamarinParser.Term_literalContext ctx) { 
		parser.addToken(TERM_LITERAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterApplication(TamarinParser.ApplicationContext ctx) { 
		parser.addChildNode(new ApplicationNode());
		parser.addToken(APPLICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitApplication(TamarinParser.ApplicationContext ctx) { 
		parser.addToken(APPLICATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterNaryOpApplication(TamarinParser.NaryOpApplicationContext ctx) { 
		parser.addChildNode(new NaryOpApplicationNode());
		parser.addToken(NARYOPAPPLICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitNaryOpApplication(TamarinParser.NaryOpApplicationContext ctx) { 
		parser.addToken(NARYOPAPPLICATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterUnaryOpApplication(TamarinParser.UnaryOpApplicationContext ctx) { 
		parser.addChildNode(new UnaryOpApplicationNode());
		parser.addToken(UNARYOPAPPLICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitUnaryOpApplication(TamarinParser.UnaryOpApplicationContext ctx) { 
		parser.addToken(UNARYOPAPPLICATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterNonUnaryOpApplication(TamarinParser.NonUnaryOpApplicationContext ctx) { 
		parser.addChildNode(new NonUnaryOpApplicationNode());
		parser.addToken(NONUNARYOPAPPLICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitNonUnaryOpApplication(TamarinParser.NonUnaryOpApplicationContext ctx) { 
		parser.addToken(NONUNARYOPAPPLICATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBinaryAlgApplication(TamarinParser.BinaryAlgApplicationContext ctx) { 
		parser.addChildNode(new BinaryAlgApplicationNode());
		parser.addToken(BINARYALGAPPLICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitBinaryAlgApplication(TamarinParser.BinaryAlgApplicationContext ctx) { 
		parser.addToken(BINARYALGAPPLICATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterNullaryApp_One(TamarinParser.NullaryApp_OneContext ctx) { 
		parser.addChildNode(new NullaryApp_OneNode());
		parser.addToken(NULLARYAPP_ONE, ctx.getStart());
	}
	@Override public void exitNullaryApp_One(TamarinParser.NullaryApp_OneContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterNullaryApp_DH(TamarinParser.NullaryApp_DHContext ctx) { 
		parser.addChildNode(new NullaryApp_DHNode());
		parser.addToken(NULLARYAPP_DH, ctx.getStart());
	}
	@Override public void exitNullaryApp_DH(TamarinParser.NullaryApp_DHContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterNullaryApp_Zero(TamarinParser.NullaryApp_ZeroContext ctx) { 
		parser.addChildNode(new NullaryApp_ZeroNode());
		parser.addToken(NULLARYAPP_ZERO, ctx.getStart());
	}
	@Override public void exitNullaryApp_Zero(TamarinParser.NullaryApp_ZeroContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterReservedBuiltins(TamarinParser.ReservedBuiltinsContext ctx) { 
		parser.addChildNode(new ReservedBuiltinsNode());
		parser.addToken(RESERVEDBUILTINS_BEGIN, ctx.getStart());
	}
	@Override public void exitReservedBuiltins(TamarinParser.ReservedBuiltinsContext ctx) { 
		parser.addToken(RESERVEDBUILTINS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterGenericRule(TamarinParser.GenericRuleContext ctx) { 
		parser.addChildNode(new GenericRuleNode());
		parser.addToken(GENERICRULE_BEGIN, ctx.getStart());
	}
	@Override public void exitGenericRule(TamarinParser.GenericRuleContext ctx) { 
		parser.addToken(GENERICRULE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterIntruderRule(TamarinParser.IntruderRuleContext ctx) { 
		parser.addChildNode(new IntruderRuleNode());
		parser.addToken(INTRUDERRULE_BEGIN, ctx.getStart());
	}
	@Override public void exitIntruderRule(TamarinParser.IntruderRuleContext ctx) { 
		parser.addToken(INTRUDERRULE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterIntruderInfo(TamarinParser.IntruderInfoContext ctx) { 
		parser.addChildNode(new IntruderInfoNode());
		parser.addToken(INTRUDERINFO_BEGIN, ctx.getStart());
	}
	@Override public void exitIntruderInfo(TamarinParser.IntruderInfoContext ctx) { 
		parser.addToken(INTRUDERINFO_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterProtoRuleAC(TamarinParser.ProtoRuleACContext ctx) { 
		parser.addChildNode(new ProtoRuleACNode());
		parser.addToken(PROTORULEAC_BEGIN, ctx.getStart());
	}
	@Override public void exitProtoRuleAC(TamarinParser.ProtoRuleACContext ctx) { 
		parser.addToken(PROTORULEAC_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterProtoRuleACInfo(TamarinParser.ProtoRuleACInfoContext ctx) { 
		parser.addChildNode(new ProtoRuleACInfoNode());
		parser.addToken(PROTORULEACINFO_BEGIN, ctx.getStart());
	}
	@Override public void exitProtoRuleACInfo(TamarinParser.ProtoRuleACInfoContext ctx) { 
		parser.addToken(PROTORULEACINFO_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterProtoRule(TamarinParser.ProtoRuleContext ctx) { 
		parser.addChildNode(new ProtoRuleNode());
		parser.addToken(PROTORULE_BEGIN, ctx.getStart());
	}
	@Override public void exitProtoRule(TamarinParser.ProtoRuleContext ctx) { 
		parser.addToken(PROTORULE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterProtoRuleInfo(TamarinParser.ProtoRuleInfoContext ctx) { 
		parser.addChildNode(new ProtoRuleInfoNode());
		parser.addToken(PROTORULEINFO_BEGIN, ctx.getStart());
	}
	@Override public void exitProtoRuleInfo(TamarinParser.ProtoRuleInfoContext ctx) { 
		parser.addToken(PROTORULEINFO_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterRuleAttributes(TamarinParser.RuleAttributesContext ctx) { 
		parser.addChildNode(new RuleAttributesNode());
		parser.addToken(RULEATTRIBUTES_BEGIN, ctx.getStart());
	}
	@Override public void exitRuleAttributes(TamarinParser.RuleAttributesContext ctx) { 
		parser.addToken(RULEATTRIBUTES_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterRuleAttribute(TamarinParser.RuleAttributeContext ctx) { 
		parser.addChildNode(new RuleAttributeNode());
		parser.addToken(RULEATTRIBUTE_BEGIN, ctx.getStart());
	}
	@Override public void exitRuleAttribute(TamarinParser.RuleAttributeContext ctx) { 
		parser.addToken(RULEATTRIBUTE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterEmbeddedRestriction(TamarinParser.EmbeddedRestrictionContext ctx) { 
		parser.addChildNode(new EmbeddedRestrictionNode());
		parser.addToken(EMBEDDEDRESTRICTION_BEGIN, ctx.getStart());
	}
	@Override public void exitEmbeddedRestriction(TamarinParser.EmbeddedRestrictionContext ctx) { 
		parser.addToken(EMBEDDEDRESTRICTION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFactOrRestriction(TamarinParser.FactOrRestrictionContext ctx) { 
		parser.addChildNode(new FactOrRestrictionNode());
		parser.addToken(FACTORRESTRICTION_BEGIN, ctx.getStart());
	}
	@Override public void exitFactOrRestriction(TamarinParser.FactOrRestrictionContext ctx) { 
		parser.addToken(FACTORRESTRICTION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterReservedRuleNames(TamarinParser.ReservedRuleNamesContext ctx) { 
		parser.addChildNode(new ReservedRuleNamesNode());
		parser.addToken(RESERVEDRULENAMES_BEGIN, ctx.getStart());
	}
	@Override public void exitReservedRuleNames(TamarinParser.ReservedRuleNamesContext ctx) { 
		parser.addToken(RESERVEDRULENAMES_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterRuleFresh(TamarinParser.RuleFreshContext ctx) { 
		parser.addChildNode(new RuleFreshNode());
		parser.addToken(RULEFRESH, ctx.getStart());
	}
	@Override public void exitRuleFresh(TamarinParser.RuleFreshContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterRulePub(TamarinParser.RulePubContext ctx) { 
		parser.addChildNode(new RulePubNode());
		parser.addToken(RULEPUB, ctx.getStart());
	}
	@Override public void exitRulePub(TamarinParser.RulePubContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterHexColor(TamarinParser.HexColorContext ctx) { 
		parser.addChildNode(new HexColorNode());
		parser.addToken(HEXCOLOR_BEGIN, ctx.getStart());
	}
	@Override public void exitHexColor(TamarinParser.HexColorContext ctx) { 
		parser.addToken(HEXCOLOR_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterModuloE(TamarinParser.ModuloEContext ctx) { 
		parser.addChildNode(new ModuloENode());
		parser.addToken(MODULOE_BEGIN, ctx.getStart());
	}
	@Override public void exitModuloE(TamarinParser.ModuloEContext ctx) { 
		parser.addToken(MODULOE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterModuloAC(TamarinParser.ModuloACContext ctx) { 
		parser.addChildNode(new ModuloACNode());
		parser.addToken(MODULOAC_BEGIN, ctx.getStart());
	}
	@Override public void exitModuloAC(TamarinParser.ModuloACContext ctx) { 
		parser.addToken(MODULOAC_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFactList(TamarinParser.FactListContext ctx) { 
		parser.addChildNode(new FactListNode());
		parser.addToken(FACTLIST_BEGIN, ctx.getStart());
	}
	@Override public void exitFactList(TamarinParser.FactListContext ctx) { 
		parser.addToken(FACTLIST_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFact(TamarinParser.FactContext ctx) { 
		parser.addChildNode(new FactNode());
		parser.addToken(FACT_BEGIN, ctx.getStart());
	}
	@Override public void exitFact(TamarinParser.FactContext ctx) { 
		parser.addToken(FACT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFreshFact(TamarinParser.FreshFactContext ctx) { 
		parser.addChildNode(new FreshFactNode());
		parser.addToken(FRESHFACT_BEGIN, ctx.getStart());
	}
	@Override public void exitFreshFact(TamarinParser.FreshFactContext ctx) { 
		parser.addToken(FRESHFACT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterReservedFact(TamarinParser.ReservedFactContext ctx) { 
		parser.addChildNode(new ReservedFactNode());
		parser.addToken(RESERVEDFACT_BEGIN, ctx.getStart());
	}
	@Override public void exitReservedFact(TamarinParser.ReservedFactContext ctx) { 
		parser.addToken(RESERVEDFACT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterProtoFact(TamarinParser.ProtoFactContext ctx) { 
		parser.addChildNode(new ProtoFactNode());
		parser.addToken(PROTOFACT_BEGIN, ctx.getStart());
	}
	@Override public void exitProtoFact(TamarinParser.ProtoFactContext ctx) { 
		parser.addToken(PROTOFACT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterMultiplicity(TamarinParser.MultiplicityContext ctx) { 
		parser.addChildNode(new MultiplicityNode());
		parser.addToken(MULTIPLICITY, ctx.getStart());
	}
	@Override public void exitMultiplicity(TamarinParser.MultiplicityContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterFactAnnotations(TamarinParser.FactAnnotationsContext ctx) { 
		parser.addChildNode(new FactAnnotationsNode());
		parser.addToken(FACTANNOTATIONS_BEGIN, ctx.getStart());
	}
	@Override public void exitFactAnnotations(TamarinParser.FactAnnotationsContext ctx) { 
		parser.addToken(FACTANNOTATIONS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFact_annotation_plus(TamarinParser.Fact_annotation_plusContext ctx) { 
		parser.addChildNode(new Fact_annotation_plusNode());
		parser.addToken(FACT_ANNOTATION_PLUS_BEGIN, ctx.getStart());
	}
	@Override public void exitFact_annotation_plus(TamarinParser.Fact_annotation_plusContext ctx) { 
		parser.addToken(FACT_ANNOTATION_PLUS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFact_annotation_minus(TamarinParser.Fact_annotation_minusContext ctx) { 
		parser.addChildNode(new Fact_annotation_minusNode());
		parser.addToken(FACT_ANNOTATION_MINUS_BEGIN, ctx.getStart());
	}
	@Override public void exitFact_annotation_minus(TamarinParser.Fact_annotation_minusContext ctx) { 
		parser.addToken(FACT_ANNOTATION_MINUS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFact_annotation_noprecomp(TamarinParser.Fact_annotation_noprecompContext ctx) { 
		parser.addChildNode(new Fact_annotation_noprecompNode());
		parser.addToken(FACT_ANNOTATION_NOPRECOMP_BEGIN, ctx.getStart());
	}
	@Override public void exitFact_annotation_noprecomp(TamarinParser.Fact_annotation_noprecompContext ctx) { 
		parser.addToken(FACT_ANNOTATION_NOPRECOMP_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemmaAttributes(TamarinParser.LemmaAttributesContext ctx) { 
		parser.addChildNode(new LemmaAttributesNode());
		parser.addToken(LEMMAATTRIBUTES_BEGIN, ctx.getStart());
	}
	@Override public void exitLemmaAttributes(TamarinParser.LemmaAttributesContext ctx) { 
		parser.addToken(LEMMAATTRIBUTES_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemma_attribute_typing(TamarinParser.Lemma_attribute_typingContext ctx) { 
		parser.addChildNode(new Lemma_attribute_typingNode());
		parser.addToken(LEMMA_ATTRIBUTE_TYPING_BEGIN, ctx.getStart());
	}
	@Override public void exitLemma_attribute_typing(TamarinParser.Lemma_attribute_typingContext ctx) { 
		parser.addToken(LEMMA_ATTRIBUTE_TYPING_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemma_attribute_sources(TamarinParser.Lemma_attribute_sourcesContext ctx) { 
		parser.addChildNode(new Lemma_attribute_sourcesNode());
		parser.addToken(LEMMA_ATTRIBUTE_SOURCES_BEGIN, ctx.getStart());
	}
	@Override public void exitLemma_attribute_sources(TamarinParser.Lemma_attribute_sourcesContext ctx) { 
		parser.addToken(LEMMA_ATTRIBUTE_SOURCES_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemma_attribute_reuse(TamarinParser.Lemma_attribute_reuseContext ctx) { 
		parser.addChildNode(new Lemma_attribute_reuseNode());
		parser.addToken(LEMMA_ATTRIBUTE_REUSE_BEGIN, ctx.getStart());
	}
	@Override public void exitLemma_attribute_reuse(TamarinParser.Lemma_attribute_reuseContext ctx) { 
		parser.addToken(LEMMA_ATTRIBUTE_REUSE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemma_attribute_diff_reuse(TamarinParser.Lemma_attribute_diff_reuseContext ctx) { 
		parser.addChildNode(new Lemma_attribute_diff_reuseNode());
		parser.addToken(LEMMA_ATTRIBUTE_DIFF_REUSE_BEGIN, ctx.getStart());
	}
	@Override public void exitLemma_attribute_diff_reuse(TamarinParser.Lemma_attribute_diff_reuseContext ctx) { 
		parser.addToken(LEMMA_ATTRIBUTE_DIFF_REUSE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemma_attribute_induction(TamarinParser.Lemma_attribute_inductionContext ctx) { 
		parser.addChildNode(new Lemma_attribute_inductionNode());
		parser.addToken(LEMMA_ATTRIBUTE_INDUCTION_BEGIN, ctx.getStart());
	}
	@Override public void exitLemma_attribute_induction(TamarinParser.Lemma_attribute_inductionContext ctx) { 
		parser.addToken(LEMMA_ATTRIBUTE_INDUCTION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemma_attribute_hide_lemma(TamarinParser.Lemma_attribute_hide_lemmaContext ctx) { 
		parser.addChildNode(new Lemma_attribute_hide_lemmaNode());
		parser.addToken(LEMMA_ATTRIBUTE_HIDE_LEMMA_BEGIN, ctx.getStart());
	}
	@Override public void exitLemma_attribute_hide_lemma(TamarinParser.Lemma_attribute_hide_lemmaContext ctx) { 
		parser.addToken(LEMMA_ATTRIBUTE_HIDE_LEMMA_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemma_attribute_heuristic(TamarinParser.Lemma_attribute_heuristicContext ctx) { 
		parser.addChildNode(new Lemma_attribute_heuristicNode());
		parser.addToken(LEMMA_ATTRIBUTE_HEURISTIC_BEGIN, ctx.getStart());
	}
	@Override public void exitLemma_attribute_heuristic(TamarinParser.Lemma_attribute_heuristicContext ctx) { 
		parser.addToken(LEMMA_ATTRIBUTE_HEURISTIC_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemma_attribute_output(TamarinParser.Lemma_attribute_outputContext ctx) { 
		parser.addChildNode(new Lemma_attribute_outputNode());
		parser.addToken(LEMMA_ATTRIBUTE_OUTPUT_BEGIN, ctx.getStart());
	}
	@Override public void exitLemma_attribute_output(TamarinParser.Lemma_attribute_outputContext ctx) { 
		parser.addToken(LEMMA_ATTRIBUTE_OUTPUT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemma_attribute_left(TamarinParser.Lemma_attribute_leftContext ctx) { 
		parser.addChildNode(new Lemma_attribute_leftNode());
		parser.addToken(LEMMA_ATTRIBUTE_LEFT_BEGIN, ctx.getStart());
	}
	@Override public void exitLemma_attribute_left(TamarinParser.Lemma_attribute_leftContext ctx) { 
		parser.addToken(LEMMA_ATTRIBUTE_LEFT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemma_attribute_right(TamarinParser.Lemma_attribute_rightContext ctx) { 
		parser.addChildNode(new Lemma_attribute_rightNode());
		parser.addToken(LEMMA_ATTRIBUTE_RIGHT_BEGIN, ctx.getStart());
	}
	@Override public void exitLemma_attribute_right(TamarinParser.Lemma_attribute_rightContext ctx) { 
		parser.addToken(LEMMA_ATTRIBUTE_RIGHT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemmaAttributeTyping(TamarinParser.LemmaAttributeTypingContext ctx) { 
		parser.addChildNode(new LemmaAttributeTypingNode());
		parser.addToken(LEMMAATTRIBUTETYPING_BEGIN, ctx.getStart());
	}
	@Override public void exitLemmaAttributeTyping(TamarinParser.LemmaAttributeTypingContext ctx) { 
		parser.addToken(LEMMAATTRIBUTETYPING_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemmaAttributeSources(TamarinParser.LemmaAttributeSourcesContext ctx) { 
		parser.addChildNode(new LemmaAttributeSourcesNode());
		parser.addToken(LEMMAATTRIBUTESOURCES_BEGIN, ctx.getStart());
	}
	@Override public void exitLemmaAttributeSources(TamarinParser.LemmaAttributeSourcesContext ctx) { 
		parser.addToken(LEMMAATTRIBUTESOURCES_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemmaAttributeReuse(TamarinParser.LemmaAttributeReuseContext ctx) { 
		parser.addChildNode(new LemmaAttributeReuseNode());
		parser.addToken(LEMMAATTRIBUTEREUSE_BEGIN, ctx.getStart());
	}
	@Override public void exitLemmaAttributeReuse(TamarinParser.LemmaAttributeReuseContext ctx) { 
		parser.addToken(LEMMAATTRIBUTEREUSE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemmaAttributeDiffReuse(TamarinParser.LemmaAttributeDiffReuseContext ctx) { 
		parser.addChildNode(new LemmaAttributeDiffReuseNode());
		parser.addToken(LEMMAATTRIBUTEDIFFREUSE_BEGIN, ctx.getStart());
	}
	@Override public void exitLemmaAttributeDiffReuse(TamarinParser.LemmaAttributeDiffReuseContext ctx) { 
		parser.addToken(LEMMAATTRIBUTEDIFFREUSE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemmaAttributeInduction(TamarinParser.LemmaAttributeInductionContext ctx) { 
		parser.addChildNode(new LemmaAttributeInductionNode());
		parser.addToken(LEMMAATTRIBUTEINDUCTION_BEGIN, ctx.getStart());
	}
	@Override public void exitLemmaAttributeInduction(TamarinParser.LemmaAttributeInductionContext ctx) { 
		parser.addToken(LEMMAATTRIBUTEINDUCTION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemmaAttributeHideLemma(TamarinParser.LemmaAttributeHideLemmaContext ctx) { 
		parser.addChildNode(new LemmaAttributeHideLemmaNode());
		parser.addToken(LEMMAATTRIBUTEHIDELEMMA_BEGIN, ctx.getStart());
	}
	@Override public void exitLemmaAttributeHideLemma(TamarinParser.LemmaAttributeHideLemmaContext ctx) { 
		parser.addToken(LEMMAATTRIBUTEHIDELEMMA_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemmaAttributeHeuristic(TamarinParser.LemmaAttributeHeuristicContext ctx) { 
		parser.addChildNode(new LemmaAttributeHeuristicNode());
		parser.addToken(LEMMAATTRIBUTEHEURISTIC_BEGIN, ctx.getStart());
	}
	@Override public void exitLemmaAttributeHeuristic(TamarinParser.LemmaAttributeHeuristicContext ctx) { 
		parser.addToken(LEMMAATTRIBUTEHEURISTIC_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemmaAttributeOutput(TamarinParser.LemmaAttributeOutputContext ctx) { 
		parser.addChildNode(new LemmaAttributeOutputNode());
		parser.addToken(LEMMAATTRIBUTEOUTPUT_BEGIN, ctx.getStart());
	}
	@Override public void exitLemmaAttributeOutput(TamarinParser.LemmaAttributeOutputContext ctx) { 
		parser.addToken(LEMMAATTRIBUTEOUTPUT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTraceQuantifier(TamarinParser.TraceQuantifierContext ctx) { 
		parser.addChildNode(new TraceQuantifierNode());
		parser.addToken(TRACEQUANTIFIER_BEGIN, ctx.getStart());
	}
	@Override public void exitTraceQuantifier(TamarinParser.TraceQuantifierContext ctx) { 
		parser.addToken(TRACEQUANTIFIER_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterForallTraces(TamarinParser.ForallTracesContext ctx) { 
		parser.addChildNode(new ForallTracesNode());
		parser.addToken(FORALLTRACES, ctx.getStart());
	}
	@Override public void exitForallTraces(TamarinParser.ForallTracesContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterExistsTrace(TamarinParser.ExistsTraceContext ctx) { 
		parser.addChildNode(new ExistsTraceNode());
		parser.addToken(EXISTSTRACE, ctx.getStart());
	}
	@Override public void exitExistsTrace(TamarinParser.ExistsTraceContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLemma(TamarinParser.LemmaContext ctx) { 
		parser.addChildNode(new LemmaNode());
		parser.addToken(LEMMA_BEGIN, ctx.getStart());
	}
	@Override public void exitLemma(TamarinParser.LemmaContext ctx) { 
		parser.addToken(LEMMA_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterStandardFormula(TamarinParser.StandardFormulaContext ctx) { 
		parser.addChildNode(new StandardFormulaNode());
		parser.addToken(STANDARDFORMULA_BEGIN, ctx.getStart());
	}
	@Override public void exitStandardFormula(TamarinParser.StandardFormulaContext ctx) { 
		parser.addToken(STANDARDFORMULA_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterImplication(TamarinParser.ImplicationContext ctx) { 
		parser.addChildNode(new ImplicationNode());
		parser.addToken(IMPLICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitImplication(TamarinParser.ImplicationContext ctx) { 
		parser.addToken(IMPLICATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterDisjunction(TamarinParser.DisjunctionContext ctx) { 
		parser.addChildNode(new DisjunctionNode());
		parser.addToken(DISJUNCTION_BEGIN, ctx.getStart());
	}
	@Override public void exitDisjunction(TamarinParser.DisjunctionContext ctx) { 
		parser.addToken(DISJUNCTION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterConjunction(TamarinParser.ConjunctionContext ctx) { 
		parser.addChildNode(new ConjunctionNode());
		parser.addToken(CONJUNCTION_BEGIN, ctx.getStart());
	}
	@Override public void exitConjunction(TamarinParser.ConjunctionContext ctx) { 
		parser.addToken(CONJUNCTION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterNegation(TamarinParser.NegationContext ctx) { 
		parser.addChildNode(new NegationNode());
		parser.addToken(NEGATION_BEGIN, ctx.getStart());
	}
	@Override public void exitNegation(TamarinParser.NegationContext ctx) { 
		parser.addToken(NEGATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFatom_bot(TamarinParser.Fatom_botContext ctx) { 
		parser.addChildNode(new Fatom_botNode());
		parser.addToken(BOTOP_BEGIN, ctx.getStart());
	}
	@Override public void exitFatom_bot(TamarinParser.Fatom_botContext ctx) { 
		parser.addToken(BOTOP_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFatom_top(TamarinParser.Fatom_topContext ctx) { 
		parser.addChildNode(new Fatom_topNode());
		parser.addToken(TOPOP_BEGIN, ctx.getStart());
	}
	@Override public void exitFatom_top(TamarinParser.Fatom_topContext ctx) { 
		parser.addToken(TOPOP_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFatom_blatom(TamarinParser.Fatom_blatomContext ctx) { 
		parser.addChildNode(new Fatom_blatomNode());
		parser.addToken(FATOM_BLATOM_BEGIN, ctx.getStart());
	}
	@Override public void exitFatom_blatom(TamarinParser.Fatom_blatomContext ctx) { 
		parser.addToken(FATOM_BLATOM_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFatom_quantification(TamarinParser.Fatom_quantificationContext ctx) { 
		parser.addChildNode(new Fatom_quantificationNode());
		parser.addToken(FATOM_QUANTIFICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitFatom_quantification(TamarinParser.Fatom_quantificationContext ctx) { 
		parser.addToken(FATOM_QUANTIFICATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFatom_formula(TamarinParser.Fatom_formulaContext ctx) { 
		parser.addChildNode(new Fatom_formulaNode());
		parser.addToken(FATOM_FORMULA_BEGIN, ctx.getStart());
	}
	@Override public void exitFatom_formula(TamarinParser.Fatom_formulaContext ctx) { 
		parser.addToken(FATOM_FORMULA_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBlatom_last(TamarinParser.Blatom_lastContext ctx) { 
		parser.addChildNode(new Blatom_lastNode());
		parser.addToken(BLATOM_LAST_BEGIN, ctx.getStart());
	}
	@Override public void exitBlatom_last(TamarinParser.Blatom_lastContext ctx) { 
		parser.addToken(BLATOM_LAST_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBlatom_action(TamarinParser.Blatom_actionContext ctx) { 
		parser.addChildNode(new Blatom_actionNode());
		parser.addToken(BLATOM_ACTION_BEGIN, ctx.getStart());
	}
	@Override public void exitBlatom_action(TamarinParser.Blatom_actionContext ctx) { 
		parser.addToken(BLATOM_ACTION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBlatom_predicate(TamarinParser.Blatom_predicateContext ctx) { 
		parser.addChildNode(new Blatom_predicateNode());
		parser.addToken(BLATOM_PREDICATE_BEGIN, ctx.getStart());
	}
	@Override public void exitBlatom_predicate(TamarinParser.Blatom_predicateContext ctx) { 
		parser.addToken(BLATOM_PREDICATE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBlatom_less(TamarinParser.Blatom_lessContext ctx) { 
		parser.addChildNode(new Blatom_lessNode());
		parser.addToken(BLATOM_LESS_BEGIN, ctx.getStart());
	}
	@Override public void exitBlatom_less(TamarinParser.Blatom_lessContext ctx) { 
		parser.addToken(BLATOM_LESS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBlatom_smallerp(TamarinParser.Blatom_smallerpContext ctx) { 
		parser.addChildNode(new Blatom_smallerpNode());
		parser.addToken(BLATOM_SMALLERP_BEGIN, ctx.getStart());
	}
	@Override public void exitBlatom_smallerp(TamarinParser.Blatom_smallerpContext ctx) { 
		parser.addToken(BLATOM_SMALLERP_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBlatom_node_eq(TamarinParser.Blatom_node_eqContext ctx) { 
		parser.addChildNode(new Blatom_node_eqNode());
		parser.addToken(BLATOM_NODE_EQ_BEGIN, ctx.getStart());
	}
	@Override public void exitBlatom_node_eq(TamarinParser.Blatom_node_eqContext ctx) { 
		parser.addToken(BLATOM_NODE_EQ_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBlatom_term_eq(TamarinParser.Blatom_term_eqContext ctx) { 
		parser.addChildNode(new Blatom_term_eqNode());
		parser.addToken(BLATOM_TERM_EQ_BEGIN, ctx.getStart());
	}
	@Override public void exitBlatom_term_eq(TamarinParser.Blatom_term_eqContext ctx) { 
		parser.addToken(BLATOM_TERM_EQ_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterQuantification(TamarinParser.QuantificationContext ctx) { 
		parser.addChildNode(new QuantificationNode());
		parser.addToken(QUANTIFICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitQuantification(TamarinParser.QuantificationContext ctx) { 
		parser.addToken(QUANTIFICATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSmallerp(TamarinParser.SmallerpContext ctx) { 
		parser.addChildNode(new SmallerpNode());
		parser.addToken(SMALLERP_BEGIN, ctx.getStart());
	}
	@Override public void exitSmallerp(TamarinParser.SmallerpContext ctx) { 
		parser.addToken(SMALLERP_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterCaseTest(TamarinParser.CaseTestContext ctx) { 
		parser.addChildNode(new CaseTestNode());
		parser.addToken(CASETEST_BEGIN, ctx.getStart());
	}
	@Override public void exitCaseTest(TamarinParser.CaseTestContext ctx) { 
		parser.addToken(CASETEST_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLemmaAcc(TamarinParser.LemmaAccContext ctx) { 
		parser.addChildNode(new LemmaAccNode());
		parser.addToken(LEMMAACC_BEGIN, ctx.getStart());
	}
	@Override public void exitLemmaAcc(TamarinParser.LemmaAccContext ctx) { 
		parser.addToken(LEMMAACC_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTactic(TamarinParser.TacticContext ctx) { 
		parser.addChildNode(new TacticNode());
		parser.addToken(TACTIC_BEGIN, ctx.getStart());
	}
	@Override public void exitTactic(TamarinParser.TacticContext ctx) { 
		parser.addToken(TACTIC_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterDeprio(TamarinParser.DeprioContext ctx) { 
		parser.addChildNode(new DeprioNode());
		parser.addToken(DEPRIO_BEGIN, ctx.getStart());
	}
	@Override public void exitDeprio(TamarinParser.DeprioContext ctx) { 
		parser.addToken(DEPRIO_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterPrio(TamarinParser.PrioContext ctx) { 
		parser.addChildNode(new PrioNode());
		parser.addToken(PRIO_BEGIN, ctx.getStart());
	}
	@Override public void exitPrio(TamarinParser.PrioContext ctx) { 
		parser.addToken(PRIO_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTacticDisjuncts(TamarinParser.TacticDisjunctsContext ctx) { 
		parser.addChildNode(new TacticDisjunctsNode());
		parser.addToken(TACTICDISJUNCTS_BEGIN, ctx.getStart());
	}
	@Override public void exitTacticDisjuncts(TamarinParser.TacticDisjunctsContext ctx) { 
		parser.addToken(TACTICDISJUNCTS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTacticConjuncts(TamarinParser.TacticConjunctsContext ctx) { 
		parser.addChildNode(new TacticConjunctsNode());
		parser.addToken(TACTICCONJUNCTS_BEGIN, ctx.getStart());
	}
	@Override public void exitTacticConjuncts(TamarinParser.TacticConjunctsContext ctx) { 
		parser.addToken(TACTICCONJUNCTS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTacticNegation(TamarinParser.TacticNegationContext ctx) { 
		parser.addChildNode(new TacticNegationNode());
		parser.addToken(TACTICNEGATION_BEGIN, ctx.getStart());
	}
	@Override public void exitTacticNegation(TamarinParser.TacticNegationContext ctx) { 
		parser.addToken(TACTICNEGATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTacticFunction(TamarinParser.TacticFunctionContext ctx) { 
		parser.addChildNode(new TacticFunctionNode());
		parser.addToken(TACTICFUNCTION_BEGIN, ctx.getStart());
	}
	@Override public void exitTacticFunction(TamarinParser.TacticFunctionContext ctx) { 
		parser.addToken(TACTICFUNCTION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSelectedPresort(TamarinParser.SelectedPresortContext ctx) { 
		parser.addChildNode(new SelectedPresortNode());
		parser.addToken(SELECTEDPRESORT_BEGIN, ctx.getStart());
	}
	@Override public void exitSelectedPresort(TamarinParser.SelectedPresortContext ctx) { 
		parser.addToken(SELECTEDPRESORT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterGoalRankingPresort(TamarinParser.GoalRankingPresortContext ctx) { 
		parser.addChildNode(new GoalRankingPresortNode());
		parser.addToken(GOALRANKINGPRESORT_BEGIN, ctx.getStart());
	}
	@Override public void exitGoalRankingPresort(TamarinParser.GoalRankingPresortContext ctx) { 
		parser.addToken(GOALRANKINGPRESORT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTacticFunctionName(TamarinParser.TacticFunctionNameContext ctx) { 
		parser.addChildNode(new TacticFunctionNameNode());
		parser.addToken(TACTICFUNCTIONNAME_BEGIN, ctx.getStart());
	}
	@Override public void exitTacticFunctionName(TamarinParser.TacticFunctionNameContext ctx) { 
		parser.addToken(TACTICFUNCTIONNAME_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterRanking_smallest(TamarinParser.Ranking_smallestContext ctx) { 
		parser.addChildNode(new Ranking_smallestNode());
		parser.addToken(RANKING_SMALLEST_BEGIN, ctx.getStart());
	}
	@Override public void exitRanking_smallest(TamarinParser.Ranking_smallestContext ctx) { 
		parser.addToken(RANKING_SMALLEST_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterRanking_id(TamarinParser.Ranking_idContext ctx) { 
		parser.addChildNode(new Ranking_idNode());
		parser.addToken(RANKING_ID_BEGIN, ctx.getStart());
	}
	@Override public void exitRanking_id(TamarinParser.Ranking_idContext ctx) { 
		parser.addToken(RANKING_ID_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTacticName(TamarinParser.TacticNameContext ctx) { 
		parser.addChildNode(new TacticNameNode());
		parser.addToken(TACTICNAME_BEGIN, ctx.getStart());
	}
	@Override public void exitTacticName(TamarinParser.TacticNameContext ctx) { 
		parser.addToken(TACTICNAME_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterHeuristic(TamarinParser.HeuristicContext ctx) { 
		parser.addChildNode(new HeuristicNode());
		parser.addToken(HEURISTIC_BEGIN, ctx.getStart());
	}
	@Override public void exitHeuristic(TamarinParser.HeuristicContext ctx) { 
		parser.addToken(HEURISTIC_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterGoal_oracle_ranking(TamarinParser.Goal_oracle_rankingContext ctx) { 
		parser.addChildNode(new Goal_oracle_rankingNode());
		parser.addToken(GOAL_ORACLE_RANKING_BEGIN, ctx.getStart());
	}
	@Override public void exitGoal_oracle_ranking(TamarinParser.Goal_oracle_rankingContext ctx) { 
		parser.addToken(GOAL_ORACLE_RANKING_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterGoal_internal_ranking(TamarinParser.Goal_internal_rankingContext ctx) { 
		parser.addChildNode(new Goal_internal_rankingNode());
		parser.addToken(GOAL_INTERNAL_RANKING_BEGIN, ctx.getStart());
	}
	@Override public void exitGoal_internal_ranking(TamarinParser.Goal_internal_rankingContext ctx) { 
		parser.addToken(GOAL_INTERNAL_RANKING_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterGoal_regular_ranking(TamarinParser.Goal_regular_rankingContext ctx) { 
		parser.addChildNode(new Goal_regular_rankingNode());
		parser.addToken(GOAL_REGULAR_RANKING_BEGIN, ctx.getStart());
	}
	@Override public void exitGoal_regular_ranking(TamarinParser.Goal_regular_rankingContext ctx) { 
		parser.addToken(GOAL_REGULAR_RANKING_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterRegularRanking(TamarinParser.RegularRankingContext ctx) { 
		parser.addChildNode(new RegularRankingNode());
		parser.addToken(REGULARRANKING_BEGIN, ctx.getStart());
	}
	@Override public void exitRegularRanking(TamarinParser.RegularRankingContext ctx) { 
		parser.addToken(REGULARRANKING_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterInternalTacticRanking(TamarinParser.InternalTacticRankingContext ctx) { 
		parser.addChildNode(new InternalTacticRankingNode());
		parser.addToken(INTERNALTACTICRANKING_BEGIN, ctx.getStart());
	}
	@Override public void exitInternalTacticRanking(TamarinParser.InternalTacticRankingContext ctx) { 
		parser.addToken(INTERNALTACTICRANKING_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterOracleRanking(TamarinParser.OracleRankingContext ctx) { 
		parser.addChildNode(new OracleRankingNode());
		parser.addToken(ORACLERANKING_BEGIN, ctx.getStart());
	}
	@Override public void exitOracleRanking(TamarinParser.OracleRankingContext ctx) { 
		parser.addToken(ORACLERANKING_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterExport(TamarinParser.ExportContext ctx) { 
		parser.addChildNode(new ExportNode());
		parser.addToken(EXPORT_BEGIN, ctx.getStart());
	}
	@Override public void exitExport(TamarinParser.ExportContext ctx) { 
		parser.addToken(EXPORT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterPredicateDeclaration(TamarinParser.PredicateDeclarationContext ctx) { 
		parser.addChildNode(new PredicateDeclarationNode());
		parser.addToken(PREDICATEDECLARATION_BEGIN, ctx.getStart());
	}
	@Override public void exitPredicateDeclaration(TamarinParser.PredicateDeclarationContext ctx) { 
		parser.addToken(PREDICATEDECLARATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterPredicate(TamarinParser.PredicateContext ctx) { 
		parser.addChildNode(new PredicateNode());
		parser.addToken(PREDICATE_BEGIN, ctx.getStart());
	}
	@Override public void exitPredicate(TamarinParser.PredicateContext ctx) { 
		parser.addToken(PREDICATE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSignatureOptions(TamarinParser.SignatureOptionsContext ctx) { 
		parser.addChildNode(new SignatureOptionsNode());
		parser.addToken(SIGNATUREOPTIONS_BEGIN, ctx.getStart());
	}
	@Override public void exitSignatureOptions(TamarinParser.SignatureOptionsContext ctx) { 
		parser.addToken(SIGNATUREOPTIONS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBuiltinOptions(TamarinParser.BuiltinOptionsContext ctx) { 
		parser.addChildNode(new BuiltinOptionsNode());
		parser.addToken(BUILTINOPTIONS_BEGIN, ctx.getStart());
	}
	@Override public void exitBuiltinOptions(TamarinParser.BuiltinOptionsContext ctx) { 
		parser.addToken(BUILTINOPTIONS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterEquations(TamarinParser.EquationsContext ctx) { 
		parser.addChildNode(new EquationsNode());
		parser.addToken(EQUATIONS_BEGIN, ctx.getStart());
	}
	@Override public void exitEquations(TamarinParser.EquationsContext ctx) { 
		parser.addToken(EQUATIONS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterEquation(TamarinParser.EquationContext ctx) { 
		parser.addChildNode(new EquationNode());
		parser.addToken(EQUATION_BEGIN, ctx.getStart());
	}
	@Override public void exitEquation(TamarinParser.EquationContext ctx) { 
		parser.addToken(EQUATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSignatureFunctions(TamarinParser.SignatureFunctionsContext ctx) { 
		parser.addChildNode(new SignatureFunctionsNode());
		parser.addToken(SIGNATUREFUNCTIONS_BEGIN, ctx.getStart());
	}
	@Override public void exitSignatureFunctions(TamarinParser.SignatureFunctionsContext ctx) { 
		parser.addToken(SIGNATUREFUNCTIONS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSignatureFunction(TamarinParser.SignatureFunctionContext ctx) { 
		parser.addChildNode(new SignatureFunctionNode());
		parser.addToken(SIGNATUREFUNCTION_BEGIN, ctx.getStart());
	}
	@Override public void exitSignatureFunction(TamarinParser.SignatureFunctionContext ctx) { 
		parser.addToken(SIGNATUREFUNCTION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFunctionAttribute(TamarinParser.FunctionAttributeContext ctx) { 
		parser.addChildNode(new FunctionAttributeNode());
		parser.addToken(FUNCTIONATTRIBUTE_BEGIN, ctx.getStart());
	}
	@Override public void exitFunctionAttribute(TamarinParser.FunctionAttributeContext ctx) { 
		parser.addToken(FUNCTIONATTRIBUTE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFunction_type_natural(TamarinParser.Function_type_naturalContext ctx) { 
		parser.addChildNode(new Function_type_naturalNode());
		parser.addToken(FUNCTION_TYPE_NATURAL_BEGIN, ctx.getStart());
	}
	@Override public void exitFunction_type_natural(TamarinParser.Function_type_naturalContext ctx) { 
		parser.addToken(FUNCTION_TYPE_NATURAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFunction_type_sapic(TamarinParser.Function_type_sapicContext ctx) { 
		parser.addChildNode(new Function_type_sapicNode());
		parser.addToken(FUNCTION_TYPE_SAPIC_BEGIN, ctx.getStart());
	}
	@Override public void exitFunction_type_sapic(TamarinParser.Function_type_sapicContext ctx) { 
		parser.addToken(FUNCTION_TYPE_SAPIC_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBuiltins(TamarinParser.BuiltinsContext ctx) { 
		parser.addChildNode(new BuiltinsNode());
		parser.addToken(BUILTINS, ctx.getStart());
	}
	@Override public void exitBuiltins(TamarinParser.BuiltinsContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNames(TamarinParser.BuiltinNamesContext ctx) { 
		parser.addChildNode(new BuiltinNamesNode());
	}
	@Override public void exitBuiltinNames(TamarinParser.BuiltinNamesContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameLocationsReport(TamarinParser.BuiltinNameLocationsReportContext ctx) { 
		parser.addChildNode(new BuiltinNameLocationsReportNode());
		parser.addToken(BUILTINNAMELOCATIONSREPORT, ctx.getStart());
	}
	@Override public void exitBuiltinNameLocationsReport(TamarinParser.BuiltinNameLocationsReportContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameReliableChannel(TamarinParser.BuiltinNameReliableChannelContext ctx) { 
		parser.addChildNode(new BuiltinNameReliableChannelNode());
		parser.addToken(BUILTINNAMERELIABLECHANNEL, ctx.getStart());
	}
	@Override public void exitBuiltinNameReliableChannel(TamarinParser.BuiltinNameReliableChannelContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameDH(TamarinParser.BuiltinNameDHContext ctx) { 
		parser.addChildNode(new BuiltinNameDHNode());
		parser.addToken(BUILTINNAMEDH, ctx.getStart());
	}
	@Override public void exitBuiltinNameDH(TamarinParser.BuiltinNameDHContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameBilinearPairing(TamarinParser.BuiltinNameBilinearPairingContext ctx) { 
		parser.addChildNode(new BuiltinNameBilinearPairingNode());
		parser.addToken(BUILTINNAMEBILINEARPAIRING, ctx.getStart());
	}
	@Override public void exitBuiltinNameBilinearPairing(TamarinParser.BuiltinNameBilinearPairingContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameMultiset(TamarinParser.BuiltinNameMultisetContext ctx) { 
		parser.addChildNode(new BuiltinNameMultisetNode());
		parser.addToken(BUILTINNAMEMULTISET, ctx.getStart());
	}
	@Override public void exitBuiltinNameMultiset(TamarinParser.BuiltinNameMultisetContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameSymmetricEncryption(TamarinParser.BuiltinNameSymmetricEncryptionContext ctx) { 
		parser.addChildNode(new BuiltinNameSymmetricEncryptionNode());
		parser.addToken(BUILTINNAMESYMMETRICENCRYPTION, ctx.getStart());
	}
	@Override public void exitBuiltinNameSymmetricEncryption(TamarinParser.BuiltinNameSymmetricEncryptionContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameAsymmetricEncryption(TamarinParser.BuiltinNameAsymmetricEncryptionContext ctx) { 
		parser.addChildNode(new BuiltinNameAsymmetricEncryptionNode());
		parser.addToken(BUILTINNAMEASYMMETRICENCRYPTION, ctx.getStart());
	}
	@Override public void exitBuiltinNameAsymmetricEncryption(TamarinParser.BuiltinNameAsymmetricEncryptionContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameSigning(TamarinParser.BuiltinNameSigningContext ctx) { 
		parser.addChildNode(new BuiltinNameSigningNode());
		parser.addToken(BUILTINNAMESIGNING, ctx.getStart());
	}
	@Override public void exitBuiltinNameSigning(TamarinParser.BuiltinNameSigningContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameDestPairing(TamarinParser.BuiltinNameDestPairingContext ctx) { 
		parser.addChildNode(new BuiltinNameDestPairingNode());
		parser.addToken(BUILTINNAMEDESTPAIRING, ctx.getStart());
	}
	@Override public void exitBuiltinNameDestPairing(TamarinParser.BuiltinNameDestPairingContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameDestSymmetricEncryption(TamarinParser.BuiltinNameDestSymmetricEncryptionContext ctx) { 
		parser.addChildNode(new BuiltinNameDestSymmetricEncryptionNode());
		parser.addToken(BUILTINNAMEDESTSYMMETRICENCRYPTION, ctx.getStart());
	}
	@Override public void exitBuiltinNameDestSymmetricEncryption(TamarinParser.BuiltinNameDestSymmetricEncryptionContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameDestAsymmetricEncryption(TamarinParser.BuiltinNameDestAsymmetricEncryptionContext ctx) { 
		parser.addChildNode(new BuiltinNameDestAsymmetricEncryptionNode());
		parser.addToken(BUILTINNAMEDESTASYMMETRICENCRYPTION, ctx.getStart());
	}
	@Override public void exitBuiltinNameDestAsymmetricEncryption(TamarinParser.BuiltinNameDestAsymmetricEncryptionContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameDestSigning(TamarinParser.BuiltinNameDestSigningContext ctx) { 
		parser.addChildNode(new BuiltinNameDestSigningNode());
		parser.addToken(BUILTINNAMEDESTSIGNING, ctx.getStart());
	}
	@Override public void exitBuiltinNameDestSigning(TamarinParser.BuiltinNameDestSigningContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameRevealingSigning(TamarinParser.BuiltinNameRevealingSigningContext ctx) { 
		parser.addChildNode(new BuiltinNameRevealingSigningNode());
		parser.addToken(BUILTINNAMEREVEALINGSIGNING, ctx.getStart());
	}
	@Override public void exitBuiltinNameRevealingSigning(TamarinParser.BuiltinNameRevealingSigningContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameHashing(TamarinParser.BuiltinNameHashingContext ctx) { 
		parser.addChildNode(new BuiltinNameHashingNode());
		parser.addToken(BUILTINNAMEHASHING, ctx.getStart());
	}
	@Override public void exitBuiltinNameHashing(TamarinParser.BuiltinNameHashingContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBuiltinNameXor(TamarinParser.BuiltinNameXorContext ctx) { 
		parser.addChildNode(new BuiltinNameXorNode());
		parser.addToken(BUILTINNAMEXOR, ctx.getStart());
	}
	@Override public void exitBuiltinNameXor(TamarinParser.BuiltinNameXorContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterProofSkeleton(TamarinParser.ProofSkeletonContext ctx) { 
		parser.addChildNode(new ProofSkeletonNode());
		parser.addToken(PROOFSKELETON_BEGIN, ctx.getStart());
	}
	@Override public void exitProofSkeleton(TamarinParser.ProofSkeletonContext ctx) { 
		parser.addToken(PROOFSKELETON_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterStartProofSkeleton(TamarinParser.StartProofSkeletonContext ctx) { 
		parser.addChildNode(new StartProofSkeletonNode());
		parser.addToken(STARTPROOFSKELETON_BEGIN, ctx.getStart());
	}
	@Override public void exitStartProofSkeleton(TamarinParser.StartProofSkeletonContext ctx) { 
		parser.addToken(STARTPROOFSKELETON_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSolvedProof(TamarinParser.SolvedProofContext ctx) { 
		parser.addChildNode(new SolvedProofNode());
		parser.addToken(SOLVEDPROOF_BEGIN, ctx.getStart());
	}
	@Override public void exitSolvedProof(TamarinParser.SolvedProofContext ctx) { 
		parser.addToken(SOLVEDPROOF_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFinalProof(TamarinParser.FinalProofContext ctx) { 
		parser.addChildNode(new FinalProofNode());
		parser.addToken(FINALPROOF_BEGIN, ctx.getStart());
	}
	@Override public void exitFinalProof(TamarinParser.FinalProofContext ctx) { 
		parser.addToken(FINALPROOF_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterInterProof(TamarinParser.InterProofContext ctx) { 
		parser.addChildNode(new InterProofNode());
		parser.addToken(INTERPROOF_BEGIN, ctx.getStart());
	}
	@Override public void exitInterProof(TamarinParser.InterProofContext ctx) { 
		parser.addToken(INTERPROOF_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterProofCase(TamarinParser.ProofCaseContext ctx) { 
		parser.addChildNode(new ProofCaseNode());
		parser.addToken(PROOFCASE_BEGIN, ctx.getStart());
	}
	@Override public void exitProofCase(TamarinParser.ProofCaseContext ctx) { 
		parser.addToken(PROOFCASE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterProof_method_sorry(TamarinParser.Proof_method_sorryContext ctx) { 
		parser.addChildNode(new Proof_method_sorryNode());
		parser.addToken(PROOF_METHOD_SORRY_BEGIN, ctx.getStart());
	}
	@Override public void exitProof_method_sorry(TamarinParser.Proof_method_sorryContext ctx) { 
		parser.addToken(PROOF_METHOD_SORRY_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterProof_method_simplify(TamarinParser.Proof_method_simplifyContext ctx) { 
		parser.addChildNode(new Proof_method_simplifyNode());
		parser.addToken(PROOF_METHOD_SIMPLIFY_BEGIN, ctx.getStart());
	}
	@Override public void exitProof_method_simplify(TamarinParser.Proof_method_simplifyContext ctx) { 
		parser.addToken(PROOF_METHOD_SIMPLIFY_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterProof_method_solve(TamarinParser.Proof_method_solveContext ctx) { 
		parser.addChildNode(new Proof_method_solveNode());
		parser.addToken(PROOF_METHOD_SOLVE_BEGIN, ctx.getStart());
	}
	@Override public void exitProof_method_solve(TamarinParser.Proof_method_solveContext ctx) { 
		parser.addToken(PROOF_METHOD_SOLVE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterProof_method_contradiction(TamarinParser.Proof_method_contradictionContext ctx) { 
		parser.addChildNode(new Proof_method_contradictionNode());
		parser.addToken(PROOF_METHOD_CONTRADICTION_BEGIN, ctx.getStart());
	}
	@Override public void exitProof_method_contradiction(TamarinParser.Proof_method_contradictionContext ctx) { 
		parser.addToken(PROOF_METHOD_CONTRADICTION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterProof_method_induction(TamarinParser.Proof_method_inductionContext ctx) { 
		parser.addChildNode(new Proof_method_inductionNode());
		parser.addToken(PROOF_METHOD_INDUCTION_BEGIN, ctx.getStart());
	}
	@Override public void exitProof_method_induction(TamarinParser.Proof_method_inductionContext ctx) { 
		parser.addToken(PROOF_METHOD_INDUCTION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterGoal(TamarinParser.GoalContext ctx) { 
		parser.addChildNode(new GoalNode());
		parser.addToken(GOAL_BEGIN, ctx.getStart());
	}
	@Override public void exitGoal(TamarinParser.GoalContext ctx) { 
		parser.addToken(GOAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterActionGoal(TamarinParser.ActionGoalContext ctx) { 
		parser.addChildNode(new ActionGoalNode());
		parser.addToken(ACTIONGOAL_BEGIN, ctx.getStart());
	}
	@Override public void exitActionGoal(TamarinParser.ActionGoalContext ctx) { 
		parser.addToken(ACTIONGOAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterPremiseGoal(TamarinParser.PremiseGoalContext ctx) { 
		parser.addChildNode(new PremiseGoalNode());
		parser.addToken(PREMISEGOAL_BEGIN, ctx.getStart());
	}
	@Override public void exitPremiseGoal(TamarinParser.PremiseGoalContext ctx) { 
		parser.addToken(PREMISEGOAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterChainGoal(TamarinParser.ChainGoalContext ctx) { 
		parser.addChildNode(new ChainGoalNode());
		parser.addToken(CHAINGOAL_BEGIN, ctx.getStart());
	}
	@Override public void exitChainGoal(TamarinParser.ChainGoalContext ctx) { 
		parser.addToken(CHAINGOAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterDisjSplitGoal(TamarinParser.DisjSplitGoalContext ctx) { 
		parser.addChildNode(new DisjSplitGoalNode());
		parser.addToken(DISJSPLITGOAL_BEGIN, ctx.getStart());
	}
	@Override public void exitDisjSplitGoal(TamarinParser.DisjSplitGoalContext ctx) { 
		parser.addToken(DISJSPLITGOAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterEqSplitGoal(TamarinParser.EqSplitGoalContext ctx) { 
		parser.addChildNode(new EqSplitGoalNode());
		parser.addToken(EQSPLITGOAL_BEGIN, ctx.getStart());
	}
	@Override public void exitEqSplitGoal(TamarinParser.EqSplitGoalContext ctx) { 
		parser.addToken(EQSPLITGOAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterNodePremise(TamarinParser.NodePremiseContext ctx) { 
		parser.addChildNode(new NodePremiseNode());
		parser.addToken(NODEPREMISE_BEGIN, ctx.getStart());
	}
	@Override public void exitNodePremise(TamarinParser.NodePremiseContext ctx) { 
		parser.addToken(NODEPREMISE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterNodeConclusion(TamarinParser.NodeConclusionContext ctx) { 
		parser.addChildNode(new NodeConclusionNode());
		parser.addToken(NODECONCLUSION_BEGIN, ctx.getStart());
	}
	@Override public void exitNodeConclusion(TamarinParser.NodeConclusionContext ctx) { 
		parser.addToken(NODECONCLUSION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterDiffEquivLemma(TamarinParser.DiffEquivLemmaContext ctx) { 
		parser.addChildNode(new DiffEquivLemmaNode());
		parser.addToken(DIFFEQUIVLEMMA_BEGIN, ctx.getStart());
	}
	@Override public void exitDiffEquivLemma(TamarinParser.DiffEquivLemmaContext ctx) { 
		parser.addToken(DIFFEQUIVLEMMA_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterEquivLemma(TamarinParser.EquivLemmaContext ctx) { 
		parser.addChildNode(new EquivLemmaNode());
		parser.addToken(EQUIVLEMMA_BEGIN, ctx.getStart());
	}
	@Override public void exitEquivLemma(TamarinParser.EquivLemmaContext ctx) { 
		parser.addToken(EQUIVLEMMA_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterProcess(TamarinParser.ProcessContext ctx) { 
		parser.addChildNode(new ProcessNode());
		parser.addToken(PROCESS_BEGIN, ctx.getStart());
	}
	@Override public void exitProcess(TamarinParser.ProcessContext ctx) { 
		parser.addToken(PROCESS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterElseProcess(TamarinParser.ElseProcessContext ctx) { 
		parser.addChildNode(new ElseProcessNode());
		parser.addToken(ELSEPROCESS_BEGIN, ctx.getStart());
	}
	@Override public void exitElseProcess(TamarinParser.ElseProcessContext ctx) { 
		parser.addToken(ELSEPROCESS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTopLevelProcess(TamarinParser.TopLevelProcessContext ctx) { 
		parser.addChildNode(new TopLevelProcessNode());
		parser.addToken(TOPLEVELPROCESS_BEGIN, ctx.getStart());
	}
	@Override public void exitTopLevelProcess(TamarinParser.TopLevelProcessContext ctx) { 
		parser.addToken(TOPLEVELPROCESS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterAction_process_replication(TamarinParser.Action_process_replicationContext ctx) { 
		parser.addChildNode(new Action_process_replicationNode());
		parser.addToken(ACTION_PROCESS_REPLICATION_BEGIN, ctx.getStart());
	}
	@Override public void exitAction_process_replication(TamarinParser.Action_process_replicationContext ctx) { 
		parser.addToken(ACTION_PROCESS_REPLICATION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterAction_process_lookup(TamarinParser.Action_process_lookupContext ctx) { 
		parser.addChildNode(new Action_process_lookupNode());
		parser.addToken(ACTION_PROCESS_LOOKUP_BEGIN, ctx.getStart());
	}
	@Override public void exitAction_process_lookup(TamarinParser.Action_process_lookupContext ctx) { 
		parser.addToken(ACTION_PROCESS_LOOKUP_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterAction_process_conditional(TamarinParser.Action_process_conditionalContext ctx) { 
		parser.addChildNode(new Action_process_conditionalNode());
		parser.addToken(ACTION_PROCESS_CONDITIONAL_BEGIN, ctx.getStart());
	}
	@Override public void exitAction_process_conditional(TamarinParser.Action_process_conditionalContext ctx) { 
		parser.addToken(ACTION_PROCESS_CONDITIONAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterAction_process_let_binding(TamarinParser.Action_process_let_bindingContext ctx) { 
		parser.addChildNode(new Action_process_let_bindingNode());
		parser.addToken(ACTION_PROCESS_LET_BINDING_BEGIN, ctx.getStart());
	}
	@Override public void exitAction_process_let_binding(TamarinParser.Action_process_let_bindingContext ctx) { 
		parser.addToken(ACTION_PROCESS_LET_BINDING_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterAction_process_null(TamarinParser.Action_process_nullContext ctx) { 
		parser.addChildNode(new Action_process_nullNode());
		parser.addToken(ACTION_PROCESS_NULL_BEGIN, ctx.getStart());
	}
	@Override public void exitAction_process_null(TamarinParser.Action_process_nullContext ctx) { 
		parser.addToken(ACTION_PROCESS_NULL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterAction_process_sequential(TamarinParser.Action_process_sequentialContext ctx) { 
		parser.addChildNode(new Action_process_sequentialNode());
		parser.addToken(ACTION_PROCESS_SEQUENTIAL_BEGIN, ctx.getStart());
	}
	@Override public void exitAction_process_sequential(TamarinParser.Action_process_sequentialContext ctx) { 
		parser.addToken(ACTION_PROCESS_SEQUENTIAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterAction_process_process(TamarinParser.Action_process_processContext ctx) { 
		parser.addChildNode(new Action_process_processNode());
		parser.addToken(ACTION_PROCESS_PROCESS_BEGIN, ctx.getStart());
	}
	@Override public void exitAction_process_process(TamarinParser.Action_process_processContext ctx) { 
		parser.addToken(ACTION_PROCESS_PROCESS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterAction_process_identifier(TamarinParser.Action_process_identifierContext ctx) { 
		parser.addChildNode(new Action_process_identifierNode());
		parser.addToken(ACTION_PROCESS_IDENTIFIER_BEGIN, ctx.getStart());
	}
	@Override public void exitAction_process_identifier(TamarinParser.Action_process_identifierContext ctx) { 
		parser.addToken(ACTION_PROCESS_IDENTIFIER_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapic_action_new(TamarinParser.Sapic_action_newContext ctx) { 
		parser.addChildNode(new Sapic_action_newNode());
		parser.addToken(SAPIC_ACTION_NEW_BEGIN, ctx.getStart());
	}
	@Override public void exitSapic_action_new(TamarinParser.Sapic_action_newContext ctx) { 
		parser.addToken(SAPIC_ACTION_NEW_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapic_action_insert(TamarinParser.Sapic_action_insertContext ctx) { 
		parser.addChildNode(new Sapic_action_insertNode());
		parser.addToken(SAPIC_ACTION_INSERT_BEGIN, ctx.getStart());
	}
	@Override public void exitSapic_action_insert(TamarinParser.Sapic_action_insertContext ctx) { 
		parser.addToken(SAPIC_ACTION_INSERT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapic_action_in(TamarinParser.Sapic_action_inContext ctx) { 
		parser.addChildNode(new Sapic_action_inNode());
		parser.addToken(SAPIC_ACTION_IN_BEGIN, ctx.getStart());
	}
	@Override public void exitSapic_action_in(TamarinParser.Sapic_action_inContext ctx) { 
		parser.addToken(SAPIC_ACTION_IN_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapic_action_out(TamarinParser.Sapic_action_outContext ctx) { 
		parser.addChildNode(new Sapic_action_outNode());
		parser.addToken(SAPIC_ACTION_OUT_BEGIN, ctx.getStart());
	}
	@Override public void exitSapic_action_out(TamarinParser.Sapic_action_outContext ctx) { 
		parser.addToken(SAPIC_ACTION_OUT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapic_action_delete(TamarinParser.Sapic_action_deleteContext ctx) { 
		parser.addChildNode(new Sapic_action_deleteNode());
		parser.addToken(SAPIC_ACTION_DELETE_BEGIN, ctx.getStart());
	}
	@Override public void exitSapic_action_delete(TamarinParser.Sapic_action_deleteContext ctx) { 
		parser.addToken(SAPIC_ACTION_DELETE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapic_action_lock(TamarinParser.Sapic_action_lockContext ctx) { 
		parser.addChildNode(new Sapic_action_lockNode());
		parser.addToken(SAPIC_ACTION_LOCK_BEGIN, ctx.getStart());
	}
	@Override public void exitSapic_action_lock(TamarinParser.Sapic_action_lockContext ctx) { 
		parser.addToken(SAPIC_ACTION_LOCK_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapic_action_unlock(TamarinParser.Sapic_action_unlockContext ctx) { 
		parser.addChildNode(new Sapic_action_unlockNode());
		parser.addToken(SAPIC_ACTION_UNLOCK_BEGIN, ctx.getStart());
	}
	@Override public void exitSapic_action_unlock(TamarinParser.Sapic_action_unlockContext ctx) { 
		parser.addToken(SAPIC_ACTION_UNLOCK_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapic_action_event(TamarinParser.Sapic_action_eventContext ctx) { 
		parser.addChildNode(new Sapic_action_eventNode());
		parser.addToken(SAPIC_ACTION_EVENT_BEGIN, ctx.getStart());
	}
	@Override public void exitSapic_action_event(TamarinParser.Sapic_action_eventContext ctx) { 
		parser.addToken(SAPIC_ACTION_EVENT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapic_action_rule(TamarinParser.Sapic_action_ruleContext ctx) { 
		parser.addChildNode(new Sapic_action_ruleNode());
		parser.addToken(SAPIC_ACTION_RULE_BEGIN, ctx.getStart());
	}
	@Override public void exitSapic_action_rule(TamarinParser.Sapic_action_ruleContext ctx) { 
		parser.addToken(SAPIC_ACTION_RULE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapicActionIn(TamarinParser.SapicActionInContext ctx) { 
		parser.addChildNode(new SapicActionInNode());
		parser.addToken(SAPICACTIONIN_BEGIN, ctx.getStart());
	}
	@Override public void exitSapicActionIn(TamarinParser.SapicActionInContext ctx) { 
		parser.addToken(SAPICACTIONIN_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapicActionOut(TamarinParser.SapicActionOutContext ctx) { 
		parser.addChildNode(new SapicActionOutNode());
		parser.addToken(SAPICACTIONOUT_BEGIN, ctx.getStart());
	}
	@Override public void exitSapicActionOut(TamarinParser.SapicActionOutContext ctx) { 
		parser.addToken(SAPICACTIONOUT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterProcessDef(TamarinParser.ProcessDefContext ctx) { 
		parser.addChildNode(new ProcessDefNode());
		parser.addToken(PROCESSDEF_BEGIN, ctx.getStart());
	}
	@Override public void exitProcessDef(TamarinParser.ProcessDefContext ctx) { 
		parser.addToken(PROCESSDEF_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapicPatternTerm(TamarinParser.SapicPatternTermContext ctx) { 
		parser.addChildNode(new SapicPatternTermNode());
		parser.addToken(SAPICPATTERNTERM_BEGIN, ctx.getStart());
	}
	@Override public void exitSapicPatternTerm(TamarinParser.SapicPatternTermContext ctx) { 
		parser.addToken(SAPICPATTERNTERM_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapicTerm(TamarinParser.SapicTermContext ctx) { 
		parser.addChildNode(new SapicTermNode());
		parser.addToken(SAPICTERM_BEGIN, ctx.getStart());
	}
	@Override public void exitSapicTerm(TamarinParser.SapicTermContext ctx) { 
		parser.addToken(SAPICTERM_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLogicalTypedPatternLiteral(TamarinParser.LogicalTypedPatternLiteralContext ctx) { 
		parser.addChildNode(new LogicalTypedPatternLiteralNode());
		parser.addToken(LOGICALTYPEDPATTERNLITERAL_BEGIN, ctx.getStart());
	}
	@Override public void exitLogicalTypedPatternLiteral(TamarinParser.LogicalTypedPatternLiteralContext ctx) { 
		parser.addToken(LOGICALTYPEDPATTERNLITERAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLogicalTypedLiteral(TamarinParser.LogicalTypedLiteralContext ctx) { 
		parser.addChildNode(new LogicalTypedLiteralNode());
		parser.addToken(LOGICALTYPEDLITERAL_BEGIN, ctx.getStart());
	}
	@Override public void exitLogicalTypedLiteral(TamarinParser.LogicalTypedLiteralContext ctx) { 
		parser.addToken(LOGICALTYPEDLITERAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapicNodeVar(TamarinParser.SapicNodeVarContext ctx) { 
		parser.addChildNode(new SapicNodeVarNode());
		parser.addToken(SAPICNODEVAR_BEGIN, ctx.getStart());
	}
	@Override public void exitSapicNodeVar(TamarinParser.SapicNodeVarContext ctx) { 
		parser.addToken(SAPICNODEVAR_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapicPatternVar(TamarinParser.SapicPatternVarContext ctx) { 
		parser.addChildNode(new SapicPatternVarNode());
		parser.addToken(SAPICPATTERNVAR_BEGIN, ctx.getStart());
	}
	@Override public void exitSapicPatternVar(TamarinParser.SapicPatternVarContext ctx) { 
		parser.addToken(SAPICPATTERNVAR_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapicVar(TamarinParser.SapicVarContext ctx) { 
		parser.addChildNode(new SapicVarNode());
		parser.addToken(SAPICVAR_BEGIN, ctx.getStart());
	}
	@Override public void exitSapicVar(TamarinParser.SapicVarContext ctx) { 
		parser.addToken(SAPICVAR_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSapicType(TamarinParser.SapicTypeContext ctx) { 
		parser.addChildNode(new SapicTypeNode());
		parser.addToken(SAPICTYPE_BEGIN, ctx.getStart());
	}
	@Override public void exitSapicType(TamarinParser.SapicTypeContext ctx) { 
		parser.addToken(SAPICTYPE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterRestriction(TamarinParser.RestrictionContext ctx) { 
		parser.addChildNode(new RestrictionNode());
		parser.addToken(RESTRICTION_BEGIN, ctx.getStart());
	}
	@Override public void exitRestriction(TamarinParser.RestrictionContext ctx) { 
		parser.addToken(RESTRICTION_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterRestrictionAttributes(TamarinParser.RestrictionAttributesContext ctx) { 
		parser.addChildNode(new RestrictionAttributesNode());
		parser.addToken(RESTRICTIONATTRIBUTES_BEGIN, ctx.getStart());
	}
	@Override public void exitRestrictionAttributes(TamarinParser.RestrictionAttributesContext ctx) { 
		parser.addToken(RESTRICTIONATTRIBUTES_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterRestrictionAttribute(TamarinParser.RestrictionAttributeContext ctx) { 
		parser.addChildNode(new RestrictionAttributeNode());
		parser.addToken(RESTRICTIONATTRIBUTE_BEGIN, ctx.getStart());
	}
	@Override public void exitRestrictionAttribute(TamarinParser.RestrictionAttributeContext ctx) { 
		parser.addToken(RESTRICTIONATTRIBUTE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLegacyAxiom(TamarinParser.LegacyAxiomContext ctx) { 
		parser.addChildNode(new LegacyAxiomNode());
		parser.addToken(LEGACYAXIOM_BEGIN, ctx.getStart());
	}
	@Override public void exitLegacyAxiom(TamarinParser.LegacyAxiomContext ctx) { 
		parser.addToken(LEGACYAXIOM_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLetBlock(TamarinParser.LetBlockContext ctx) { 
		parser.addChildNode(new LetBlockNode());
		parser.addToken(LETBLOCK_BEGIN, ctx.getStart());
	}
	@Override public void exitLetBlock(TamarinParser.LetBlockContext ctx) { 
		parser.addToken(LETBLOCK_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterProtoFactIdentifier(TamarinParser.ProtoFactIdentifierContext ctx) { 
		parser.addChildNode(new ProtoFactIdentifierNode());
	}
	@Override public void exitProtoFactIdentifier(TamarinParser.ProtoFactIdentifierContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterReserved_fact_out(TamarinParser.Reserved_fact_outContext ctx) { 
		parser.addChildNode(new Reserved_fact_outNode());
		parser.addToken(RESERVED_FACT_OUT, ctx.getStart());
	}
	@Override public void exitReserved_fact_out(TamarinParser.Reserved_fact_outContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterReserved_fact_in(TamarinParser.Reserved_fact_inContext ctx) { 
		parser.addChildNode(new Reserved_fact_inNode());
		parser.addToken(RESERVED_FACT_IN, ctx.getStart());
	}
	@Override public void exitReserved_fact_in(TamarinParser.Reserved_fact_inContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterReserved_fact_ku(TamarinParser.Reserved_fact_kuContext ctx) { 
		parser.addChildNode(new Reserved_fact_kuNode());
		parser.addToken(RESERVED_FACT_KU, ctx.getStart());
	}
	@Override public void exitReserved_fact_ku(TamarinParser.Reserved_fact_kuContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterReserved_fact_kd(TamarinParser.Reserved_fact_kdContext ctx) { 
		parser.addChildNode(new Reserved_fact_kdNode());
		parser.addToken(RESERVED_FACT_KD, ctx.getStart());
	}
	@Override public void exitReserved_fact_kd(TamarinParser.Reserved_fact_kdContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterReserved_fact_ded(TamarinParser.Reserved_fact_dedContext ctx) { 
		parser.addChildNode(new Reserved_fact_dedNode());
		parser.addToken(RESERVED_FACT_DED, ctx.getStart());
	}
	@Override public void exitReserved_fact_ded(TamarinParser.Reserved_fact_dedContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterIdentifier(TamarinParser.IdentifierContext ctx) { 
		parser.addChildNode(new IdentifierNode());
		parser.addToken(IDENTIFIER, ctx.getStart());
	}
	@Override public void exitIdentifier(TamarinParser.IdentifierContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterNatural(TamarinParser.NaturalContext ctx) { 
		parser.addChildNode(new NaturalNode());
		parser.addToken(NATURAL_BEGIN, ctx.getStart());
	}
	@Override public void exitNatural(TamarinParser.NaturalContext ctx) { 
		parser.addToken(NATURAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterIndexedIdentifier(TamarinParser.IndexedIdentifierContext ctx) { 
		parser.addChildNode(new IndexedIdentifierNode());
	}
	@Override public void exitIndexedIdentifier(TamarinParser.IndexedIdentifierContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterFilePath(TamarinParser.FilePathContext ctx) { 
		parser.addChildNode(new FilePathNode());
		parser.addToken(FILEPATH, ctx.getStart());
	}
	@Override public void exitFilePath(TamarinParser.FilePathContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterKeywords(TamarinParser.KeywordsContext ctx) { 
		parser.addChildNode(new KeywordsNode());
		parser.addToken(KEYWORDS_BEGIN, ctx.getStart());
	}
	@Override public void exitKeywords(TamarinParser.KeywordsContext ctx) { 
		parser.addToken(KEYWORDS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLsortMsg(TamarinParser.LsortMsgContext ctx) { 
		parser.addChildNode(new LsortMsgNode());
	}
	@Override public void exitLsortMsg(TamarinParser.LsortMsgContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterMsgVar(TamarinParser.MsgVarContext ctx) { 
		parser.addChildNode(new MsgVarNode());
		parser.addToken(MSGVAR_BEGIN, ctx.getStart());
	}
	@Override public void exitMsgVar(TamarinParser.MsgVarContext ctx) { 
		parser.addToken(MSGVAR_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLogicalLiteral(TamarinParser.LogicalLiteralContext ctx) { 
		parser.addChildNode(new LogicalLiteralNode());
	}
	@Override public void exitLogicalLiteral(TamarinParser.LogicalLiteralContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLogicalLiteralWithNode(TamarinParser.LogicalLiteralWithNodeContext ctx) { 
		parser.addChildNode(new LogicalLiteralWithNodeNode());
		parser.addToken(LOGICALLITERALWITHNODE_BEGIN, ctx.getStart());
	}
	@Override public void exitLogicalLiteralWithNode(TamarinParser.LogicalLiteralWithNodeContext ctx) { 
		parser.addToken(LOGICALLITERALWITHNODE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLogicalLiteralNoPub(TamarinParser.LogicalLiteralNoPubContext ctx) { 
		parser.addChildNode(new LogicalLiteralNoPubNode());
	}
	@Override public void exitLogicalLiteralNoPub(TamarinParser.LogicalLiteralNoPubContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterLogVar(TamarinParser.LogVarContext ctx) { 
		parser.addChildNode(new LogVarNode());
		parser.addToken(LOGVAR_BEGIN, ctx.getStart());
	}
	@Override public void exitLogVar(TamarinParser.LogVarContext ctx) { 
		parser.addToken(LOGVAR_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterLiteral(TamarinParser.LiteralContext ctx) { 
		parser.addChildNode(new LiteralNode());
		parser.addToken(LITERAL_BEGIN, ctx.getStart());
	}
	@Override public void exitLiteral(TamarinParser.LiteralContext ctx) { 
		parser.addToken(LITERAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterNodeVar(TamarinParser.NodeVarContext ctx) { 
		parser.addChildNode(new NodeVarNode());
		parser.addToken(NODEVAR_BEGIN, ctx.getStart());
	}
	@Override public void exitNodeVar(TamarinParser.NodeVarContext ctx) { 
		parser.addToken(NODEVAR_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFreshName(TamarinParser.FreshNameContext ctx) { 
		parser.addChildNode(new FreshNameNode());
		parser.addToken(FRESHNAME_BEGIN, ctx.getStart());
	}
	@Override public void exitFreshName(TamarinParser.FreshNameContext ctx) { 
		parser.addToken(FRESHNAME_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterPubName(TamarinParser.PubNameContext ctx) { 
		parser.addChildNode(new PubNameNode());
		parser.addToken(PUBNAME_BEGIN, ctx.getStart());
	}
	@Override public void exitPubName(TamarinParser.PubNameContext ctx) { 
		parser.addToken(PUBNAME_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterFormalComment(TamarinParser.FormalCommentContext ctx) { 
		parser.addChildNode(new FormalCommentNode());
		parser.addToken(FORMALCOMMENT_BEGIN, ctx.getStart());
	}
	@Override public void exitFormalComment(TamarinParser.FormalCommentContext ctx) { 
		parser.addToken(FORMALCOMMENT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterTacticFunctionIdentifier(TamarinParser.TacticFunctionIdentifierContext ctx) { 
		parser.addChildNode(new TacticFunctionIdentifierNode());
		parser.addToken(TACTICFUNCTIONIDENTIFIER_BEGIN, ctx.getStart());
	}
	@Override public void exitTacticFunctionIdentifier(TamarinParser.TacticFunctionIdentifierContext ctx) { 
		parser.addToken(TACTICFUNCTIONIDENTIFIER_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterInternalTacticName(TamarinParser.InternalTacticNameContext ctx) { 
		parser.addChildNode(new InternalTacticNameNode());
		parser.addToken(INTERNALTACTICNAME_BEGIN, ctx.getStart());
	}
	@Override public void exitInternalTacticName(TamarinParser.InternalTacticNameContext ctx) { 
		parser.addToken(INTERNALTACTICNAME_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterOracleRelativePath(TamarinParser.OracleRelativePathContext ctx) { 
		parser.addChildNode(new OracleRelativePathNode());
		parser.addToken(ORACLERELATIVEPATH_BEGIN, ctx.getStart());
	}
	@Override public void exitOracleRelativePath(TamarinParser.OracleRelativePathContext ctx) { 
		parser.addToken(ORACLERELATIVEPATH_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterExportBodyChars(TamarinParser.ExportBodyCharsContext ctx) { 
		parser.addChildNode(new ExportBodyCharsNode());
		parser.addToken(EXPORTBODYCHARS_BEGIN, ctx.getStart());
	}
	@Override public void exitExportBodyChars(TamarinParser.ExportBodyCharsContext ctx) { 
		parser.addToken(EXPORTBODYCHARS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterCharDir(TamarinParser.CharDirContext ctx) { 
		parser.addChildNode(new CharDirNode());
		parser.addToken(CHARDIR_BEGIN, ctx.getStart());
	}
	@Override public void exitCharDir(TamarinParser.CharDirContext ctx) { 
		parser.addToken(CHARDIR_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterEquivOp(TamarinParser.EquivOpContext ctx) { 
		parser.addChildNode(new EquivOpNode());
		parser.addToken(EQUIVOP, ctx.getStart());
	}
	@Override public void exitEquivOp(TamarinParser.EquivOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterOrOp(TamarinParser.OrOpContext ctx) { 
		parser.addChildNode(new OrOpNode());
		parser.addToken(OROP, ctx.getStart());
	}
	@Override public void exitOrOp(TamarinParser.OrOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterNotOp(TamarinParser.NotOpContext ctx) { 
		parser.addChildNode(new NotOpNode());
		parser.addToken(NOTOP, ctx.getStart());
	}
	@Override public void exitNotOp(TamarinParser.NotOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterAndOp(TamarinParser.AndOpContext ctx) { 
		parser.addChildNode(new AndOpNode());
		parser.addToken(ANDOP, ctx.getStart());
	}
	@Override public void exitAndOp(TamarinParser.AndOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterEqOp(TamarinParser.EqOpContext ctx) { 
		parser.addChildNode(new EqOpNode());
		parser.addToken(EQOP, ctx.getStart());
	}
	@Override public void exitEqOp(TamarinParser.EqOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterBotOp(TamarinParser.BotOpContext ctx) { 
		parser.addChildNode(new BotOpNode());
		parser.addToken(BOTOP, ctx.getStart());
	}
	@Override public void exitBotOp(TamarinParser.BotOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterFOp(TamarinParser.FOpContext ctx) { 
		parser.addChildNode(new FOpNode());
		parser.addToken(FOP, ctx.getStart());
	}
	@Override public void exitFOp(TamarinParser.FOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTOp(TamarinParser.TOpContext ctx) { 
		parser.addChildNode(new TOpNode());
		parser.addToken(TOP, ctx.getStart());
	}
	@Override public void exitTOp(TamarinParser.TOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterTopOp(TamarinParser.TopOpContext ctx) { 
		parser.addChildNode(new TopOpNode());
		parser.addToken(TOPOP, ctx.getStart());
	}
	@Override public void exitTopOp(TamarinParser.TopOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterForallOp(TamarinParser.ForallOpContext ctx) { 
		parser.addChildNode(new ForallOpNode());
		parser.addToken(FORALLOP, ctx.getStart());
	}
	@Override public void exitForallOp(TamarinParser.ForallOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterExistsOp(TamarinParser.ExistsOpContext ctx) { 
		parser.addChildNode(new ExistsOpNode());
		parser.addToken(EXISTSOP, ctx.getStart());
	}
	@Override public void exitExistsOp(TamarinParser.ExistsOpContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterSuffixFresh(TamarinParser.SuffixFreshContext ctx) { 
		parser.addChildNode(new SuffixFreshNode());
		parser.addToken(SUFFIXFRESH_BEGIN, ctx.getStart());
	}
	@Override public void exitSuffixFresh(TamarinParser.SuffixFreshContext ctx) { 
		parser.addToken(SUFFIXFRESH_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterSuffixPub(TamarinParser.SuffixPubContext ctx) { 
		parser.addChildNode(new SuffixPubNode());
		parser.addToken(SUFFIXPUB_BEGIN, ctx.getStart());
	}
	@Override public void exitSuffixPub(TamarinParser.SuffixPubContext ctx) { 
		parser.addToken(SUFFIXPUB_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBuiltinXor(TamarinParser.BuiltinXorContext ctx) { 
		parser.addChildNode(new BuiltinXorNode());
		parser.addToken(BUILTINXOR_BEGIN, ctx.getStart());
	}
	@Override public void exitBuiltinXor(TamarinParser.BuiltinXorContext ctx) { 
		parser.addToken(BUILTINXOR_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBuiltinMun(TamarinParser.BuiltinMunContext ctx) { 
		parser.addChildNode(new BuiltinMunNode());
		parser.addToken(BUILTINMUN_BEGIN, ctx.getStart());
	}
	@Override public void exitBuiltinMun(TamarinParser.BuiltinMunContext ctx) { 
		parser.addToken(BUILTINMUN_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBuiltinOne(TamarinParser.BuiltinOneContext ctx) { 
		parser.addChildNode(new BuiltinOneNode());
		parser.addToken(BUILTINONE_BEGIN, ctx.getStart());
	}
	@Override public void exitBuiltinOne(TamarinParser.BuiltinOneContext ctx) { 
		parser.addToken(BUILTINONE_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBuiltinExp(TamarinParser.BuiltinExpContext ctx) { 
		parser.addChildNode(new BuiltinExpNode());
		parser.addToken(BUILTINEXP_BEGIN, ctx.getStart());
	}
	@Override public void exitBuiltinExp(TamarinParser.BuiltinExpContext ctx) { 
		parser.addToken(BUILTINEXP_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBuiltinMult(TamarinParser.BuiltinMultContext ctx) { 
		parser.addChildNode(new BuiltinMultNode());
		parser.addToken(BUILTINMULT_BEGIN, ctx.getStart());
	}
	@Override public void exitBuiltinMult(TamarinParser.BuiltinMultContext ctx) { 
		parser.addToken(BUILTINMULT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBuiltinInv(TamarinParser.BuiltinInvContext ctx) { 
		parser.addChildNode(new BuiltinInvNode());
		parser.addToken(BUILTININV_BEGIN, ctx.getStart());
	}
	@Override public void exitBuiltinInv(TamarinParser.BuiltinInvContext ctx) { 
		parser.addToken(BUILTININV_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBuiltinPmult(TamarinParser.BuiltinPmultContext ctx) { 
		parser.addChildNode(new BuiltinPmultNode());
		parser.addToken(BUILTINPMULT_BEGIN, ctx.getStart());
	}
	@Override public void exitBuiltinPmult(TamarinParser.BuiltinPmultContext ctx) { 
		parser.addToken(BUILTINPMULT_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBuiltinEm(TamarinParser.BuiltinEmContext ctx) { 
		parser.addChildNode(new BuiltinEmNode());
		parser.addToken(BUILTINEM_BEGIN, ctx.getStart());
	}
	@Override public void exitBuiltinEm(TamarinParser.BuiltinEmContext ctx) { 
		parser.addToken(BUILTINEM_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterBuiltinZero(TamarinParser.BuiltinZeroContext ctx) { 
		parser.addChildNode(new BuiltinZeroNode());
		parser.addToken(BUILTINZERO_BEGIN, ctx.getStart());
	}
	@Override public void exitBuiltinZero(TamarinParser.BuiltinZeroContext ctx) { 
		parser.addToken(BUILTINZERO_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterDhNeutral(TamarinParser.DhNeutralContext ctx) { 
		parser.addChildNode(new DhNeutralNode());
		parser.addToken(DHNEUTRAL_BEGIN, ctx.getStart());
	}
	@Override public void exitDhNeutral(TamarinParser.DhNeutralContext ctx) { 
		parser.addToken(DHNEUTRAL_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterOperators(TamarinParser.OperatorsContext ctx) { 
		parser.addChildNode(new OperatorsNode());
		parser.addToken(OPERATORS_BEGIN, ctx.getStart());
	}
	@Override public void exitOperators(TamarinParser.OperatorsContext ctx) { 
		parser.addToken(OPERATORS_END, ctx.getStart());
		parser.finishChild();
	}
	@Override public void enterPunctuation(TamarinParser.PunctuationContext ctx) { 
		parser.addChildNode(new PunctuationNode());
		parser.addToken(PUNCTUATION, ctx.getStart());
	}
	@Override public void exitPunctuation(TamarinParser.PunctuationContext ctx) { 
		parser.finishChild();
	}
	@Override public void enterAllowedIdentifierKeywords(TamarinParser.AllowedIdentifierKeywordsContext ctx) { 
		parser.addChildNode(new AllowedIdentifierKeywordsNode());
		parser.addToken(ALLOWEDIDENTIFIERKEYWORDS_BEGIN, ctx.getStart());
	}
	@Override public void exitAllowedIdentifierKeywords(TamarinParser.AllowedIdentifierKeywordsContext ctx) { 
		parser.addToken(ALLOWEDIDENTIFIERKEYWORDS_END, ctx.getStart());
		parser.finishChild();
	}
    @Override public void visitTerminal(TerminalNode node) {
       Token token = node.getSymbol();
       String tokenText = token.getText();
       Optional<TamarinTokenType> type = switch (tokenText) {
           case "let" -> Optional.of(TOKEN_KEYWORDLET);
           case "in" -> Optional.of(TOKEN_KEYWORDIN);
           case "rule" -> Optional.of(TOKEN_KEYWORDRULE);
           case "diff" -> Optional.of(TOKEN_KEYWORDDIFF);
           case "by" -> Optional.of(TOKEN_KEYWORDBY);
           case "lemma" -> Optional.of(TOKEN_KEYWORDLEMMA);
           case "if" -> Optional.of(TOKEN_KEYWORDIF);
           case "else" -> Optional.of(TOKEN_KEYWORDELSE);
           case "process" -> Optional.of(TOKEN_KEYWORDPROCESS);
           case "as" -> Optional.of(TOKEN_KEYWORDAS);
           case "lookup" -> Optional.of(TOKEN_KEYWORDLOOKUP);
           case "last" -> Optional.of(TOKEN_KEYWORDLAST);
           case "prio" -> Optional.of(TOKEN_KEYWORDPRIO);
           case "deprio" -> Optional.of(TOKEN_KEYWORDDEPRIO);
           case "presort" -> Optional.of(TOKEN_KEYWORDPRESORT);
           case "smallest" -> Optional.of(TOKEN_KEYWORDSMALLEST);
           case "id" -> Optional.of(TOKEN_KEYWORDID);
           case "tactic" -> Optional.of(TOKEN_KEYWORDTACTIC);
           case "restriction" -> Optional.of(TOKEN_KEYWORDRESTRICTION);
           case "axiom" -> Optional.of(TOKEN_KEYWORDAXIOM);
           case "Any" -> Optional.of(TOKEN_KEYWORDANY);
           case "test" -> Optional.of(TOKEN_KEYWORDTEST);
           case "no_precomp" -> Optional.of(TOKEN_KEYWORDNOPRECOMP);
           case "then" -> Optional.of(TOKEN_KEYWORDTHEN);
           case "out" -> Optional.of(TOKEN_KEYWORDOUT);
           case "fresh" -> Optional.of(TOKEN_KEYWORDFRESH);
           case "pub" -> Optional.of(TOKEN_KEYWORDPUB);
           case "xor" -> Optional.of(TOKEN_KEYWORDXOR);
           case "output" -> Optional.of(TOKEN_KEYWORDOUTPUT);
           case "accounts for" -> Optional.of(TOKEN_KEYWORDACCOUNTSFOR);
           case "account for" -> Optional.of(TOKEN_KEYWORDACCOUNTSFOR);
           case "modulo" -> Optional.of(TOKEN_KEYWORDMODULO);
           case "theory" -> Optional.of(TOKEN_KEYWORDTHEORY);
           case "begin" -> Optional.of(TOKEN_KEYWORDBEGIN);
           case "end" -> Optional.of(TOKEN_KEYWORDEND);
           case "variants" -> Optional.of(TOKEN_KEYWORDVARIANTS);
           case "_restrict" -> Optional.of(TOKEN_KEYWORDRESTRICT);
           case "heuristic" -> Optional.of(TOKEN_KEYWORDHEURISTIC);
           case "export" -> Optional.of(TOKEN_KEYWORDEXPORT);
           case "predicate" -> Optional.of(TOKEN_KEYWORDPREDICATE);
           case "predicates" -> Optional.of(TOKEN_KEYWORDPREDICATE);
           case "options" -> Optional.of(TOKEN_KEYWORDOPTIONS);
           case "equations" -> Optional.of(TOKEN_KEYWORDEQUATIONS);
           case "functions" -> Optional.of(TOKEN_KEYWORDFUNCTIONS);
           case "function" -> Optional.of(TOKEN_KEYWORDFUNCTIONS);
           case "builtins" -> Optional.of(TOKEN_KEYWORDBUILTINS);
           case "case" -> Optional.of(TOKEN_KEYWORDCASE);
           case "qed" -> Optional.of(TOKEN_KEYWORDQED);
           case "next" -> Optional.of(TOKEN_KEYWORDNEXT);
           case "splitEqs" -> Optional.of(TOKEN_KEYWORDSPLITEQS);
           case "equivLemma" -> Optional.of(TOKEN_KEYWORDEQUIVLEMMA);
           case "diffEquivLemma" -> Optional.of(TOKEN_KEYWORDDIFFEQUIVLEMMA);
           case "E" -> Optional.of(TOKEN_E);
           case "AC" -> Optional.of(TOKEN_AC);
           case "Fresh" -> Optional.of(TOKEN_RULEFRESH);
           case "irecv" -> Optional.of(TOKEN_RULEIRECV);
           case "isend" -> Optional.of(TOKEN_RULEISEND);
           case "coerce" -> Optional.of(TOKEN_RULECOERCE);
           case "iequality" -> Optional.of(TOKEN_RULEIEQUALITY);
           case "DH_neutral" -> Optional.of(TOKEN_DHNEUTRAL);
           case "mun" -> Optional.of(TOKEN_BUILTINMUN);
           case "one" -> Optional.of(TOKEN_BUILTINONE);
           case "exp" -> Optional.of(TOKEN_BUILTINEXP);
           case "mult" -> Optional.of(TOKEN_BUILTINMULT);
           case "inv" -> Optional.of(TOKEN_BUILTININV);
           case "pmult" -> Optional.of(TOKEN_BUILTINPMULT);
           case "em" -> Optional.of(TOKEN_BUILTINEM);
           case "zero" -> Optional.of(TOKEN_BUILTINZERO);
           case "SOLVED" -> Optional.of(TOKEN_SOLVEDPROOF);
           case "sorry" -> Optional.of(TOKEN_PROOFMETHODSORRY);
           case "simplify" -> Optional.of(TOKEN_PROOFMETHODSIMPLIFY);
           case "solve" -> Optional.of(TOKEN_PROOFMETHODSOLVE);
           case "contradiction" -> Optional.of(TOKEN_PROOFMETHODCONTRADICTION);
           case "induction" -> Optional.of(TOKEN_PROOFMETHODINDUCTION);
           case "translation-progress" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "translation-allow-pattern-lookups" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "translation-state-optimisation" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "translation-asynchronous-channels" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "translation-compress-events" -> Optional.of(TOKEN_BUILTINOPTIONS);
           case "locations-report" -> Optional.of(TOKEN_BUILTINNAMELOCATIONSREPORT);
           case "reliable-channel" -> Optional.of(TOKEN_BUILTINNAMERELIABLECHANNEL);
           case "diffie-hellman" -> Optional.of(TOKEN_BUILTINNAMEDH);
           case "bilinear-pairing" -> Optional.of(TOKEN_BUILTINNAMEBILINEARPAIRING);
           case "multiset" -> Optional.of(TOKEN_BUILTINNAMEMULTISET);
           case "symmetric-encryption" -> Optional.of(TOKEN_BUILTINNAMESYMMETRICENCRYPTION);
           case "asymmetric-encryption" -> Optional.of(TOKEN_BUILTINNAMEASYMMETRICENCRYPTION);
           case "signing" -> Optional.of(TOKEN_BUILTINNAMESIGNING);
           case "dest-pairing" -> Optional.of(TOKEN_BUILTINNAMEDESTPAIRING);
           case "dest-symmetric-encryption" -> Optional.of(TOKEN_BUILTINNAMEDESTSYMMETRICENCRYPTION);
           case "dest-asymmetric-encryption" -> Optional.of(TOKEN_BUILTINNAMEDESTASYMMETRICENCRYPTION);
           case "dest-signing" -> Optional.of(TOKEN_BUILTINNAMEDESTSIGNING);
           case "revealing-signing" -> Optional.of(TOKEN_BUILTINNAMEREVEALINGSIGNING);
           case "hashing" -> Optional.of(TOKEN_BUILTINNAMEHASHING);
           case "regex" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "isFactName" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "isInFactTerms" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "nonAbsurdGoal" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "dhreNoise" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "defaultNoise" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "reasonableNoncesNoise" -> Optional.of(TOKEN_TACTICFUNCTIONNAME);
           case "new" -> Optional.of(TOKEN_SAPICACTIONNEW);
           case "insert" -> Optional.of(TOKEN_SAPICACTIONINSERT);
           case "delete" -> Optional.of(TOKEN_SAPICACTIONDELETE);
           case "lock" -> Optional.of(TOKEN_SAPICACTIONLOCK);
           case "unlock" -> Optional.of(TOKEN_SAPICACTIONUNLOCK);
           case "event" -> Optional.of(TOKEN_SAPICACTIONEVENT);
           case "∨" -> Optional.of(TOKEN_OROP);
           case "|" -> Optional.of(TOKEN_PIPEOP);
           case "||" -> Optional.of(TOKEN_DOUBLEPIPEOP);
           case "&" -> Optional.of(TOKEN_ANDOP);
           case "∧" -> Optional.of(TOKEN_ANDOP);
           case "¬" -> Optional.of(TOKEN_NOTOP);
           case "not" -> Optional.of(TOKEN_NOTOP);
           case "+" -> Optional.of(TOKEN_PLUSOP);
           case "-" -> Optional.of(TOKEN_MINUSOP);
           case "XOR" -> Optional.of(TOKEN_XOROP);
           case "⊕" -> Optional.of(TOKEN_XOROP);
           case "*" -> Optional.of(TOKEN_MULTOP);
           case "^" -> Optional.of(TOKEN_EXPOP);
           case "=" -> Optional.of(TOKEN_EQOP);
           case "@" -> Optional.of(TOKEN_ATOP);
           case "1" -> Optional.of(TOKEN_ONEOP);
           case "0" -> Optional.of(TOKEN_NULLOP);
           case "▶" -> Optional.of(TOKEN_REQUIRESOP);
           case "∥" -> Optional.of(TOKEN_SPLITOP);
           case "!" -> Optional.of(TOKEN_BANGOP);
           case "<" -> Optional.of(TOKEN_LESSOP);
           case "(<)" -> Optional.of(TOKEN_LESSTERMOP);
           case ">" -> Optional.of(TOKEN_GREATEROP);
           case "All" -> Optional.of(TOKEN_FORALLOP);
           case "∀" -> Optional.of(TOKEN_FORALLOP);
           case "Ex" -> Optional.of(TOKEN_EXISTSOP);
           case "∃" -> Optional.of(TOKEN_EXISTSOP);
           case "==>" -> Optional.of(TOKEN_IMPLIESOP);
           case "⇒" -> Optional.of(TOKEN_IMPLIESOP);

           case "left" -> Optional.of(TOKEN_LEFT);
           case "right" -> Optional.of(TOKEN_RIGHT);
           case "both" -> Optional.of(TOKEN_BOTH);
           case "#define" -> Optional.of(TOKEN_DEFINE);
           case "#include" -> Optional.of(TOKEN_INCLUDE);
           case "#ifdef" -> Optional.of(TOKEN_IFDEF);
           case "#else" -> Optional.of(TOKEN_ELSE);
           case "#endif" -> Optional.of(TOKEN_ENDIF);
           case "all-traces" -> Optional.of(TOKEN_FORALLTRACES);
           case "exists-trace" -> Optional.of(TOKEN_EXISTSTRACE);
           case "OUT" -> Optional.of(TOKEN_OUTFACTIDENT);
           case "Out" -> Optional.of(TOKEN_OUTFACTIDENT);
           case "IN" -> Optional.of(TOKEN_INFACTIDENT);
           case "In" -> Optional.of(TOKEN_INFACTIDENT);
           case "KU" -> Optional.of(TOKEN_KUFACTIDENT);
           case "Ku" -> Optional.of(TOKEN_KUFACTIDENT);
           case "KD" -> Optional.of(TOKEN_KDFACTIDENT);
           case "Kd" -> Optional.of(TOKEN_KDFACTIDENT);
           case "DED" -> Optional.of(TOKEN_DEDLOGFACTIDENT);
           case "Ded" -> Optional.of(TOKEN_DEDLOGFACTIDENT);
           case "FR" -> Optional.of(TOKEN_FRESHFACTIDENT);
           case "Fr" -> Optional.of(TOKEN_FRESHFACTIDENT);
           case "typing" -> Optional.of(TOKEN_LEMMAATTRIBUTETYPING);
           case "sources" -> Optional.of(TOKEN_LEMMAATTRIBUTESOURCES);
           case "reuse" -> Optional.of(TOKEN_LEMMAATTRIBUTEREUSE);
           case "diff_reuse" -> Optional.of(TOKEN_LEMMAATTRIBUTEDIFFREUSE);
           case "use_induction" -> Optional.of(TOKEN_LEMMAATTRIBUTEINDUCTION);
           case "hide_lemma" -> Optional.of(TOKEN_LEMMAATTRIBUTEHIDELEMMA);
           case "private" -> Optional.of(TOKEN_FUNCTIONATTRIBUTE);
           case "destructor" -> Optional.of(TOKEN_FUNCTIONATTRIBUTE);
           default -> Optional.empty();
       };
       type.ifPresent(tokenType -> parser.addToken(tokenType, token));
                }
                               	public JplagTamarinListener(Parser parser) { this.parser = parser; }
	public void visitErrorNode(Error node) { }
}