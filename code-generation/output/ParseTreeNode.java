package de.jplag.tamarin;

import de.jplag.Token;
import de.jplag.TokenType;
import de.jplag.tamarin.grammar.TamarinParser;
import de.jplag.tamarin.grammar.TamarinLexer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static de.jplag.tamarin.TamarinTokenType.*;



abstract class ParseTreeNode implements Comparable<ParseTreeNode>{
    ArrayList<ParseTreeNode> Children;
    ParseTreeNode parent;
    ArrayList<Token> nodeTokens;
    int priority;
    ParseTreeNode() {
        Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
    }

    abstract void sort();
    List<Token> getTokens() {
        List<Token> tokens = new ArrayList<Token>();
        int childIndex = 0;
        for(int i = 0; i < nodeTokens.size(); i++) {
            if(nodeTokens.get(i).getType() == CHILD) {
                tokens.addAll(Children.get(childIndex).getTokens());
                childIndex++;
            }
            else {
                tokens.add(nodeTokens.get(i));
            }
        }
        return tokens;
    }
    void setParent(ParseTreeNode parent) {
        this.parent = parent;
    }
    void addChild(ParseTreeNode child) {
        Children.add(child);
    }
    void addToken(Token token)  {
        nodeTokens.add(token);
    }
    public int compareTo(ParseTreeNode other) {
        if (this.priority < other.priority) {
            return -1;
        }
        else if(this.priority > other.priority) {
            return 1;
        }
        else {
            this.sort();
            other.sort();
            for(int i = 0; i < Math.max(this.Children.size(), other.Children.size()); i++) {
                int comp = this.Children.get(i).compareTo(other.Children.get(i));
                if(comp != 0) {
                    return comp;
                }
            }
            return 0;
        }
    }
}

class InitialNode extends ParseTreeNode {

    InitialNode() {
        Children = new ArrayList<>();
        nodeTokens = new ArrayList<>();
        priority = -1;
    }

    @Override
    void sort() {
        Children.forEach(c -> c.sort());
    }

    @Override
    void addToken(Token token)  {
        if(token.getType() == CHILD) {
            nodeTokens.add(token);
        }
        else {
            System.err.println("Trying to add token " + token.getType().getDescription() + " to initial node!");
        }
    }

}class Tamarin_fileNode extends ParseTreeNode {

	Tamarin_fileNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 0;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class TheoryNode extends ParseTreeNode {

	TheoryNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 1;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class BodyNode extends ParseTreeNode {

	BodyNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 2;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class DefineNode extends ParseTreeNode {

	DefineNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 3;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class IncludeNode extends ParseTreeNode {

	IncludeNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 4;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class IfdefNode extends ParseTreeNode {

	IfdefNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 5;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FlagDisjunctsNode extends ParseTreeNode {

	FlagDisjunctsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 6;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class FlagConjunctsNode extends ParseTreeNode {

	FlagConjunctsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 7;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class FlagNegationNode extends ParseTreeNode {

	FlagNegationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 8;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FlagAtomNode extends ParseTreeNode {

	FlagAtomNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 9;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class TupleTermNode extends ParseTreeNode {

	TupleTermNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class MsettermNode extends ParseTreeNode {

	MsettermNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 11;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class XortermNode extends ParseTreeNode {

	XortermNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 12;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class MulttermNode extends ParseTreeNode {

	MulttermNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 13;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class ExptermNode extends ParseTreeNode {

	ExptermNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 14;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class TermNode extends ParseTreeNode {

	TermNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 15;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class Term_tupletermNode extends ParseTreeNode {

	Term_tupletermNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 16;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Term_msettermNode extends ParseTreeNode {

	Term_msettermNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 17;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Term_oneopNode extends ParseTreeNode {

	Term_oneopNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 18;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Term_dhneutralNode extends ParseTreeNode {

	Term_dhneutralNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 19;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Term_applicationNode extends ParseTreeNode {

	Term_applicationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 20;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Term_nullaryNode extends ParseTreeNode {

	Term_nullaryNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 21;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Term_literalNode extends ParseTreeNode {

	Term_literalNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 22;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ApplicationNode extends ParseTreeNode {

	ApplicationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 23;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class NaryOpApplicationNode extends ParseTreeNode {

	NaryOpApplicationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 24;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class UnaryOpApplicationNode extends ParseTreeNode {

	UnaryOpApplicationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 25;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class NonUnaryOpApplicationNode extends ParseTreeNode {

	NonUnaryOpApplicationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 26;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class BinaryAlgApplicationNode extends ParseTreeNode {

	BinaryAlgApplicationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 27;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class NullaryApplicationNode extends ParseTreeNode {

	NullaryApplicationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 28;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class NullaryApp_OneNode extends ParseTreeNode {

	NullaryApp_OneNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 29;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class NullaryApp_DHNode extends ParseTreeNode {

	NullaryApp_DHNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 30;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class NullaryApp_ZeroNode extends ParseTreeNode {

	NullaryApp_ZeroNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 31;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ReservedBuiltinsNode extends ParseTreeNode {

	ReservedBuiltinsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class GenericRuleNode extends ParseTreeNode {

	GenericRuleNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 33;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class IntruderRuleNode extends ParseTreeNode {

	IntruderRuleNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 34;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class IntruderInfoNode extends ParseTreeNode {

	IntruderInfoNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 35;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ProtoRuleACNode extends ParseTreeNode {

	ProtoRuleACNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 36;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ProtoRuleACInfoNode extends ParseTreeNode {

	ProtoRuleACInfoNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 37;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ProtoRuleNode extends ParseTreeNode {

	ProtoRuleNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 38;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class ProtoRuleInfoNode extends ParseTreeNode {

	ProtoRuleInfoNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 39;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class RuleAttributesNode extends ParseTreeNode {

	RuleAttributesNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class RuleAttributeNode extends ParseTreeNode {

	RuleAttributeNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class EmbeddedRestrictionNode extends ParseTreeNode {

	EmbeddedRestrictionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 42;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FactOrRestrictionNode extends ParseTreeNode {

	FactOrRestrictionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 43;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ReservedRuleNamesNode extends ParseTreeNode {

	ReservedRuleNamesNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class RuleFreshNode extends ParseTreeNode {

	RuleFreshNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 45;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class RulePubNode extends ParseTreeNode {

	RulePubNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 46;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class HexColorNode extends ParseTreeNode {

	HexColorNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class ModuloENode extends ParseTreeNode {

	ModuloENode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class ModuloACNode extends ParseTreeNode {

	ModuloACNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class FactListNode extends ParseTreeNode {

	FactListNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 50;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class FactNode extends ParseTreeNode {

	FactNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 51;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FreshFactNode extends ParseTreeNode {

	FreshFactNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 52;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ReservedFactNode extends ParseTreeNode {

	ReservedFactNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 53;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ProtoFactNode extends ParseTreeNode {

	ProtoFactNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 54;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class MultiplicityNode extends ParseTreeNode {

	MultiplicityNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 55;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FactAnnotationsNode extends ParseTreeNode {

	FactAnnotationsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class FactAnnotationNode extends ParseTreeNode {

	FactAnnotationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Fact_annotation_plusNode extends ParseTreeNode {

	Fact_annotation_plusNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Fact_annotation_minusNode extends ParseTreeNode {

	Fact_annotation_minusNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 59;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Fact_annotation_noprecompNode extends ParseTreeNode {

	Fact_annotation_noprecompNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class LemmaAttributesNode extends ParseTreeNode {

	LemmaAttributesNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class LemmaAttributeNode extends ParseTreeNode {

	LemmaAttributeNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Lemma_attribute_typingNode extends ParseTreeNode {

	Lemma_attribute_typingNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Lemma_attribute_sourcesNode extends ParseTreeNode {

	Lemma_attribute_sourcesNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Lemma_attribute_reuseNode extends ParseTreeNode {

	Lemma_attribute_reuseNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Lemma_attribute_diff_reuseNode extends ParseTreeNode {

	Lemma_attribute_diff_reuseNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Lemma_attribute_inductionNode extends ParseTreeNode {

	Lemma_attribute_inductionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Lemma_attribute_hide_lemmaNode extends ParseTreeNode {

	Lemma_attribute_hide_lemmaNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Lemma_attribute_heuristicNode extends ParseTreeNode {

	Lemma_attribute_heuristicNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Lemma_attribute_outputNode extends ParseTreeNode {

	Lemma_attribute_outputNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Lemma_attribute_leftNode extends ParseTreeNode {

	Lemma_attribute_leftNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Lemma_attribute_rightNode extends ParseTreeNode {

	Lemma_attribute_rightNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class LemmaAttributeTypingNode extends ParseTreeNode {

	LemmaAttributeTypingNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class LemmaAttributeSourcesNode extends ParseTreeNode {

	LemmaAttributeSourcesNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class LemmaAttributeReuseNode extends ParseTreeNode {

	LemmaAttributeReuseNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class LemmaAttributeDiffReuseNode extends ParseTreeNode {

	LemmaAttributeDiffReuseNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class LemmaAttributeInductionNode extends ParseTreeNode {

	LemmaAttributeInductionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class LemmaAttributeHideLemmaNode extends ParseTreeNode {

	LemmaAttributeHideLemmaNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 78;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LemmaAttributeHeuristicNode extends ParseTreeNode {

	LemmaAttributeHeuristicNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 79;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LemmaAttributeOutputNode extends ParseTreeNode {

	LemmaAttributeOutputNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 80;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class TraceQuantifierNode extends ParseTreeNode {

	TraceQuantifierNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class ForallTracesNode extends ParseTreeNode {

	ForallTracesNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 82;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ExistsTraceNode extends ParseTreeNode {

	ExistsTraceNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 83;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LemmaNode extends ParseTreeNode {

	LemmaNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 84;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class StandardFormulaNode extends ParseTreeNode {

	StandardFormulaNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 85;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class ImplicationNode extends ParseTreeNode {

	ImplicationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 86;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class DisjunctionNode extends ParseTreeNode {

	DisjunctionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 87;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class ConjunctionNode extends ParseTreeNode {

	ConjunctionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 88;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class NegationNode extends ParseTreeNode {

	NegationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 89;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FatomNode extends ParseTreeNode {

	FatomNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 90;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Fatom_botNode extends ParseTreeNode {

	Fatom_botNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 91;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Fatom_topNode extends ParseTreeNode {

	Fatom_topNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 92;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Fatom_blatomNode extends ParseTreeNode {

	Fatom_blatomNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 93;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Fatom_quantificationNode extends ParseTreeNode {

	Fatom_quantificationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 94;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Fatom_formulaNode extends ParseTreeNode {

	Fatom_formulaNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 95;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class BlatomNode extends ParseTreeNode {

	BlatomNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 96;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Blatom_lastNode extends ParseTreeNode {

	Blatom_lastNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 97;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Blatom_actionNode extends ParseTreeNode {

	Blatom_actionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 98;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Blatom_predicateNode extends ParseTreeNode {

	Blatom_predicateNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 99;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Blatom_lessNode extends ParseTreeNode {

	Blatom_lessNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 100;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Blatom_smallerpNode extends ParseTreeNode {

	Blatom_smallerpNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 101;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Blatom_node_eqNode extends ParseTreeNode {

	Blatom_node_eqNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 102;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class Blatom_term_eqNode extends ParseTreeNode {

	Blatom_term_eqNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 103;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class QuantificationNode extends ParseTreeNode {

	QuantificationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 104;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class SmallerpNode extends ParseTreeNode {

	SmallerpNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 105;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class CaseTestNode extends ParseTreeNode {

	CaseTestNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class LemmaAccNode extends ParseTreeNode {

	LemmaAccNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class TacticNode extends ParseTreeNode {

	TacticNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class DeprioNode extends ParseTreeNode {

	DeprioNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class PrioNode extends ParseTreeNode {

	PrioNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class TacticDisjunctsNode extends ParseTreeNode {

	TacticDisjunctsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class TacticConjunctsNode extends ParseTreeNode {

	TacticConjunctsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class TacticNegationNode extends ParseTreeNode {

	TacticNegationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class TacticFunctionNode extends ParseTreeNode {

	TacticFunctionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class SelectedPresortNode extends ParseTreeNode {

	SelectedPresortNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class GoalRankingPresortNode extends ParseTreeNode {

	GoalRankingPresortNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class TacticFunctionNameNode extends ParseTreeNode {

	TacticFunctionNameNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class RankingIdentifierNode extends ParseTreeNode {

	RankingIdentifierNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Ranking_smallestNode extends ParseTreeNode {

	Ranking_smallestNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Ranking_idNode extends ParseTreeNode {

	Ranking_idNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class TacticNameNode extends ParseTreeNode {

	TacticNameNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class HeuristicNode extends ParseTreeNode {

	HeuristicNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class GoalRankingNode extends ParseTreeNode {

	GoalRankingNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Goal_oracle_rankingNode extends ParseTreeNode {

	Goal_oracle_rankingNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Goal_internal_rankingNode extends ParseTreeNode {

	Goal_internal_rankingNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Goal_regular_rankingNode extends ParseTreeNode {

	Goal_regular_rankingNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class RegularRankingNode extends ParseTreeNode {

	RegularRankingNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class InternalTacticRankingNode extends ParseTreeNode {

	InternalTacticRankingNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class OracleRankingNode extends ParseTreeNode {

	OracleRankingNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class ExportNode extends ParseTreeNode {

	ExportNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class PredicateDeclarationNode extends ParseTreeNode {

	PredicateDeclarationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 131;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class PredicateNode extends ParseTreeNode {

	PredicateNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 132;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class SignatureOptionsNode extends ParseTreeNode {

	SignatureOptionsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinOptionsNode extends ParseTreeNode {

	BuiltinOptionsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class EquationsNode extends ParseTreeNode {

	EquationsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 135;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class EquationNode extends ParseTreeNode {

	EquationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 136;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class SignatureFunctionsNode extends ParseTreeNode {

	SignatureFunctionsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 137;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class SignatureFunctionNode extends ParseTreeNode {

	SignatureFunctionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 138;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FunctionAttributeNode extends ParseTreeNode {

	FunctionAttributeNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class FunctionTypeNode extends ParseTreeNode {

	FunctionTypeNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 140;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Function_type_naturalNode extends ParseTreeNode {

	Function_type_naturalNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 141;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Function_type_sapicNode extends ParseTreeNode {

	Function_type_sapicNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 142;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class BuiltinsNode extends ParseTreeNode {

	BuiltinsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNamesNode extends ParseTreeNode {

	BuiltinNamesNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameLocationsReportNode extends ParseTreeNode {

	BuiltinNameLocationsReportNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameReliableChannelNode extends ParseTreeNode {

	BuiltinNameReliableChannelNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameDHNode extends ParseTreeNode {

	BuiltinNameDHNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameBilinearPairingNode extends ParseTreeNode {

	BuiltinNameBilinearPairingNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameMultisetNode extends ParseTreeNode {

	BuiltinNameMultisetNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameSymmetricEncryptionNode extends ParseTreeNode {

	BuiltinNameSymmetricEncryptionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameAsymmetricEncryptionNode extends ParseTreeNode {

	BuiltinNameAsymmetricEncryptionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameSigningNode extends ParseTreeNode {

	BuiltinNameSigningNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameDestPairingNode extends ParseTreeNode {

	BuiltinNameDestPairingNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameDestSymmetricEncryptionNode extends ParseTreeNode {

	BuiltinNameDestSymmetricEncryptionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameDestAsymmetricEncryptionNode extends ParseTreeNode {

	BuiltinNameDestAsymmetricEncryptionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameDestSigningNode extends ParseTreeNode {

	BuiltinNameDestSigningNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameRevealingSigningNode extends ParseTreeNode {

	BuiltinNameRevealingSigningNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameHashingNode extends ParseTreeNode {

	BuiltinNameHashingNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinNameXorNode extends ParseTreeNode {

	BuiltinNameXorNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class ProofSkeletonNode extends ParseTreeNode {

	ProofSkeletonNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class StartProofSkeletonNode extends ParseTreeNode {

	StartProofSkeletonNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class SolvedProofNode extends ParseTreeNode {

	SolvedProofNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class FinalProofNode extends ParseTreeNode {

	FinalProofNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class InterProofNode extends ParseTreeNode {

	InterProofNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class ProofCaseNode extends ParseTreeNode {

	ProofCaseNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class ProofMethodNode extends ParseTreeNode {

	ProofMethodNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Proof_method_sorryNode extends ParseTreeNode {

	Proof_method_sorryNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Proof_method_simplifyNode extends ParseTreeNode {

	Proof_method_simplifyNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Proof_method_solveNode extends ParseTreeNode {

	Proof_method_solveNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Proof_method_contradictionNode extends ParseTreeNode {

	Proof_method_contradictionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Proof_method_inductionNode extends ParseTreeNode {

	Proof_method_inductionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class GoalNode extends ParseTreeNode {

	GoalNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class ActionGoalNode extends ParseTreeNode {

	ActionGoalNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class PremiseGoalNode extends ParseTreeNode {

	PremiseGoalNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class ChainGoalNode extends ParseTreeNode {

	ChainGoalNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class DisjSplitGoalNode extends ParseTreeNode {

	DisjSplitGoalNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class EqSplitGoalNode extends ParseTreeNode {

	EqSplitGoalNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class NodePremiseNode extends ParseTreeNode {

	NodePremiseNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class NodeConclusionNode extends ParseTreeNode {

	NodeConclusionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class DiffEquivLemmaNode extends ParseTreeNode {

	DiffEquivLemmaNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class EquivLemmaNode extends ParseTreeNode {

	EquivLemmaNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class ProcessNode extends ParseTreeNode {

	ProcessNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class ElseProcessNode extends ParseTreeNode {

	ElseProcessNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class TopLevelProcessNode extends ParseTreeNode {

	TopLevelProcessNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class ActionProcessNode extends ParseTreeNode {

	ActionProcessNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Action_process_replicationNode extends ParseTreeNode {

	Action_process_replicationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Action_process_lookupNode extends ParseTreeNode {

	Action_process_lookupNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Action_process_conditionalNode extends ParseTreeNode {

	Action_process_conditionalNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Action_process_let_bindingNode extends ParseTreeNode {

	Action_process_let_bindingNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Action_process_nullNode extends ParseTreeNode {

	Action_process_nullNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Action_process_sequentialNode extends ParseTreeNode {

	Action_process_sequentialNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Action_process_processNode extends ParseTreeNode {

	Action_process_processNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Action_process_identifierNode extends ParseTreeNode {

	Action_process_identifierNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class SapicActionNode extends ParseTreeNode {

	SapicActionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Sapic_action_newNode extends ParseTreeNode {

	Sapic_action_newNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Sapic_action_insertNode extends ParseTreeNode {

	Sapic_action_insertNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Sapic_action_inNode extends ParseTreeNode {

	Sapic_action_inNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Sapic_action_outNode extends ParseTreeNode {

	Sapic_action_outNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Sapic_action_deleteNode extends ParseTreeNode {

	Sapic_action_deleteNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Sapic_action_lockNode extends ParseTreeNode {

	Sapic_action_lockNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Sapic_action_unlockNode extends ParseTreeNode {

	Sapic_action_unlockNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Sapic_action_eventNode extends ParseTreeNode {

	Sapic_action_eventNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class Sapic_action_ruleNode extends ParseTreeNode {

	Sapic_action_ruleNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class SapicActionInNode extends ParseTreeNode {

	SapicActionInNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class SapicActionOutNode extends ParseTreeNode {

	SapicActionOutNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class ProcessDefNode extends ParseTreeNode {

	ProcessDefNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class SapicPatternTermNode extends ParseTreeNode {

	SapicPatternTermNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 207;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class SapicTermNode extends ParseTreeNode {

	SapicTermNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 208;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LogicalTypedPatternLiteralNode extends ParseTreeNode {

	LogicalTypedPatternLiteralNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 209;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LogicalTypedLiteralNode extends ParseTreeNode {

	LogicalTypedLiteralNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 210;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class SapicNodeVarNode extends ParseTreeNode {

	SapicNodeVarNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 211;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class SapicPatternVarNode extends ParseTreeNode {

	SapicPatternVarNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 212;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class SapicVarNode extends ParseTreeNode {

	SapicVarNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 213;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class SapicTypeNode extends ParseTreeNode {

	SapicTypeNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 214;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class RestrictionNode extends ParseTreeNode {

	RestrictionNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 215;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class RestrictionAttributesNode extends ParseTreeNode {

	RestrictionAttributesNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class RestrictionAttributeNode extends ParseTreeNode {

	RestrictionAttributeNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class LegacyAxiomNode extends ParseTreeNode {

	LegacyAxiomNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 218;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LetBlockNode extends ParseTreeNode {

	LetBlockNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 219;
}
	@Override void sort() {

		Children.forEach(c -> sort());

		Collections.sort(Children);

                        }

}

class ProtoFactIdentifierNode extends ParseTreeNode {

	ProtoFactIdentifierNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 220;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ReservedFactIdentifierNode extends ParseTreeNode {

	ReservedFactIdentifierNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 221;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Reserved_fact_outNode extends ParseTreeNode {

	Reserved_fact_outNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 222;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Reserved_fact_inNode extends ParseTreeNode {

	Reserved_fact_inNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 223;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Reserved_fact_kuNode extends ParseTreeNode {

	Reserved_fact_kuNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 224;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Reserved_fact_kdNode extends ParseTreeNode {

	Reserved_fact_kdNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 225;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class Reserved_fact_dedNode extends ParseTreeNode {

	Reserved_fact_dedNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 226;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class IdentifierNode extends ParseTreeNode {

	IdentifierNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 227;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class NaturalNode extends ParseTreeNode {

	NaturalNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class IndexedIdentifierNode extends ParseTreeNode {

	IndexedIdentifierNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 229;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FilePathNode extends ParseTreeNode {

	FilePathNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 230;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class KeywordsNode extends ParseTreeNode {

	KeywordsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class LsortMsgNode extends ParseTreeNode {

	LsortMsgNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 232;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class MsgVarNode extends ParseTreeNode {

	MsgVarNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 233;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LogicalLiteralNode extends ParseTreeNode {

	LogicalLiteralNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 234;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LogicalLiteralWithNodeNode extends ParseTreeNode {

	LogicalLiteralWithNodeNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class LogicalLiteralNoPubNode extends ParseTreeNode {

	LogicalLiteralNoPubNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 236;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LogVarNode extends ParseTreeNode {

	LogVarNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 237;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class LiteralNode extends ParseTreeNode {

	LiteralNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 238;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class NodeVarNode extends ParseTreeNode {

	NodeVarNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 239;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FreshNameNode extends ParseTreeNode {

	FreshNameNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 240;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class PubNameNode extends ParseTreeNode {

	PubNameNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 241;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FormalCommentNode extends ParseTreeNode {

	FormalCommentNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class TacticFunctionIdentifierNode extends ParseTreeNode {

	TacticFunctionIdentifierNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class InternalTacticNameNode extends ParseTreeNode {

	InternalTacticNameNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class OracleRelativePathNode extends ParseTreeNode {

	OracleRelativePathNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class ExportBodyCharsNode extends ParseTreeNode {

	ExportBodyCharsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class CharDirNode extends ParseTreeNode {

	CharDirNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class EquivOpNode extends ParseTreeNode {

	EquivOpNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 248;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class OrOpNode extends ParseTreeNode {

	OrOpNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 249;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class NotOpNode extends ParseTreeNode {

	NotOpNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 250;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class AndOpNode extends ParseTreeNode {

	AndOpNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 251;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class EqOpNode extends ParseTreeNode {

	EqOpNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 252;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class BotOpNode extends ParseTreeNode {

	BotOpNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 253;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class FOpNode extends ParseTreeNode {

	FOpNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 254;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class TOpNode extends ParseTreeNode {

	TOpNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 255;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class TopOpNode extends ParseTreeNode {

	TopOpNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 256;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ForallOpNode extends ParseTreeNode {

	ForallOpNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 257;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class ExistsOpNode extends ParseTreeNode {

	ExistsOpNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 258;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class SuffixFreshNode extends ParseTreeNode {

	SuffixFreshNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class SuffixPubNode extends ParseTreeNode {

	SuffixPubNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinXorNode extends ParseTreeNode {

	BuiltinXorNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinMunNode extends ParseTreeNode {

	BuiltinMunNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinOneNode extends ParseTreeNode {

	BuiltinOneNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinExpNode extends ParseTreeNode {

	BuiltinExpNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinMultNode extends ParseTreeNode {

	BuiltinMultNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinInvNode extends ParseTreeNode {

	BuiltinInvNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinPmultNode extends ParseTreeNode {

	BuiltinPmultNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinEmNode extends ParseTreeNode {

	BuiltinEmNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class BuiltinZeroNode extends ParseTreeNode {

	BuiltinZeroNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class DhNeutralNode extends ParseTreeNode {

	DhNeutralNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class OperatorsNode extends ParseTreeNode {

	OperatorsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

class PunctuationNode extends ParseTreeNode {

	PunctuationNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 272;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

}

class AllowedIdentifierKeywordsNode extends ParseTreeNode {

	AllowedIdentifierKeywordsNode() {

		Children = new ArrayList<>();

		nodeTokens = new ArrayList<>();

		priority = 10000;
}
	@Override void sort() { Children.forEach(c -> c.sort());}

	@Override List<Token> getTokens() { return new ArrayList<>(); }

}

