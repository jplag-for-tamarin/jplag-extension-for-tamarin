### Facts
```c
protoFact(plit) := multiplicity protoFactIdentifier '(' [msetterms(False,plit)] ')' [factAnnotations]
reservedFact(plit) := multiplicity reservedfactIdentifier '(' msetterm(False,plit) ')' [factAnnotations]
freshFact := freshFactIdent '(' msetterm(False,plit) ')' [factAnnotations]
fact(plit) := freshFact(plit) | reservedFact(plit) | protoFact(plit)
factList(plit) := '[' [fact(plit) (',' fact(plit))* [',']] ']`'
// priorities have to be in this order, otherwise protoFact covers the other two
// MANUAL: the manual says KU-facts have arity two but the parser requires a single fact

msetterms(eqn,plit) := msetterm(eqn,plit) (',' msetterm(eqn,plit))* [','] 

multiplicity := ['!']

factAnnotation := '+' | '-' | 'no_precomp'
factAnnotations := '[' [factAnnotation (',' factAnnotation)* [',']] ']'
/* MANUAL: the manual does not represent the parsers behavior, the manual does not allow an empty list of annotations, while the parser does */
``` 

### Tokens
```c
identifier := alphaNum (alphaNum | '_')*
indexedIdentifier := identifier ['.' natural]
keywords := 'let' | 'in' | 'rule' | 'diff'
// cannot be in reservedNames ["in", "let", "rule", "diff"]
reservedFactIdentifier := outFactIdent | inFactIdent| kuFactIdent | kdFactIdent | dedLogFactIdent

outFactIdent := 'OUT' 
inFactIdent := 'IN'
kuFactIdent := 'KU'
kdFactIdent := 'KD'
dedLogFactIdent := 'DED'
freshFactIdent := 'FR'
// above facts can only have one term
protoFactIdentifier := Alpha (alphaNum | '_')*
// cannot include any of the keywords


LSortMsg := indexedIdentifier ':' 'msg' | indexedIdentifier
msgVar := indexedIdentifier ':' ('fresh' | 'pub' | 'msg') | ['~' | '$' ] indexedIdentifier

logicalLiteral := freshName | pubName | msgVar
logicalLiteralWithNode := freshName | pubName | logVar
logicalLiteralNoPub := freshName | msgVar
logVar := indexedIdentifier ':' ('pub' | 'fresh' | 'msg' | 'node') | ['$' | '~' | '#'] indexedIdentifier
nodeVar := indexedIdentifier ':' 'node' | ['#'] indexedIdentifier

vlit(varp) := freshName | pubName | varp

freshName := '~' '\'' identifier '\''
pubName := '\'' identifier '\''


formalComment := letter+ '{*' ~['\,*']+ '*}' 

filePath := '"' charDir* '"'
charDir := alphaNum | '.' | pathSeparator
pathSeparator = '\\' | '/'

Alpha := [A-Z]
alpha := [a-z]
letter := alpha | Alpha
alphaNum := (alpha | Alpha | digit)
digit := [0-9]
naturalSubscript := ('₀'|'₁'|'₂'|'₃'|'₄'|'₅'|'₆'|'₇'|'₈'|'₉')+
natural := digit+
hexDigit := [0-9] | [A-F] | [a-f]
```

### Formulas
```c
negation(varp,nodep) := ['¬' | 'not'] fatom(varp,nodep)
conjunction(varp,nodep) := negation(varp,nodep) (('&' | '∧') negation(varp,nodep))* 
disjunction(varp,nodep) := conjunction(varp,nodep) (('|' | '∨') conjunction(varp,nodep))* 

quantification(varp,nodep) := ('All' | '∀' | 'Ex' | '∃')  (varp,nodep)+ '.' standardFormula(varp,nodep)

smallerp(varp) := msetterm(False,vlit(varp)) '(<)' msetterm(False,vlit(varp))
// mset has to be enabled for this parser to work

// bound logical atom
blatom(varp,nodep) := 'last' '(' nodep ')' // last for induction
        | fact(vlit(varp)) '@' nodep // action 
        | fact(vlit(varp | nodep)) // predicate  
        | nodep '<' nodep // node ordering
		| smallerp(varp)  // multiset comparison
        | msetterm(False, vlit(varp)) '=' msetterm(False,vlit(varp)) // term equality
        | nodep '=' nodep // node equality
// somehow a different plit (literal parser?) is used for predicates than for actions, maybe this has to do with some variables having to be defined
// plit determines which literals can be in a term
// MANUAL: the manual misses multiset comparison and predicate atoms

// formula atom
fatom(varp,nodep) := '⊥' | 'F' | '⊤' | 'T' | blatom(varp,nodep) |
         quantification(varp,nodep) | '(' standardFormula(varp,nodep) ')' 
// F is a reserved identifier, it cannot be followed by an identifier letter

implication(varp,nodep) := disjunction(varp,nodep) [('==>' | '⇒') implication(nodep)] 

standardFormula(varp,nodep) := implication(varp,nodep) [('<=>' | '⇔') implication(varp,nodep)]
// what are guarded and plain formulas?
```

### Lemma
```c
lemmaAtribute(diff,workDir) := 'typing' | 'sources' | 'reuse' | 'diff_reuse' | 'use_induction' | 'hide_lemma=' identifier | 'heuristic=' goalRanking(diff,workDir) | 'output=' literals | 'left' | 'right'
// MANUAL: misses output and diff_reuse attributes. Alos it allows one or more goal rankings while the parser only allows a single one

literals // I really don't know what this parser does

lemmaAtributes(diff,workDir) := '[' [lemmaAttribute(diff,workDir) (',' lemmaAttribute(diff,workDir))* [',']] ']'
// MANUAL: if [] are written the manual expect one or more attributes while it can be empty in the parser.

traceQuantifier := 'all-traces' | 'exists-trace'  

protoLemma(formula,workDir) := 'lemma' [moduloE] identifier [lemmaAttributes(False,workDir)] ':' [traceQuantifier] '"' formula '"' [proofSkeleton]
// MANUAL: the manual requires a proof whereas the parser does (I think) not

lemma(workDir) := protoLemma(standardFormula,workDir)
plainLemma(workDir) := protoLemma(plainFormula)
diffLemma(workDir) := 'diffLemma' identifier [lemmaAttributes(True,workDir)] ':' [diffProofSkeleton]
 
```

### Terms
```c
tupleTerm(eqn,plit) := (msetterm(eqn,plit) ',')* msetterm(eqn,plit)
// right associative

msettermEnabledAndFalse(plit) := xorterm(False,plit) ('+' xorterm(False,plit))*
// left associative 
msettermDisabledOrTrue(eqn,plit) := xorterm(eqn,plit)

xortermEnabledAndFalse(plit) := multterm(False,plit) (('XOR' | '⊕') multterm(False,plit))*
xortermDisabledOrTrue(eqn,plit) := multterm(eqn,plit)

// does DH refer to diffie hellman i.e. that we are in a group?
multtermEnabledAndFalse(plit) := expterm(False,plit) ('*' expterm(False,plit))*
multtermDisabledOrTrue(eqn) := term(eqn,plit)

expterm(eqn,plit) := term(eqn,plit) ('^' term(eqn,plit))*

term(eqn,plit) := '<' tupleTerm(eqn,plit) '>' | '(' msetterm(eqn,plit) ')' | '1' | 'DH_neutral' | application(eqn,plit) | nullaryApplication | literal(plit)

application(eqn,plit) := naryOpApplication(eqn,plit) | binaryAlgApplication(eqn,plit) | diffOp(eqn,plit)

naryOpApplication(eqn,plit) := unaryOpApp(eqn,plit) | nonUnaryOpApp(eqn,plit)
unaryOpApp(eqn,plit) := identifier '(' tupleTerm(eqn,plit) ')'
nonUnaryOpApp(eqn,plit) := identifer '(' [msetterm(eqn,plit) (',' msetterm(eqn,plit))* [',']] ')'
// the number of terms parsed has to be equal to the arity of the operator
// the identifier cannot be an element of reserved builtins and eqn has to be false otherwise this parser will always fail
// MANUAL: in the multterms in nary_app need to be comma separated

binaryAlgApplication(eqn,plit) := identifier '{' tupleTerm(eqn,plit) '}' term(eqn,plit)
// the identifier cannot be an element of reserved builtins and eqn has to be false otherwise this parser will always fail
// the arity of the operator has to be two
// MANUAL: the manual specifies tupleterms encapsulated in <> but the tupleterm parser does not require them

diffOp(eqn,plit) := 'diff' '(' [msetterm(eqn,plit) (',' msetterm(eqn,plit))* [',']] ')'
// This parser will fail unless diff is enabled and it is not an equation (eqn = false)
// the number of terms has to be two

// function symbol of a NoEq function with arity 0
nullaryApplication := 'one' | 'DH_neutral' | 'zero'
reservedBuiltins := 'mun' | 'one' | 'exp' | 'mult' | 'inv' | 'pmult' | 'em' | 'zero' | 'xor'
```

### Restriction
```c
restriction(varp,nodep) := 'restriction' identifier ':' '"' standardFormula(varp,nodep) '"'
// MANUAL: the normal restriction parser does not allow restriction attributes

restrictionAttribute := 'left' | 'right' | 'both'
restrictionAttributes := '[' [restrictionAttribute (',' restrictionAttribute)* [',']] ']'
// MANUAL: the parser accepts an attribute both

legacyAxiom := 'axiom' identifier ':' '"' standardFormula(msgvar,nodevar) '"'
diffRestriction := 'restriction' identifier [restrictionAttributes] ':' '"' standardFormula(msgvar,nodevar) '"'
// the parser takes a plain formula but that parses a standard formula and converts it
legacyDiffAxiom := 'axiom' identifier [restrictionAttributes] ':' '"' standardFormula '"'
```

### Let
```c
genericLetBlock(varp,termp) := (varp '=' termp)+
letBlock := 'let' genericLetBlock(LSortMsg, msetterm(False,logicalLiteral)) 'in' 
```

###  Rule
```c
genericRule(varp,nodep) := factList(vlit(varp)) ('-->' | '--[' [factOrRestr(varp,nodep) (',' factOrRestr(varp,nodep))*] ']->') factList(vlit(varp))

intruderRule := 'rule' moduloAC intruderInfo ':' genericRule(msgvar,nodeVar) 

intruderInfo := identifier [natural] 
// identifier has to start with 'c' or 'd'

protoRuleAC := protoRuleACInfo [letBlock] genericRule(msgVar,nodeVar)

protoRuleACInfo := 'rule' moduloAC identifier [ruleAttributes] ':'
// identifier cannot be in reservedRuleNames

protoRule := protoRuleInfo [letBlock] genericRule(msgVar,nodeVar) ['variants' protoRuleAC (',' protoRuleAC)* [',']]

diffRule := [protoRuleInfo] [letBlock] genericRule(msgVar,nodeVar) ['left' protoRule 'right' protoRule] 

protoRuleInfo := 'rule' [moduloE] identifier [ruleAttributes] ':'
// identifier cannot be in reservedRuleNames

ruleAttribute := 'colour=' hexColor | 'color=' hexColor | 'process=' '"' anyChar* '"'
ruleAttributes := '[' [ruleAttribute (',' ruleAttribute)* [',']] ']'
// rule attributes in the parser accepts the empty string so it should always be used as an optional

embeddedRestriction(factParser) := '_restrict' '(' factParser ')' 
factOrRestriction(varp,nodep) := fact(vlit(varp)) | embeddedRestriction(standardFormula(varp,nodep))

reservedRuleNames := 'Fresh' | 'irecv' | 'isend' | 'corece' | 'fresh' | 'pub' | 'iequality'

hexColor := '\'' ['#'] hexDigit+ '\'' | ['#'] hexDigit+

moduloE = '(' 'modulo' 'E' ')'
moduloAC = '(' 'modulo' 'AC' ')'
```

### Accountability
```c
caseTest := 'test' identifier ':' '"' standardFormula(msgVar,nodeVar) '"'

lemmaAcc(workDir) := 'lemma' identifier [lemmaAttributes(False,workDir)] ':' identifier (',' identifier)* [','] ('accounts for' | 'account for') '"' standardFormula(msgVar,nodeVar) '"'
```

### Proof
```c
diffProofSkeleton := solvedDiffProof | finalDiffProof | interDiffProof 
solvedDiffProof := 'MIRRORED'
finalDiffProof := 'by' diffProofMethod
interDiffProof := diffProofMethod (diffCase ('next' diffCase)* 'qed' | diffProofSkeleton)
diffCase := 'case' identifier diffProofSkeleton

diffProofMethod := 'sorry' | 'rule-equivalence' | 'backward-search' | 'step' '(' proofMethod ')' | 'ATTACK'

proofSkeleton := solvedProof | finalProof | interProof
startProofSkeleton := finalProof | interProof
solvedProof := 'SOLVED' 
finalProof := 'by' proofMethod
interProof := proofMethod (case ('next' case)* 'qed' | proofSkeleton)  
case := 'case' identifier proofSkeleton

proofMethod := 'sorry' | 'simplify' | 'solve' '(' goal ')' | 'contradiction' | 'induction'

goal := actionGoal | premiseGoal | chainGoal | disjSplitGoal | eqSplitGoal
actionGoal := fact(logicalLiteral) '@' nodeVar
premiseGoal := fact(logicalLiteral) '▶' naturalSubscript nodeVar
chainGoal := nodeConclusion '~~>' nodePremise
disjSplitGoal := standardFormula(msgVar,nodeVar) ('∥' standardFormula(msgVar,nodeVar))*
// the parser wants a guardedFormula
eqSplitGoal := 'splitEqs' '(' natural ')'

nodePremise := '(' nodeVar ',' natural ')'
nodeConclusion := '(' nodeVar ',' natural ')'
```

### Signatures
```c
heuristic(diff,workDir) := 'heuristic' ':' goalRanking(diff,workDir)+
// why are spaces skipped so explicitly here and why are the rankings not comma separated?
goalRanking(diff,workDir) := oracleRanking(diff, workDir) | internalTacticRanking(diff,workDir) | regularRanking(diff,workDir)
regularRanking(False,workDir) := ('s' | 'S' | 'p' | 'P' | 'c' | 'C' | 'i' | 'I')+
regularRanking(True,workDir) := ('s' | 'S' | 'c' | 'C')+
internalTacticRanking(diff,workDir) := '{' ~['",\n,\r,{,}']+ '}'
// the optionMaybe makes it possible to just write '{' as long as no characters follow except linebreaks '\' or '{}' follow. (confirmed). Anything that follows will be piped to the next parser
// I don't think it used unless non-letters are used because it is covered by regularRanking
oracleRanking(diff,workDir) := ('o' | 'O') '"' ~['",\n,\r']+ '"' 
// again 'o' | 'O' is fine if nothing follows, if something follows, that is piped to the next parser


export := 'export' identifier ':' '"' ~['\,"']* '"'

predicateDeclaration := ('predicates' | 'predicate') ':' predicate (',' predicate)* [',']
// MANUAL: does not include predicates
predicate := fact(logVar) '<=>' standardFormula(msgVar,nodeVar)
// parser wants a plainFormula

options := 'options' ':' builtinOptions (',' builtinOptions)* [',']
builtinOptions := 'translation-progress' | 'translation-allow-pattern-lookups' | 'translation-state-optimisation' | 'translation-asynchronous-channels' | 'translation-compress-events'

equations := 'equations' ':' equation (',' equation)* [',']
equation := term(True,logicalLiteralNoPub) '=' term(True,logicalLiteralNoPub)

functions := ('functions' | 'function') ':' function (',' function)* [',']
// MANUAL: only allows 'functions'
function := identifier functionType ['[' [functionAttribute (',' functionAttribute)* [',']] ']']
// identifier cannot be in reservedBuiltins
// identifier has to be in defined functions
// MANUAL: does not know the 'destructor' function attribute

functionAttribute := 'private' | 'destructor'

functionType := '/' natural | '(' [sapicType (',' sapicType)* [',']] ')' ':' sapicType 

builtins := 'builtins' ':' builtinNames (',' builtinNames)* [',']
diffbuiltins := 'builtins' ':' builtinDiffNames (',' builtinDiffNames)* [',']

builtinNames := 'locations-report' | 'reliable-channel' | builtinDiffNames
builtinDiffNames :=  'diffie-hellman' | 'bilinear-pairing' | 'multiset' | 'xor' | 'symmetric-encryption' | 'asymmetric-encryption' | 'signing' | 'dest-pairing' | 'dest-symmetric-encryption' | 'dest-asymmetric-encryption' | 'revealing-signing' | 'hashing'
```

### Tactics
```c
// maybe I have to revisit this file
tactic(diff) := tacticName [selectedPresort(diff)] [prio+] [deprio+]

deprio := 'deprio' ':' ['{' rankingIdentifier '}'] disjuncts+
prio := 'prio' ':' ['{' rankingIdentifier '}'] disjuncts+

disjuncts := conjuncts (('|' | '∨') conjuncts)*
conjuncts := negation (('&' | '∧' negation)*
negation := ['¬' | 'not'] function 
// overlapping names

function := tacticFunctionName ('"' functionValue '"')+ 
// overlap with signatures function, mentioned it is for pretty printing
functionValue := ~['"']*

selectedPresort(diff) := 'presort' ':' goalRankingPresort(diff)

goalRankingPresort(True) :=  's' | 'S' | 'c' | 'C'
goalRankingPresort(False) := 's' | 'S' | 'p' | 'P' | 'c' | 'C' | 'i' | 'I'
// what is the use of this?

tacticFunctionName := 'regex' | 'isFactName' | 'isInFactTerms' | 'nonAbsurdGoal' | 'dhreNoise' | 'defaultNoise' | 'reasonableNoncesNoise'
rankingIdentifier := 'smallest' | 'id'
tacticName := 'tactic' ':' identifier
```

### Sapic
```c
diffEquivLemma(thy) := 'diffEquivLemma' ':' process(thy)
// enables diff flag in parser state
equivLemma(thy) := 'equivLemma' ':' process(thy) process(thy)

process(thy) := actionProcess(thy) (('+' | '||' | '|') actionProcess(thy))*
elseProcess(thy) := ['else' process(thy)]
topLevelProcess(thy) := 'process' ':' process(thy) 
actionProcess(thy) := '!' process(thy)
                    | 'lookup' sapicTerm 'as' sapicVar 'in' process(thy) elseprocess(thy) 
                    | 'if' (sapicTerm '=' sapicTerm | standardFormula(sapicVar,sapicNodeVar) 'then' process(thy) elseprocess(thy)
                    | 'let' genericLetBlock(sapicPatternTerm,sapicTerm) 'in' process(thy) elseprocess(thy)
                    | '0'
                    | sapicAction [';' actionProcess(thy)]
                    | '(' process(thy) ')' ['@' sapicTerm]
					| identifier ['(' [msetterm(False,logicalTypedLiteral) (',' msetterm(logicalTypedLiteral))* [',']] ')'] // identifier has to be a defined process. Not sure what applyM does in this context

sapicAction := 'new' sapicVar
             | 'insert' msetterm(False,logicalTypedLiteral) ',' msetterm(False,logicalTypedLiteral)
			 | 'in' '(' (msetterm(False,logicalTypedPatternLitreal) ')' | msetterm(False,logicalTypedLiteral) ',' msetterm(logicalTypedPatternLiteral) ')')
			 | 'out' '(' (msetterm(False,logicalTypedLiteral) ')' | msetterm(False,logicalTypedLiteral) ',' msetterm(False,logicalTypedLiteral) ')')
			 | 'delete' msetterm(False,logicalTypedLiteral)
			 | 'lock' msetterm(False,logicalTypedLiteral)
			 | 'unlock' msetterm(False,logicalTypedLiteral)
			 | 'event' fact(logicalTypedLiteral)
			 | genericRule(sapicPatternVar,sapicNodeVar)

processDef(thy) := 'let' identifier ['(' [sapicVar (',' sapicVar)* [',']] ')'] '=' process(thy) 

sapicPatternTerm := msetterm(False,logicalTypedPatternLiteral)
sapicTerm := msetterm(False,logicalTypedLiteral)

logicalTypedPatternLiteral := vlit(sapicPatternVar)
logicalTypedLiteral := vlit(sapicVar)

sapicNodeVar := nodeVar
sapicPatternVar := ['='] sapicVar 
sapicVar := logVar [':' sapicType]
sapicType := 'Any' | identifier
// called typep in the parser
```

### Theory
```c
diffTheory := 'theory' identifier 'begin' diffBody* 'end'
diffBody := (heuristic(True,inFile)
			| tactic(True)
			| diffbuiltins
			| functions
			| equations
			| diffRestriction
			| legacyDiffAxiom
			| plainLemma
			| diffLemma
			| diffRule
			| intruderRule
			| formalComment
			| ifdef
			| define
			| include)

theory(inFile) := 'theory' identifier 'begin' body* 'end'
body := (heuristic(False,inFile)
		| tactic(False)
		| builtins
		| options
		| functions // signature ones
		| equations
		| restriction(msgVar, nodeVar)
		| legacyAxiom
		| caseTest
		| lemmaAcc(inFile)
		| lemma(inFile)
		| protoRule
		| intruderRule
		| formalComment
		| topLevelProcess
		| processDef
		| equivLemma
		| diffEquivLemma // why is this allowed out of diff mode?
		| predicateDeclaration
		| export
		| ifdef
		| define
		| include) 

define := '#define' identifier
include := '#include' filePath // here the file at filepath would get parsed and the state updated for the rest of the parse
ifdef := '#ifdef' flagDisjuncts body* ['#else' body*] '#endif'

flagDisjuncts := flagConjuncts (('|' | '∨') flagConjuncts)*
flagConjuncts := flagNegation (('&' | '∧') flagNegation)*
flagNegation := [('¬' | 'not')] flagAtom

flagAtom := identifier
```